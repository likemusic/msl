<?php

return [
    'login' => '179502964155',//175819213947
    'currency_id' => '643',
    'success_url' => 'http://onlinekarty.ru/core/funcs/payment_ok.php',
    'fail_url' => 'http://onlinekarty.ru/',
    'method'=> 'get',
    'disabled_currencies' => [
        'WalletOne',
        'WalletOneRUB',
        'WalletOneUAH',
        'WalletOneUSD',
        'WalletOneEUR',
        'WalletOneZAR',
        'WalletOneBYR',
        'WalletOneGEL',
        'WalletOneKZT',
        'WalletOnePLN',
        'WalletOneTJS'
    ]
];