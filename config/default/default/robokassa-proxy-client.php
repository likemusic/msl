<?php
return [
    'use-proxy' => env('PAYMENT_USE_PROXY'),
    'proxy-url' => env('PAYMENT_PROXY_URL','http://mapiki.ru/PaymentInit.php')
];