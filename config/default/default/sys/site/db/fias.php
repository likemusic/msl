<?php

/* Настройки сайта на бд ФИАС */
return [
    'tables' =>[
        'category' => [
            'part' => 'fias_categories',
            'type' => 'fias_category_types',
            'template' => 'category_templates',
            'adm_type' => 'fias_category_adm_types'
        ],
        'product' => [
            'independent' => 'fias_products'
        ],
    ],
    'category_type' => [
        1 => ['регион','регионы','регионов'],
        3 => ['район','районы','районов'],
        4 => ['город','города','городов'],
        6 => ['населенный пункт','населенные пункты','населенных пунктов'],
        90 => ['дополнительная территория','дополнительные территории','дополнительных территорий'],
    ],
];