<?php

/* Настройки сайта на старой БД */
return [
    'tables' =>[
        'category' => [
            'part' => 'categories',
            'type' => 'category_types',
            'adm_type' => 'category_adm_types',
            'template' => 'category_templates'
        ],
        'product' => [
            'independent' => 'products'
        ],
        'category_type' => [
            4 => ['город','города','городов'],
            5 => ['район','районы','районов'],
            6 => ['город','города','городов'],//TODO: remove duplicates
        ],
    ]
];