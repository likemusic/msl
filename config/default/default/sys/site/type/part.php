<?php


return [
    'category_part' => [
        /*
            Используемая стратегия для определения корневых элементов:
            - include - указываются только id которые могут использоваться в качестве корневых;
            - exclude - указываются только id которые не могут использоваться в качестве корневых;
            - all - без ограничений;
         */
        'strategy' => 'include',//'exclude','include'
        //добавить параметр root используемый при поиске корневых???
        //'include' => [5,10,16,18,35],
        //'exclude' => [6,7,8,9,11,12,13,14,15],
    ],

];