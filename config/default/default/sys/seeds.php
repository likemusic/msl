<?php

return [
    //названия категорий
    'categories_names' => [
        'Category',
        //'Категория'
    ],

    //кол-во категорий на один уровень
    'categories_count' => 4,

    //кол-во уровней
    'categories_levels' => 5,

    //кол-во типов для одного уровня
    'types_per_level' => 2
];