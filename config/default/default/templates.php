<?php

return
[
    'main' => 'index',
    'bindings' => [
        \App\Models\PageEntity\StoreEntity\Category\ICategory::class    => 'category.tpl.html',
        \App\Models\PageEntity\StoreEntity\Product\IProduct::class      => 'product_detailed.tpl.html',
        \App\Models\PageEntity\StoreEntity\SubProduct\ISubProduct::class=> 'product_detailed.tpl.html',

        \App\Models\PageEntity\Article\IArticle::class      => 'article.tpl.html',

        \App\Models\PageEntity\Feedback\IFeedback::class    => 'feedback.tpl.html',
        \App\Models\PageEntity\Callback\ICallback::class    => 'callback.tpl.html',
        \App\Models\PageEntity\SearchResult\ISearchResult::class    => 'search.tpl.html',

        \App\Models\PageEntity\SpecificPage\MainPage\IMainPage::class => 'category.tpl.html'
        //IRedirect::class
        //доделать остальные типы страниц
        //TODO: какой тип у Callback и Feedback - отдельные страницы или тип Evaluated
    ]
];
