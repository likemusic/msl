<?php

$ret = [
    'DomainRegion' => env('DOMAIN_REGION','Test'),
    'DomainEnds' =>  env('DOMAIN_ENDS', 'shop.test'),

    'html_charset' => 'utf-8',
    'DomainDescription' => env('DOMAIN_DESCRIPTION', 'Интернет-магазин карт тестовый'),
    'ContactPhoneNumber' => '+7(499)940-42-09',
    'DOMAIN_EMAIL' => 'mapsshop@gmail.com',
    'DOMAIN_EMAIL_NAME' => 'ОАО МапИнвест',
    'PHONE_PLACEHOLDER' => '+7(499)940-42-09',
    'NAME_PLACEHOLDER' => 'Петр Петрович',
    'EMAIL_PLACEHOLDER' => 'ivanov@gmail.com',

    'PRICE_CURRENCY' => 'р.',

    'MainPageItems' => env('MAIN_PAGE_ITEMS') ? explode(',', env('MAIN_PAGE_ITEMS')) : null,//[5,10,16,18,35],

    'tables' =>[
        /*'category' => [
            'part' => 'categories',
            'type' => 'category_types',
            'template' => 'category_templates'
        ],*/
        'product' => [
            'set' => 'product_sets',
            'template' => 'product_templates',
            'types' => 'product_types',
            //'independent' => 'products'
        ],
        'subproduct' => [
            'template' => 'subproduct_templates',
            'description' => 'subproduct_descriptions',
            'set' => 'subproduct_sets'
        ]
    ],
    'category_part' => [
        /*
            Используемая стратегия для определения корневых элементов:
            - include - указываются только id которые могут использоваться в качестве корневых;
            - exclude - указываются только id которые не могут использоваться в качестве корневых;
            - all - без ограничений;
         */
        'strategy' => env('SITE_STRATEGY', 'all'),//'exclude'
        //добавить параметр root используемый при поиске корневых???
        'include' => explode(',', env('INCLUDE_IDS')),//[5,10,16,18,35],
        'exclude' => explode(',',env('EXCLUDE_IDS')),//[6,7,8,9,11,12,13,14,15],

        'root_category_id' => env('ROOT_CATEGORY_ID',null)//Виртуальная корневая категория
    ],
    //Названия выводимые перед круппой дочерних одного типа
    //Теоретически должны быть для разных уровней разные - сейчас сидируются одинаковыми
    /*'category_type' => [
        4 => ['город','города','городов'],
        5 => ['район','районы','районов'],
        6 => ['город','города','городов'],//TODO: remove duplicates
    ],*/
    'category_tree' => [
        'auto-expand-on-main' => env('AUTO_EXPAND_ON_MAIN_CATEGORY_ID'),//id строки автоматически расскрываемой/выделяемой на главной странице
        //TODO: для ФИАС не задается т.к. при заданном include always_show_root_elements
        //TODO: сделать задание через название
        'always_show_root_elements' => true, //всегда показывать все коневые элементы
        'selected_move_to_top' => true, //выбранный элемент показывать первым в списке
        'dont_move_to_top_selected_roots' => true,
        'show_products' => false,//не показывать в дереве товары
        'show_subproducts' => false,//не показывать в дереве подтовары,
        'hide-siblings-for-parents' => true,//скрывать в родительских ветках все кроме родителей текущего
        'show_siblings_for_items_without_childs' => true,
        'group_by_type' => true,
        'max_cache_level' => 1 //кэшировать дерево до какого уровня level
    ],
    'dirs' => [
        'product_templates_images' => '/data/pictures_tpl/',
    ],

    'YANDEX_METRIKA_ID' => env('YANDEX_METRIKA_ID'),
    'GOOGLE_ANALYTICS_ID' => env('GOOGLE_ANALYTICS_ID'),

    /*'counters' => [
        'Yandex.Informer'   => config_file_content('YandexInformer.txt'),
        'Yandex.Metrika'    => config_file_content('YandexMetrika.txt'),
        'Google.Analitics'   => config_file_content('GoogleAnalytics.txt'),
        'Rating@Mail.ru'    => config_file_content('RatingMail.ru.txt'),
    ]*/
];

$site_db = env('SITE_DB');
if(!$site_db) $site_db = 'fias';
$site_db_file = "sys/site/db/{$site_db}.php";
//$site_db_file = __DIR__."/sys/site/db/{$site_db}.php";
$site_db_config = include($site_db_file);

$ret = array_replace_recursive($ret,$site_db_config);//TODO: проверить что чем переписывать
return $ret;