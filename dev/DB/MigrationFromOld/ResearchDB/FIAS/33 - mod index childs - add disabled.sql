ALTER TABLE `categories`
	DROP INDEX `childs`,
	ADD INDEX `childs` (`parent`, `sort_order`, `type_id`, `src_name`, `name`, `disabled`);

ALTER TABLE `fias_categories`
	DROP INDEX `childs`,
	ADD INDEX `childs` (`parent`, `sort_order`, `type_id`, `src_name`, `name`, `disabled`);
