# --- ADR_OBJ
#Всего строк
SELECT COUNT(*) FROM d_fias_addrobj;#1 378 527

#Актуальных
SELECT COUNT(*) FROM d_fias_addrobj WHERE actstatus=1;#1 119 107

#У скольких formalname != offname
SELECT COUNT(*) FROM d_fias_addrobj WHERE actstatus=1 AND offname != formalname;#26 477
#смотрим
SELECT * FROM d_fias_addrobj WHERE actstatus=1 AND offname != formalname LIMIT 100;
#смотрим какие у них Level
SELECT aolevel, COUNT(*) FROM d_fias_addrobj WHERE actstatus=1 AND offname != formalname GROUP BY aolevel;
/*
4 - 704
5 - 3
6 - 1 430
7 - 10 360
90 - 12 135
91 - 1 845
*/
SELECT * FROM d_fias_addrobj WHERE (actstatus=1) AND (offname != formalname) AND (aolevel=4) LIMIT 100;
