DELIMITER $$

DROP FUNCTION IF EXISTS `transliterate_ru_ci` $$
CREATE DEFINER=`root`@`localhost`
  FUNCTION `transliterate_ru_ci`(str TEXT)
  RETURNS text CHARSET utf8
DETERMINISTIC SQL SECURITY INVOKER
BEGIN
  DECLARE strlow TEXT;
  DECLARE sub VARCHAR(3);
  DECLARE res TEXT;
  DECLARE len INT(11);
  DECLARE i INT(11);
  DECLARE pos INT(11);
  DECLARE alphabeth CHAR(44);

  SET i = 0;
  SET res = '';
  SET strlow = LOWER(str);
  SET len = CHAR_LENGTH(str)-1; 
  SET alphabeth = '0123456789 абвгдеёжзийклмнопрстуфхцчшщъыьэюя';

  /* идем циклом по символам строки */

  WHILE i <= len DO

  SET i = i + 1;
  SET pos = INSTR(alphabeth, SUBSTR(strlow,i,1));

  /*выполняем преобразование припомощи ф-ии ELT */

  SET sub = elt(pos,
  '0','1','2','3','4','5','6','7','8','9', ' ',
    'a','b','v','g', 'd', 'e', 'e','zh', 'z',
  'i','j','k','l', 'm', 'n', 'o', 'p', 'r',
  's','t','u','f', 'ch', 'c','cz','sz','sz',
  '', 'y', '','e','ju','ja' 
);

  IF sub IS NOT NULL THEN
    SET res = CONCAT(res, sub);
  ELSE
  	 SET res = CONCAT(res, SUBSTR(strlow,i,1));
	END IF;

  END WHILE;

  RETURN res;
END $$

DELIMITER ;