#удаляем длинный первичный ключ
ALTER TABLE d_fias_addrobj DROP PRIMARY KEY;

ALTER TABLE d_fias_addrobj add column id INT NOT NULL AUTO_INCREMENT FIRST, ADD primary KEY(id);

ALTER TABLE d_fias_addrobj add column `parent` INT NULL DEFAULT NULL AFTER `id`;


ALTER TABLE d_fias_addrobj DROP INDEX parent;
ALTER TABLE d_fias_addrobj ADD INDEX(parent);

ALTER TABLE d_fias_addrobj ADD UNIQUE(aoid);

ALTER TABLE `d_fias_addrobj` ADD KEY `parentguid` (`parentguid`), ADD KEY `aoguid` (`aoguid`);

UPDATE d_fias_addrobj t1 
JOIN d_fias_addrobj t2 ON (t1.parentguid = t2.aoguid)
SET t1.parent = t2.id
WHERE (t1.actstatus = 1) AND (t2.actstatus = 1);

SELECT COUNT(*) FROM d_fias_addrobj;#1378527
SELECT COUNT(*) FROM fias.d_fias_addrobj WHERE (actstatus = 1) AND 
(
	(aolevel IN(1,3,4,6)) 
	OR ((aolevel=90) AND (shortname IN ('снт','промзона')))
);#219 631

SELECT COUNT(DISTINCT aoguid) FROM fias.d_fias_addrobj WHERE (actstatus = 1) AND 
(
	(aolevel IN(1,3,4,6)) 
	OR ((aolevel=90) AND (shortname IN ('снт','промзона')))
);#219 631 - OK


#Всавляем в связующую таблицу все нужные aoguid
INSERT INTO msl.fias_categories_fias (id, category_id, fias_aoid,fias_aoguid)
SELECT id, id, aoid, aoguid FROM fias_opt.d_fias_addrobj WHERE (actstatus = 1) AND 
(
	(aolevel IN(1,3,4,6)) 
	OR ((aolevel=90) AND (shortname IN ('снт','промзона')))
);#219 631

SELECT COUNT(*) FROM msl.fias_categories_fias;#219 631 - OK

#устанавливаем для категорий src_name
INSERT INTO msl.fias_categories (id,parent, src_name)
SELECT f.id, f.parent, f.formalname FROM fias_opt.d_fias_addrobj as f 
WHERE (actstatus = 1) AND 
(
	(aolevel IN(1,3,4,6)) 
	OR ((aolevel=90) AND (shortname IN ('снт','промзона')))
);#219 631

SELECT COUNT(*) FROM msl.fias_categories;#219 631 - OK