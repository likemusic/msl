#SET GENITIVE

#Создаем таблицы
CREATE TABLE `tmp_src_name_uniques` (
	`id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
	`name` VARCHAR(255) NOT NULL COLLATE 'utf8_unicode_ci',
	PRIMARY KEY (`id`)
	);


CREATE TABLE `tmp_src_name_genitive_uniques` (
	`id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
	`name` VARCHAR(255) NOT NULL COLLATE 'utf8_unicode_ci',
	PRIMARY KEY (`id`)
	);
	
#Перевод строки должен быть в формате UNIX
LOAD DATA INFILE 'c:\src_names_sorted.txt' INTO TABLE `tmp_src_name_uniques` (`name`);#118 844
SELECT COUNT(*) FROM `tmp_src_name_uniques`;

LOAD DATA INFILE 'c:\cities.txt' INTO TABLE `tmp_src_name_genitive_uniques` (`name`);#118 844
SELECT COUNT(*) FROM `tmp_src_name_genitive_uniques`;

#Поля только из цифр в родительском пажеже пустые - делаем равными именительному падежу
SELECT COUNT(*) FROM `tmp_src_name_genitive_uniques` WHERE `name` = '';#100

SELECT s.id, s.name, g.name
FROM `tmp_src_name_uniques` s JOIN `tmp_src_name_genitive_uniques` g USING(id)
WHERE g.`name` = '';

UPDATE `tmp_src_name_uniques` s JOIN `tmp_src_name_genitive_uniques` g USING(id)
SET g.name = s.name
WHERE g.`name` = '';#100


SELECT COUNT(*) FROM fias_categories WHERE src_genitive ='';#219474
SELECT COUNT(*) FROM fias_categories WHERE src_genitive !='';#90
SELECT COUNT(*) FROM fias_categories WHERE parent = 0;#88
SELECT * FROM fias_categories WHERE (src_genitive !='') AND (parent !=0);#88
SELECT * FROM `tmp_src_name_uniques` s WHERE name IN (
SELECT name FROM fias_categories WHERE (src_genitive !='') AND (parent !=0)
);#0


SELECT COUNT(*) FROM fias_categories c 
JOIN `tmp_src_name_uniques` s ON (c.src_name = s.name)
JOIN `tmp_src_name_genitive_uniques` g ON (s.id = g.id) WHERE c.src_genitive = '';#219 474

UPDATE fias_categories c 
JOIN `tmp_src_name_uniques` s ON (c.src_name = s.name)
JOIN `tmp_src_name_genitive_uniques` g ON (s.id = g.id)
SET c.src_genitive = g.name
WHERE c.parent != 0;# - 155





#N заменяем на №
SELECT * FROM `tmp_src_name_uniques` WHERE `name` != REPLACE(`name`,'N','№');#911
SELECT * FROM `fias_categories` WHERE `src_name` != REPLACE(`src_name`,'N','№');#911

UPDATE `tmp_src_name_uniques` s SET `name` = REPLACE(`name`,'N','№');
