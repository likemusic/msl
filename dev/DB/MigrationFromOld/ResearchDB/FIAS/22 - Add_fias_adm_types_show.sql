ALTER TABLE fias_category_adm_types
	ADD `show` TINYINT(4) NOT NULL DEFAULT '0' AFTER `use_position`,
	ADD INDEX `show` (`show`);

UPDATE fias_category_adm_types SET `show` = 1 WHERE `use_position` = 2;#6

ALTER TABLE category_adm_types
	ADD `show` TINYINT(4) NOT NULL DEFAULT '0' AFTER `use_position`,
	ADD INDEX `show` (`show`);

UPDATE category_adm_types SET `show` = 1 WHERE `use_position` = 2;#0