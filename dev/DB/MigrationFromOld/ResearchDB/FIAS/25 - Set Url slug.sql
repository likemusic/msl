#транслитом
SELECT 
transliterate_ru_cs(
CASE t.use_position
WHEN 0 THEN c.src_genitive
WHEN 1 THEN CONCAT(t.used_genitive,' ',c.src_genitive)
WHEN 2 THEN CONCAT(c.src_genitive,' ',t.used_genitive) 
END)
FROM fias_categories c
JOIN fias_category_adm_types t ON(c.adm_type_id = t.id)
WHERE t.use_position = 2
LIMIT 20;

#транслит+lower+replace_special_chars+replace_space
#транслитом
SELECT
REPLACE( 
replace_special_chars(
LOWER(
transliterate_ru_cs(
CASE t.use_position
WHEN 0 THEN c.src_genitive
WHEN 1 THEN CONCAT(t.used_genitive,' ',c.src_genitive)
WHEN 2 THEN CONCAT(c.src_genitive,' ',t.used_genitive) 
END)))
,' ','_')
FROM fias_categories c
JOIN fias_category_adm_types t ON(c.adm_type_id = t.id)
WHERE t.use_position = 2
LIMIT 20;

SELECT
url_slug(
CASE t.use_position
WHEN 0 THEN c.src_genitive
WHEN 1 THEN CONCAT(t.used_genitive,' ',c.src_genitive)
WHEN 2 THEN CONCAT(c.src_genitive,' ',t.used_genitive) 
END)
FROM fias_categories c
JOIN fias_category_adm_types t ON(c.adm_type_id = t.id)
WHERE t.use_position = 2
LIMIT 20;

#Ищем одинаковые дочерние у родительских
SELECT COUNT(DISTINCT c1.parent, c1.src_genitive) FROM
fias_categories c1
JOIN fias_categories c2 
ON (c1.parent = c2.parent) AND (c1.src_genitive = c2.src_genitive) AND (c1.id != c2.id);#5035

SELECT DISTINCT c1.parent, c1.src_genitive FROM
fias_categories c1
JOIN fias_categories c2 
ON (c1.parent = c2.parent) AND (c1.src_genitive = c2.src_genitive) AND (c1.id != c2.id)
INTO OUTFILE 'd:/duplicates.txt';#5035


SELECT * FROM fias_categories WHERE id = 325276;#Нерльское 34229
SELECT * FROM fias_categories WHERE id = 34229;#Калязинский 571576
SELECT * FROM fias_categories WHERE id = 571576;#Тверская 0

#94235	Надежды СНТ	30
SELECT * FROM fias_categories WHERE id = 94235;#Можайский 1133504
SELECT * FROM fias_categories WHERE id = 1133504;#Московская 0


SELECT * FROM fias_categories WHERE parent = 325276;

SELECT DISTINCT c1.parent, c1.src_genitive FROM
fias_categories c1
JOIN fias_categories c2 
ON (c1.parent = c2.parent) AND (c1.src_genitive = c2.src_genitive) AND (c1.adm_type_id = c2.adm_type_id) AND (c1.id != c2.id)
INTO OUTFILE 'd:/duplicates_by_adm_type_id.txt';#5035

SELECT c1.parent, c1.src_genitive, COUNT(*) as cnt FROM
fias_categories c1
JOIN fias_categories c2 
ON (c1.parent = c2.parent) AND (c1.src_genitive = c2.src_genitive) AND (c1.adm_type_id = c2.adm_type_id) AND (c1.id != c2.id)
GROUP BY c1.parent, c1.src_genitive
ORDER BY `cnt` DESC
INTO OUTFILE 'd:/duplicates_by_adm_type_id_grouped.txt';#5035


#----------------------------------------------------------------------------------
#создаем таблицу с дублями (которые сохраняем)
DROP TABLE IF EXISTS `tmp_src_name_duplicates`;
CREATE TABLE `tmp_src_name_duplicates` (
	`parent` INT(10) UNSIGNED NULL DEFAULT NULL,
	`id` INT(10) UNSIGNED NOT NULL,#заполним позже
	`adm_type_id` INT(10) UNSIGNED NOT NULL,
	`src_name` VARCHAR(255) NOT NULL COLLATE 'utf8_unicode_ci',
	`cnt` INT(10) UNSIGNED NOT NULL,
	INDEX(`parent`),
	INDEX(`id`)
);

TRUNCATE TABLE `tmp_src_name_duplicates`;

INSERT INTO `tmp_src_name_duplicates`;
SELECT c1.parent, 0, c1.adm_type_id, c1.src_name, COUNT(*) as cnt FROM
fias_categories c1
JOIN fias_categories c2 
ON (c1.parent = c2.parent) 
AND (c1.src_name = c2.src_name) 
AND (c1.adm_type_id = c2.adm_type_id)
AND (c1.id != c2.id)
GROUP BY c1.parent, adm_type_id, c1.src_name
ORDER BY `cnt` DESC;#2509

#создаем таблицу с дублями (которые удаляем), чтобы проверить нет ли у них дочерних
DROP TABLE IF EXISTS `tmp_src_name_duplicates_del`;
CREATE TABLE `tmp_src_name_duplicates_del` (
	`id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
	`is_parent` INT(10) UNSIGNED NOT NULL DEFAULT 0,#заполним позже
	`save` INT(10) UNSIGNED NOT NULL DEFAULT 0,#заполним позже
	PRIMARY KEY(`id`)
);

INSERT INTO `tmp_src_name_duplicates_del` (`id`)
SELECT c.id
FROM `tmp_src_name_duplicates` as d
JOIN `fias_categories` as c 
ON (d.parent = c.parent)
AND (d.adm_type_id = c.adm_type_id)
AND (d.src_name = c.src_name);#5 262


/*SELECT d.parent, d.adm_type_id, d.src_name, c.*
FROM `tmp_src_name_duplicates` as d
JOIN `fias_categories` as c 
ON (d.parent = c.parent)
AND (d.adm_type_id = c.adm_type_id)
AND (d.src_name = c.src_name)
WHERE d.parent = 94235;#571 827*/
#94235 15 Надежда СНТ 6


#Проверяем нет ли у удаляемых дочерних
SELECT DISTINCT parent FROM fias_categories WHERE parent IN (SELECT id FROM `tmp_src_name_duplicates_del`);#103

UPDATE `tmp_src_name_duplicates_del` SET `is_parent` = 1 WHERE id IN(
	SELECT * FROM (
	SELECT DISTINCT parent FROM fias_categories WHERE parent IN (SELECT id FROM `tmp_src_name_duplicates_del`)) as t
);#103

#помечаем для сохранения все имеющие детей
UPDATE `tmp_src_name_duplicates_del` SET `save` = 1 WHERE `is_parent` = 1;#103

#помечаем для сохранения связанные с минимальным ID
#Сперва устанавливаем id в таблице дублей для имеющих детей
SELECT du.parent, du.adm_type_id, du.src_name, del.id
FROM tmp_src_name_duplicates du
JOIN fias_categories c ON (du.parent = c.parent) AND (du.src_name = c.src_name) AND (du.adm_type_id = c.adm_type_id)
JOIN tmp_src_name_duplicates_del del ON (c.id = del.id)
WHERE del.is_parent = 1;#103


UPDATE tmp_src_name_duplicates du
JOIN fias_categories c ON (du.parent = c.parent) AND (du.src_name = c.src_name) AND (du.adm_type_id = c.adm_type_id)
JOIN tmp_src_name_duplicates_del del ON (c.id = del.id)
SET du.id = del.id
WHERE del.is_parent = 1;#83 - OK

SELECT id, COUNT(*) as `cnt` FROM tmp_src_name_duplicates GROUP BY id ORDER BY cnt DESC;#0 - 2426

#Затем устанавливаем для неустановленных
SELECT du.*, del.*
FROM tmp_src_name_duplicates du
JOIN fias_categories c ON (du.parent = c.parent) AND (du.src_name = c.src_name) AND (du.adm_type_id = c.adm_type_id)
JOIN tmp_src_name_duplicates_del del ON (c.id = del.id)
WHERE du.id = 0;#5093

UPDATE tmp_src_name_duplicates du
JOIN fias_categories c ON (du.parent = c.parent) AND (du.src_name = c.src_name) AND (du.adm_type_id = c.adm_type_id)
JOIN tmp_src_name_duplicates_del del ON (c.id = del.id)
SET du.id = del.id
WHERE du.id = 0;#2426

#Установленные id в таблице du, помечаем как сохраненные в таблице del
SELECT COUNT(*) FROM tmp_src_name_duplicates_del del 
JOIN tmp_src_name_duplicates du ON (del.id = du.id);#2509

UPDATE tmp_src_name_duplicates_del del 
JOIN tmp_src_name_duplicates du ON (del.id = du.id)
SET `save` = 1;#2426

SELECT `save`, COUNT(*) FROM tmp_src_name_duplicates_del GROUP BY `save`;
/*
save	COUNT(*)
0	2733
1	2529
*/

#Удаляем удаляемые
DELETE FROM fias_categories WHERE id IN(SELECT id FROM tmp_src_name_duplicates_del WHERE `save` = 0);#2733

#-----------------------------------------------------------
#чистим таблицу дублей и создаем заново уже на очищенной БД, для нахождения элементов к которым надо будет добавлять id

#создаем таблицу с дублями (которые сохраняем)
DROP TABLE IF EXISTS `tmp_src_name_duplicates2`;
CREATE TABLE `tmp_src_name_duplicates2` (
	`parent` INT(10) UNSIGNED NULL DEFAULT NULL,
	`id` INT(10) UNSIGNED NOT NULL,#заполним позже
	`adm_type_id` INT(10) UNSIGNED NOT NULL,
	`src_name` VARCHAR(255) NOT NULL COLLATE 'utf8_unicode_ci',
	`cnt` INT(10) UNSIGNED NOT NULL,
	INDEX(`parent`),
	INDEX(`id`)
);

TRUNCATE TABLE `tmp_src_name_duplicates2`;

INSERT INTO `tmp_src_name_duplicates2`
SELECT c1.parent, 0, c1.adm_type_id, c1.src_name, COUNT(*) as cnt FROM
fias_categories c1
JOIN fias_categories c2 
ON (c1.parent = c2.parent) 
AND (c1.src_name = c2.src_name) 
AND (c1.adm_type_id = c2.adm_type_id)
AND (c1.id != c2.id)
GROUP BY c1.parent, adm_type_id, c1.src_name
ORDER BY `cnt` DESC;#20

#Устанавливаем id по уже существующей таблице
SELECT f.*, p.id FROM tmp_src_name_duplicates2 f
JOIN tmp_src_name_duplicates p ON (f.parent = p.parent) AND (f.adm_type_id = p.adm_type_id) AND (f.src_name = p.src_name);#20

UPDATE tmp_src_name_duplicates2 f
JOIN tmp_src_name_duplicates p ON (f.parent = p.parent) AND (f.adm_type_id = p.adm_type_id) AND (f.src_name = p.src_name)
SET f.id = p.id;#20

#находим id для которых надо добавить id к url
SELECT f.*, c.id
FROM tmp_src_name_duplicates2 f
JOIN fias_categories c ON (f.parent = c.parent) AND (f.adm_type_id = c.adm_type_id) AND (f.src_name = c.src_name)
AND (f.id != c.id);#20


DROP TABLE IF EXISTS `tmp_url_with_id`;
CREATE TABLE `tmp_url_with_id` (
	`id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,#заполним позже
	PRIMARY KEY(`id`)
);

INSERT INTO `tmp_url_with_id` (`id`)
SELECT c.id
FROM tmp_src_name_duplicates2 f
JOIN fias_categories c ON (f.parent = c.parent) AND (f.adm_type_id = c.adm_type_id) AND (f.src_name = c.src_name)
AND (f.id != c.id);#20

#--------------------------- END
#TRUNCATE TABLE `tmp_url_with_id`;


#проверяем нет ли дублей у каждого их которых есть дочерние
SELECT c.*
FROM tmp_src_name_duplicates du
JOIN fias_categories c ON (du.parent = c.parent) AND (du.src_name = c.src_name) AND (du.adm_type_id = c.adm_type_id)
JOIN tmp_src_name_duplicates_del del ON (c.id = del.id)
WHERE del.is_parent = 1;#49

SELECT du.parent, du.adm_type_id, du.src_name, COUNT(*) cnt
FROM tmp_src_name_duplicates du
JOIN fias_categories c ON (du.parent = c.parent) AND (du.src_name = c.src_name) AND (du.adm_type_id = c.adm_type_id)
JOIN tmp_src_name_duplicates_del del ON (c.id = del.id)
WHERE del.is_parent = 1
GROUP BY du.parent, du.adm_type_id, du.src_name 
ORDER BY cnt DESC;#83

SELECT * FROM fias_categories WHERE (parent = 466502) AND (adm_type_id = 28) AND (src_name = 'Жилино');
SELECT * FROM fias_categories WHERE (id = 466502);#466502 Солнечногорский 1133504
SELECT * FROM fias_categories WHERE (id = 1133504);#1133504 Московская 0

/*
parent	adm_type_id	src_name	cnt
466502	28	Жилино	2
1340576	28	Пешково	2
213829	28	Дурнево	2
589250	24	Ушаково	2
284004	24	Куликово	2
475327	28	Аксеново	2
213829	28	Мишнево	2
213829	28	Фролово	2
475327	28	Стеблево	2
131676	28	Панино	2
475327	28	Пашково	1
686581	28	Каменево	1
133591	28	Митино	1
338438	28	Гридино	1
1189143	28	Андрейково	1
498496	28	Мешково	1
466502	28	Исаково	1
1172340	28	Лазарево	1
1103405	28	Колычево	1
356875	28	Лаптево	1
356875	28	Бекренево	1
94235	28	Головино	1
466502	28	Чашниково	1
385866	24	Зендиково	1
703697	28	Крюково	1
1133627	28	Лягушкино	1
686581	28	Волобуево	1
475327	28	Калеево	1
498496	28	Голохвастово	1
1053212	38	Подрезково	1
338438	28	Кресцово	1
133591	28	Бобошино	1
131676	28	Головино	1
475327	28	Морозово	1
1041872	28	Каликино	1
713391	28	Захарово	1
570666	28	Кузнецово	1
973015	24	Лежнево	1
1103405	28	Сотниково	1
*/



#Удаляем все дубли, кроме тех которые имеют детей

#779489 Ярославич-2 183830
SELECT * FROM fias_categories WHERE id = 356875;#183830 Ярославский 1165916
SELECT * FROM fias_categories WHERE id = 1165916;#1165916 Ярославская 0



