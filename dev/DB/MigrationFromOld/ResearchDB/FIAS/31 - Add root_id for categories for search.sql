#Добавляем поле корневого id
ALTER TABLE `categories`
	ADD COLUMN `root_id` INT UNSIGNED NOT NULL DEFAULT '0' AFTER `disabled`,
	ADD INDEX `root_id` (`root_id`);

ALTER TABLE `fias_categories`
	ADD COLUMN `root_id` INT UNSIGNED NOT NULL DEFAULT '0' AFTER `disabled`,
	ADD INDEX `root_id` (`root_id`);

#Устанавливаем для всех
## Сначала для корневых
UPDATE fias_categories SET `root_id` = id WHERE parent = 0;#86

#И рекурсивно все дочерние
UPDATE fias_categories t1 
JOIN fias_categories t2 ON(t1.id = t2.parent)
SET t2.`root_id` = t1.root_id
WHERE (t1.root_id != 0) AND (t2.root_id=0);#2 436 - 193 242 - 20 545 - 512 - 0


