#создаем таблицу с дублями (которые сохраняем)
DROP TABLE IF EXISTS `tmp_url_slug`;
CREATE TABLE `tmp_url_slug` (
	`id` INT(10) UNSIGNED NOT NULL,#заполним позже
	`parent` INT(10) UNSIGNED NULL DEFAULT NULL,
	`url_slug` VARCHAR(255) NOT NULL COLLATE 'utf8_unicode_ci',
	INDEX(`id`)
);

INSERT INTO tmp_url_slug
SELECT c.id, c.parent,
url_slug(
CASE t.use_position
WHEN 0 THEN CONCAT(c.src_genitive)
WHEN 1 THEN CONCAT(t.used_genitive,' ',c.src_genitive)
WHEN 2 THEN CONCAT(c.src_genitive,' ',t.used_genitive) 
END)
FROM fias_categories c 
JOIN fias_category_adm_types t ON(c.adm_type_id = t.id)
WHERE c.url_slug IS NULL;#20

SELECT COUNT(*) FROM tmp_url_slug;#216802
SELECT COUNT(*) FROM fias_categories WHERE url_slug IS NULL;#216802
SELECT COUNT(*) FROM fias_categories WHERE url_slug IS NOT NULL;#20

SELECT MIN(id),parent, `url_slug`, COUNT(*) as cnt 
FROM tmp_url_slug GROUP BY parent, `url_slug` HAVING cnt > 1 ORDER BY cnt DESC;#75
#max 2

DROP TABLE IF EXISTS `tmp_url_slug_dupl`;
CREATE TABLE `tmp_url_slug_dupl` (
	`id` INT(10) UNSIGNED NOT NULL,#заполним позже
	`parent` INT(10) UNSIGNED NULL DEFAULT NULL,
	`url_slug` VARCHAR(255) NOT NULL COLLATE 'utf8_unicode_ci',
	`cnt` INT(10) UNSIGNED NULL DEFAULT NULL,
	INDEX(`id`)
);

INSERT INTO tmp_url_slug_dupl 
SELECT MAX(id),parent, `url_slug`, COUNT(*) as cnt FROM tmp_url_slug 
GROUP BY parent, `url_slug` HAVING cnt > 1 ORDER BY cnt DESC;#75



