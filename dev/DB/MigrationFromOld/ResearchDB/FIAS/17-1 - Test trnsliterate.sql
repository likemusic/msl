SELECT src_name, transliterate_ru_cs(src_name), transliterate_ru_ci(src_name) FROM fias_categories LIMIT 10;

SELECT src_name, LOWER(src_name), CHAR_LENGTH(LOWER(src_name)) FROM fias_categories LIMIT 10;

SELECT src_name, SUBSTR(LOWER(src_name),5,1) FROM fias_categories LIMIT 10;

SELECT src_name, transliterate_ru_cs(src_name), transliterate_ru_ci(src_name), replace_special_chars(transliterate_ru_cs(src_name)) FROM fias_categories LIMIT 10;

SELECT replace_special_chars(' aze фыв '), CHAR_LENGTH(replace_special_chars(' '));

SELECT SUBSTR(' 01',1,1), CHAR_LENGTH(SUBSTR(' 01',1,1));

SET @ch = '';
SET @alphabeth = '0123456789abcdifghijklmnopqrstuvwxyz ';
SELECT INSTR(@alphabeth,SUBSTR(' 01',1,1) );

SELECT COUNT(*) FROM fias_categories WHERE  src_name != replace_special_chars(src_name);#10292
SELECT src_name FROM fias_categories WHERE  src_name != replace_special_chars(src_name) LIMIT 100;#10292

#c точками
SELECT COUNT(*) FROM fias_categories WHERE  (src_name != replace_special_chars(src_name)) AND (src_name LIKE '%.%');#766

SELECT src_name FROM fias_categories WHERE  (src_name != replace_special_chars(src_name)) AND (src_name NOT LIKE '%.%') LIMIT 100;#10292

#cо слешем
SELECT COUNT(*) FROM fias_categories WHERE  (src_name != replace_special_chars(src_name)) AND (src_name LIKE '%/%');#4615


SELECT COUNT(*) FROM fias_categories WHERE  (src_name != replace_special_chars(src_name)) AND (src_name NOT LIKE '%.%') AND (src_name NOT LIKE '%/%');#4932
SELECT src_name FROM fias_categories WHERE  (src_name != replace_special_chars(src_name)) AND (src_name NOT LIKE '%.%') AND (src_name NOT LIKE '%/%') LIMIT 100;#10292

#cо скобками
SELECT COUNT(*) FROM fias_categories WHERE  (src_name != replace_special_chars(src_name)) AND ((src_name LIKE '%(%') OR (src_name LIKE '%)%'));#8548


SELECT COUNT(*) FROM fias_categories WHERE  (src_name != replace_special_chars(src_name)) AND (src_name NOT LIKE '%.%') AND (src_name NOT LIKE '%/%') AND (src_name NOT LIKE '%(%') AND (src_name NOT LIKE '%)%');#921
SELECT src_name FROM fias_categories WHERE  (src_name != replace_special_chars(src_name)) AND (src_name NOT LIKE '%.%') AND (src_name NOT LIKE '%/%') AND (src_name NOT LIKE '%(%') AND (src_name NOT LIKE '%)%') LIMIT 100;#4932

#cо знаком номера
SELECT COUNT(*) FROM fias_categories WHERE  (src_name != replace_special_chars(src_name)) AND (src_name LIKE '%№%');#1001

SELECT COUNT(*) FROM fias_categories WHERE  (src_name != replace_special_chars(src_name)) AND (src_name NOT LIKE '%.%') AND (src_name NOT LIKE '%/%') AND (src_name NOT LIKE '%(%') AND (src_name NOT LIKE '%)%') AND (src_name NOT LIKE '%№%');#5
SELECT src_name FROM fias_categories WHERE  (src_name != replace_special_chars(src_name)) AND (src_name NOT LIKE '%.%') AND (src_name NOT LIKE '%/%') AND (src_name NOT LIKE '%(%') AND (src_name NOT LIKE '%)%') AND (src_name NOT LIKE '%№%') LIMIT 100;
#нижнее подчеркивание - фиксим

UPDATE fias_categories SET src_name = REPLACE(src_name, '_',' ') WHERE src_name LIKE '%_%';#13
#SELECT * FROM fias_categories WHERE src_genitive LIKE '%_%';
SELECT * FROM fias_categories WHERE src_genitive != REPLACE(src_genitive, '_',' ');#13

UPDATE fias_categories SET src_genitive = REPLACE(src_genitive, '_',' ') WHERE src_genitive != REPLACE(src_genitive, '_',' ');#13



