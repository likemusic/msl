SELECT COUNT(*) FROM fias_categories WHERE SUBSTRING(src_name,6,1) = ' ';#24309
SELECT COUNT(*) FROM fias_categories WHERE (SUBSTRING(src_name,6,1) = ' ') AND (CHAR_LENGTH(src_name)>5);#6413


#смотрим какие совпадают с сокращениями
SELECT DISTINCT SUBSTRING(src_name,1,5) FROM fias_categories WHERE (SUBSTRING(src_name,6,1) = ' ') AND (CHAR_LENGTH(src_name)>5);#755

#Группируем т.к. очень много
SELECT SUBSTRING(src_name,1,5) as str , COUNT(*) as cnt FROM fias_categories WHERE (SUBSTRING(src_name,6,1) = ' ') AND (CHAR_LENGTH(src_name)>5)
GROUP BY SUBSTRING(src_name,1,5) ORDER BY cnt DESC;#755


#совпадают с сокращениями
SELECT id,shortname,fullname,used_genitive,not_used,use_position,`show` 
FROM fias_category_adm_types WHERE shortname IN(
	SELECT DISTINCT SUBSTRING(src_name,1,5) FROM fias_categories WHERE (SUBSTRING(src_name,6,1) = ' ') AND (CHAR_LENGTH(src_name)>5)
	
);#3

/*
89	платф	Платформа	\N	1	0	0
97	ферма	Ферма	\N	1	0	0
101	берег	Берег	\N	1	0	0
*/

#не совпадают с сокращениями
SELECT SUBSTRING(src_name,1,5) as str , COUNT(*) as cnt FROM fias_categories WHERE (SUBSTRING(src_name,6,1) = ' ') AND (CHAR_LENGTH(src_name)>5)
AND (SUBSTRING(src_name,1,5) NOT IN (
	SELECT * FROM (
		SELECT shortname 
		FROM fias_category_adm_types WHERE shortname IN(
			SELECT DISTINCT SUBSTRING(src_name,1,5) FROM fias_categories WHERE (SUBSTRING(src_name,6,1) = ' ')  AND (CHAR_LENGTH(src_name)>5)
		)	
	) as t
))
GROUP BY SUBSTRING(src_name,1,5) ORDER BY cnt DESC;#450



#проверяем совпадающие
#--- 89	платф	Платформа	\N	1	0	0
SELECT COUNT(*) FROM fias_categories WHERE (SUBSTRING(src_name,1,6) = 'платф ');#15
SELECT * FROM fias_categories WHERE (SUBSTRING(src_name,1,6) = 'платф ');#15
SELECT DISTINCT adm_type_id FROM fias_categories WHERE (SUBSTRING(src_name,1,6) = 'платф ');#1 39 - нп
SELECT adm_type_id, COUNT(*) as cnt FROM fias_categories WHERE (SUBSTRING(src_name,1,6) = 'платф ') GROUP BY adm_type_id ORDER BY cnt DESC;#9

#удаляем
SELECT  src_name, SUBSTRING(src_name,6)  FROM fias_categories WHERE (SUBSTRING(src_name,1,5) = 'уч-к ');#16
UPDATE fias_categories SET src_name = SUBSTRING(src_name,6)  WHERE (SUBSTRING(src_name,1,5) = 'уч-к ');#16

SELECT  src_genitive, SUBSTRING(src_genitive,6)  FROM fias_categories WHERE (SUBSTRING(src_genitive,1,5) = 'уч-к ');#16
UPDATE fias_categories SET src_genitive = SUBSTRING(src_genitive,6)  WHERE (SUBSTRING(src_genitive,1,5) = 'уч-к ');#16

/*
97	ферма	Ферма	\N	1	0	0
101	берег	Берег	\N	1	0	0
*/

#--- 98	мост	Мост	\N	1	0	0
SELECT COUNT(*) FROM fias_categories WHERE (SUBSTRING(src_name,1,5) = 'мост ');#2
SELECT * FROM fias_categories WHERE (SUBSTRING(src_name,1,5) = 'мост ');#2
SELECT DISTINCT adm_type_id FROM fias_categories WHERE (SUBSTRING(src_name,1,5) = 'мост ');#1 24 - п поселок
#SELECT adm_type_id, COUNT(*) as cnt FROM fias_categories WHERE (SUBSTRING(src_name,1,5) = 'мост ') GROUP BY adm_type_id ORDER BY cnt DESC;#9
#Все с большой буквы - оставляем


#--- 105	зона	Зона	\N	1	0	0
SELECT COUNT(*) FROM fias_categories WHERE (SUBSTRING(src_name,1,5) = 'зона ');#2
SELECT * FROM fias_categories WHERE (SUBSTRING(src_name,1,5) = 'зона ');#2
SELECT DISTINCT adm_type_id FROM fias_categories WHERE (SUBSTRING(src_name,1,5) = 'зона ');#1 24 - п поселок
#SELECT adm_type_id, COUNT(*) as cnt FROM fias_categories WHERE (SUBSTRING(src_name,1,5) = 'мост ') GROUP BY adm_type_id ORDER BY cnt DESC;#9
#Все с большой буквы - оставляем

