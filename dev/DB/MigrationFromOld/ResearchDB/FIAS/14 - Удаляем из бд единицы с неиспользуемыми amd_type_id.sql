#----------- 58 - автодорога
SELECT * FROM fias_categories WHERE adm_type_id = 58;#63
#типы
SELECT DISTINCT type_id FROM fias_categories WHERE adm_type_id = 58;#6 - населенный пункт

#есть ли дочерние
SELECT * FROM fias_categories WHERE parent IN (
SELECT id FROM fias_categories WHERE adm_type_id = 58
);#9
/*
type_id = 90 - дополнительная территория
adm_type_id = 128 - снт - Садовое неком-е товарищество
*/

#есть ли дочерние у дочерних
SELECT * FROM fias_categories WHERE parent IN (
	SELECT id FROM fias_categories WHERE parent IN (
		SELECT id FROM fias_categories WHERE adm_type_id = 58
));#0


#Удаляем дочерние
DELETE FROM fias_categories WHERE parent IN (
SELECT * FROM
(SELECT id FROM fias_categories WHERE adm_type_id = 58) as t
);#9 - OK

#Удаляем их самих
DELETE FROM fias_categories WHERE adm_type_id = 58;#63- OK

#Помечаем тип как неиспользуемый
UPDATE fias_category_adm_types SET not_used = 2 WHERE id = 58;#1 - OK


/*
#----------- 29	ж/д_будка		Железнодорожная будка
*/
SELECT * FROM fias_categories WHERE adm_type_id = 29;#122
#типы
SELECT DISTINCT type_id FROM fias_categories WHERE adm_type_id = 29;#6 - населенный пункт

#есть ли дочерние
SELECT * FROM fias_categories WHERE parent IN (
SELECT id FROM fias_categories WHERE adm_type_id = 29
);#2
/*
type_id = 90 - дополнительная территория
adm_type_id = 128 - снт - Садовое неком-е товарищество
*/

#есть ли дочерние у дочерних
SELECT * FROM fias_categories WHERE parent IN (
	SELECT id FROM fias_categories WHERE parent IN (
		SELECT id FROM fias_categories WHERE adm_type_id = 29
));#0 - OK


#Удаляем дочерние
DELETE FROM fias_categories WHERE parent IN (
SELECT * FROM
(SELECT id FROM fias_categories WHERE adm_type_id = 29) as t
);#2 - OK

#Удаляем их самих
DELETE FROM fias_categories WHERE adm_type_id = 29;#122- OK

#Помечаем тип как неиспользуемый
UPDATE fias_category_adm_types SET not_used = 2 WHERE id = 29;#1 - OK



/*
#----------- 30	ж/д_казарм		Железнодорожная казарма
*/
SELECT * FROM fias_categories WHERE adm_type_id = 30;#72
#типы
SELECT DISTINCT type_id FROM fias_categories WHERE adm_type_id = 30;#6 - населенный пункт

#есть ли дочерние
SELECT * FROM fias_categories WHERE parent IN (
SELECT id FROM fias_categories WHERE adm_type_id = 30
);#0
/*
type_id = 90 - дополнительная территория
adm_type_id = 128 - снт - Садовое неком-е товарищество
*/

#есть ли дочерние у дочерних
SELECT * FROM fias_categories WHERE parent IN (
	SELECT id FROM fias_categories WHERE parent IN (
		SELECT id FROM fias_categories WHERE adm_type_id = 30
));#0 - OK


#Удаляем дочерние
DELETE FROM fias_categories WHERE parent IN (
SELECT * FROM
(SELECT id FROM fias_categories WHERE adm_type_id = 30) as t
);#2 - OK

#Удаляем их самих
DELETE FROM fias_categories WHERE adm_type_id = 30;#72- OK

#Помечаем тип как неиспользуемый
UPDATE fias_category_adm_types SET not_used = 2 WHERE id = 30;#1 - OK


/*
#----------- -	31	ж/д_оп		ж/д останов. (обгонный) пункт
*/
SELECT * FROM fias_categories WHERE adm_type_id = 31;#72
#типы
SELECT DISTINCT type_id FROM fias_categories WHERE adm_type_id = 31;#6 - населенный пункт

#есть ли дочерние
SELECT * FROM fias_categories WHERE parent IN (
SELECT id FROM fias_categories WHERE adm_type_id = 31
);#0
/*
type_id = 90 - дополнительная территория
adm_type_id = 128 - снт - Садовое неком-е товарищество
*/

#есть ли дочерние у дочерних
SELECT * FROM fias_categories WHERE parent IN (
	SELECT id FROM fias_categories WHERE parent IN (
		SELECT id FROM fias_categories WHERE adm_type_id = 31
));#0 - OK


#Удаляем дочерние
DELETE FROM fias_categories WHERE parent IN (
SELECT * FROM
(SELECT id FROM fias_categories WHERE adm_type_id = 31) as t
);#0 - OK

#Удаляем их самих
DELETE FROM fias_categories WHERE adm_type_id = 31;#72- OK

#Помечаем тип как неиспользуемый
UPDATE fias_category_adm_types SET not_used = 2 WHERE id = 31;#1 - OK


/*
#----------- -	-	32	ж/д_пост		Железнодорожный пост
*/
SELECT * FROM fias_categories WHERE adm_type_id = 32;#27
#типы
SELECT DISTINCT type_id FROM fias_categories WHERE adm_type_id = 32;#6 - населенный пункт

#есть ли дочерние
SELECT * FROM fias_categories WHERE parent IN (
SELECT id FROM fias_categories WHERE adm_type_id = 32
);#0
/*
type_id = 90 - дополнительная территория
adm_type_id = 128 - снт - Садовое неком-е товарищество
*/

#есть ли дочерние у дочерних
SELECT * FROM fias_categories WHERE parent IN (
	SELECT id FROM fias_categories WHERE parent IN (
		SELECT id FROM fias_categories WHERE adm_type_id = 32
));#0 - OK


#Удаляем дочерние
DELETE FROM fias_categories WHERE parent IN (
SELECT * FROM
(SELECT id FROM fias_categories WHERE adm_type_id = 32) as t
);#0 - OK

#Удаляем их самих
DELETE FROM fias_categories WHERE adm_type_id = 32;#27- OK

#Помечаем тип как неиспользуемый
UPDATE fias_category_adm_types SET not_used = 2 WHERE id = 32;#1 - OK



/*
#----------- -	-	33	ж/д_рзд		Железнодорожный разъезд
*/
SELECT * FROM fias_categories WHERE adm_type_id = 33;#169
#типы
SELECT DISTINCT type_id FROM fias_categories WHERE adm_type_id = 33;#6 - населенный пункт

#есть ли дочерние
SELECT * FROM fias_categories WHERE parent IN (
SELECT id FROM fias_categories WHERE adm_type_id = 33
);#1
/*
type_id = 90 - дополнительная территория
adm_type_id = 128 - снт - Садовое неком-е товарищество
*/

#есть ли дочерние у дочерних
SELECT * FROM fias_categories WHERE parent IN (
	SELECT id FROM fias_categories WHERE parent IN (
		SELECT id FROM fias_categories WHERE adm_type_id = 33
));#0 - OK


#Удаляем дочерние
DELETE FROM fias_categories WHERE parent IN (
SELECT * FROM
(SELECT id FROM fias_categories WHERE adm_type_id = 33) as t
);#1 - OK

#Удаляем их самих
DELETE FROM fias_categories WHERE adm_type_id = 33;#169- OK

#Помечаем тип как неиспользуемый
UPDATE fias_category_adm_types SET not_used = 2 WHERE id = 33;#1 - OK


/*
#----------- 60	жилзона		Жилая зона
*/
SELECT * FROM fias_categories WHERE adm_type_id = 60;#5
#типы
SELECT DISTINCT type_id FROM fias_categories WHERE adm_type_id = 60;#6 - населенный пункт

#есть ли дочерние
SELECT * FROM fias_categories WHERE parent IN (
SELECT id FROM fias_categories WHERE adm_type_id = 60
);#0
/*
type_id = 90 - дополнительная территория
adm_type_id = 128 - снт - Садовое неком-е товарищество
*/

#есть ли дочерние у дочерних
SELECT * FROM fias_categories WHERE parent IN (
	SELECT id FROM fias_categories WHERE parent IN (
		SELECT id FROM fias_categories WHERE adm_type_id = 60
));#0 - OK


#Удаляем дочерние
DELETE FROM fias_categories WHERE parent IN (
SELECT * FROM
(SELECT id FROM fias_categories WHERE adm_type_id = 60) as t
);#1 - OK

#Удаляем их самих
DELETE FROM fias_categories WHERE adm_type_id = 60;#5- OK

#Помечаем тип как неиспользуемый
UPDATE fias_category_adm_types SET not_used = 2 WHERE id = 60;#1 - OK


/*
#----------- 59	жилрайон		Жилой район
*/
SELECT * FROM fias_categories WHERE adm_type_id = 59;#30
#типы
SELECT DISTINCT type_id FROM fias_categories WHERE adm_type_id = 59;#6 - населенный пункт

#есть ли дочерние
SELECT * FROM fias_categories WHERE parent IN (
SELECT id FROM fias_categories WHERE adm_type_id = 59
);#0
/*
type_id = 90 - дополнительная территория
adm_type_id = 128 - снт - Садовое неком-е товарищество
*/

#есть ли дочерние у дочерних
SELECT * FROM fias_categories WHERE parent IN (
	SELECT id FROM fias_categories WHERE parent IN (
		SELECT id FROM fias_categories WHERE adm_type_id = 59
));#0 - OK


#Удаляем дочерние
DELETE FROM fias_categories WHERE parent IN (
SELECT * FROM
(SELECT id FROM fias_categories WHERE adm_type_id = 59) as t
);#1 - OK

#Удаляем их самих
DELETE FROM fias_categories WHERE adm_type_id = 59;#30- OK

#Помечаем тип как неиспользуемый
UPDATE fias_category_adm_types SET not_used = 2 WHERE id = 59;#1 - OK


/*
#----------- 53	кв-л		Квартал
*/
SELECT * FROM fias_categories WHERE adm_type_id = 53;#38
#типы
SELECT DISTINCT type_id FROM fias_categories WHERE adm_type_id = 53;#6 - населенный пункт

#есть ли дочерние
SELECT * FROM fias_categories WHERE parent IN (
SELECT id FROM fias_categories WHERE adm_type_id = 53
);#15
/*
type_id = 90 - дополнительная территория
adm_type_id = 128 - снт - Садовое неком-е товарищество
*/

#есть ли дочерние у дочерних
SELECT * FROM fias_categories WHERE parent IN (
	SELECT id FROM fias_categories WHERE parent IN (
		SELECT id FROM fias_categories WHERE adm_type_id = 53
));#0 - OK


#Удаляем дочерние
DELETE FROM fias_categories WHERE parent IN (
SELECT * FROM
(SELECT id FROM fias_categories WHERE adm_type_id = 53) as t
);#15 - OK

#Удаляем их самих
DELETE FROM fias_categories WHERE adm_type_id = 53;#38- OK

#Помечаем тип как неиспользуемый
UPDATE fias_category_adm_types SET not_used = 2 WHERE id = 53;#1 - OK



/*
#----------- 55	лпх		Леспромхоз
*/
SELECT * FROM fias_categories WHERE adm_type_id = 55;#5
#типы
SELECT DISTINCT type_id FROM fias_categories WHERE adm_type_id = 55;#6 - населенный пункт

#есть ли дочерние
SELECT * FROM fias_categories WHERE parent IN (
SELECT id FROM fias_categories WHERE adm_type_id = 55
);#0
/*
type_id = 90 - дополнительная территория
adm_type_id = 128 - снт - Садовое неком-е товарищество
*/

#есть ли дочерние у дочерних
SELECT * FROM fias_categories WHERE parent IN (
	SELECT id FROM fias_categories WHERE parent IN (
		SELECT id FROM fias_categories WHERE adm_type_id = 55
));#0 - OK


#Удаляем дочерние
DELETE FROM fias_categories WHERE parent IN (
SELECT * FROM
(SELECT id FROM fias_categories WHERE adm_type_id = 55) as t
);#0 - OK

#Удаляем их самих
DELETE FROM fias_categories WHERE adm_type_id = 55;#5- OK

#Помечаем тип как неиспользуемый
UPDATE fias_category_adm_types SET not_used = 2 WHERE id = 55;#1 - OK

/*
#----------- 41	п/р		Планировочный район
*/
SELECT * FROM fias_categories WHERE adm_type_id = 41;#7
#типы
SELECT DISTINCT type_id FROM fias_categories WHERE adm_type_id = 41;#6 - населенный пункт
#SELECT type_id, COUNT(*) FROM fias_categories WHERE adm_type_id = 20 GROUP BY type_id;

#есть ли дочерние
SELECT * FROM fias_categories WHERE parent IN (
SELECT id FROM fias_categories WHERE adm_type_id = 41
);#0

#SELECT type_id, COUNT(*) FROM fias_categories WHERE parent IN (
#SELECT id FROM fias_categories WHERE adm_type_id = 41 ) GROUP BY type_id;#0

#SELECT adm_type_id, COUNT(*) FROM fias_categories WHERE parent IN (
#SELECT id FROM fias_categories WHERE adm_type_id = 20
#) GROUP BY adm_type_id;#1 185


#есть ли дочерние у дочерних
SELECT * FROM fias_categories WHERE parent IN (
	SELECT id FROM fias_categories WHERE parent IN (
		SELECT id FROM fias_categories WHERE adm_type_id = 41
));#0 - OK


#Удаляем дочерние
DELETE FROM fias_categories WHERE parent IN (
SELECT * FROM
(SELECT id FROM fias_categories WHERE adm_type_id = 41) as t
);#0 - OK

#Удаляем их самих
DELETE FROM fias_categories WHERE adm_type_id = 41;#7- OK

#Помечаем тип как неиспользуемый
UPDATE fias_category_adm_types SET not_used = 2 WHERE id = 41;#1 - OK

#Оставили
#----------- 23	массив		Массив - пока оставляем т.к. много дочерних есть
#----------- 38	мкр		Микрорайон - пока оставляю т.к. всего 512 и делится на снт
#----------- 20	п/о		Почтовое отделение - пока оставляю т.к. много дочерних деревень, сел, и поселков
