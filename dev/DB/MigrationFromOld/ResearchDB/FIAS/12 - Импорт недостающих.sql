#Импортируем недостающие
/*
Если надо оставить и шоссе и снт, то пролучается что надо снизу вверх данные из фиас вытягивать. Т.е. помечаем все уровня 91 как не нужные. Затем помечаем из уровня 90 как нужные только снт и промзона, и затем для них помечаем рекурсивно все родительские как нужные. Затем все неотмеченные как нужные помечаем ненужными уровня 7 улица. Затем помечаем нужными уровень 6 и их все дочерние. Затем помечаем ненужными те которые не отмечены нужные из уровня 5 внутригородская территория. А зетем уже все оставшиеся уровни 4,3, 1 поемечаем нужными, и после этого импортируем только нужные. Я правильно мыслю? Так делать?
Т.е. при таком подходе останутся родительские для оставляемых уровней и групп, которые находятся на удаляемом уровне. Так делать?
*/


/* Надо дотянуть для безродительских рекурсивно родителей */
/* Для безродительских на данный момент в БД level=NULL */

/* Надо установить

id,
parent
src_name
type_id = aolevel

level

adm_type_id
(url_slug)
*/

/* Временно для новых импортированных устанавливаем фейковый `level` чтобы различать их */
SELECT DISTINCT parent FROM fias_categories WHERE `level` IS NULL;#572

INSERT INTO msl.fias_categories (id, parent, src_name, type_id,`level`)
SELECT id, parent, formalname, aolevel, 101 FROM fias_opt.d_fias_addrobj WHERE id IN (
	SELECT DISTINCT parent FROM fias_categories WHERE `level` IS NULL
);#572

#Всего родительских для уже вставленных
SELECT DISTINCT parent FROM fias_categories WHERE `level` = 101;#182
SELECT id FROM 
(SELECT DISTINCT parent as `id` FROM fias_categories WHERE `level` = 101) as s
LEFT JOIN fias_categories  c USING(id)
WHERE c.id IS NOT NULL;#0

#получается все остальные уже есть в бд

#добавляем новые в таблицу - связку
INSERT INTO fias_categories_fias (id,category_id,fias_aoid,fias_aoguid)
SELECT f.id, f.id, f.aoid, f.aoguid
FROM msl.fias_categories c 
JOIN fias_opt.d_fias_addrobj f USING(id)
LEFT JOIN msl.fias_categories_fias l USING(id)
WHERE l.id IS NULL;#572

#устанавливаем по предыдущему для всех level

#устанавливаем для новых adm_type_id
SELECT COUNT(*) FROM fias_categories WHERE adm_type_id IS NULL;#572 - 0

UPDATE fias_categories c
JOIN fias_opt.d_fias_addrobj f USING(id)
JOIN fias_category_adm_types t ON (f.shortname = t.shortname COLLATE 'utf8_unicode_ci')
SET c.adm_type_id = t.id
WHERE c.adm_type_id IS NULL;#572 - OK



#Теперь надо отметить используемые типы
SELECT DISTINCT adm_type_id FROM fias_categories;#63
SELECT COUNT(*) FROM fias_category_adm_types WHERE not_used=0;#49 - не сходится, надо обновлять


SELECT DISTINCT t.id, t.shortname, t.fullname FROM fias_categories c
JOIN fias_category_adm_types t ON(c.adm_type_id=t.id)
WHERE t.not_used = 1;#13

UPDATE fias_category_adm_types SET not_used = 0 WHERE id IN(
SELECT * FROM (
SELECT DISTINCT t.id FROM fias_categories c
JOIN fias_category_adm_types t ON(c.adm_type_id=t.id)
WHERE t.not_used = 1) as e);#13-OK

