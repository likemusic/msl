UPDATE product_templates SET url_slug = LOWER(url_slug);#29

#--------- FIAS

#Находим используемые шаблоны
SELECT DISTINCT pt.id
FROM fias_categories c
JOIN fias_category_types ct ON (c.type_id = ct.id)
JOIN product_sets ps ON (ct.product_set_id = ps.id)
JOIN product_sets_product_templates ps2pt ON (ps.id = ps2pt.product_set_id)
JOIN product_templates pt ON (ps2pt.product_template_id=pt.id);#22

#Находим неиспользуемые шаблоны
SELECT id, tpl_name, url_slug FROM product_templates WHERE id NOT IN(
SELECT DISTINCT pt.id
FROM fias_categories c
JOIN fias_category_types ct ON (c.type_id = ct.id)
JOIN product_sets ps ON (ct.product_set_id = ps.id)
JOIN product_sets_product_templates ps2pt ON (ps.id = ps2pt.product_set_id)
JOIN product_templates pt ON (ps2pt.product_template_id=pt.id)
);#9


#----------- OLD
#Находим используемые шаблоны
SELECT DISTINCT pt.id
FROM categories c
JOIN category_types ct ON (c.type_id = ct.id)
JOIN product_sets ps ON (ct.product_set_id = ps.id)
JOIN product_sets_product_templates ps2pt ON (ps.id = ps2pt.product_set_id)
JOIN product_templates pt ON (ps2pt.product_template_id=pt.id);#30

#Находим неиспользуемые шаблоны
SELECT id, tpl_name, url_slug FROM product_templates WHERE id NOT IN(
SELECT DISTINCT pt.id
FROM categories c
JOIN category_types ct ON (c.type_id = ct.id)
JOIN product_sets ps ON (ct.product_set_id = ps.id)
JOIN product_sets_product_templates ps2pt ON (ps.id = ps2pt.product_set_id)
JOIN product_templates pt ON (ps2pt.product_template_id=pt.id)
);#1


#Добавляем в таблице типов товаров (карт) поле url_slug_part используемое для генерации url_slug шаблонов товаров
ALTER TABLE `product_types`
	ADD COLUMN `url_slug_part` VARCHAR(255) NULL DEFAULT NULL AFTER `alt`;
	
SELECT url_slug(`name`) FROM product_types;

ALTER TABLE `product_templates`
	ADD COLUMN `scale` VARCHAR(50) NOT NULL AFTER `product_type_id`;

CREATE TABLE `adm_types` (
	`id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
	`name` VARCHAR(255) NOT NULL COLLATE 'utf8_unicode_ci',
	`created_at` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00',
	`updated_at` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00',
	PRIMARY KEY (`id`),
	UNIQUE INDEX `name` (`name`));

INSERT INTO adm_types (`id`,`name`) VALUES
(1,'страна'),
(2,'область'),
(3,'район'),
(4,'город');


UPDATE product_templates SET `adm_type_id` = 1 WHERE tpl_name LIKE '%страны%';#8
UPDATE product_templates SET `adm_type_id` = 2 WHERE tpl_name LIKE '%области%';#7
UPDATE product_templates SET `adm_type_id` = 3 WHERE tpl_name LIKE '%района%';#9
UPDATE product_templates SET `adm_type_id` = 4 WHERE tpl_name LIKE '%города%';#5
SELECT * FROM product_templates WHERE adm_type_id IS NULL;#2

SELECT `adm_type_id`, COUNT(*) as cnt FROM product_templates GROUP BY adm_type_id;
SELECT COUNT(*) as cnt FROM product_templates;#31

#Устанавливаем url_slug
SELECT pt.id, pt.tpl_name, pt.url_slug, pty.name, CONCAT(url_slug(CONCAT(pty.name,' карта')),'_{parent_url_slug}',
IF(pt.scale,CONCAT('_', url_slug(CONCAT('1см-',pt.scale))),'')
)
FROM product_templates pt
JOIN adm_types as `aty` ON(pt.adm_type_id = aty.id)
JOIN product_types pty ON(pt.product_type_id = pty.id);









