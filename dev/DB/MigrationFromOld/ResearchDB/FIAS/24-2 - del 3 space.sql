SELECT COUNT(*) FROM fias_categories WHERE SUBSTRING(src_name,4,1) = ' ';#13 048
SELECT COUNT(*) FROM fias_categories WHERE (SUBSTRING(src_name,4,1) = ' ') AND (CHAR_LENGTH(src_name)>3);#8774


#Группируем т.к. очень много
SELECT SUBSTRING(src_name,1,3) as str , COUNT(*) as cnt FROM fias_categories WHERE (SUBSTRING(src_name,4,1) = ' ') AND (CHAR_LENGTH(src_name)>3)
GROUP BY SUBSTRING(src_name,1,3) ORDER BY cnt DESC;#455

#смотрим какие совпадают с сокращениями
SELECT DISTINCT SUBSTRING(src_name,1,3) FROM fias_categories WHERE (SUBSTRING(src_name,4,1) = ' ') AND (CHAR_LENGTH(src_name)>3);#457 - 455

#Группируем т.к. очень много
SELECT SUBSTRING(src_name,1,3) as str , COUNT(*) as cnt FROM fias_categories WHERE (SUBSTRING(src_name,4,1) = ' ') AND (CHAR_LENGTH(src_name)>3)
GROUP BY SUBSTRING(src_name,1,3) ORDER BY cnt DESC;#455


#совпадают с сокращениями
SELECT id,shortname,fullname,used_genitive,not_used,use_position,`show` 
FROM fias_category_adm_types WHERE shortname IN(
	SELECT DISTINCT SUBSTRING(src_name,1,3) FROM fias_categories WHERE (SUBSTRING(src_name,4,1) = ' ')  AND (CHAR_LENGTH(src_name)>3)	
);#14 - 7


#не совпадают с сокращениями
SELECT SUBSTRING(src_name,1,3) as str , COUNT(*) as cnt FROM fias_categories WHERE (SUBSTRING(src_name,4,1) = ' ') AND (CHAR_LENGTH(src_name)>3)
AND (SUBSTRING(src_name,1,3) NOT IN (
	SELECT * FROM (
		SELECT shortname 
		FROM fias_category_adm_types WHERE shortname IN(
			SELECT DISTINCT SUBSTRING(src_name,1,3) FROM fias_categories WHERE (SUBSTRING(src_name,4,1) = ' ')  AND (CHAR_LENGTH(src_name)>3)
		)	
	) as t
))
GROUP BY SUBSTRING(src_name,1,3) ORDER BY cnt DESC;#450

#--- Снт - 4699
SELECT COUNT(*) FROM fias_categories WHERE (SUBSTRING(src_name,1,4) = 'Снт ');#4699
SELECT DISTINCT adm_type_id FROM fias_categories WHERE (SUBSTRING(src_name,1,4) = 'Снт ');#5
SELECT adm_type_id, COUNT(*) as cnt FROM fias_categories WHERE (SUBSTRING(src_name,1,4) = 'Снт ') GROUP BY adm_type_id ORDER BY cnt DESC;#9
/*
128	2704	снт	Садовое неком-е товарищество
11		1585	11		Территория
15		392	дп		Дачный поселок
39		17		нп		Населенный пункт
37		1		м		Местечко
*/

# для типа 128 снт - удаляем
SELECT  src_name, SUBSTRING(src_name,5)  FROM fias_categories WHERE (SUBSTRING(src_name,1,4) = 'Снт ') AND (adm_type_id=128);#2704
UPDATE fias_categories SET src_name = SUBSTRING(src_name,5)  WHERE (SUBSTRING(src_name,1,4) = 'Снт ') AND (adm_type_id=128);#1845

SELECT  src_genitive, SUBSTRING(src_genitive,5)  FROM fias_categories WHERE (SUBSTRING(src_genitive,1,4) = 'Снт ') AND (adm_type_id=128);#2704
UPDATE fias_categories SET src_genitive = SUBSTRING(src_genitive,5)  WHERE (SUBSTRING(src_genitive,1,4) = 'Снт ') AND (adm_type_id=128);#1845

# для остальных - тоже
SELECT  src_name, SUBSTRING(src_name,5)  FROM fias_categories WHERE (SUBSTRING(src_name,1,4) = 'Снт ');#1 995
UPDATE fias_categories SET src_name = SUBSTRING(src_name,5)  WHERE (SUBSTRING(src_name,1,4) = 'Снт ');#1 995

SELECT  src_genitive, SUBSTRING(src_genitive,5)  FROM fias_categories WHERE (SUBSTRING(src_genitive,1,4) = 'Снт ');#1 995
UPDATE fias_categories SET src_genitive = SUBSTRING(src_genitive,5)  WHERE (SUBSTRING(src_genitive,1,4) = 'Снт ');#1 995




#не совпадают с сокращениями


/*
21	с/п	Сельское поселение	с/п	0	1	0
45	рзд	Разъезд	рзд.	0	1	0
64	дор	Дорога	\N	0	1	0
81	сад	Сад	\N	0	1	0
93	гск	Гаражно-строительный кооперат	\N	1	0	0
106	ДОМ	Дом	\N	1	0	0
*/

/* Deleted
#9	р-н	Район	района	0	2	1
11	тер	Территория	тер.	0	1	0
18	с/о	Сельский округ	с/о	0	1	0
20	п/о	Почтовое отделение	п/о	0	1	0
39	нп	Населенный пункт	нп.	0	1	0
83	стр	Строение	\N	1	0	0
96	вал	Вал	\N	1	0	0
128	снт	Садовое неком-е товарищество	снт.	0	1	0
*/
#--- 9	р-н	Район	района	0	2	1
SELECT COUNT(*) FROM fias_categories WHERE (SUBSTRING(src_name,1,4) = 'р-н ');#7
SELECT src_name FROM fias_categories WHERE (SUBSTRING(src_name,1,4) = 'р-н ');#7

SELECT DISTINCT adm_type_id FROM fias_categories WHERE (SUBSTRING(src_name,1,4) = 'р-н ');#1 - 11 - тер Территория

SELECT adm_type_id, COUNT(*) as cnt FROM fias_categories WHERE (SUBSTRING(src_name,1,4) = 'Снт ') GROUP BY adm_type_id ORDER BY cnt DESC;#9

#удаляем везде
SELECT  src_name, SUBSTRING(src_name,5)  FROM fias_categories WHERE (SUBSTRING(src_name,1,4) = 'р-н ');#7
UPDATE fias_categories SET src_name = SUBSTRING(src_name,5)  WHERE (SUBSTRING(src_name,1,4) = 'р-н ');#7

SELECT  src_genitive, SUBSTRING(src_genitive,5)  FROM fias_categories WHERE (SUBSTRING(src_genitive,1,4) = 'р-н ');#7
UPDATE fias_categories SET src_genitive = SUBSTRING(src_genitive,5)  WHERE (SUBSTRING(src_genitive,1,4) = 'р-н ');#7


#--- 11	тер	Территория	тер.	0	1	0
SELECT COUNT(*) FROM fias_categories WHERE (SUBSTRING(src_name,1,4) = 'тер ');#1
SELECT * FROM fias_categories WHERE (SUBSTRING(src_name,1,4) = 'тер ');#1
#491295  тер Волковского грузового района	1108177	44	промзона
SELECT * FROM fias_categories  WHERE id = 1108177;#Чайковский 1068591
SELECT * FROM fias_categories  WHERE id = 1068591;#Пермский 0

#удаляем
DELETE FROM fias_categories WHERE (SUBSTRING(src_name,1,4) = 'тер ');#1

#--- 18	с/о	Сельский округ	с/о	0	1	0
SELECT COUNT(*) FROM fias_categories WHERE (SUBSTRING(src_name,1,4) = 'с/о ');#4
SELECT * FROM fias_categories WHERE (SUBSTRING(src_name,1,4) = 'с/о ');#4

/*
adm_type_id
1	11		тер 	Территория
2	39		нп		Населенный пункт
1	128	снт	Садовое неком-е товарищество
*/

#удаляем
SELECT  src_name, SUBSTRING(src_name,5)  FROM fias_categories WHERE (SUBSTRING(src_name,1,4) = 'с/о ');#4
UPDATE fias_categories SET src_name = SUBSTRING(src_name,5)  WHERE (SUBSTRING(src_name,1,4) = 'с/о ');#4

SELECT  src_genitive, SUBSTRING(src_genitive,5)  FROM fias_categories WHERE (SUBSTRING(src_genitive,1,4) = 'с/о ');#4
UPDATE fias_categories SET src_genitive = SUBSTRING(src_genitive,5)  WHERE (SUBSTRING(src_genitive,1,4) = 'с/о ');#4


#-----20	п/о	Почтовое отделение	п/о	0	1	0
SELECT COUNT(*) FROM fias_categories WHERE (SUBSTRING(src_name,1,4) = 'п/о ');#2
SELECT * FROM fias_categories WHERE (SUBSTRING(src_name,1,4) = 'п/о ');#2
/*
1084484	П/О МИР			687167	128	снт
1149168	П/о Вороново	148586	39		нп	Населенный пункт
*/
#удаляем
SELECT  src_name, SUBSTRING(src_name,5)  FROM fias_categories WHERE (SUBSTRING(src_name,1,4) = 'П/О ');#2
UPDATE fias_categories SET src_name = SUBSTRING(src_name,5)  WHERE (SUBSTRING(src_name,1,4) = 'П/О ');#2

SELECT  src_genitive, SUBSTRING(src_genitive,5)  FROM fias_categories WHERE (SUBSTRING(src_genitive,1,4) = 'П/О ');#2
UPDATE fias_categories SET src_genitive = SUBSTRING(src_genitive,5)  WHERE (SUBSTRING(src_genitive,1,4) = 'П/О ');#2


#----- 21	с/п	Сельское поселение	с/п	0	1	0
SELECT COUNT(*) FROM fias_categories WHERE (SUBSTRING(src_name,1,4) = 'с/п ');#1
SELECT * FROM fias_categories WHERE (SUBSTRING(src_name,1,4) = 'с/п ');#1
/*
1284668	С/п им Воровского			1208898	11 тер
*/
#удаляем
SELECT  src_name, SUBSTRING(src_name,5)  FROM fias_categories WHERE (SUBSTRING(src_name,1,4) = 'с/п ');#1
UPDATE fias_categories SET src_name = SUBSTRING(src_name,5)  WHERE (SUBSTRING(src_name,1,4) = 'с/п ');#1

SELECT  src_genitive, SUBSTRING(src_genitive,5)  FROM fias_categories WHERE (SUBSTRING(src_genitive,1,4) = 'с/п ');#1
UPDATE fias_categories SET src_genitive = SUBSTRING(src_genitive,5)  WHERE (SUBSTRING(src_genitive,1,4) = 'с/п ');#1



#----- 45	рзд	Разъезд	рзд.	0	1	0
SELECT COUNT(*) FROM fias_categories WHERE (SUBSTRING(src_name,1,4) = 'рзд ');#14
SELECT * FROM fias_categories WHERE (SUBSTRING(src_name,1,4) = 'рзд ');#14
SELECT DISTINCT adm_type_id FROM fias_categories WHERE (SUBSTRING(src_name,1,4) = 'рзд ');#14

/*
24	п	Поселок
28	д Деревня
45 рзд рзд
*/

#удаляем
SELECT  src_name, SUBSTRING(src_name,5)  FROM fias_categories WHERE (SUBSTRING(src_name,1,4) = 'рзд ');#14
UPDATE fias_categories SET src_name = SUBSTRING(src_name,5)  WHERE (SUBSTRING(src_name,1,4) = 'рзд ');#14

SELECT  src_genitive, SUBSTRING(src_genitive,5)  FROM fias_categories WHERE (SUBSTRING(src_genitive,1,4) = 'рзд ');#14
UPDATE fias_categories SET src_genitive = SUBSTRING(src_genitive,5)  WHERE (SUBSTRING(src_genitive,1,4) = 'рзд ');#14




#----- 64	дор	Дорога	\N	0	1	0
SELECT COUNT(*) FROM fias_categories WHERE (SUBSTRING(src_name,1,4) = 'дор ');#56
SELECT * FROM fias_categories WHERE (SUBSTRING(src_name,1,4) = 'дор ');#14
SELECT DISTINCT adm_type_id FROM fias_categories WHERE (SUBSTRING(src_name,1,4) = 'дор ');#14
SELECT adm_type_id, COUNT(*) FROM fias_categories WHERE (SUBSTRING(src_name,1,4) = 'дор ') GROUP BY adm_type_id;#14
/*
24	1		п	Поселок
28	53		д Деревня
46	1		с Село
128	1	снт
*/

#все с большой буквы - Названия - оставляем

#----- 81	сад	Сад	\N	0	1	0
SELECT COUNT(*) FROM fias_categories WHERE (SUBSTRING(src_name,1,4) = 'сад ');#215
SELECT COUNT(*) FROM fias_categories WHERE (SUBSTRING(src_name,1,4) = 'сад ') AND (CHAR_LENGTH(src_name)>3);#203
SELECT adm_type_id, COUNT(*) FROM fias_categories WHERE (SUBSTRING(src_name,1,4) = 'сад ') AND (CHAR_LENGTH(src_name)>3)  GROUP BY adm_type_id;#14
/*
11	83		тер
15	4		дп
39	1		нп
128 115	снт
*/

#смотрим по группам
SELECT * FROM fias_categories WHERE (SUBSTRING(src_name,1,4) = 'сад ') AND (CHAR_LENGTH(src_name)>3) AND (adm_type_id = 11);
#83 - почти все с большой - оставляем

SELECT * FROM fias_categories WHERE (SUBSTRING(src_name,1,4) = 'сад ') AND (CHAR_LENGTH(src_name)>3) AND (adm_type_id = 15);
#83 - почти все с большой - оставляем

SELECT * FROM fias_categories WHERE (SUBSTRING(src_name,1,4) = 'сад ') AND (CHAR_LENGTH(src_name)>3) AND (adm_type_id = 39);
#83 - почти все с большой - оставляем

SELECT * FROM fias_categories WHERE (SUBSTRING(src_name,1,4) = 'сад ') AND (CHAR_LENGTH(src_name)>3) AND (adm_type_id = 128);
#83 - почти все с большой - оставляем



#----- 93	гск	Гаражно-строительный кооперат	\N	1	0	0
SELECT COUNT(*) FROM fias_categories WHERE (SUBSTRING(src_name,1,4) = 'гск ');#290
SELECT COUNT(*) FROM fias_categories WHERE (SUBSTRING(src_name,1,4) = 'гск ') AND (CHAR_LENGTH(src_name)>3);#290
SELECT adm_type_id, COUNT(*) FROM fias_categories WHERE (SUBSTRING(src_name,1,4) = 'гск ')  GROUP BY adm_type_id;#14
/*
11	288 - тер
128	2 - снт

*/
SELECT * FROM fias_categories WHERE (SUBSTRING(src_name,1,4) = 'гск ') AND (adm_type_id = 11);#288 - большинство с большой

SELECT * FROM fias_categories WHERE (SUBSTRING(src_name,1,4) = 'гск ') AND (adm_type_id = 128);#2 - c большой


#----- 106	ДОМ	Дом	\N	1	0	0*/
SELECT COUNT(*) FROM fias_categories WHERE (SUBSTRING(src_name,1,4) = 'ДОМ ');#35
SELECT COUNT(*) FROM fias_categories WHERE (SUBSTRING(src_name,1,4) = 'ДОМ ') AND (CHAR_LENGTH(src_name)>3);#35
SELECT * FROM fias_categories WHERE (SUBSTRING(src_name,1,4) = 'ДОМ ') AND (CHAR_LENGTH(src_name)>3);#35
SELECT adm_type_id, COUNT(*) FROM fias_categories WHERE (SUBSTRING(src_name,1,4) = 'ДОМ ')  GROUP BY adm_type_id;#14
/*
11	4
14	1
24	14
28	4
39	8
50	1
128	3
*/

#Везде все с большой "Дома отдыха, дома Инвалидов, дома Интарнеты
