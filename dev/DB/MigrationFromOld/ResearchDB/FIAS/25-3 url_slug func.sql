DELIMITER $$
DROP FUNCTION IF EXISTS `url_slug` $$
CREATE DEFINER=`root`@`localhost` FUNCTION `url_slug`(`str` TEXT)
	RETURNS text CHARSET utf8
	LANGUAGE SQL
	DETERMINISTIC
	CONTAINS SQL
	SQL SECURITY INVOKER
	COMMENT ''
BEGIN
  	DECLARE res TEXT;
	SET res = transliterate_ru_cs(str);
	SET res = LOWER(res);
	SET res = replace_special_chars(res);
	SET res = REPLACE(res,' ','_');
	SET res = REPLACE(res,'-_','_');
	SET res = REPLACE(res,'_-','_');
	SET res = REPLACE(res,'--','-');
	SET res = REPLACE(res,'__','_');
	SET res = TRIM(BOTH '-' FROM res);
	SET res = TRIM(BOTH '_' FROM res);
	SET res = TRIM(BOTH '-' FROM res);
	SET res = TRIM(BOTH '_' FROM res);
	RETURN res;
END $$

DELIMITER ;