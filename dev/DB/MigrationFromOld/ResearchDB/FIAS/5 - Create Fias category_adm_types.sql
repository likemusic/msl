#Создаем таблицу типов населенных пунтков и административных единиц
CREATE TABLE `fias_category_adm_types` (
	`id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
	`shortname` VARCHAR(255) NOT NULL COLLATE 'utf8_unicode_ci',
	`fullname` VARCHAR(255) NOT NULL COLLATE 'utf8_unicode_ci',
	`created_at` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00',
	`updated_at` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00',
	PRIMARY KEY (`id`)
)
COLLATE='utf8_unicode_ci';


SELECT DISTINCT scname, socrname FROM d_fias_socrbase;#108
SELECT DISTINCT scname FROM d_fias_socrbase;#107
/*
scname;socrname;socrname
снт;Садовое товарищество;Садовое неком-е товарищество
снт;Садовое неком-е товарищество;Садовое товарищество
*/


INSERT INTO msl.fias_category_adm_types (shortname,fullname)
SELECT DISTINCT scname,socrname FROM fias_opt.d_fias_socrbase WHERE scname != 'снт';#106 - OK

INSERT INTO msl.fias_category_adm_types (shortname,fullname)
VALUES('снт', 'Садовое неком-е товарищество');#1 - OK

SELECT COUNT(*) FROM msl.fias_category_adm_types;#107 - OK

ALTER TABLE fias_category_adm_types ADD UNIQUE(shortname);
ALTER TABLE category_adm_types ADD UNIQUE(shortname);

SELECT COUNT(*)
FROM msl.fias_categories fc
JOIN fias_opt.d_fias_addrobj f USING(id)
JOIN msl.fias_category_adm_types  as t ON (f.shortname = t.shortname COLLATE 'utf8_unicode_ci');#217 742

UPDATE msl.fias_categories fc
JOIN fias_opt.d_fias_addrobj f USING(id)
JOIN msl.fias_category_adm_types  as t ON (f.shortname = t.shortname COLLATE 'utf8_unicode_ci')
SET fc.adm_type_id = t.id;
#219 631 - OK

