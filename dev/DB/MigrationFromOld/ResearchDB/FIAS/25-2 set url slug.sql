#Кирилическое название с типом в родительном падеже
SELECT 
CASE t.use_position
WHEN 0 THEN c.src_genitive
WHEN 1 THEN CONCAT(t.used_genitive,' ',c.src_genitive)
WHEN 2 THEN CONCAT(c.src_genitive,' ',t.used_genitive) 
END
FROM fias_categories c
JOIN fias_category_adm_types t ON(c.adm_type_id = t.id)
WHERE t.use_position = 2
LIMIT 20;


UPDATE fias_categories SET url_slug = NULL;#216 250
SELECT COUNT(*) FROM tmp_url_with_id;#20

#Устанавливаем для дублей, которые должны быть с id в url по таблице дублей
UPDATE fias_categories c 
JOIN fias_category_adm_types t ON(c.adm_type_id = t.id)
SET c.url_slug = url_slug(
CASE t.use_position
WHEN 0 THEN CONCAT(c.src_genitive,'-',c.id)
WHEN 1 THEN CONCAT(t.used_genitive,' ',c.src_genitive,'-',c.id)
WHEN 2 THEN CONCAT(c.src_genitive,' ',t.used_genitive,'-',c.id) 
END)
WHERE c.id IN(
	SELECT id FROM tmp_url_with_id
);#20


#Устанавливаем для дублей, которые должны быть с id в url по таблице дублей url_slug
UPDATE fias_categories c 
JOIN fias_category_adm_types t ON(c.adm_type_id = t.id)
SET c.url_slug = url_slug(
CASE t.use_position
WHEN 0 THEN CONCAT(c.src_genitive,'-',c.id)
WHEN 1 THEN CONCAT(t.used_genitive,' ',c.src_genitive,'-',c.id)
WHEN 2 THEN CONCAT(c.src_genitive,' ',t.used_genitive,'-',c.id) 
END)
WHERE c.id IN(
	SELECT id FROM tmp_url_slug_dupl
);#75


#Устанавливаем для дублей, которые должны быть с id в url не по таблице дублей 
#(обнаруженных в процессе выполнения последнего (следующего))
/*UPDATE fias_categories c 
JOIN fias_category_adm_types t ON(c.adm_type_id = t.id)
SET c.url_slug = url_slug(
CASE t.use_position
WHEN 0 THEN CONCAT(c.src_genitive,'-',c.id)
WHEN 1 THEN CONCAT(t.used_genitive,' ',c.src_genitive,'-',c.id)
WHEN 2 THEN CONCAT(c.src_genitive,' ',t.used_genitive,'-',c.id) 
END)
WHERE c.id IN(
	890502,#Елкино = Елькино
	641603,#Солнечный	Солнечного	49660
	1180733, #Озерный		Озерного		53893
	1345176,	#Марьинская	Марьинской	62227	28
	268427,	#Мирный	Мирного	62800
	367369,	#Курское	Курского	71046
	1061784,	#Родничок СНТ	Родничка СНТ
	1241234,	#Акиньшино	Акиньшино	94235
	294038,	#Марьино	Марьино	133591
	178659,	#Красный	Красного	156056
	1323076,	#Степаньково	Степаньково
	465098,	#Марьино	Марьино	235879
	199950,	#Васильево	Васильево	253541
	352852,	#Иваньково	Иваньково
	1271053,	#Солнечный	Солнечного	282841
	599476,	#Южный	Южного	296786
	1306094,	#Загорный	Загорного	309686
	958902,	#Южный	Южного	336136
	492705,	#Эскино	Эскино	338438
	330664,	#Пригородный	Пригородного	350356
	146454,	#Юбилейный	Юбилейного	350356
	1078131,	#Озерный	Озерного	350356
	894391,	#Васильево	Васильево	356875
	1185649,	#Юбилейный	Юбилейного	385657
	241636	#Орловское	Орловского	397561
);#2
*/
SELECT COUNT(*) FROM fias_categories WHERE `url_slug` IS NOT NULL;#89
SELECT COUNT(*) FROM fias_categories WHERE `url_slug` IS NULL;#216 733

#Устанавливаем для всех остальных
UPDATE fias_categories c 
JOIN fias_category_adm_types t ON(c.adm_type_id = t.id)
SET c.url_slug = url_slug(
CASE t.use_position
WHEN 0 THEN CONCAT(c.src_genitive)
WHEN 1 THEN CONCAT(t.used_genitive,' ',c.src_genitive)
WHEN 2 THEN CONCAT(c.src_genitive,' ',t.used_genitive) 
END)
WHERE c.url_slug IS NULL;#216 733



#заменяем "-_" >> "_"; "_-" >> "_" ,т.е. убираем пробелы-подчеркивания перед и после знака тире
#не надо т.к. теперь делается в ф-йии url_slug
SELECT * FROM fias_categories WHERE url_slug LIKE '%-_%';#22 866
UPDATE fias_categories SET url_slug = REPLACE(url_slug,'-_','_');#0
SELECT * FROM fias_categories WHERE url_slug LIKE '%_-%';#22 866
UPDATE fias_categories SET url_slug = REPLACE(url_slug,'_-','_');#0

#---------------------------------------------------------------


SELECT c.src_genitive, c.src_genitive, t.use_position, 
url_slug(
CASE t.use_position
WHEN 0 THEN CONCAT(c.src_genitive)
WHEN 1 THEN CONCAT(t.used_genitive,' ',c.src_genitive)
WHEN 2 THEN CONCAT(c.src_genitive,' ',t.used_genitive) 
END), 
t.used_genitive

FROM fias_categories c 
JOIN fias_category_adm_types t ON(c.adm_type_id = t.id)
WHERE parent = 20331;#Жуковский 1133504

#находим все используемые незаданные used_genitive

SELECT DISTINCT t.*
FROM fias_categories c 
JOIN fias_category_adm_types t ON(c.adm_type_id = t.id)
WHERE (t.used_genitive IS NULL)
AND (c.disabled=0);#2 - 0
/*
10	у	Улус
8	Чувашия	Чувашия
*/
UPDATE `msl`.`fias_category_adm_types` SET `used_genitive`='улуса' WHERE  `id`=10;
UPDATE `msl`.`fias_category_adm_types` SET `used_genitive`='' WHERE  `id`=8;


#вообще все - и и спользуемые и нет - для установки url_slug
SELECT DISTINCT t.id, t.shortname, t.fullname, t.used_genitive,t.use_position, t.`show`
FROM fias_categories c 
JOIN fias_category_adm_types t ON(c.adm_type_id = t.id)
WHERE t.used_genitive IS NULL;#14
/*
id	shortname	fullname	use_position	show
86	ул	Улица	1	0
88	ш	Шоссе	1	0
72	пер	Переулок	1	0
77	пр-кт	Проспект	1	0
81	сад	Сад	1	0
84	тракт	Тракт	1	0
67	км	Километр	0	0
87	уч-к	Участок	1	0
53	кв-л	Квартал	1	0
76	проезд	Проезд	1	0
62	б-р	Бульвар	1	0
70	наб	Набережная	1	0
64	дор	Дорога	1	0
78	просек	Просек	1	0
*/
UPDATE `msl`.`fias_category_adm_types` SET `used_genitive`='ул.' WHERE  `id`=86;
UPDATE `msl`.`fias_category_adm_types` SET `used_genitive`='ш.' WHERE  `id`=88;
UPDATE `msl`.`fias_category_adm_types` SET `used_genitive`='пер.' WHERE  `id`=72;
UPDATE `msl`.`fias_category_adm_types` SET `used_genitive`='пр-кт.' WHERE  `id`=77;
UPDATE `msl`.`fias_category_adm_types` SET `used_genitive`='сада' WHERE  `id`=81;
UPDATE `msl`.`fias_category_adm_types` SET `used_genitive`='тракта' WHERE  `id`=84;
UPDATE `msl`.`fias_category_adm_types` SET `used_genitive`='км.' WHERE  `id`=67;
UPDATE `msl`.`fias_category_adm_types` SET `used_genitive`='уч-ка' WHERE  `id`=87;
UPDATE `msl`.`fias_category_adm_types` SET `used_genitive`='кв-ла' WHERE  `id`=53;
UPDATE `msl`.`fias_category_adm_types` SET `used_genitive`='проезда' WHERE  `id`=76;
UPDATE `msl`.`fias_category_adm_types` SET `used_genitive`='б-ра' WHERE  `id`=62;
UPDATE `msl`.`fias_category_adm_types` SET `used_genitive`='наб.' WHERE  `id`=70;
UPDATE `msl`.`fias_category_adm_types` SET `used_genitive`='дор.' WHERE  `id`=64;
UPDATE `msl`.`fias_category_adm_types` SET `used_genitive`='просека' WHERE  `id`=78;









