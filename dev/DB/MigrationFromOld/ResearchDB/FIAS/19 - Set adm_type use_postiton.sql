ALTER TABLE `fias_category_adm_types`
	ADD COLUMN `use_position` TINYINT(4) NOT NULL DEFAULT '0' AFTER `not_used`;

ALTER TABLE `category_adm_types`
	ADD COLUMN `use_position` TINYINT(4) NOT NULL DEFAULT '0' AFTER `not_used`;
#0 - не использовать
#1 - в начале
#2 - в конце

#1 - в начале
UPDATE fias_category_adm_types SET `use_position` = 1 WHERE id IN(6,25,58,54,26,62,27,3,51,28,64,15,29,30,31,52,32,33,34,60,59,35,36,53,57,14,55,37,23,38,70,39,40,24,20,41,42,12,72,56,43,76,44,78,77,45,13,46,17,22,18,21,16,81,47,128,48,49,11,84,10,86,87,50,88);#65

#2 - в конце
UPDATE fias_category_adm_types SET `use_position` = 2 WHERE id IN(1,2,4,5,7,9);#6

