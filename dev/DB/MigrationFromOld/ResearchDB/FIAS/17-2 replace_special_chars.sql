DELIMITER $$

DROP FUNCTION IF EXISTS `replace_special_chars` $$
CREATE DEFINER=`root`@`localhost`
  FUNCTION `replace_special_chars`(str TEXT)
  RETURNS text CHARSET utf8
DETERMINISTIC SQL SECURITY INVOKER
BEGIN
  	DECLARE ch VARCHAR(1);
  	DECLARE res TEXT;
  	DECLARE len INT(11);
  	DECLARE i INT(11);
  	DECLARE pos INT(11);
  	DECLARE alphabeth CHAR(72);

  SET i = 0;
  SET res = '';
  SET len = CHAR_LENGTH(str)-1; 
  SET alphabeth = '0123456789 abcdefghijklmnopqrstuvwxyzабвгдеёжзийклмнопрстуфхцчшщъыьэюя_';

  /* идем циклом по символам строки */

  	WHILE i <= len DO

  		SET i = i + 1;
  		SET ch = SUBSTR(str,i,1);
  		SET pos = INSTR(alphabeth, ch );
		IF pos > 0 THEN
    		SET res = CONCAT(res,ch);
  		ELSE
  	 		SET res = CONCAT(res, '-');
		END IF;

  	END WHILE;

  RETURN res;
END $$

DELIMITER ;