SELECT c.id, c.parent, c.src_name,c.genitive, c.url_slug, url_slug(
CASE t.use_position
WHEN 0 THEN CONCAT(c.src_genitive,'-',c.id)
WHEN 1 THEN CONCAT(t.used_genitive,' ',c.src_genitive,'-',c.id)
WHEN 2 THEN CONCAT(c.src_genitive,' ',t.used_genitive,'-',c.id) 
END) as calc_url_slug

FROM fias_categories c 
JOIN fias_category_adm_types t ON(c.adm_type_id = t.id)
WHERE url_slug(
CASE t.use_position
WHEN 0 THEN CONCAT(c.src_genitive,'-',c.id)
WHEN 1 THEN CONCAT(t.used_genitive,' ',c.src_genitive,'-',c.id)
WHEN 2 THEN CONCAT(c.src_genitive,' ',t.used_genitive,'-',c.id) 
END) != url_slug;

SELECT * FROM fias_categories WHERE url_slug(`url_slug`) = `url_slug`;