#Ищем названия включающие тип
#Ищем по кол-ву, начинающимся с нескольких букв и пробела.
SELECT COUNT(*) FROM fias_categories WHERE src_name != TRIM(src_name);#0

SELECT COUNT(*) FROM fias_categories WHERE SUBSTRING(src_name,1,1) = ' ';#0

SELECT COUNT(*) FROM fias_categories WHERE SUBSTRING(src_name,2,1) = ' ';#315
SELECT * FROM fias_categories WHERE SUBSTRING(src_name,2,1) = ' ';#315
/*
Много начинающихся с цифр.
Много "У чего-то"
Несколько "В районе"
Несколько вида "№ Число"
*/

#Написанные через пробелы
UPDATE `msl`.`fias_categories` SET `src_name`='ВЕТЕРАН', `src_genitive`='ВЕТЕРАН' WHERE  `id`=801118;
UPDATE `msl`.`fias_categories` SET `src_name`='Восток', `src_genitive`='Восток' WHERE  `id`=1100595;
UPDATE `msl`.`fias_categories` SET `src_name`='Рыжиково', `src_genitive`='Рыжиково' WHERE  `id`=544069;
UPDATE `msl`.`fias_categories` SET `src_name`='Федотовка', `src_genitive`='Федотовка' WHERE  `id`=1368494;


/*
id	name	genitive	genitive_translit	src_name	src_genitive	parent	type_id	level	adm_parent	adm_level	adm_type_id	url_slug	url	group_id	sort_order	disabled	created_at	updated_at
77375				Ф ГРЕГ	Ф ГРЕГА	39336	90	4	\N	\N	128	Ф ГРЕГ-77375	\N	\N	\N	0	0000-00-00 00:00:00	0000-00-00 00:00:00
*/
SELECT * FROM fias_categories WHERE id = 39336;#39336 Сенькино 1195728
SELECT * FROM fias_categories WHERE id = 1195728;#1195728 Серпуховский 1133504
SELECT * FROM fias_categories WHERE id = 1133504;#1133504 Московская 0
#http://www.ifias.ru/fias6.html?i=f6501fa9-f468-4d27-ba3b-a56ae2605f88 - нету, но т.к. не знаю, пока оставляю

#д Глебово СОТ Глебово и подобные - пока все оставляем т.к. не относятся к деревням на данный момент
#365724 д Глебово СОТ Глебово 310155 6 11
SELECT * FROM fias_categories WHERE id = 310155;#310155 Новгородский 1017979
SELECT * FROM fias_categories WHERE id = 1017979;#1017979 Новгородская 0
#оставляем

SELECT * FROM fias_categories WHERE id = 310155;#1017979 Новгородская 0
