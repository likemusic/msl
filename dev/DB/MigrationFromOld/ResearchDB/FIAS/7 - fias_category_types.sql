CREATE TABLE `fias_category_types` (
	`id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
	`name` VARCHAR(255) NOT NULL COLLATE 'utf8_unicode_ci',
	`template_id` INT(10) UNSIGNED NULL DEFAULT NULL,
	`created_at` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00',
	`updated_at` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00',
	PRIMARY KEY (`id`)
)
COLLATE='utf8_unicode_ci';


INSERT INTO `fias_category_types` (`id`,`name`,`template_id`) VALUES
(1, 'Регион',7),
(2, 'Автономный округ', 7),
(3, 'Район',7),
(4, 'Город', 7),
(5, 'Внутригородская территория',7),
(6, 'Населенный пункт', 7),
(7, 'Улица', 7),
(90, 'Дополнительная территория', 7),
(91, 'Улица на допоолнительной территории', 7);

UPDATE msl.fias_categories fc
JOIN fias_opt.d_fias_addrobj f USING(id)
SET fc.type_id = f.aolevel;

SELECT DISTINCT type_id FROM msl.fias_categories;