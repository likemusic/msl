#----- SOCR_BASE
SELECT COUNT(*) FROM d_fias_socrbase;#239
SELECT `level`, COUNT(*) FROM d_fias_socrbase GROUP BY `level`;
/*
1	8
2	1
3	4
4	15
5	2
6	48
7	77
8	1
90	6
91	77
*/

#смотрим есть ли одинаковые названия вообще
SELECT COUNT( DISTINCT scname ) FROM d_fias_socrbase;#107 < 239 - есть одинаковые для разных уровней

#есть ли одинаковое сокращение но разная расшифровка
SELECT DISTINCT t1.scname, t1.socrname, t2.socrname
FROM d_fias_socrbase t1
JOIN d_fias_socrbase t2 USING(scname)
WHERE t1.socrname != t2.socrname;#2
/*
scname;socrname;socrname
снт;Садовое товарищество;Садовое неком-е товарищество
снт;Садовое неком-е товарищество;Садовое товарищество
*/

#регион
SELECT scname,socrname FROM d_fias_socrbase WHERE `level` = 1;#8

#автономный округ
SELECT scname,socrname FROM d_fias_socrbase WHERE `level` = 2;#1

#район
SELECT scname,socrname FROM d_fias_socrbase WHERE `level` = 3;#4

#город
SELECT scname,socrname FROM d_fias_socrbase WHERE `level` = 4;#15

#внутригородская территория
SELECT scname,socrname FROM d_fias_socrbase WHERE `level` = 5;#2

#населенный пункт
SELECT scname,socrname FROM d_fias_socrbase WHERE `level` = 6;#48

#улица
SELECT scname,socrname FROM d_fias_socrbase WHERE `level` = 7;#48

#не используется, видимо в удаленных
SELECT scname,socrname FROM d_fias_socrbase WHERE `level` = 8;#1 - ДОМ	Дом

#не используется, видимо в удаленных
SELECT scname,socrname FROM d_fias_socrbase WHERE `level` = 90;#6 - ДОМ	Дом

#не используется, видимо в удаленных
SELECT scname,socrname FROM d_fias_socrbase WHERE `level` = 91;#77 - ДОМ	Дом
