ALTER TABLE `fias_category_adm_types`
	ADD COLUMN `used_genitive` VARCHAR(255) NULL AFTER `fullname`;

ALTER TABLE `category_adm_types`
	ADD COLUMN `used_genitive` VARCHAR(255) NULL AFTER `fullname`;


UPDATE `msl`.`fias_category_adm_types` SET `used_genitive`='автономного округа' WHERE  `id`=1;
