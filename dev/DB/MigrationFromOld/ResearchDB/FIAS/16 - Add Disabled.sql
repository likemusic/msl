ALTER TABLE `fias_categories`
	ADD COLUMN `disabled` TINYINT UNSIGNED NULL DEFAULT NULL AFTER `sort_order`,
	ADD INDEX `disabled` (`disabled`);

ALTER TABLE `categories`
	ADD COLUMN `disabled` TINYINT UNSIGNED NULL DEFAULT NULL AFTER `sort_order`,
	ADD INDEX `disabled` (`disabled`);

ALTER TABLE `fias_categories`
	CHANGE COLUMN `disabled` `disabled` TINYINT(3) UNSIGNED NULL DEFAULT '0' AFTER `sort_order`;

ALTER TABLE `categories`
	CHANGE COLUMN `disabled` `disabled` TINYINT(3) UNSIGNED NULL DEFAULT '0' AFTER `sort_order`;
	
ALTER TABLE `fias_categories`
	CHANGE COLUMN `disabled` `disabled` TINYINT(3) UNSIGNED NOT NULL DEFAULT '0' AFTER `sort_order`;

ALTER TABLE `categories`
	CHANGE COLUMN `disabled` `disabled` TINYINT(3) UNSIGNED NOT NULL DEFAULT '0' AFTER `sort_order`;

SELECT COUNT(*) FROM fias_categories WHERE `disabled` = 0;
SELECT COUNT(*) FROM categories WHERE `disabled` = 0;

#Помечаем как отключенные те, которые не нужны по критериям
UPDATE fias_categories SET `disabled` = 2 WHERE
type_id IN (2,5,7,91)
OR (
	(type_id = 90) and (adm_type_id NOT IN(128,44))
);#572 - OK

#отключаем дочерние для отключенных
UPDATE fias_categories SET `disabled` = 2 WHERE `parent` IN (
	SELECT * FROM 
		(SELECT id FROM fias_categories WHERE `disabled` !=0) as t
) AND (`disabled` = 0);#1 246 - 0


