ALTER TABLE categories DROP INDEX `categories_url_slug_unique`,
ADD UNIQUE INDEX `categories_url_slug_unique` (`url_slug`, `parent`);

UPDATE fias_categories SET url_slug = CONCAT(src_name,'-',id);
UPDATE fias_categories SET url_slug = REPLACE(url_slug,'/','-');


#Шульгино
SELECT * FROM fias_categories WHERE src_name LIKE 'Шульгино%';#parent = 384637
SELECT * FROM fias_categories WHERE id = 384637;#Заокский - parent = 145408
SELECT * FROM fias_categories WHERE id = 145408;#Тульская - parent = 0

#Высоково - 338438
SELECT * FROM fias_categories WHERE src_name LIKE 'Высоково%';#parent = 338438
SELECT * FROM fias_categories WHERE id = 338438;#Некрасовский - parent = 1165916
SELECT * FROM fias_categories WHERE id = 1165916;#Ярославская - parent = 1165916


