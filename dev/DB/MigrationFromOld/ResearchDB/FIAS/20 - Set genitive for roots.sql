#--- 6-республики
SELECT * FROM fias_categories WHERE (adm_type_id = 6);#20
#установил src_genitive и переименовал src_name на краткие названия

#--- 4-край
SELECT * FROM fias_categories WHERE (adm_type_id = 4);#9

#--- 5-область
SELECT * FROM fias_categories WHERE (adm_type_id = 5);#46

#--- 2	Аобл		Автономная область
SELECT * FROM fias_categories WHERE (adm_type_id = 2);#1

#--- 1	АО		Автономный округ
SELECT * FROM fias_categories WHERE (adm_type_id = 1);#10

#--- 3	г		Город
SELECT * FROM fias_categories WHERE (adm_type_id = 3) AND (parent = 0);#3


#--- 7	округ		Округ
SELECT * FROM fias_categories WHERE (adm_type_id = 7);#2 - 0

#--- 8	Чувашия		Чувашия
SELECT * FROM fias_categories WHERE (adm_type_id = 8);#2 - 0

SELECT * FROM fias_categories WHERE  (parent = 0) AND (adm_type_id NOT IN (6,4,5,2,1,3,7,8));#10
