SELECT scname FROM 
( SELECT DISTINCT scname FROM d_fias_socrbase WHERE (`level` IN (2,5,7,91)) OR ((`level` = 90 ) AND (scname NOT IN('снт','промзона')))
) as t
WHERE t.scname NOT IN 
(
	SELECT scname FROM d_fias_socrbase WHERE (`level` IN (1,3,4,6)) OR ((`level`=90) AND (scname IN ('снт','промзона')))
);#45

#Список сокращений в неиспользованных
SELECT DISTINCT scname FROM d_fias_socrbase WHERE (`level` IN (2,5,7,91)) OR ((`level` = 90 ) AND (scname NOT IN('снт','промзона')));


ALTER TABLE `fias_category_adm_types`
	ADD  COLUMN `not_used` TINYINT NOT NULL DEFAULT '0' AFTER `fullname`,
	ADD INDEX `not_used` (`not_used`);

ALTER TABLE `category_adm_types`
	ADD  COLUMN `not_used` TINYINT NOT NULL DEFAULT '0' AFTER `fullname`,
	ADD INDEX `not_used` (`not_used`);

#используемых
SELECT DISTINCT adm_type_id FROM fias_categories;#60

#Устанавливаем реально неиспользуемые типы
UPDATE fias_category_adm_types SET `not_used` = 0;
UPDATE fias_category_adm_types SET `not_used` = 1 WHERE id NOT IN(
	SELECT DISTINCT adm_type_id FROM fias_categories
);#47

#Всего
SELECT COUNT(*) FROM fias_category_adm_types;#107
#Используется
SELECT COUNT(*) FROM fias_category_adm_types WHERE not_used <> 1;#60
SELECT * FROM fias_category_adm_types WHERE not_used <> 1;#60

#НЕ используется
SELECT COUNT(*) FROM fias_category_adm_types WHERE not_used = 1;#47
SELECT shortname FROM fias_category_adm_types WHERE not_used = 1;#47


#почему используются x/д_будка и прочее - из 6 населенный пункт

#Аобл
SELECT * FROM fias_categories WHERE adm_type_id = 2;#260879 - Еврейская

#Чувашия
SELECT * FROM fias_categories WHERE adm_type_id = 8;#214253 - Чувашская Республика -
SELECT * FROM d_fias_addrobj WHERE id = 214253;#Чувашская Республика -

#округ
SELECT * FROM fias_categories WHERE adm_type_id = 7;
/*
352420 - Забайкальский край Агинский Бурятский
1099060 - Иркутская обл Усть-Ордынский Бурятский
*/


#60 - жилзона
SELECT * FROM fias_categories WHERE adm_type_id = 60;
/*
22378		Новли					22378
65921		Станция Пикалево
239332	Новая Деревня
895480	Обрино
1293039	Гузеево
*/

SELECT * FROM fias_categories WHERE id = 623316;#623316 - Пикалево - 994599
SELECT * FROM fias_categories WHERE id = 994599;#994599 - Бокситогорский - 1075011
SELECT * FROM fias_categories WHERE id = 1075011;#1075011 - Ленинградская - 0


#8 - Чувашия
SELECT * FROM fias_categories WHERE adm_type_id = 8;
SELECT * FROM d_fias_addrobj WHERE shortname = 'Чувашия';#


