ALTER TABLE `fias_categories_fias`
	ADD COLUMN `used` TINYINT NULL DEFAULT NULL AFTER `updated_at`;
ALTER TABLE `fias_categories_fias`
	CHANGE COLUMN `used` `used` TINYINT(4) NULL DEFAULT NULL AFTER `fias_aoguid`;

ALTER TABLE `fias_categories_fias`
	ADD INDEX(`used`);
	
	
#Проверяем через level - для которых в итоге не установлен - те свободные

SELECT DISTINCT `level` FROM fias_categories;#0
SELECT DISTINCT `adm_level` FROM fias_categories;#NULL

ALTER TABLE `fias_categories`
	CHANGE COLUMN `level` `level` INT(11) NULL DEFAULT NULL AFTER `type_id`;

UPDATE fias_categories SET `level` = NULL;#218 994
SELECT COUNT(*) FROM fias_categories;#218 994 - 219 566

#level = 1
UPDATE fias_categories SET `level` = 1 WHERE parent = 0;#90

#level = 2
UPDATE fias_categories SET `level` = 2 WHERE parent IN(
	SELECT * FROM
		(SELECT id FROM fias_categories WHERE `level` = 1) as t
);#2890

#level = 3
UPDATE fias_categories SET `level` = 3 WHERE parent IN(
	SELECT * FROM
		(SELECT id FROM fias_categories WHERE `level` = 2) as t
);#195 204 - 195 493

#level = 4
UPDATE fias_categories SET `level` = 4 WHERE parent IN(
	SELECT * FROM
		(SELECT id FROM fias_categories WHERE `level` = 3) as t
);#19 456 - 20 581

#level = 5
UPDATE fias_categories SET `level` = 5 WHERE parent IN(
	SELECT * FROM
		(SELECT id FROM fias_categories WHERE `level` = 4) as t
);#108 - 512

#level = 6
UPDATE fias_categories SET `level` = 6 WHERE parent IN(
	SELECT * FROM
		(SELECT id FROM fias_categories WHERE `level` = 5) as t
);#0

SELECT DISTINCT `level` FROM fias_categories;#есть NULL - нету NULL

SELECT `level`, COUNT(*) FROM fias_categories GROUP BY `level` ORDER BY `level`;#есть NULL
/*"level"	"COUNT(*)"
\N	"1246"
"1"	"90"
"2"	"2890"
"3"	"195204"
"4"	"19456"
"5"	"108"

"level"	"COUNT(*)"
"1"	"90"
"2"	"2890"
"3"	"195493"
"4"	"20581"
"5"	"512"
*/

#Смотрим неиспользуемые
SELECT * FROM fias_categories WHERE `level` IS NULL;

SELECT type_id, COUNT(*) as `cnt` FROM fias_categories WHERE `level` IS NULL GROUP BY `type_id` ORDER BY `cnt` DESC;
/*
"type_id"	"cnt"
"90"	"1172" 	- 90 - дополнительная территория
"6"	"74"		- 6 - населенный пункт
*/

SELECT adm_type_id, COUNT(*) as `cnt` FROM fias_categories WHERE `level` IS NULL GROUP BY `adm_type_id` ORDER BY `cnt` DESC;
/*
"adm_type_id"	"cnt"
"128"	"1238"	снт - Садовое неком-е товарищество
"44"	"6"		промзона - Промышленная зона
"24"	"1"		п	Поселок
"11"	"1"		тер	Территория
*/


SELECT parent, COUNT(*) as `cnt` FROM fias_categories WHERE `level` IS NULL GROUP BY `parent` ORDER BY `cnt` DESC;
SELECT * FROM d_fias_addrobj WHERE id = 350356;#shortname = Рассказовское Шоссе, aolevel 7 - улица, parent = 333037
SELECT * FROM d_fias_addrobj WHERE id = 333037;#Тамбов parent = 30351
SELECT * FROM d_fias_addrobj WHERE id = 30351;#Тамбовская parent = null
SELECT * FROM d_fias_addrobj WHERE parent = 350356;#shortname = Шоссе, aolevel 7 - улица
SELECT aolevel, COUNT(*) FROM d_fias_addrobj WHERE parent = 350356;#85 - 90 - дополнительная территория
SELECT shortname, COUNT(*) FROM d_fias_addrobj WHERE parent = 350356;#85 - снт

/*
"parent"	"cnt"
"350356"	"83"
"366841"	"63"
"571505"	"43"
"1310894"	"37"
"574800"	"34"
"1321724"	"24"
"723060"	"16"
"774425"	"16"
"729504"	"11"
"1027550"	"9"
"735651"	"9"
"1169572"	"8"
"781807"	"8"
"987976"	"8"
"883171"	"8"
"689290"	"8"
"374806"	"7"
"1275795"	"7"
"1206964"	"7"
"208887"	"7"
"664762"	"7"
"146048"	"7"
"577041"	"7"
"235855"	"7"
"449412"	"7"
"1116084"	"7"
"567394"	"6"
"1244734"	"6"
"1117660"	"6"
"510091"	"6"
"1256563"	"6"
"1124855"	"6"
"220910"	"6"
"1020674"	"6"
"1024879"	"6"
"158022"	"6"
"932155"	"5"
"1002830"	"5"
"1330885"	"5"
"480583"	"5"
"28280"	"5"
"484799"	"5"
"1062090"	"5"
"242072"	"4"
"503948"	"4"
"934775"	"4"
"205613"	"4"
"1182127"	"4"
"939168"	"4"
"314608"	"4"
"729152"	"4"
"1012811"	"4"
"621236"	"4"
"366433"	"4"
"335255"	"3"
"550151"	"3"
"689531"	"3"
"853607"	"3"
"240793"	"3"
"39965"	"3"
"464089"	"3"
"1123558"	"3"
"1208432"	"3"
"1213971"	"3"
"469089"	"3"
"389559"	"3"
"1095816"	"3"
"315040"	"3"
"1127965"	"3"
"1162257"	"3"
"1190082"	"3"
"21371"	"3"
"318224"	"3"
"69677"	"3"
"628222"	"3"
"865272"	"3"
"980748"	"3"
"275789"	"3"
"580687"	"3"
"674367"	"3"
"1269640"	"3"
"1373515"	"3"
"1113681"	"3"
"1238982"	"3"
"561872"	"3"
"637483"	"3"
"782205"	"3"
"1030721"	"3"
"683065"	"3"
"1139716"	"3"
"190283"	"3"
"407235"	"3"
"128765"	"2"
"931882"	"2"
"1000259"	"2"
"1352701"	"2"
"503890"	"2"
"130612"	"2"
"159118"	"2"
"204809"	"2"
"303737"	"2"
"962953"	"2"
"1077515"	"2"
"1276763"	"2"
"1045821"	"2"
"1327008"	"2"
"857370"	"2"
"894915"	"2"
"1046213"	"2"
"46533"	"2"
"617962"	"2"
"661456"	"2"
"976704"	"2"
"1334807"	"2"
"1365785"	"2"
"268710"	"2"
"556863"	"2"
"573488"	"2"
"729890"	"2"
"775219"	"2"
"1222764"	"2"
"1336855"	"2"
"395436"	"2"
"437659"	"2"
"818684"	"2"
"1168251"	"2"
"1370298"	"2"
"28018"	"2"
"527916"	"2"
"948407"	"2"
"1267754"	"2"
"1290205"	"2"
"734537"	"2"
"910888"	"2"
"676125"	"2"
"1306839"	"2"
"588928"	"2"
"1240501"	"2"
"1307622"	"2"
"87922"	"2"
"123685"	"2"
"157916"	"2"
"564783"	"2"
"1240555"	"2"
"1270823"	"2"
"88592"	"2"
"299090"	"2"
"374423"	"2"
"1039090"	"2"
"1139795"	"2"
//остальные с cnt=1
*/
