ALTER TABLE d_fias_addrobj
	ADD `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT;


CREATE TABLE `fias_categories` (
	`id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
	`name` VARCHAR(255) NOT NULL COLLATE 'utf8_unicode_ci',
	`genitive` VARCHAR(255) NOT NULL COLLATE 'utf8_unicode_ci',
	`genitive_translit` VARCHAR(255) NOT NULL COLLATE 'utf8_unicode_ci',
	`src_name` VARCHAR(255) NOT NULL COLLATE 'utf8_unicode_ci',
	`src_genitive` VARCHAR(255) NOT NULL COLLATE 'utf8_unicode_ci',
	`parent` INT(10) UNSIGNED NULL DEFAULT NULL,
	`type_id` INT(10) UNSIGNED NULL DEFAULT NULL,
	`level` INT(11) NOT NULL,
	`adm_parent` INT(10) UNSIGNED NULL DEFAULT NULL,
	`adm_level` INT(10) UNSIGNED NULL DEFAULT NULL,
	`adm_type_id` INT(10) UNSIGNED NULL DEFAULT NULL,
	`url_slug` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci',
	`url` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci',
	`group_id` INT(10) UNSIGNED NULL DEFAULT NULL,
	`sort_order` INT(10) UNSIGNED NULL DEFAULT NULL,
	`created_at` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00',
	`updated_at` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00',
	PRIMARY KEY (`id`),
	UNIQUE INDEX `categories_url_slug_unique` (`url_slug`),
	UNIQUE INDEX `categories_url_unique` (`url`),
	INDEX `categories_src_name_index` (`src_name`),
	INDEX `categories_parent_index` (`parent`),
	INDEX `childs` (`parent`, `sort_order`, `type_id`, `src_name`, `name`)
)
COLLATE='utf8_unicode_ci'
ENGINE=InnoDB;


CREATE TABLE `fias_categories_fias` (
	`id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
	`category_id` INT(10) UNSIGNED NOT NULL,
	`fias_aoid` CHAR(36) NOT NULL COMMENT 'Уникальный идентификатор записи',
	`fias_aoguid` VARCHAR(36) NOT NULL COMMENT 'Глобальный уникальный идентификатор адресного объекта',
	`created_at` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00',
	`updated_at` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00',
	PRIMARY KEY (`id`),
	INDEX `category_id` (`category_id`),
	UNIQUE `fias_aoid` (`fias_aoid`),
	UNIQUE `fias_aoguid` (`fias_aoguid`)
)
COLLATE='utf8_unicode_ci'
ENGINE=InnoDB;
