SELECT COUNT(*) FROM fias_categories WHERE SUBSTRING(src_name,3,1) = ' ';#4256

#Группируем т.к. очень много
SELECT SUBSTRING(src_name,1,2) as str , COUNT(*) as cnt FROM fias_categories WHERE SUBSTRING(src_name,3,1) = ' '
GROUP BY SUBSTRING(src_name,1,2) ORDER BY cnt DESC;#172
/*
Много чисел

*/

#СТ - 1845 - #48 - Станция
SELECT COUNT(*) FROM fias_categories WHERE (SUBSTRING(src_name,1,3) = 'СТ ');#1845
SELECT DISTINCT adm_type_id FROM fias_categories WHERE (SUBSTRING(src_name,1,3) = 'СТ ');#9
SELECT adm_type_id, COUNT(*) as cnt FROM fias_categories WHERE (SUBSTRING(src_name,1,3) = 'СТ ') GROUP BY adm_type_id ORDER BY cnt DESC;#9
/*
adm_type_id	cnt
128	1378	снт Садовое неком-е товарищество
11	315		тер Территория
15	131		дп Дачный поселок
24	12			пгт Поселок городского типа
39	3			нп Населенный пункт
46	2			с	Село
23	2			массив Массив
28	1			д Деревня
42	1			п/ст Поселок и(при) станция(и)
*/

#Удаляем СТ
SELECT  src_name, SUBSTRING(src_name,4)  FROM fias_categories WHERE (SUBSTRING(src_name,1,3) = 'СТ ');#1845
UPDATE fias_categories SET src_name = SUBSTRING(src_name,4)  WHERE (SUBSTRING(src_name,1,3) = 'СТ ');#1845

SELECT src_genitive, SUBSTRING(src_genitive,4)  FROM fias_categories WHERE (SUBSTRING(src_genitive,1,3) = 'СТ ');#1845
UPDATE fias_categories SET src_genitive = SUBSTRING(src_genitive,4)  WHERE (SUBSTRING(src_genitive,1,3) = 'СТ ');#1845


#ГК 1385 #нету ГК в сокращениях
SELECT COUNT(*) FROM fias_categories WHERE (SUBSTRING(src_name,1,3) = 'ГК ');#1385
SELECT DISTINCT adm_type_id FROM fias_categories WHERE (SUBSTRING(src_name,1,3) = 'ГК ');#2
SELECT adm_type_id, COUNT(*) as cnt FROM fias_categories WHERE (SUBSTRING(src_name,1,3) = 'ГК ') GROUP BY adm_type_id ORDER BY cnt DESC;#9
/*
adm_type_id	cnt
11	1380	тер	Территория - территории вообще не должно быть? откуда? - из населенных пунктов
39	5		нп		Населенный пункт
*/

SELECT type_id, COUNT(*) as cnt FROM fias_categories WHERE (SUBSTRING(src_name,1,3) = 'ГК ') GROUP BY type_id ORDER BY cnt DESC;#9
#6	1385 - 6 - населенный пункт

SELECT src_name FROM fias_categories WHERE (SUBSTRING(src_name,1,3) = 'ГК ') AND (adm_type_id=11);
SELECT src_name FROM fias_categories WHERE (SUBSTRING(src_name,1,3) = 'ГК ') AND (adm_type_id=39);


#Удаляем ГК
SELECT  src_name, SUBSTRING(src_name,4)  FROM fias_categories WHERE (SUBSTRING(src_name,1,3) = 'ГК ');#1385
UPDATE fias_categories SET src_name = SUBSTRING(src_name,4)  WHERE (SUBSTRING(src_name,1,3) = 'ГК ');#1385

SELECT src_genitive, SUBSTRING(src_genitive,4)  FROM fias_categories WHERE (SUBSTRING(src_genitive,1,3) = 'ГК ');#1385
UPDATE fias_categories SET src_genitive = SUBSTRING(src_genitive,4)  WHERE (SUBSTRING(src_genitive,1,3) = 'ГК ');#1385


#--- №1
SELECT * FROM fias_categories WHERE (SUBSTRING(src_name,1,3) = '№1 ');#14 - как с текстом после числа так и без

#--- г.
SELECT * FROM fias_categories WHERE (SUBSTRING(src_name,1,3) = 'г. ');#2 - adm_type_id = 16 с/с - Сельсовет
/*
1177269	г. Белинский	508373
1280453	г. Каменка		1234892
*/
SELECT * FROM fias_categories WHERE id = 508373;#508373	Белинский	857087
SELECT * FROM fias_categories WHERE id = 857087;#857087	Пензенская	0
#http://www.ifias.ru/334783/458679.html - город

SELECT * FROM fias_categories WHERE id = 1234892;#1234892	Каменский	857087
SELECT * FROM fias_categories WHERE id = 857087;#857087	Пензенская	0
#http://www.ifias.ru/fias4.html?i=e5313349-e01d-47a2-b329-b1629c243520 - город

#Оба делаем городами - adm_type_id - 3
UPDATE `msl`.`fias_categories` SET `src_name`='Белинский', `src_genitive`='Белинского', `adm_type_id`=3 WHERE  `id`=1177269;
UPDATE `msl`.`fias_categories` SET `src_name`='Каменка', `src_genitive`='Каменки', `adm_type_id`=3 WHERE  `id`=1280453;

#--- д.
SELECT * FROM fias_categories WHERE (SUBSTRING(src_name,1,3) = 'д. ');#2 - adm_type_id = 11 - тер Территория
/*
39214		д. Белая гора СОТ Дубок		310155
850770	д. Чурилово СОТ Чурилово	310155
*/

SELECT * FROM fias_categories WHERE id = 310155;#310155	Новгородский	1017979
SELECT * FROM fias_categories WHERE id = 1017979;#1017979	Новгородская	0

#
SELECT id,shortname,fullname,used_genitive,not_used,use_position,`show` FROM fias_category_adm_types WHERE shortname IN ('АК','АО','АП','АС','Ай','Ар','Аю','Ая','ГМ','ГО','Гу','ДК','ДН','Ей','Ея','ЖД','ЖО','За','Иг','Иж','Ий','Ик','Ир','Ис','Ия','КК','КП','КС','Км','ЛО','МО','МП','НП','На','ОТ','ОЦ','Од','Ой','ПК','ПХ','По','СА','СБ','СК','СО','СП','СУ','ТВ','ТД','ТО','ТУ','УМ','Ук','Ур','Ут','Уя','ФХ','ЦУ','Ыб','ЭХ','Эе','Юг','Юм','Юр','Яг','Яз','Ям','Яр','Яя','зд','им','оп','ос','пл');
/*
id	shortname	fullname	used_genitive	not_used	use_position	show
1	АО	Автономный округ	автономного округа	0	2	1
14	кп	Курортный поселок	кп.	0	1	0
39	нп	Населенный пункт	нп.	0	1	0
67	км	Километр	\N	0	0	0
74	пл	Площадь	\N	1	0	0
*/

#--- AO
SELECT * FROM fias_categories WHERE (SUBSTRING(src_name,1,3) = 'АО ');#28 - АКЦИОНЕРНЫЕ ОБЩЕСТВА

#--- кп
SELECT * FROM fias_categories WHERE (SUBSTRING(src_name,1,3) = 'кп ');#33 - adm_type_id - 11 тер Территория
SELECT src_name FROM fias_categories WHERE (SUBSTRING(src_name,1,3) = 'кп ');#33 - adm_type_id - 11 тер Территория
SELECT DISTINCT parent FROM fias_categories WHERE (SUBSTRING(src_name,1,3) = 'кп ');#3
/*
parent
811122
958215
992108
*/
SELECT * FROM fias_categories WHERE id = 811122;#811122	Читинский	762731	3	9 - р-н
SELECT * FROM fias_categories WHERE id = 958215;#958215	Атнинский	752901	3	9 - р-н
SELECT * FROM fias_categories WHERE id = 992108;#992108	Арский	752901	3	9 - р-н

SELECT * FROM fias_categories WHERE id = 762731;#762731	Забайкальский	0
SELECT * FROM fias_categories WHERE id = 752901;#752901	Татарстан	0

#пока все оставляю

#--- нп
SELECT * FROM fias_categories WHERE (SUBSTRING(src_name,1,3) = 'нп ');#5
/*
adm_type_id
4 - 128	- снт Садовое неком-е товарищество
1 - 48 - ст	Станция
*/
#нп убирать
SELECT  src_name, SUBSTRING(src_name,4)  FROM fias_categories WHERE (SUBSTRING(src_name,1,3) = 'нп ');#5
UPDATE fias_categories SET src_name = SUBSTRING(src_name,4)  WHERE (SUBSTRING(src_name,1,3) = 'нп ');#5

SELECT  src_genitive, SUBSTRING(src_genitive,4)  FROM fias_categories WHERE (SUBSTRING(src_genitive,1,3) = 'нп ');#5
UPDATE fias_categories SET src_genitive = SUBSTRING(src_genitive,4)  WHERE (SUBSTRING(src_genitive,1,3) = 'нп ');#5

#--- км
SELECT * FROM fias_categories WHERE (SUBSTRING(src_name,1,3) = 'км ');#6
SELECT DISTINCT adm_type_id FROM fias_categories WHERE (SUBSTRING(src_name,1,3) = 'км ');#6
/*
4 - 24 - п Поселок
2 - 39 - нп Населенный пункт
*/
#parent = 672416
SELECT * FROM fias_categories WHERE id = 672416;#672416	Луга	1003586	3 г Город
SELECT * FROM fias_categories WHERE id = 1003586;#1003586	Лужский	1075011 9 - р-н
SELECT * FROM fias_categories WHERE id = 1075011;#1075011	Ленинградская	0

#убирайте - км
DELETE FROM fias_categories WHERE (SUBSTRING(src_name,1,3) = 'км ');#6

#--- пл
SELECT * FROM fias_categories WHERE (SUBSTRING(src_name,1,3) = 'пл ');#842348	пл Сады Массив Дунай массив 1268707
#adm_type_id - 15 - дп - Дачный поселок

SELECT * FROM fias_categories WHERE id = 1268707;#1268707	Всеволожский	1075011
SELECT * FROM fias_categories WHERE id = 1075011;#1075011	Ленинградская	0

#удаляем



#3-буквенные с маленькой
#зд, им, оп, ос

#----- зд
SELECT * FROM fias_categories WHERE (SUBSTRING(src_name,1,3) = 'зд ');#1 - снт
SELECT COUNT(*) FROM fias_categories WHERE (SUBSTRING(src_name,1,3) = 'гск ') AND (CHAR_LENGTH(src_name)>3);#290
SELECT adm_type_id, COUNT(*) FROM fias_categories WHERE (SUBSTRING(src_name,1,3) = 'гск ')  GROUP BY adm_type_id;#14

#----- им
SELECT * FROM fias_categories WHERE (SUBSTRING(src_name,1,3) = 'им ');#117
SELECT COUNT(*) FROM fias_categories WHERE (SUBSTRING(src_name,1,3) = 'гск ') AND (CHAR_LENGTH(src_name)>3);#290
SELECT adm_type_id, COUNT(*) FROM fias_categories WHERE (SUBSTRING(src_name,1,3) = 'гск ')  GROUP BY adm_type_id;#14

#фиксим все genitive т.к. получился има, а где им - все равно не правильно
UPDATE fias_categories SET src_genitive=src_name WHERE (SUBSTRING(src_name,1,3) = 'им ');#111

#----- ос
SELECT * FROM fias_categories WHERE (SUBSTRING(src_name,1,3) = 'ос ');#1
SELECT COUNT(*) FROM fias_categories WHERE (SUBSTRING(src_name,1,3) = 'гск ') AND (CHAR_LENGTH(src_name)>3);#290
SELECT adm_type_id, COUNT(*) FROM fias_categories WHERE (SUBSTRING(src_name,1,3) = 'гск ')  GROUP BY adm_type_id;#14
