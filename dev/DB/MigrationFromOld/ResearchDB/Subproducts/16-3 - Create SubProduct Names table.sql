CREATE TABLE `subproduct_names` (
	`id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
	`name` VARCHAR(255) NOT NULL COLLATE 'utf8_unicode_ci',
	`created_at` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00',
	`updated_at` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00',
	PRIMARY KEY (`id`)
);

INSERT INTO `subproduct_names` (`name`) 
SELECT DISTINCT `name` FROM subproduct_uniques;#4

SELECT DISTINCT product_type_id,name FROM subproduct_uniques;#не от типа товара зависит

#смотрим зависимость от типа подтовара
SELECT DISTINCT tpls.subproduct_type_id, uqs.name FROM
subproduct_uniques as uqs
JOIN subproduct_templates as tpls ON(tpls.id = uqs.subproduct_template_id);#тоже не зависит

#поэтому привязываем names к uniques
SELECT `names`.id as name_id, uniques.subproduct_name_id, uniques.id FROM
subproduct_uniques as uniques
JOIN `subproduct_names` as `names` ON (uniques.name = `names`.name);

UPDATE subproduct_uniques as uniques
JOIN `subproduct_names` as `names` ON (uniques.name = `names`.name)
SET uniques.subproduct_name_id = `names`.id;#89
