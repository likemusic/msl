#Топографические
#5 - Топографическая карта 500 м ххх области
#6 - Топографическая карта 250 м xxx района
UPDATE product_templates SET description = REPLACE(description,'{sub_val_1}','{sub:dla_garmin}') WHERE id IN(5,6);#2
UPDATE product_templates SET description = REPLACE(description,'{sub_val_2}','{sub:dla_digma}') WHERE id IN(5,6);#2
UPDATE product_templates SET description = REPLACE(description,'{sub_val_3}','{sub:dla_texet}') WHERE id IN(5,6);#2
UPDATE product_templates SET description = REPLACE(description,'{sub_val_4}','{sub:dla_prology}') WHERE id IN(5,6);#2
UPDATE product_templates SET description = REPLACE(description,'{sub_val_5}','{sub:dla_lexand}') WHERE id IN(5,6);#2
UPDATE product_templates SET description = REPLACE(description,'{sub_val_6}','{sub:dla_prestigio}') WHERE id IN(5,6);#2
UPDATE product_templates SET description = REPLACE(description,'{sub_val_7}','{sub:dla_pioneer}') WHERE id IN(5,6);#2

#отличаются у 5 и 6
UPDATE product_templates SET description = REPLACE(description,'{sub_val_8}','{sub:dla_explay}') WHERE id IN(5);#2
UPDATE product_templates SET description = REPLACE(description,'{sub_val_8}','{sub:dla_pechati}') WHERE id IN(6);#2

#дальше одинаковые
UPDATE product_templates SET description = REPLACE(description,'{sub_val_9}','{sub:dla_jj-connect}') WHERE id IN(5,6);#2
UPDATE product_templates SET description = REPLACE(description,'{sub_val_10}','{sub:online}') WHERE id IN(5,6);#2
UPDATE product_templates SET description = REPLACE(description,'{sub_val_11}','{sub:novaja}') WHERE id IN(5,6);#2
UPDATE product_templates SET description = REPLACE(description,'{sub_val_12}','{sub:{year}}') WHERE id IN(5,6);#2
UPDATE product_templates SET description = REPLACE(description,'{sub_val_13}','{sub:podrobnaja}') WHERE id IN(5,6);#2
UPDATE product_templates SET description = REPLACE(description,'{sub_val_14}','{sub:dla_navitel}') WHERE id IN(5,6);#2
UPDATE product_templates SET description = REPLACE(description,'{sub_val_15}','{sub:dla_pk}') WHERE id IN(5,6);#2
UPDATE product_templates SET description = REPLACE(description,'{sub_val_16}','{sub:dla_windows}') WHERE id IN(5,6);#2
UPDATE product_templates SET description = REPLACE(description,'{sub_val_17}','{sub:yandex}') WHERE id IN(5,6);#2
UPDATE product_templates SET description = REPLACE(description,'{sub_val_18}','{sub:dla_android}') WHERE id IN(5,6);#2
UPDATE product_templates SET description = REPLACE(description,'{sub_val_19}','{sub:google}') WHERE id IN(5,6);#2
UPDATE product_templates SET description = REPLACE(description,'{sub_val_20}','{sub:dla_telefona}') WHERE id IN(5,6);#2
UPDATE product_templates SET description = REPLACE(description,'{sub_val_21}','{sub:dla_smartfona}') WHERE id IN(5,6);#2
UPDATE product_templates SET description = REPLACE(description,'{sub_val_22}','{sub:dla_plansheta}') WHERE id IN(5,6);#2
UPDATE product_templates SET description = REPLACE(description,'{sub_val_23}','{sub:dla_pechati}') WHERE id IN(5,6);#2
UPDATE product_templates SET description = REPLACE(description,'{sub_val_24}','{sub:dla_symbian}') WHERE id IN(5,6);#2
UPDATE product_templates SET description = REPLACE(description,'{sub_val_25}','{sub:dla_samsung}') WHERE id IN(5,6);#2
UPDATE product_templates SET description = REPLACE(description,'{sub_val_26}','{sub:dla_nokia}') WHERE id IN(5,6);#2
UPDATE product_templates SET description = REPLACE(description,'{sub_val_27}','{sub:dla_iphone}') WHERE id IN(5,6);#2
UPDATE product_templates SET description = REPLACE(description,'{sub_val_28}','{sub:dla_ipad}') WHERE id IN(5,6);#2
UPDATE product_templates SET description = REPLACE(description,'{sub_val_29}','{sub:bymagnaja}') WHERE id IN(5,6);#2


#Cпутниковая
#9 - Спутниковая карта 20 м ХХХ района
#13 - Спутниковая карта 250 м XXX области
UPDATE product_templates SET description = REPLACE(description,'{sub_val_1}','{sub:dla_telefona}') WHERE id IN(9,13);#2
UPDATE product_templates SET description = REPLACE(description,'{sub_val_2}','{sub:dla_smartfona}') WHERE id IN(9,13);#2
UPDATE product_templates SET description = REPLACE(description,'{sub_val_3}','{sub:dla_plansheta}') WHERE id IN(9,13);#2
UPDATE product_templates SET description = REPLACE(description,'{sub_val_4}','{sub:dla_pechati}') WHERE id IN(9,13);#2
UPDATE product_templates SET description = REPLACE(description,'{sub_val_5}','{sub:dla_pk}') WHERE id IN(9,13);#2
UPDATE product_templates SET description = REPLACE(description,'{sub_val_6}','{sub:dla_symbian}') WHERE id IN(9,13);#2
UPDATE product_templates SET description = REPLACE(description,'{sub_val_7}','{sub:dla_samsung}') WHERE id IN(9,13);#2
UPDATE product_templates SET description = REPLACE(description,'{sub_val_8}','{sub:dla_nokia}') WHERE id IN(9,13);#2
UPDATE product_templates SET description = REPLACE(description,'{sub_val_9}','{sub:dla_navitel}') WHERE id IN(9,13);#2
UPDATE product_templates SET description = REPLACE(description,'{sub_val_10}','{sub:dla_iphone}') WHERE id IN(9,13);#2
UPDATE product_templates SET description = REPLACE(description,'{sub_val_11}','{sub:dla_ipad}') WHERE id IN(9,13);#2
UPDATE product_templates SET description = REPLACE(description,'{sub_val_12}','{sub:dla_garmin}') WHERE id IN(9,13);#2
UPDATE product_templates SET description = REPLACE(description,'{sub_val_13}','{sub:bymagnaja}') WHERE id IN(9,13);#2
UPDATE product_templates SET description = REPLACE(description,'{sub_val_14}','{sub:dla_navigatora}') WHERE id IN(9,13);#2
UPDATE product_templates SET description = REPLACE(description,'{sub_val_15}','{sub:dla_prology}') WHERE id IN(9,13);#2
UPDATE product_templates SET description = REPLACE(description,'{sub_val_16}','{sub:dla_digma}') WHERE id IN(9,13);#2
UPDATE product_templates SET description = REPLACE(description,'{sub_val_17}','{sub:dla_prestigio}') WHERE id IN(9,13);#2
UPDATE product_templates SET description = REPLACE(description,'{sub_val_18}','{sub:dla_pioneer}') WHERE id IN(9,13);#2
UPDATE product_templates SET description = REPLACE(description,'{sub_val_19}','{sub:dla_lexand}') WHERE id IN(9,13);#2
UPDATE product_templates SET description = REPLACE(description,'{sub_val_20}','{sub:dla_texet}') WHERE id IN(9,13);#2
UPDATE product_templates SET description = REPLACE(description,'{sub_val_21}','{sub:dla_explay}') WHERE id IN(9,13);#2
UPDATE product_templates SET description = REPLACE(description,'{sub_val_22}','{sub:dla_jj-connect}') WHERE id IN(9,13);#2
UPDATE product_templates SET description = REPLACE(description,'{sub_val_23}','{sub:yandex}') WHERE id IN(9,13);#2
UPDATE product_templates SET description = REPLACE(description,'{sub_val_24}','{sub:google}') WHERE id IN(9,13);#2
UPDATE product_templates SET description = REPLACE(description,'{sub_val_25}','{sub:novaja}') WHERE id IN(9,13);#2
UPDATE product_templates SET description = REPLACE(description,'{sub_val_26}','{sub:dla_windows}') WHERE id IN(9,13);#2
UPDATE product_templates SET description = REPLACE(description,'{sub_val_27}','{sub:online}') WHERE id IN(9,13);#2
UPDATE product_templates SET description = REPLACE(description,'{sub_val_28}','{sub:{year}}') WHERE id IN(9,13);#2
UPDATE product_templates SET description = REPLACE(description,'{sub_val_29}','{sub:podrobnaja}') WHERE id IN(9,13);#2
UPDATE product_templates SET description = REPLACE(description,'{sub_val_30}','{sub:dla_android}') WHERE id IN(9,13);#2


#GPS КАРТЫ
#3 - GPS карта ХХХ района
#10 - GPS карта XXX области

UPDATE product_templates SET description = REPLACE(description,'{sub_val_1}','{sub:dla_nokia}') WHERE id IN(3,10);#2
UPDATE product_templates SET description = REPLACE(description,'{sub_val_2}','{sub:dla_android}') WHERE id IN(3,10);#2
UPDATE product_templates SET description = REPLACE(description,'{sub_val_3}','{sub:{year}}') WHERE id IN(3,10);#2
UPDATE product_templates SET description = REPLACE(description,'{sub_val_4}','{sub:bymagnaja}') WHERE id IN(3,10);#2
UPDATE product_templates SET description = REPLACE(description,'{sub_val_5}','{sub:dla_pechati}') WHERE id IN(3,10);#2
UPDATE product_templates SET description = REPLACE(description,'{sub_val_6}','{sub:dla_garmin}') WHERE id IN(3,10);#2
UPDATE product_templates SET description = REPLACE(description,'{sub_val_7}','{sub:dla_iphone}') WHERE id IN(3,10);#2
UPDATE product_templates SET description = REPLACE(description,'{sub_val_8}','{sub:dla_ipad}') WHERE id IN(3,10);#2
UPDATE product_templates SET description = REPLACE(description,'{sub_val_9}','{sub:dla_navitel}') WHERE id IN(3,10);#2
UPDATE product_templates SET description = REPLACE(description,'{sub_val_10}','{sub:dla_samsung}') WHERE id IN(3,10);#2
UPDATE product_templates SET description = REPLACE(description,'{sub_val_11}','{sub:dla_pk}') WHERE id IN(3,10);#2
UPDATE product_templates SET description = REPLACE(description,'{sub_val_12}','{sub:dla_plansheta}') WHERE id IN(3,10);#2
UPDATE product_templates SET description = REPLACE(description,'{sub_val_13}','{sub:dla_telefona}') WHERE id IN(3,10);#2
UPDATE product_templates SET description = REPLACE(description,'{sub_val_14}','{sub:novaja}') WHERE id IN(3,10);#2
UPDATE product_templates SET description = REPLACE(description,'{sub_val_15}','{sub:podrobnaja}') WHERE id IN(3,10);#2
UPDATE product_templates SET description = REPLACE(description,'{sub_val_16}','{sub:dla_symbian}') WHERE id IN(3,10);#2
UPDATE product_templates SET description = REPLACE(description,'{sub_val_17}','{sub:dla_digma}') WHERE id IN(3,10);#2
UPDATE product_templates SET description = REPLACE(description,'{sub_val_18}','{sub:dla_explay}') WHERE id IN(3,10);#2
UPDATE product_templates SET description = REPLACE(description,'{sub_val_19}','{sub:dla_jj-connect}') WHERE id IN(3,10);#2
UPDATE product_templates SET description = REPLACE(description,'{sub_val_20}','{sub:dla_lexand}') WHERE id IN(3,10);#2
UPDATE product_templates SET description = REPLACE(description,'{sub_val_21}','{sub:dla_pioneer}') WHERE id IN(3,10);#2
UPDATE product_templates SET description = REPLACE(description,'{sub_val_22}','{sub:dla_prology}') WHERE id IN(3,10);#2
UPDATE product_templates SET description = REPLACE(description,'{sub_val_23}','{sub:dla_prestigio}') WHERE id IN(3,10);#2
UPDATE product_templates SET description = REPLACE(description,'{sub_val_24}','{sub:dla_texet}') WHERE id IN(3,10);#2
UPDATE product_templates SET description = REPLACE(description,'{sub_val_25}','{sub:dla_windows}') WHERE id IN(3,10);#2
UPDATE product_templates SET description = REPLACE(description,'{sub_val_26}','{sub:dla_navigatora}') WHERE id IN(3,10);#2
UPDATE product_templates SET description = REPLACE(description,'{sub_val_27}','{sub:yandex}') WHERE id IN(3,10);#2
UPDATE product_templates SET description = REPLACE(description,'{sub_val_28}','{sub:online}') WHERE id IN(3,10);#2
UPDATE product_templates SET description = REPLACE(description,'{sub_val_29}','{sub:google}') WHERE id IN(3,10);#2
UPDATE product_templates SET description = REPLACE(description,'{sub_val_30}','{sub:dla_nokia}') WHERE id IN(3,10);#2


