INSERT INTO `msl`.subproduct_uniques (`product_type_id`,`subproduct_template_id`,`name`,`title`,description,`pictures_dir`)

SELECT `old_templates`.`map_type` as `product_type_id`,
`new_subproduct_templates`.`id` as `subproduct_template_id`,
 `old_templates`.`name`, `old_templates`.`title`,
`old_templates`.`description`,
`old_templates`.`folder_pictures` as `pictures_dir`
FROM mapsshop_mapsshopruglownastrona.avl_products_subtpl as `old_templates`
JOIN msl.subproduct_templates as `new_subproduct_templates`
ON (`new_subproduct_templates`.url_slug = SUBSTR(old_templates.sub_title,2));#89