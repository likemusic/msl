#убираем лишние пробелы
UPDATE subproduct_uniques SET name = REPLACE(name,'  ',' ');#30-0

UPDATE subproduct_uniques SET name = REPLACE(name,'GPS','{map_type}');#29
UPDATE subproduct_uniques SET name = REPLACE(name,'Топографическая','{map_type}');#30
UPDATE subproduct_uniques SET name = REPLACE(name,'Спутниковая','{map_type}');#30

UPDATE subproduct_uniques SET name = REPLACE(name,'Nokia','{target}');#3
UPDATE subproduct_uniques SET name = REPLACE(name,'Android','{target}');#3
UPDATE subproduct_uniques SET name = REPLACE(name,'2013','{target}');#3
UPDATE subproduct_uniques SET name = REPLACE(name,'Garmin','{target}');#3
UPDATE subproduct_uniques SET name = REPLACE(name,'iPhone','{target}');#3
UPDATE subproduct_uniques SET name = REPLACE(name,'iPad','{target}');#3
UPDATE subproduct_uniques SET name = REPLACE(name,'Navitel','{target}');#3
UPDATE subproduct_uniques SET name = REPLACE(name,'Samsung','{target}');#3
UPDATE subproduct_uniques SET name = REPLACE(name,'компьютера','{target}');#3
UPDATE subproduct_uniques SET name = REPLACE(name,'печати','{target}');#3
UPDATE subproduct_uniques SET name = REPLACE(name,'планшета','{target}');#3
UPDATE subproduct_uniques SET name = REPLACE(name,'телефона','{target}');#3
UPDATE subproduct_uniques SET name = REPLACE(name,'Symbian','{target}');#3
UPDATE subproduct_uniques SET name = REPLACE(name,'навигатора','{target}');#3
UPDATE subproduct_uniques SET name = REPLACE(name,'Digma','{target}');#3
UPDATE subproduct_uniques SET name = REPLACE(name,'Explay','{target}');#3
UPDATE subproduct_uniques SET name = REPLACE(name,'JJ-connect','{target}');#3
UPDATE subproduct_uniques SET name = REPLACE(name,'Lexand','{target}');#3
UPDATE subproduct_uniques SET name = REPLACE(name,'Prestigio','{target}');#3
UPDATE subproduct_uniques SET name = REPLACE(name,'Prology','{target}');#3
UPDATE subproduct_uniques SET name = REPLACE(name,'Pioneer','{target}');#3
UPDATE subproduct_uniques SET name = REPLACE(name,'teXet','{target}');#3
UPDATE subproduct_uniques SET name = REPLACE(name,'Windows','{target}');#3
UPDATE subproduct_uniques SET name = REPLACE(name,'смартфона','{target}');#3

UPDATE subproduct_uniques SET name = REPLACE(name,'бумажная','{target}');#2
UPDATE subproduct_uniques SET name = REPLACE(name,'Бумажная','{target}');#1
UPDATE subproduct_uniques SET name = REPLACE(name,'Новая','{target}');#1
UPDATE subproduct_uniques SET name = REPLACE(name,'новая','{target}');#2
UPDATE subproduct_uniques SET name = REPLACE(name,'Подробная','{target}');#1
UPDATE subproduct_uniques SET name = REPLACE(name,'подробная','{target}');#1
UPDATE subproduct_uniques SET name = REPLACE(name,'Google','{target}');#3
UPDATE subproduct_uniques SET name = REPLACE(name,'online','{target}');#1

UPDATE subproduct_uniques SET name = REPLACE(name,'Яндекса','{target_ru}');#2
UPDATE subproduct_uniques SET name = REPLACE(name,'Яндекс','{target_ru}');#1
UPDATE subproduct_uniques SET name = REPLACE(name,'онлайн','{target_ru}');#2

SELECT DISTINCT name FROM subproduct_uniques;
