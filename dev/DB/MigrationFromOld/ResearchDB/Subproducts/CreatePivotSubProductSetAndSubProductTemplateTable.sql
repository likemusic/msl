CREATE TABLE `subproduct_sets_subproduct_templates` (
	`id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
	`subproduct_set_id` INT(10) UNSIGNED NOT NULL,
	`subproduct_template_id` INT(10) UNSIGNED NOT NULL,
	PRIMARY KEY (`id`),
	INDEX `parent_child` (`subproduct_set_id`, `subproduct_template_id`)
);
