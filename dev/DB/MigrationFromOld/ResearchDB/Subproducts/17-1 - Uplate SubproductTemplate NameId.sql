# -- Name Id
#1 => '{pname} {target}', >> 2 - {pname} {target}
UPDATE subproduct_templates SET subproduct_name_id = 2 WHERE subproduct_title_id=1;#8

#2 => '{pname} для {target}' >> 1 - {pname} для {target}
UPDATE subproduct_templates SET subproduct_name_id =1 WHERE subproduct_title_id=2;#37


#Title Id
UPDATE subproduct_titles SET title = REPLACE(title,'{map_type} карта {city} для {target}','{pname}');#19
UPDATE subproduct_titles SET title = REPLACE(title,'{map_type} карта {city} {target}','{pname}');#8
UPDATE subproduct_titles SET title = REPLACE(title,'{pname}','{sname}');#8
UPDATE subproduct_titles SET title = REPLACE(title,'{map_type} карта {city}','{pname}');#6

#SELECT DISTINCT title FROM subproduct_titles; 33 - дублей не появилось

#1 => '{pname} {target}', 		>> 24 - 	{sname} - карты {city} {target_ru} скачать
UPDATE subproduct_templates SET subproduct_title_id = 24 WHERE subproduct_title_id=1;#8

#2 => '{pname} для {target}' 	>> 1 - 	{sname} - карты {city} для {target_ru} скачать
UPDATE subproduct_templates SET subproduct_title_id =1 WHERE subproduct_title_id=2;#37

