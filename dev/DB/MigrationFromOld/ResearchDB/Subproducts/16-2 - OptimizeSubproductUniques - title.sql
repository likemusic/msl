#убираем лишние пробелы
UPDATE subproduct_uniques SET title = REPLACE(title,'  ',' ');#30-0

UPDATE subproduct_uniques SET title = REPLACE(title,'GPS','{map_type}');#29
UPDATE subproduct_uniques SET title = REPLACE(title,'Топографическая','{map_type}');#30
UPDATE subproduct_uniques SET title = REPLACE(title,'Спутниковая','{map_type}');#30
UPDATE subproduct_uniques SET title = REPLACE(title,'Векторная','{map_type}');#1

UPDATE subproduct_uniques SET title = REPLACE(title,'Nokia','{target}');#3
UPDATE subproduct_uniques SET title = REPLACE(title,'Android','{target}');#3
UPDATE subproduct_uniques SET title = REPLACE(title,'2013','{target}');#3
UPDATE subproduct_uniques SET title = REPLACE(title,'Garmin','{target}');#3
UPDATE subproduct_uniques SET title = REPLACE(title,'iPhone','{target}');#3
UPDATE subproduct_uniques SET title = REPLACE(title,'iPad','{target}');#3
UPDATE subproduct_uniques SET title = REPLACE(title,'Navitel','{target}');#3
UPDATE subproduct_uniques SET title = REPLACE(title,'Samsung','{target}');#3
UPDATE subproduct_uniques SET title = REPLACE(title,'компьютера','{target}');#3
UPDATE subproduct_uniques SET title = REPLACE(title,'печати','{target}');#3
UPDATE subproduct_uniques SET title = REPLACE(title,'планшета','{target}');#3
UPDATE subproduct_uniques SET title = REPLACE(title,'телефона','{target}');#3
UPDATE subproduct_uniques SET title = REPLACE(title,'Symbian','{target}');#3
UPDATE subproduct_uniques SET title = REPLACE(title,'навигатора','{target}');#3
UPDATE subproduct_uniques SET title = REPLACE(title,'Digma','{target}');#3
UPDATE subproduct_uniques SET title = REPLACE(title,'Explay','{target}');#3
UPDATE subproduct_uniques SET title = REPLACE(title,'JJ-connect','{target}');#3
UPDATE subproduct_uniques SET title = REPLACE(title,'Lexand','{target}');#3
UPDATE subproduct_uniques SET title = REPLACE(title,'Prestigio','{target}');#3
UPDATE subproduct_uniques SET title = REPLACE(title,'Prology','{target}');#3
UPDATE subproduct_uniques SET title = REPLACE(title,'Pioneer','{target}');#3
UPDATE subproduct_uniques SET title = REPLACE(title,'teXet','{target}');#3
UPDATE subproduct_uniques SET title = REPLACE(title,'Windows','{target}');#3
UPDATE subproduct_uniques SET title = REPLACE(title,'смартфона','{target}');#3

UPDATE subproduct_uniques SET title = REPLACE(title,'бумажная','{target}');#2
UPDATE subproduct_uniques SET title = REPLACE(title,'Бумажная','{target}');#1
UPDATE subproduct_uniques SET title = REPLACE(title,'Новая','{target}');#1
UPDATE subproduct_uniques SET title = REPLACE(title,'новая','{target}');#2
UPDATE subproduct_uniques SET title = REPLACE(title,'Подробная','{target}');#1
UPDATE subproduct_uniques SET title = REPLACE(title,'подробная','{target}');#1
UPDATE subproduct_uniques SET title = REPLACE(title,'Google','{target}');#3
UPDATE subproduct_uniques SET title = REPLACE(title,'online','{target}');#1
UPDATE subproduct_uniques SET title = REPLACE(title,'Yandex','{target}');#3

UPDATE subproduct_uniques SET title = REPLACE(title,'Гугл','{target_ru}');#3
UPDATE subproduct_uniques SET title = REPLACE(title,'Яндекса','{target_ru}');#2
UPDATE subproduct_uniques SET title = REPLACE(title,'Яндекс','{target_ru}');#1
UPDATE subproduct_uniques SET title = REPLACE(title,'онлайн','{target_ru}');#2
UPDATE subproduct_uniques SET title = REPLACE(title,'нокиа','{target_ru}');#2
UPDATE subproduct_uniques SET title = REPLACE(title,'андроид','{target_ru}');#3
UPDATE subproduct_uniques SET title = REPLACE(title,'Айпада','{target_ru}');#1
UPDATE subproduct_uniques SET title = REPLACE(title,'айпада','{target_ru}');#1
UPDATE subproduct_uniques SET title = REPLACE(title,'айпэда','{target_ru}');#1
UPDATE subproduct_uniques SET title = REPLACE(title,'Айфона','{target_ru}');#1
UPDATE subproduct_uniques SET title = REPLACE(title,'айфона','{target_ru}');#2
UPDATE subproduct_uniques SET title = REPLACE(title,'Виндовс','{target_ru}');#1
UPDATE subproduct_uniques SET title = REPLACE(title,'виндовс','{target_ru}');#1
UPDATE subproduct_uniques SET title = REPLACE(title,'Гармин','{target_ru}');#1
UPDATE subproduct_uniques SET title = REPLACE(title,'гармин','{target_ru}');#1
UPDATE subproduct_uniques SET title = REPLACE(title,'Дигма','{target_ru}');#1
UPDATE subproduct_uniques SET title = REPLACE(title,'дигма','{target_ru}');#1
UPDATE subproduct_uniques SET title = REPLACE(title,'Лександ','{target_ru}');#1
UPDATE subproduct_uniques SET title = REPLACE(title,'лександ','{target_ru}');#1
UPDATE subproduct_uniques SET title = REPLACE(title,'Навител','{target_ru}');#2
UPDATE subproduct_uniques SET title = REPLACE(title,'навител','{target_ru}');#2
UPDATE subproduct_uniques SET title = REPLACE(title,'Нокия','{target_ru}');#1
UPDATE subproduct_uniques SET title = REPLACE(title,'Пионер','{target_ru}');#2
UPDATE subproduct_uniques SET title = REPLACE(title,'пионер','{target_ru}');#2
UPDATE subproduct_uniques SET title = REPLACE(title,'Престижио','{target_ru}');#2
UPDATE subproduct_uniques SET title = REPLACE(title,'престижио','{target_ru}');#2
UPDATE subproduct_uniques SET title = REPLACE(title,'Пролоджи','{target_ru}');#2
UPDATE subproduct_uniques SET title = REPLACE(title,'пролоджи','{target_ru}');#2
UPDATE subproduct_uniques SET title = REPLACE(title,'Самсунг','{target_ru}');#2
UPDATE subproduct_uniques SET title = REPLACE(title,'самсунг','{target_ru}');#2
UPDATE subproduct_uniques SET title = REPLACE(title,'Симбиан','{target_ru}');#2
UPDATE subproduct_uniques SET title = REPLACE(title,'симбиан','{target_ru}');#2
UPDATE subproduct_uniques SET title = REPLACE(title,'Тексет','{target_ru}');#2
UPDATE subproduct_uniques SET title = REPLACE(title,'тексет','{target_ru}');#2
UPDATE subproduct_uniques SET title = REPLACE(title,'Эксплей','{target_ru}');#2
UPDATE subproduct_uniques SET title = REPLACE(title,'эксплей','{target_ru}');#2


UPDATE subproduct_uniques SET title = REPLACE(title,'векторную','{map_type_what}');#3


UPDATE subproduct_uniques SET title = REPLACE(title,'со спутника','{map_type_alt}');#30
UPDATE subproduct_uniques SET title = REPLACE(title,'топокарта','{map_type_alt}');#30

