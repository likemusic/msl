CREATE TABLE `subproduct_titles` (
	`id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
	`title` VARCHAR(255) NOT NULL COLLATE 'utf8_unicode_ci',
	`created_at` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00',
	`updated_at` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00',
	PRIMARY KEY (`id`)
);

INSERT INTO `subproduct_titles` (`title`) 
SELECT DISTINCT `title` FROM subproduct_uniques;#33


#привязываем title к uniques
SELECT `titles`.id as title_id, uniques.subproduct_title_id, uniques.id FROM
subproduct_uniques as uniques
JOIN `subproduct_titles` as `titles` ON (uniques.title = `titles`.title);

UPDATE subproduct_uniques as uniques
JOIN `subproduct_titles` as `titles` ON (uniques.title = `titles`.title)
SET uniques.subproduct_title_id = `titles`.id;#89
