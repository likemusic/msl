#-- TITLE
#map_type_genitive - только в description - пропускаем
#{map_type_alt} - работает
#{map_type_what} - ищем

SELECT * FROM subproduct_titles WHERE title LIKE '%{map_type_what}%';
/*
5	{target} {pname} - {map_type_what} карту {city} для {target} скачать
7	{sname} - {map_type_what} карту {city} для {target_ru} скачать
10	{sname} - {map_type_what} карту {city} для ПК скачать
*/
#ищим их шаблоны
#в UNIQUES
SELECT * FROM subproduct_uniques WHERE subproduct_title_id IN (5,7,10);#4
/* id - product_type_id - subproduct_template_id
6	1	2
8	1	23
9	1	22
12	1	3
*/
#product_type_id=1 - GPS

#Ищем подтовары с id = 4 - {pname} {target_ru}
SELECT * FROM subproduct_templates WHERE id IN (2,3,22,23);#0
/*
2	Бумажная
3	Для комптютера
22	Для iPad
23 Для iPhone	
*/
#вроде как должно было быть векторная

SELECT * FROM subproduct_uniques WHERE subproduct_name_id = 4;#0

/* product_type_id - subproduct_template_id
1	45	- GPS - Яндекс
1	32 - GPS - Онлайн
3	45 - Спутниковая - Яндекс
4	32 - Топографическая - Онлайн
4	45 - Топографическая - Яндекс
*/

/* Product types
1 - GPS
3 - Спутниковая
4 - Топографическая
*/

/* subproduct_template_id
45 - Яндекс
32 - Онлайн
*/

#Cмотрим в каких наборах используются шаблоны
SELECT * FROM subproduct_sets_subproduct_templates WHERE subproduct_template_id IN (45,32);#2
#subproduct_set_id = [1,1];

#Смотрим для каких типов карт используется
SELECT * FROM product_types WHERE subproduct_set_id IN (1);#3
/*
1 - GPS
3 - Спутниковая
4 - Топографическая
*/

#Смотрим на сайте для них
SELECT COUNT(*) FROM subproduct_uniques;#89
SELECT DISTINCT product_type_id,subproduct_template_id FROM subproduct_uniques;#88
#Ищем дубль
SELECT product_type_id,subproduct_template_id, COUNT(*) as cnt FROM subproduct_uniques
GROUP BY product_type_id, subproduct_template_id
ORDER BY cnt DESC;#
/*
product_type_id - subproduct_template_id
4	16 - Топографическая - Для Digma
*/

#Все ок!)