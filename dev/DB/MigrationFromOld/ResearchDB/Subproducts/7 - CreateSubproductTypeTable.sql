ALTER TABLE `subproduct_templates`
	ADD COLUMN `subproduct_type_id` INT NULL DEFAULT NULL AFTER `order`;


CREATE TABLE `subproduct_types` (
	`id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
	`name` VARCHAR(255) NOT NULL,
	`created_at` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00',
	`updated_at` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00',
	PRIMARY KEY (`id`)
);

INSERT INTO `subproduct_types` (`id`, `name`) VALUES
(1,'Телефоны'),
(2, 'ОС/Платформы телефонов'),
(3, 'GPS-навигаторы'),
(4, 'ОС GPS-навигаторов'),
(5, 'Типы компьютеров');
