#--- `name`
SELECT COUNT(DISTINCT `name`) FROM avl_categories;#91 051 - уникальных намного меньше общего кол-ва

#проверяем на пустые значения
SELECT COUNT(*) FROM avl_categories WHERE `name` IS NULL;#0 - OK
SELECT COUNT(*) FROM avl_categories WHERE `name` = '';#0 - OK
SELECT `name`, COUNT(*) as `cnt` FROM avl_categories GROUP BY `name` ORDER BY `cnt` DESC;# - OK
#дубли популярных названий
