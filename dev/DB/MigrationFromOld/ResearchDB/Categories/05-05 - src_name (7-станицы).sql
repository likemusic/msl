#7		1		станицы
#всего
SELECT `name` FROM avl_categories WHERE `type` = 7;#434

#`name`== "{src_name}"
SELECT COUNT(*) FROM avl_categories WHERE (`type` = 7) AND (`name` = `src_name`);#1-0 - OK
#`name`!= "{type} {src_name}"
SELECT COUNT(*) FROM avl_categories WHERE (`type` = 7) AND (`name` != CONCAT('станицы ',`src_name`));#1 - OK
#разница - смотрим какие вообще никуда не подходят
SELECT COUNT(*) FROM avl_categories WHERE (`type` = 7) AND (`name` != `src_name`) AND (`name` != CONCAT('станицы ',`src_name`));#0 - OK
#SELECT * FROM avl_categories WHERE (`type` = 7) AND (`name` != `src_name`) AND (`name` != CONCAT('рп ',`src_name`));#2-0 - OK
/*
id		name						src_name	parent	parent	import_state
4784	поселка Горбатовка	Горбатовка			31			NULL				(в Нижегородской области) - поселок городского типа - меняем type=3 - поселок
4788	поселка Желнино		Пустозерск			31			NULL				(в Нижегородской области) - поселок городского типа - меняем type=3 - поселок
4970	поселка Большевик		Большевик			3255		NULL				(Сусуманский район > Магаданской области) - поселок городского типа - меняем type=3 - поселок
*/

#- смотрим import_state для тех, где одинаковые
SELECT `import_state`, COUNT(*) FROM avl_categories WHERE (`type` = 7) AND (`name` != CONCAT('станицы ',`src_name`));#1(NULL) - OK

#cмотрим визуально
SELECT * FROM avl_categories WHERE (`type` = 7) AND (`name` != CONCAT('станицы ',`src_name`));#2
/*
id		name		src_name	parent	import_state
1827	Ханская	Ханская	41			NULL		(в Адыгеи) - станица - обновляем `name`
*/
SELECT * FROM avl_categories WHERE categoryID=41;#2

#обновляем для них значение `name`
