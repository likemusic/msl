SELECT `sort_order`, COUNT(*) as cnt FROM avl_categories GROUP BY sort_order ORDER BY cnt DESC;
/*
`sort_order`	`count`
10001				144353  is_russia = 1
0					561
1					1
>10001			55
*/

SELECT DISTINCT is_russia FROM avl_categories WHERE `sort_order`=10001;#1 - OK

#смотрим в разрезе
#-- is_russia = 1
SELECT `sort_order`, COUNT(*) as cnt FROM avl_categories WHERE is_russia=1 GROUP BY sort_order ORDER BY cnt DESC;
/*
10001		144353
Value		все остальные (есть дубли)
*/
#смотрим визуально отличия 10001 и 54 остальных
(SELECT * FROM avl_categories WHERE is_russia=1 AND `sort_order`=10001 LIMIT 5)
UNION ALL
(SELECT * FROM avl_categories WHERE is_russia=1 AND `sort_order`!=10001 LIMIT 5);

#визуально никаких отличий по полям не видно - считаем исторически остались изменненные sort_order

SELECT * FROM avl_categories WHERE (is_russia=1) AND (`sort_order`!=10001);#79
#у все parent=1
SELECT * FROM avl_categories WHERE `parent` = 1;#81 - у 2-х установлено стандартное значение 10001
SELECT * FROM avl_categories WHERE (`parent` = 1) AND (`sort_order`=10001);#81 - у 2-х установлено стандартное значение 10001
/*
2	Алтайского края
41	Адыгеи
*/

#-- rs_group = 2
SELECT `sort_order`, COUNT(*) as cnt FROM avl_categories WHERE rs_group=2 GROUP BY sort_order ORDER BY cnt DESC;
/*
0	536
1	1
*/
#смотрим у какой =1
SELECT * FROM avl_categories WHERE (rs_group=2) AND (sort_order=1);#OK - id = 135 - все страны мира


#-- rs_group = 3
SELECT `sort_order`, COUNT(*) as cnt FROM avl_categories WHERE rs_group=3 GROUP BY sort_order ORDER BY cnt DESC;
/*
0	25
1110	1
*/
#смотрим у какой 1110
SELECT * FROM avl_categories WHERE (rs_group=3) AND (sort_order=1110);#OK - id = 229 - Старинные карты


