#----- `type` -----#
SELECT DISTINCT `type` FROM avl_categories;#63
SELECT DISTINCT `type` FROM avl_categories WHERE `is_russia` = 0;#NULL - OK
SELECT DISTINCT `type` FROM avl_categories WHERE `is_russia` = 1;#NULL, 1...63 - ??? (почему есть NULL?);Fixed - добавил новые типы

#- разбираемся откуда в России NUll
SELECT COUNT(*) FROM avl_categories WHERE (`is_russia` = 1) AND (`type` IS NULL);#31
#изучаем визуально
SELECT * FROM avl_categories WHERE (`is_russia` = 1) AND (`type` IS NULL);#32-5-4-

#много parent = 1
#сколько всего детей у id=1
SELECT COUNT(*) FROM avl_categories WHERE parent=1;#81
#какие вообще типы есть у детей и их кол-во
SELECT `type`, COUNT(*) FROM avl_categories WHERE parent=1 GROUP BY `type`;
/*
type COUNT(*)  AO		Республики
\N	26				21 	-		ОК
14	46 			<<		<<		области	
23	9 				<<		<<		края
98	-				<<		21		респкублики
99	-				5		<<		AO (автономный округ)
*/

#смотрим какие в корне России но без типа
SELECT categoryID, name, src_name, parent, cat_h1, `type` FROM avl_categories WHERE (parent=1) AND (`type` IS NULL);#0 - OK


#-- раскуриваем оставшиеся 4 штуки не из корня
SELECT categoryID, name, src_name, parent, cat_h1,`type` FROM avl_categories WHERE (`is_russia` = 1) AND (`type` IS NULL);#32-5-4-0 - OK

/*
categoryID	name	src_name	parent	cat_h1	type
541	Пашино	Пашино	33	Пашино	\N
613	Эжва	Эжва	50	Эжвы	\N
5182	Краснобродского	Краснобродского	19	Краснобродского	\N
5310	Воронеж-45	Воронеж-45	2472	Воронежа-45	\N
*/

#--- 541	Пашино	Пашино	33	Пашино	\N - историческая часть
#---- Узнаем родителя
SELECT * FROM avl_categories WHERE categoryID = 33;#OK


#--- 613	Эжва	Эжва	50	Эжвы	\N - район города
#---- Узнаем родителя
SELECT * FROM avl_categories WHERE categoryID = 50;#OK


#--- 5182	Краснобродского	Краснобродского	19	Краснобродского	\N - городской округ
#---- Узнаем родителя
SELECT * FROM avl_categories WHERE categoryID = 19;#OK - Кемеровская область России

#--- 5310	Воронеж-45	Воронеж-45	2472	Воронежа-45	\N - в/ч или город
#---- Узнаем родителя
SELECT * FROM avl_categories WHERE categoryID = 2472;#OK - Грибановский район, parent=11
SELECT * FROM avl_categories WHERE categoryID = 11;#OK - Воронежской области

