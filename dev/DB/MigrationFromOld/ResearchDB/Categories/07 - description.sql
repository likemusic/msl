#---- `description` -----#
SELECT `description`, COUNT(*) FROM avl_categories GROUP BY `description`;#483
/*
NULL	144486
''		25
остальные со значением 481
*/

#изучаем по группам
#У всех ли в Росси NULL
SELECT `description`, COUNT(*) FROM avl_categories WHERE is_russia = 1 GROUP BY `description` ;#NULL - 144432
#rs_grpoup = 2 //Все страны
SELECT `description`, COUNT(*) FROM avl_categories WHERE rs_group=2 GROUP BY `description` ;#482
/*
NULL	54
остальные со значением 481
*/
#смотрим визуально признаки 54-х с NULL
SELECT * FROM avl_categories WHERE (rs_group=2) AND (`description` IS NULL);#54-OK
#У всех `parent` = 135 (Все страны)

#сколько всего детей у id=135
SELECT COUNT(*) FROM avl_categories WHERE `parent` = 135;#168

#смотрим визуально чем они отличаются
SELECT * FROM avl_categories WHERE rs_group=2;#482
#products_count NULL - 7, NOT NULL - 3
#поле `description` на сайте для обоих одинаковое (отличается только на {city})
#там где products_count = 7 карт действительно больше


#rs_grpoup = 3 //Старинные карты
SELECT `description`, COUNT(*) FROM avl_categories WHERE rs_group=3 GROUP BY `description` ;#2 - 26 - OK
/*
''		25
value	1 (id=229 - Старинные карты)
*/
#изучаем визуельно отличающися один товар
SELECT * FROM avl_categories WHERE rs_group=3;
#обновляем description для id=229 чтобы не путаться

SELECT `description`, COUNT(*) FROM avl_categories WHERE rs_group=2 GROUP BY `description`;
SELECT `description`, COUNT(*) as cnt FROM avl_categories WHERE rs_group=2 GROUP BY `description` ORDER BY `cnt` DESC;
#дубли у карт "Дзержинска" и "Абая"
#-- "Дзержинска"
SELECT * FROM avl_categories WHERE (rs_group=2) AND (`description` LIKE "%Дзержинска%");
/*
`parent`
87			Минская область
93			Донецкая область
OK - сверил по Википедии
*/
#SELECT * FROM avl_categories WHERE categoryID = 93;

#-- "Абая"
SELECT * FROM avl_categories WHERE (rs_group=2) AND (`description` LIKE "%Абая%");
/*
`parent`
116			Карагандинская область (parent=115) (Абайский район)
120			Южно-Казахстанская область (parent=115) - только селы - надо ли оставлять?
OK - сверил по Википедии
*/
/* parent=115 - Казахстан */
SELECT * FROM avl_categories WHERE categoryID = 115;



