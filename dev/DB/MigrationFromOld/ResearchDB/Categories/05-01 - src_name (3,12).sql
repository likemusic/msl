#по очереди фиксим каждый тип
#для поселка есть id=3 и id=12 - пытаемся понять в чем отличия
SELECT `name` FROM avl_categories WHERE `type` = 3;#592 - вроде бы все соответствуют "{type} {name}"
SELECT `name` FROM avl_categories WHERE `type` = 12;#976 - вроде бы не все соответствуют "{type} {name}"

#разбираемся c id=3
SELECT COUNT(*) FROM avl_categories WHERE (`type` = 3) AND (`name` != CONCAT('поселка ',`src_name`));#6-0-OK
SELECT * FROM avl_categories WHERE (`type` = 3) AND (`name` != CONCAT('поселка ',`src_name`));#6-0-OK
#- смотрим import_state для тех, где одинаковые
SELECT `import_state`, COUNT(*) FROM avl_categories WHERE (`type` = 3) AND (`name` != CONCAT('поселка ',`src_name`));#6 - OK
#обновляем для них значение `name`
#UPDATE avl_categories SET `name` = CONCAT('поселка ',`src_name`) WHERE (`type` = 3) AND (`name` != CONCAT('поселка ',`src_name`);

#разбираемся c id=12
SELECT COUNT(*) FROM avl_categories WHERE (`type` = 12) AND (`name` != CONCAT('поселка ',`src_name`));#110-0-OK
#SELECT * FROM avl_categories WHERE (`type` = 12) AND (`name` != CONCAT('поселка ',`src_name`));#110-0-OK
#- смотрим import_state для тех, где одинаковые
SELECT `import_state`, COUNT(*) FROM avl_categories WHERE (`type` = 12) AND (`name` != CONCAT('поселка ',`src_name`));#110-0 OK
#обновляем для них значение `name`
#UPDATE avl_categories SET `name` = CONCAT('поселка ',`src_name`) WHERE (`type` = 12) AND (`name` != CONCAT('поселка ',`src_name`);

#перепроверяем для id=3
#8		14		города
#всего
SELECT `name` FROM avl_categories WHERE `type` = 3;#593
#`name`== "{src_name}"
SELECT COUNT(*) FROM avl_categories WHERE (`type` = 3) AND (`name` = `src_name`);#0 - OK
#`name`!= "{type} {src_name}"
SELECT COUNT(*) FROM avl_categories WHERE (`type` = 3) AND (`name` != CONCAT('поселка ',`src_name`));#0 - OK

#перепроверяем для id=12
SELECT `name` FROM avl_categories WHERE `type` = 12;#976
#`name`== "{src_name}"
SELECT COUNT(*) FROM avl_categories WHERE (`type` = 12) AND (`name` = `src_name`);#0 - OK
#`name`!= "{type} {src_name}"
SELECT COUNT(*) FROM avl_categories WHERE (`type` = 12) AND (`name` != CONCAT('поселка ',`src_name`));#0 - OK

