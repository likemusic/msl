SELECT tplID, COUNT(*) as cnt FROM avl_products WHERE categoryID IN (

SELECT categoryID FROM avl_categories WHERE rs_group = 2
)
GROUP BY tplID
ORDER BY `cnt` DESC, tplID;

#используемые шаблоны с стариныых картах
SELECT p.tplID, COUNT(*) as cnt, t.*  FROM avl_products as p
JOIN avl_products_tpl as t USING (tplID)
 WHERE categoryID IN (
SELECT categoryID FROM avl_categories WHERE rs_group = 2)
GROUP BY p.tplID
ORDER BY `cnt` DESC, p.tplID;


#Все дочерние для стран мира
SELECT * FROM avl_categories as c1 WHERE c1.parent = 135;#168

#Страны мира имеющие дочерние категории
SELECT DISTINCT c1.categoryID, c1.name FROM avl_categories as c1 
JOIN avl_categories as c2
ON (c1.categoryID = c2.parent)
WHERE c1.parent = 135;#168


