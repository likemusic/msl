SELECT `level` FROM avl_categories WHERE categoryID = 229;

SELECT COUNT(DISTINCT UID) FROM avl_categories;#144995
SELECT * FROM avl_categories WHERE  cat_h1_translit = "";#144995




#добавляем поле для анализа по группам (Россия, Весь мир, Старинные карты)
ALTER TABLE avl_categories
	ADD `rs_group` INT(11) NULL DEFAULT NULL;#research group
/*
NULL - NOT SET
1 - Россия (id not in[135,229,1])
2 - Все страны мира (id=135, levels = 3)
3 - Старинные карты (id=229, level = 1)
*/


#3 - Старинные карты (id=229, level = 1)
UPDATE avl_categories SET `rs_group` = 3 WHERE (categoryID = 229);#1
UPDATE avl_categories SET `rs_group` = 3 WHERE (parent = 229);#25
#Total:26

#выносим в корень
UPDATE avl_categories SET `parent` = 0 WHERE (categoryID = 229);#1

#2 - Все страны мира (id=135, levels = 3)
UPDATE avl_categories SET `rs_group` = 2 WHERE (categoryID = 135);#1

UPDATE avl_categories t1 JOIN avl_categories t2
ON `t1`.`parent` = `t2`.`categoryID`
SET `t1`.`rs_group` = 2 
WHERE (`t2`.`rs_group` = 2) AND (`t1`.`rs_group` IS NULL);#168,106,262,0
#Total: 537#

#выносим в корень
UPDATE avl_categories SET `parent` = 0 WHERE (categoryID = 135);#1


#Россия
SELECT * FROM avl_categories WHERE (categoryID NOT IN (135, 229)) AND (`parent` = 1);#81
SELECT * FROM avl_categories WHERE (`parent` = 1);#83

UPDATE avl_categories SET `rs_group` = 1 
WHERE (categoryID NOT IN (135, 229)) AND (`parent` = 1);#81

UPDATE avl_categories t1 JOIN avl_categories t2
ON `t1`.`parent` = `t2`.`categoryID`
SET `t1`.`rs_group` = 1 
WHERE (`t2`.`rs_group` = 1) AND (`t1`.`rs_group` IS NULL);#

#главную категорию считаем Россия
UPDATE avl_categories SET `rs_group` = 1 WHERE categoryID = 1;

#чтобы не путать при анализе type == null
UPDATE avl_categories SET `type` = 100 WHERE categoryID = 1;

#чтобы не путать название
UPDATE avl_categories SET `name` = 'Глваная категория (Россия)' WHERE categoryID = 1;

#чтобы не путаться при анализе (level ==null)
UPDATE avl_categories SET `level` = 4 WHERE categoryID = 1;

#чтобы не путаться при анализе (src_name ==null)
UPDATE avl_categories SET `src_name` = 'Россия' WHERE categoryID = 1;

#выносим в корень
UPDATE avl_categories SET `parent` = 0 WHERE (categoryID = 1);#1

#добавляем недостающие `type`
#- AO - автономные области в корне type = 99
#-- Еврейской АО - id=12 
UPDATE avl_categories SET `type` = 99 WHERE categoryID = 12;#1-OK
#-- Ханты-Мансийского АО - id=77
UPDATE avl_categories SET `type` = 99 WHERE categoryID = 77;#1-OK
#-- Ямало-Ненецкого АО - id=80
UPDATE avl_categories SET `type` = 99 WHERE categoryID = 80;#1-OK
#-- Ненецкого АО - id=277
UPDATE avl_categories SET `type` = 99 WHERE categoryID = 277;#1-OK
#-- Чукотки (Чукотского автономного округа) id=79
UPDATE avl_categories SET `type` = 99 WHERE categoryID = 79;#1-OK

#- Республики - в корне type=98
#-- Адыгея - 41
UPDATE avl_categories SET `type` = 98 WHERE categoryID = 41;#1-OK
#-- Алтая - 42
UPDATE avl_categories SET `type` = 98 WHERE categoryID = 42;#1-OK
#-- Башкирии - 43
UPDATE avl_categories SET `type` = 98 WHERE categoryID = 43;#1-OK
#-- Бурятии - 44
UPDATE avl_categories SET `type` = 98 WHERE categoryID = 44;#1-OK
#-- Дагестана - 45
UPDATE avl_categories SET `type` = 98 WHERE categoryID = 45;#1-OK
#-- Ингушетии - 46
UPDATE avl_categories SET `type` = 98 WHERE categoryID = 46;#1-OK
#-- Кабардино-Балкарии - 46
UPDATE avl_categories SET `type` = 98 WHERE categoryID = 47;#1-OK
#-- Калмыкии - 48
UPDATE avl_categories SET `type` = 98 WHERE categoryID = 48;#1-OK
#-- Карелии - 49
UPDATE avl_categories SET `type` = 98 WHERE categoryID = 49;#1-OK
#-- Коми - 50
UPDATE avl_categories SET `type` = 98 WHERE categoryID = 50;#1-OK
#-- Карачаево-Черкесии - 51
UPDATE avl_categories SET `type` = 98 WHERE categoryID = 51;#1-OK
#-- Марий Эл - 52
UPDATE avl_categories SET `type` = 98 WHERE categoryID = 52;#1-OK
#-- Мордовии - 53
UPDATE avl_categories SET `type` = 98 WHERE categoryID = 53;#1-OK
#-- Северная Осетия - 54
UPDATE avl_categories SET `type` = 98 WHERE categoryID = 54;#1-OK
#-- Татарстана - 55
UPDATE avl_categories SET `type` = 98 WHERE categoryID = 55;#1-OK
#-- Тувы - 56
UPDATE avl_categories SET `type` = 98 WHERE categoryID = 56;#1-OK
#-- Удмуртии - 57
UPDATE avl_categories SET `type` = 98 WHERE categoryID = 57;#1-OK
#-- Хакасии - 58
UPDATE avl_categories SET `type` = 98 WHERE categoryID = 58;#1-OK
#-- Чечни - 59
UPDATE avl_categories SET `type` = 98 WHERE categoryID = 59;#1-OK
#-- Чувашии - 60
UPDATE avl_categories SET `type` = 98 WHERE categoryID = 60;#1-OK
#-- Саха - 61
UPDATE avl_categories SET `type` = 98 WHERE categoryID = 61;#1-OK


#- округ - type=97
#-- Агинский Бурятский АО(был) стал просто округ - 322
UPDATE avl_categories SET `type` = 97 WHERE categoryID = 322;#1-OK

#- историческия часть - 96
#-- Пашино - 541
UPDATE avl_categories SET `type` = 96 WHERE categoryID = 541;#1-OK

#- район города - 95 
#-- Эжва - 613
UPDATE avl_categories SET `type` = 95 WHERE categoryID = 613;#1-OK

#- городской округ - 94 
#-- Краснобродского - 5182
UPDATE avl_categories SET `type` = 94 WHERE categoryID = 5182;#1-OK

#- в/ч - военная часть - 93
#-- Воронеж-45 - 5310
UPDATE avl_categories SET `type` = 93 WHERE categoryID = 5310;#1-OK


#---------- `src_name` == `name`
#смотрим их уникальные типы и кол-во
SELECT `type`, COUNT(*) as `cnt` FROM avl_categories 
WHERE (`src_name` IS NOT NULL) AND (`src_name` = `name`) AND `type` NOT IN('97','98','99')
GROUP BY `type` ORDER BY `cnt` DESC;
/*
type	cnt	name
12		25		поселка (повтор id=3 ?)
8		14		города
2		6		села
3		6		поселка (повтор id=12 ?)
15		2		рп
7		1		станицы
93		1		в/ч (военной части)
94		1		городского округа
95		1		района города
96		1		исторической части
*/

#по очереди фиксим каждый тип
#12		25		поселка (повтор id=3 ?)
#3		6		поселка (повтор id=12 ?)

UPDATE avl_categories SET `name` = CONCAT('поселка ',`src_name`) WHERE (`type` = 3) AND (`name` != CONCAT('поселка ',`src_name`));#6 - OK
UPDATE avl_categories SET `name` = CONCAT('поселка ',`src_name`) WHERE (`type` = 12) AND (`name` != CONCAT('поселка ',`src_name`));#110 - OK

#8		14		города
/*
id		name						src_name	parent	import_state
4629	поселка Семендер		Семендер	45			NULL - меняем тип на поселка - `type` = 3
4859	поселка Пустозерск	Пустозерск			NULL - обновляем `name` на "города"
*/
UPDATE avl_categories SET `type` = 3 WHERE categoryID=4629;#1 - OK
UPDATE avl_categories SET `name` = CONCAT('города ',`src_name`) WHERE categoryID=4859;#1 - OK
UPDATE avl_categories SET `name` = CONCAT('города ',`src_name`) WHERE (`type` = 8) AND (`name` != CONCAT('города ',`src_name`));#14


#2		6		села
#SELECT categoryID, name, src_name, parent, `type`,`level`, rs_group, import_state FROM avl_categories WHERE (`type` = 2) AND (`name` != CONCAT('села ',`src_name`));#14
#import_state = NULL(all 6)
/*
categoryID	name					src_name	parent	type	level	rs_group
546			Тоцкое Второе		Тоцкое Второе		35		2	1	1			село
1676			Алакуртти			Алакуртти			30		2	1	1			село
1908			Султан-Янги-Юрт	Султан-Янги-Юрт	3732	2	3	1			село
1936			Чегем Второй		Чегем Второй		3707	2	3	1			село
1938			Кенже					Кенже					47		2	1	1			село
1939			Хасанья				Хасанья				47		2	1	1			село
*/
UPDATE avl_categories SET `name` = CONCAT('села ',`src_name`) WHERE (`type` = 2) AND (`name` != CONCAT('села ',`src_name`));#6


#15		2		рп
/*
id		name						src_name	parent	parent	import_state
4784	поселка Горбатовка	Горбатовка			31			NULL				(в Нижегородской области) - поселок городского типа - меняем type=3 - поселок
4788	поселка Желнино		Пустозерск			31			NULL				(в Нижегородской области) - поселок городского типа - меняем type=3 - поселок
4970	поселка Большевик		Большевик			3255		NULL				(Сусуманский район > Магаданской области) - поселок городского типа - меняем type=3 - поселок
*/

#UPDATE avl_categories SET `type` = 3 WHERE (`type` = 15) AND (`name` != `src_name`) AND (`name` != CONCAT('рп ',`src_name`));#14
UPDATE avl_categories SET `type` = 3 WHERE categoryID IN (4784, 4788, 4970);#3

#SELECT * FROM avl_categories WHERE (`type` = 15) AND (`name` != CONCAT('рп ',`src_name`));#2
/*
id		name		src_name	parent	parent	import_state

1651	Сокол		Сокол		28			NULL		(в Магаданской области) - рабочий поселок - обновляем `name`
1652	Уптар		Уптар		28			NULL		(в Магаданской области) - рабочий поселок - обновляем `name`

*/
UPDATE avl_categories SET `name` = CONCAT('рп ',`src_name`) WHERE (`type`=15) AND (`name` != CONCAT('рп ',`src_name`));#2


#7		1		станицы
#SELECT * FROM avl_categories WHERE (`type` = 7) AND (`name` != CONCAT('станицы ',`src_name`));#2
/*
id		name		src_name	parent	import_state
1827	Ханская	Ханская	41			NULL		(в Адыгеи) - станица - обновляем `name`
*/

UPDATE avl_categories SET `name`=CONCAT('станицы ',`src_name`) WHERE (`type`=7) AND (`name`!=CONCAT('станицы ',`src_name`));#1-OK

#93		1		в/ч (военной части)
#SELECT `name` FROM avl_categories WHERE `type` = 93;#1
#Воронеж-45
UPDATE avl_categories SET `name`=CONCAT('в/ч ',`src_name`) WHERE (`type`=93) AND (`name`!=CONCAT('в/ч ',`src_name`));#1-OK


#94		1		городского округа
#SELECT * FROM avl_categories WHERE `type` = 94;#1
/*
id		name						src_name	parent	parent	import_state
5182	Краснобродского		Краснобродского	19			NULL				(Кемеровской области)
*/

UPDATE avl_categories SET `type`=3, name = CONCAT('поселка ', src_name) WHERE categoryID = 5182;#1 - OK


#95		1		района города Эжва - Эжвинский район города Сыктывкара > Республики Коми
#всего
#SELECT * FROM avl_categories WHERE `type` = 95;#307-304

/*
id		name	src_name	parent	parent	import_state
613	Эжва	Эжва		50			NULL		
*/

#обновляем для них значение `name`
UPDATE avl_categories SET `name`=CONCAT('р-на ',`src_name`) WHERE categoryID=613;#1-OK


#96		1		исторической части
SELECT * FROM avl_categories WHERE `type` = 96;#307-304
/*
id		name		src_name	parent	parent	import_state
541	Пашино	Пашино	31			NULL		(Новосибирской области)  - историческая часть
*/

UPDATE avl_categories SET `name`=CONCAT('и/ч ',`src_name`) WHERE categoryID=541;#1-OK
