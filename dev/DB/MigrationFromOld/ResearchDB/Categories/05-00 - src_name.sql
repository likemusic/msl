#--- `src_name`
SELECT COUNT(DISTINCT `src_name`) FROM avl_categories;#79 030

#проверяем на пустые значения
SELECT COUNT(*) FROM avl_categories WHERE `src_name` IS NULL;#563 = count(is_russia=0)

#оцениваем визуально значение поля `name` там где не установлено
SELECT `name` FROM avl_categories WHERE `src_name` IS NULL;#некоторые с {type}, но в основном просто {name}
SELECT COUNT(*) FROM avl_categories WHERE `is_russia`=0 AND (`src_name` IS NOT NULL);#0 - OK
SELECT COUNT(*) FROM avl_categories WHERE `is_russia`=0 AND (`src_name` IS NULL);#563 - OK
SELECT `is_russia`, COUNT(*) FROM avl_categories WHERE `src_name` IS NULL GROUP BY `is_russia`;#563 - OK

#проверяем там где установлено везде ли отличается от src_name
SELECT COUNT(*) FROM avl_categories WHERE (`src_name` IS NOT NULL);#144432 - OK (вся Росссия)
SELECT COUNT(*) FROM avl_categories WHERE (`src_name` IS NOT NULL) AND (`src_name` = `name`);#85-40-33-27 ???

#смотрим в каких равны и пытаемся понять почему
SELECT * FROM avl_categories WHERE (`src_name` IS NOT NULL) AND (`src_name` = `name`);#85-27 - ???
SELECT categoryID, name,src_name, parent, `type` FROM avl_categories WHERE (`src_name` IS NOT NULL) AND (`src_name` = `name`);#85 - ???
#для вновь добавленных type - часть - республики в составе РФ - оставляем  их как есть, остальное исправляем

#смотрим без Республик и других региональных которые надо переделать (type=97,98,99)
SELECT categoryID, name,src_name, parent, `type` FROM avl_categories
WHERE (`src_name` IS NOT NULL) AND (`src_name` = `name`) AND `type` NOT IN('97','98','99');#58-18-0 - OK ???

#предполагаем зависимость от type (и возможно parent)
SELECT `type`, COUNT(*) FROM avl_categories WHERE (`src_name` IS NOT NULL) AND (`src_name` = `name`) GROUP BY `type`;#86 - ???
/*
type	COUNT(*)
97		1
98		21
99		5

*/

#смотрим их уникальные типы и кол-во
SELECT `type`, COUNT(*) as `cnt` FROM avl_categories 
WHERE (`src_name` IS NOT NULL) AND (`src_name` = `name`) AND `type` NOT IN('97','98','99')
GROUP BY `type` ORDER BY `cnt` DESC;
/*
type	cnt	name
12		25		+поселка (повтор id=3 ?)
8		14		города
2		6		села
3		6		+поселка (повтор id=12 ?)
15		2		рп
7		1		станицы
93		1		в/ч (военной части)
94		1		городского округа
95		1		района города
96		1		исторической части
*/
