#8		14		города
#всего
SELECT `name` FROM avl_categories WHERE `type` = 8;#357-356
#`name`== "{src_name}"
SELECT COUNT(*) FROM avl_categories WHERE (`type` = 8) AND (`name` = `src_name`);#14-0 - OK
#`name`!= "{type} {src_name}"
SELECT COUNT(*) FROM avl_categories WHERE (`type` = 8) AND (`name` != CONCAT('города ',`src_name`));#16-14-0 - OK
#14-16 = на 2 разница - смотрим какие вообще никуда не подходят
SELECT COUNT(*) FROM avl_categories WHERE (`type` = 8) AND (`name` != `src_name`) AND (`name` != CONCAT('города ',`src_name`));#2-0 - OK
SELECT * FROM avl_categories WHERE (`type` = 8) AND (`name` != `src_name`) AND (`name` != CONCAT('города ',`src_name`));#2-0
/*
id		name						src_name	parent	import_state
4629	поселка Семендер		Семендер	45			NULL - меняем тип на поселка - `type` = 3
4859	поселка Пустозерск	Пустозерск			NULL - обновляем `name` на "города"
*/

#cмотрим визуально
SELECT * FROM avl_categories WHERE (`type` = 8) AND (`name` != CONCAT('города ',`src_name`));#14
#- смотрим import_state для тех, где одинаковые
SELECT `import_state`, COUNT(*) FROM avl_categories WHERE (`type` = 8) AND (`name` != CONCAT('города ',`src_name`));#14(NULL) - OK
#обновляем для них значение `name`
