/*
"categoryID"	"name"
"115"	"Казахстан" - Области	Города
"130"	"Беларусь" - Области	Города
"132"	"Украина" - Области	Города
"148"	"Бельгия" - Города (и ниже)
"155"	"Греция"
"166"	"Финляндия"
"181"	"Аргентина"
"188"	"Новая Зеландия"
"193"	"Польша"
"4130"	"Малави"

*/

#Области	Города
SELECT * FROM categories WHERE parent IN(115,130,132);#45

#3 - Области
UPDATE categories SET type_id = 3 WHERE parent IN(115,130,132);#45

#4 - Города в области
SELECT * FROM categories WHERE parent IN (
	SELECT id FROM (
		SELECT id FROM categories WHERE parent IN(115,130,132)
	) as a
);#262

UPDATE categories SET type_id = 4 WHERE parent IN(
	SELECT id FROM (
		SELECT id FROM categories WHERE parent IN(115,130,132)
	) as a
);#262


#-------- Города в странах
/*
"categoryID"	"name"
"115"	"Казахстан" - Области	Города
"130"	"Беларусь" - Области	Города
"132"	"Украина" - Области	Города
"148"	"Бельгия" - Города (и ниже)
"155"	"Греция"
"166"	"Финляндия"
"181"	"Аргентина"
"188"	"Новая Зеландия"
"193"	"Польша"
"4130"	"Малави"

*/
SELECT * FROM categories WHERE parent IN (148,155,166,181,188,193,4130);#61

#4 - Город в области
UPDATE categories SET type_id = 4 WHERE parent IN(148,155,166,181,188,193,4130);#61
