SELECT cat_h1, COUNT(*) cnt FROM avl_categories GROUP BY cat_h1 ORDER BY `cnt` DESC;#есть дубли

#сравниваем с полем `name`
SELECT COUNT(*) FROM avl_categories;#144 995 - всего
SELECT COUNT(*) FROM avl_categories WHERE cat_h1 = name;#140 537 - совпадают
SELECT COUNT(*) FROM avl_categories WHERE cat_h1 != name;#4458 - не совпадают

#смотрим в разрезе
#- rs_group=1 - Россия
SELECT COUNT(*) FROM avl_categories WHERE rs_group=1;#144432
SELECT COUNT(*) FROM avl_categories WHERE (cat_h1 = name) AND (rs_group=1);#140 427 - совпадают
SELECT COUNT(*) FROM avl_categories WHERE cat_h1 != name AND (rs_group=1);#4005 - не совпадают

#- rs_group=2 - Все страны
SELECT COUNT(*) FROM avl_categories WHERE rs_group=2;#537
SELECT COUNT(*) FROM avl_categories WHERE (cat_h1 = name) AND (rs_group=2);#109 - совпадают
SELECT COUNT(*) FROM avl_categories WHERE cat_h1 != name AND (rs_group=2);#428 - не совпадают

#cмотрим совпадающие
SELECT categoryID, name, src_name, parent, cat_h1 FROM avl_categories WHERE (cat_h1 = name) AND (rs_group=2);#109 - совпадают
#cмотрим не совпадающие
SELECT categoryID, name, src_name, parent, cat_h1 FROM avl_categories WHERE (cat_h1 != name) AND (rs_group=2);#109 - совпадают
#надо править вручную

#- rs_group=3 - Все страны
SELECT COUNT(*) FROM avl_categories WHERE rs_group=3;#26
SELECT COUNT(*) FROM avl_categories WHERE (cat_h1 = name) AND (rs_group=3);#1 - совпадают
SELECT COUNT(*) FROM avl_categories WHERE cat_h1 != name AND (rs_group=3);#25 - не совпадают
#смотрим где совпадает
SELECT * FROM avl_categories WHERE (cat_h1 = name) AND (rs_group=3);#id = 229 - Старинные карты - OK


#сколько всего уникальных пар надо проверить
SELECT COUNT(DISTINCT src_name) FROM avl_categories;#79030
SELECT COUNT(DISTINCT name) FROM avl_categories;#91019
SELECT COUNT(DISTINCT cat_h1) FROM avl_categories;#91141

SELECT COUNT(DISTINCT name, src_name) FROM avl_categories;#90458 - как так?
SELECT COUNT(DISTINCT name, cat_h1) FROM avl_categories;#91313
SELECT COUNT(DISTINCT name, cat_h1, src_name) FROM avl_categories;#90752 - как так?
