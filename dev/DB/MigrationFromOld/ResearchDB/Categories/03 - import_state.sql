#----- `import_state` -----#
SELECT DISTINCT `import_state` FROM avl_categories;#8 - NULL, 1 ... 7
SELECT `import_state`, COUNT(*) as cnt FROM avl_categories GROUP BY `import_state` ORDER BY `cnt` DESC;#8 - NULL(718), 1 ... 7

#Есть ли `import_state` != NULL для is_russia=0;
SELECT COUNT(*) FROM avl_categories WHERE (is_russia=0) AND (`import_state` IS  NOT NULL);#0 - OK
#т.е. для не России ничего не импортировали

#смотрим кол-во незатронутых при импорте сторк для России
SELECT COUNT(*) FROM avl_categories WHERE (is_russia=1) AND (`import_state` IS NULL);#155 - OK
#просматриваем визуально
SELECT * FROM avl_categories WHERE (is_russia=1) AND (`import_state` IS NULL);#155 - OK
#примерно 50% name без {type}

#----- /`import_state` -----#
