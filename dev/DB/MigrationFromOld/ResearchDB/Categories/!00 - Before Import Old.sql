#----- CATEGORIES
INSERT INTO `msl`.`categories`
(
	`id`, `name`, `src_name`, `parent`, `genitive`, `genitive_translit`, `url`,
	`group_id`, `type_id`, `adm_type_id`
)
SELECT
 	`categoryID`, `name`, `src_name`, `parent`, `cat_h1`, `cat_h1_translit`, SUBSTRING(`UID`, 2),
	`rs_group`, 

#по таблице соответствия ниже
CASE `level`
	#WHEN IS NULL THEN 1 - хз как на него тут проверить
 	WHEN -1 THEN 2
 	WHEN 0 THEN 3
 	WHEN 1 THEN 4
 	WHEN 2 THEN 5
 	WHEN 3 THEN 6
END as `type_id`,
`type`
FROM `mapsshop_mapsshopruglownastrona`.`avl_categories`;
 
#доустаналвивем type_id
UPDATE `msl`.`categories` SET `type_id` = 1 WHERE `type_id` IS NULL;#563
 /*SELECT
 `categoryID`, `name`, `src_name`, `parent`, `cat_h1`, SUBSTRING(`UID`,2), SUBSTRING(`UID`,2), `cat_h1_translit`, 
CASE `level` 
	#WHEN IS NULL THEN 1
 	WHEN -1 THEN 2
 	WHEN 0 THEN 3
 	WHEN 1 THEN 4
 	WHEN 2 THEN 5
 	WHEN 3 THEN 6
END as `type_id`
 FROM `rmap_altai`.`avl_categories` LIMIT 10;*/

#старый level = новый - type_id

#В конце (после проверки и исправления всех url (слеши и другие символы)) устанавливаем url_slug = url - 
#пока нет, устанвливаем сразу равными, а потом не забываем править взаимно столбцы

#----------- CATEGORY TEMPLATES
INSERT INTO `msl`.`category_templates`
(`id`,`name`, `description`,`meta_description`,`meta_keywords`, `title`)
SELECT `id`,`name`, `description`, `meta_description`, `meta_keywords`, `title`
FROM `mapsshop_mapsshopruglownastrona`.`avl_categories_tpl`;

#используется ттолько шаблон id=7, возможно остальные удалить просто?

#---------- CATEGORY TYPES
#old `level` = new `type_id`
#смотрим все используемые типы
#SELECT DISTINCT `level` FROM `rmap_altai`.`avl_categories`;
/*
Стало - было
1 - NULL - is_russia = 0 (страны мира?)
2 - -1 - старинные карты
3 - 0 - области
4 - 1 - города в области
5 - 2 - районы
6 - 3 - города в районах
*/

#Если бы добавляли автоматом - использовали бы запрос ниже
#INSERT INTO `msl`.`category_types` (`id`);
#SELECT DISTINCT `level` FROM `rmap_altai`.`avl_categories` WHERE `level`;

#Т.к. мало типов и специфичны - добавляем вручную
INSERT INTO category_types (`id`, `name`) VALUES
	(1, 'Страны мира'),
	(2, 'Стариннные карты'),
	(3, 'Области'),
	(4, 'Города в области'),
	(5, 'Районы (в областях)'),
	(6, 'Города в районах'),
	(7, 'Корень группы');

#для всех одинаковый шаблон
UPDATE category_types SET template_id = 7;#6


#Устанавливаем url_slug
#сделать нормальным через транслитерацию
UPDATE categories SET `url_slug` = `url`;#6


#Главную категорию переименовываем в Россию
UPDATE categories SET `name` = 'Россия', genitive = 'России', genitive_translit = 'Rossii', src_name='Россия', src_genitive='России', url_slug='Rossii', url='Rossii' WHERE id=1;

#Устанавливаем для корневых `sort_order`
UPDATE categories SET `sort_order` = 1 WHERE id = 229;#Старинные
UPDATE categories SET `sort_order` = 2 WHERE id = 135;#Все страны мира
UPDATE categories SET `sort_order` = 3 WHERE id = 1;#Россия

#Устанавливаем Genitive для корневых
UPDATE categories SET `genitive` = 'старинные' WHERE id = 229;#Старинные
UPDATE categories SET `genitive` = 'всех стран мира' WHERE id = 135;#Все страны мира
UPDATE categories SET `genitive` = 'России' WHERE id = 1;#Россия
