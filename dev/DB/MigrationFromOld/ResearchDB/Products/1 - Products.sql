#Products
INSERT INTO `msl`.`product_templates`
(
	`id`, `tpl_name`, `name`, `description`, `url_slug`, `price`, `pictures_dir`, `title`, 
	`meta_keywords`, `meta_description`, `type_id`, `sort_order`
)
SELECT
`tplID`, `tpl_name`, `name`, `description`, `folder_pictures`, `Price`, `folder_pictures`, `title`,
`meta_keywords`, `meta_description`, `map_type`, `sort_order`

FROM `mapsshop_mapsshopruglownastrona`.`avl_products_tpl`;
