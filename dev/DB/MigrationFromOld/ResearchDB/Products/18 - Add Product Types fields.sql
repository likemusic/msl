ALTER TABLE `product_types`
	ADD COLUMN `genitive` VARCHAR(255) NULL AFTER `name`,
	ADD COLUMN `what` VARCHAR(255) NULL AFTER `genitive`,
	ADD COLUMN `alt` VARCHAR(255) NULL AFTER `what`;
ALTER TABLE `product_types`
	CHANGE COLUMN `what` `what` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci' AFTER `genitive`;

