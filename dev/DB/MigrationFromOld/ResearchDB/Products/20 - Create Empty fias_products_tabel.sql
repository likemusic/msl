CREATE TABLE `fias_products` (
	`id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
	`category_id` INT(10) NOT NULL,
	`name` VARCHAR(255) NOT NULL COLLATE 'utf8_unicode_ci',
	`genitive` VARCHAR(255) NOT NULL COLLATE 'utf8_unicode_ci',
	`genitive_translit` VARCHAR(255) NOT NULL COLLATE 'utf8_unicode_ci',
	`src_name` VARCHAR(255) NOT NULL COLLATE 'utf8_unicode_ci',
	`src_genitive` VARCHAR(255) NOT NULL COLLATE 'utf8_unicode_ci',
	`url_slug` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci',
	`url` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci',
	`price` DECIMAL(9,2) NOT NULL,
	`product_picture_id` INT(10) NULL DEFAULT NULL,
	`sort_order` INT(10) UNSIGNED NULL DEFAULT NULL,
	`description` MEDIUMTEXT NULL COLLATE 'utf8_unicode_ci',
	`created_at` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00',
	`updated_at` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00',
	PRIMARY KEY (`id`),
	UNIQUE INDEX `products_url_slug_unique` (`url_slug`),
	UNIQUE INDEX `products_url_unique` (`url`),
	INDEX `products_src_name_index` (`src_name`),
	INDEX `category_id` (`category_id`),
	INDEX `sel` (`category_id`, `sort_order`, `id`)
)
COLLATE='utf8_unicode_ci';