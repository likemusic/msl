SELECT DISTINCT avl_categories.`level`, avl_products.maps_sort, avl_products.tplID, avl_products.sort_order
FROM avl_categories JOIN avl_products on(avl_categories.categoryID = avl_products.categoryID)
WHERE avl_categories.is_russia = 1
ORDER BY avl_categories.`level`, avl_products.maps_sort, avl_products.tplID, avl_products.sort_order;

SELECT sort_order, COUNT(*) FROM avl_products GROUP BY sort_order;


SELECT c.`level`, pt.map_type, p.tplID, COUNT(*) cnt
FROM avl_categories c JOIN avl_products p ON(c.categoryID = p.categoryID)
JOIN avl_products_tpl pt ON(p.tplID=pt.tplID)
WHERE c.is_russia = 1
GROUP BY c.`level`, pt.map_type, p.tplID
ORDER BY c.`level`, pt.map_type, p.tplID;

SELECT c.*, pt.map_type, p.tplID, COUNT(*) cnt
FROM avl_categories c JOIN avl_products p ON(c.categoryID = p.categoryID)
JOIN avl_products_tpl pt ON(p.tplID=pt.tplID)
WHERE c.is_russia = 1
GROUP BY c.`level`, pt.map_type, p.tplID
HAVING cnt=1
ORDER BY c.`level`, pt.map_type, p.tplID;

SELECT * FROM avl_categories WHERE categoryID = 13;


SELECT CASE `level`
	#WHEN IS NULL THEN 1 - хз как на него тут проверить
 	WHEN -1 THEN 2
 	WHEN 0 THEN 3
 	WHEN 1 THEN 4
 	WHEN 2 THEN 5
 	WHEN 3 THEN 6
END as `type_id`,
tplID
FROM
(
SELECT c.`level`, p.tplID, COUNT(*) cnt
FROM avl_categories c JOIN avl_products p ON(c.categoryID = p.categoryID)
JOIN avl_products_tpl pt ON(p.tplID=pt.tplID)
WHERE c.is_russia = 1
GROUP BY c.`level`, pt.map_type, p.tplID
HAVING cnt>1
ORDER BY c.`level`, pt.map_type, p.tplID) a;
