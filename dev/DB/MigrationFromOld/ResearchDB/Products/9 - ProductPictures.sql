SELECT COUNT(*) FROM avl_product_pictures;#61 453

SELECT COUNT(*) FROM avl_product_pictures WHERE 
(filename <> thumbnail) OR (filename <> enlarged) OR (thumbnail <> enlarged);#2009

SELECT * FROM avl_product_pictures 
WHERE (filename <> thumbnail) OR (filename <> enlarged) OR (thumbnail <> enlarged);#2009

SELECT DISTINCT `typ` FROM avl_product_pictures;#0, 1;

SELECT `typ`, COUNT(*) FROM avl_product_pictures GROUP BY `typ`;
/*
"typ"	"COUNT(*)"
""		"26280"
"0"	"35170"
"1"	"3"
*/

SELECT `typ`, COUNT(*) FROM avl_product_pictures 
WHERE (filename <> thumbnail) OR (filename <> enlarged) OR (thumbnail <> enlarged)
GROUP BY `typ`;#0 - 2009

SELECT COUNT(*) FROM avl_product_pictures as pics
JOIN avl_products as prod ON(pics.productID = prod.productID)
JOIN avl_categories as cat ON(prod.categoryID = cat.categoryID)
WHERE cat.rs_group=3;#408

#картинки используемые для товаров в категории "Старинные карты"
SELECT COUNT(*) FROM avl_product_pictures as pics
JOIN avl_products as prod ON(pics.productID = prod.productID)
JOIN avl_categories as cat ON(prod.categoryID = cat.categoryID)
WHERE (cat.rs_group=3)
AND (
(filename <> thumbnail) OR (filename <> enlarged) OR (thumbnail <> enlarged)
)
;#355

SELECT pics.typ, COUNT(*) FROM avl_product_pictures as pics
JOIN avl_products as prod ON(pics.productID = prod.productID)
JOIN avl_categories as cat ON(prod.categoryID = cat.categoryID)
WHERE cat.rs_group=3
GROUP BY pics.typ;#0-407; 1-1