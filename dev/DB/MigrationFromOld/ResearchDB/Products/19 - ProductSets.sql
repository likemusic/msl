CREATE TABLE `product_sets` (
	`id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
	`name` VARCHAR(255) NOT NULL COLLATE 'utf8_unicode_ci',
	`created_at` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00',
	`updated_at` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00',
	PRIMARY KEY (`id`)
)
COLLATE='utf8_unicode_ci';

SELECT DISTINCT category_type_id FROM category_types_product_templates;#5
/*
1 - Страны мира
3 - Области
4 - Города в области (или стране)
5 - Районы (в областях)
6 - Города в районах
*/
SELECT product_template_id FROM category_types_product_templates WHERE category_type_id = 6;
SELECT * FROM product_templates WHERE id IN(1,2,23,27,30,31);
#30 Векторная карта ХХХ города (в области)
#31 Векторная карта ХХХ города (в районе)
#равны - удаляем 31
UPDATE `msl`.`product_templates` SET `tpl_name`='Векторная карта ХХХ города (в области или районе)' WHERE  `id`=30;
UPDATE category_types_product_templates SET product_template_id = 30 WHERE product_template_id = 31;#1 - OK
DELETE FROM product_templates WHERE id = 31;#1 - OK


INSERT INTO `product_sets` (`id`, `name`) VALUES
(1, 'Страна мира'),
(3, 'Область'),
(4, 'Город (в стране, области, районе)'),
(5, 'Район (в области)');#4 - OK


CREATE TABLE `product_sets_product_templates` (
	`id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
	`product_set_id` INT(10) UNSIGNED NOT NULL,
	`product_template_id` INT(10) UNSIGNED NOT NULL,
	`created_at` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00',
	`updated_at` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00',
	PRIMARY KEY (`id`),
	INDEX `product_set_id` (`product_set_id`),
	INDEX `product_template_id` (`product_template_id`)
)
COLLATE='utf8_unicode_ci';

INSERT INTO `product_sets_product_templates` (`product_set_id`,`product_template_id`) VALUES
(1,8),
(1,14),
(1,16),
(1,17),
(1,18),
(1,19),
(1,20),
(1,32),

(3,4),
(3,5),
(3,10),
(3,11),
(3,12),
(3,13),
(3,25),
(3,29),

(4,1),
(4,2),
(4,23),
(4,27),
(4,30),

(5,3),
(5,6),
(5,7),
(5,9),
(5,15),
(5,22),
(5,24),
(5,26),
(5,28);#30 - OK

#Cвязываем с типами категорий
ALTER TABLE `category_types`
	ADD COLUMN `product_set_id` INT NULL AFTER `template_id`,
	ADD INDEX `product_set_id` (`product_set_id`),
	ADD INDEX `template_id` (`template_id`);

UPDATE `msl`.`category_types` SET `product_set_id`=1 WHERE  `id`=1;
UPDATE `msl`.`category_types` SET `product_set_id`=3 WHERE  `id`=3;
UPDATE `msl`.`category_types` SET `product_set_id`=4 WHERE  `id`=4;
UPDATE `msl`.`category_types` SET `product_set_id`=5 WHERE  `id`=5;
UPDATE `msl`.`category_types` SET `product_set_id`=4 WHERE  `id`=6;


UPDATE `msl`.`fias_category_types` SET `product_set_id`=3 WHERE  `id`=1;
UPDATE `msl`.`fias_category_types` SET `product_set_id`=5 WHERE  `id`=3;
UPDATE `msl`.`fias_category_types` SET `product_set_id`=4 WHERE  `id`=4;
UPDATE `msl`.`fias_category_types` SET `product_set_id`=4 WHERE  `id`=6;
UPDATE `msl`.`fias_category_types` SET `product_set_id`=4 WHERE  `id`=90;

