CREATE TABLE `products` (
	`id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
	`category_id` INT(10) NOT NULL,

	`name` VARCHAR(255) NOT NULL,
	`genitive` VARCHAR(255) NOT NULL,
	`genitive_translit` VARCHAR(255) NOT NULL,
	`src_name` VARCHAR(255) NOT NULL,
	`src_genitive` VARCHAR(255) NOT NULL,
	
	`url_slug` VARCHAR(255) NULL DEFAULT NULL,
	`url` VARCHAR(255) NULL DEFAULT NULL,
	
	`price` DECIMAL(9,2) NOT NULL,
	
	`product_picture_id` INT(10) NULL DEFAULT NULL,

	`sort_order` INT(10) UNSIGNED NULL DEFAULT NULL,

	`description` MEDIUMTEXT NULL,

	`created_at` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00',
	`updated_at` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00',

	PRIMARY KEY (`id`),
	UNIQUE INDEX `products_url_slug_unique` (`url_slug`),
	UNIQUE INDEX `products_url_unique` (`url`),
	INDEX `products_src_name_index` (`src_name`)
);
