#Russia
INSERT INTO `msl`.`category_types_product_templates` (`category_type_id`,`product_template_id`)
SELECT CASE `level`
	#WHEN IS NULL THEN 1 - хз как на него тут проверить
 	WHEN -1 THEN 2
 	WHEN 0 THEN 3
 	WHEN 1 THEN 4
 	WHEN 2 THEN 5
 	WHEN 3 THEN 6
END as `type_id`,
tplID
FROM
(
SELECT c.`level`, p.tplID, COUNT(*) cnt
FROM `mapsshop_mapsshopruglownastrona`.`avl_categories` c JOIN `mapsshop_mapsshopruglownastrona`.avl_products p ON(c.categoryID = p.categoryID)
JOIN `mapsshop_mapsshopruglownastrona`.avl_products_tpl pt ON(p.tplID=pt.tplID)
WHERE c.is_russia = 1
GROUP BY c.`level`, pt.map_type, p.tplID
HAVING cnt>1
ORDER BY c.`level`, pt.map_type, p.tplID) a;

/**
type_id	tplID
3	10
3	25
3	12
3	13
3	4
3	5
3	11
3	29
4	2
4	27
4	1
4	23
4	30
5	3
5	10
5	26
5	7
5	9
5	12
5	13
5	15
5	22
5	4
5	5
5	6
5	11
5	24
5	28
6	2
6	27
6	1
6	23
6	31
*/