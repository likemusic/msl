SELECT DISTINCT p.tplID, p.folder_pictures 
FROM avl_categories c 
JOIN avl_products p USING(categoryID)
WHERE c.rs_group = 3;#0 - "";

SELECT p.* FROM avl_categories c 
JOIN avl_products p USING(categoryID)
WHERE c.rs_group = 3;#127


SELECT p.productID, p.categoryID, p.name, p.Price, p.default_picture,SUBSTR(p.UID, 2) 
FROM avl_categories c 
JOIN avl_products p USING(categoryID)
WHERE c.rs_group = 3;#127

SELECT * FROM avl_categories c WHERE c.rs_group = 3;#26

INSERT INTO msl.products (id,category_id,name,price,sort_order, product_picture_id,url,description)
SELECT p.productID, p.categoryID, p.name, p.Price, p.sort_order, p.default_picture, SUBSTR(p.UID, 2), p.description
FROM mapsshop_mapsshopruglownastrona.avl_categories c
JOIN mapsshop_mapsshopruglownastrona.avl_products p USING(categoryID)
WHERE c.rs_group = 3;

#Fix theme_path in descriptions
#design/user/main/images
UPDATE products SET description = REPLACE(description,'design/user/main','{theme_path}');#127
#"images/plus.gif"
UPDATE products SET description = REPLACE(description,'"images/plus.gif"','"{theme_path}/images/plus.gif"');#127


#set genitive
UPDATE products SET genitive = TRIM(REPLACE(`name`, 'Карта', ''));#127
UPDATE products SET genitive = REPLACE(`genitive`, 'карта', '');#2
UPDATE products SET genitive = REPLACE(`genitive`, '  ', ' ');#2-0

#meta_description - прописал шаблон в коде
SELECT p.productID, p.name, p.meta_description
FROM avl_categories c 
JOIN avl_products p USING(categoryID)
WHERE c.rs_group = 3;#127

#meta_keywords - оставил пустой, в коде сделал = meta_description
SELECT p.productID, p.name, p.meta_keywords, p.title
FROM avl_categories c 
JOIN avl_products p USING(categoryID)
WHERE c.rs_group = 3;#127

#title - сделал в коде = GetName()