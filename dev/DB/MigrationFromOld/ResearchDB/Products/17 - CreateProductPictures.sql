CREATE TABLE `product_pictures` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`product_id` INT(11) NOT NULL DEFAULT '0',
	`filename` VARCHAR(50) NULL DEFAULT NULL,
	`thumbnail` VARCHAR(50) NULL DEFAULT NULL,
	`enlarged` VARCHAR(50) NULL DEFAULT NULL,
	`type_id` ENUM('0','1') NOT NULL DEFAULT '0',
	PRIMARY KEY (`id`)
);


#картинки используемые для товаров в категории "Старинные карты"
SELECT pics.* FROM mapsshop_mapsshopruglownastrona.avl_product_pictures as pics
JOIN mapsshop_mapsshopruglownastrona.avl_products as prod ON(pics.productID = prod.productID)
JOIN mapsshop_mapsshopruglownastrona.avl_categories as cat ON(prod.categoryID = cat.categoryID)
WHERE (cat.rs_group=3);#408

#картинки используемые для товаров в категории "Старинные карты" с отличающимися
SELECT pics.* FROM mapsshop_mapsshopruglownastrona.avl_product_pictures as pics
JOIN mapsshop_mapsshopruglownastrona.avl_products as prod ON(pics.productID = prod.productID)
JOIN mapsshop_mapsshopruglownastrona.avl_categories as cat ON(prod.categoryID = cat.categoryID)
WHERE (cat.rs_group=3)
AND ((filename <> thumbnail) OR (filename <> enlarged) OR (thumbnail <> enlarged));#355


INSERT INTO msl.product_pictures (id,product_id,filename,thumbnail,enlarged,type_id)
SELECT pics.* FROM mapsshop_mapsshopruglownastrona.avl_product_pictures as pics
JOIN mapsshop_mapsshopruglownastrona.avl_products as prod ON(pics.productID = prod.productID)
JOIN mapsshop_mapsshopruglownastrona.avl_categories as cat ON(prod.categoryID = cat.categoryID)
WHERE (cat.rs_group=3);#408

