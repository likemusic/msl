<?php

 function rrmdir($dir) { 
   if (is_dir($dir)) { 
     $objects = scandir($dir); 
     foreach ($objects as $object) { 
       if ($object != "." && $object != "..") { 
         if (is_dir($dir."/".$object))
           rrmdir($dir."/".$object);
         else
           unlink($dir."/".$object); 
       } 
     }
     rmdir($dir); 
   } 
 }

class DomainBuilder
{
    const FRAMEWORK_PART_CONFIG = 1;
    const FRAMEWORK_PART_STORAGE = 2;
    const FRAMEWORK_PART_VIEWS = 3;
    const FRAMEWORK_PART_COMPILED = 4;

    const DOMAIN_TYPE_LOCALS = 1;
    const DOMAIN_TYPE_TESTS = 2;
    const DOMAIN_TYPE_REALS =3;

    const OUT_OPERATION_CHECK = 1;
    const OUT_OPERATION_MAKE = 2;

    const OPERATION_COPY_RECURSIVE = 1;
    const OPERATION_SYMLINK = 2;

    const REQ_TYPE_DIRECTORY_NOT_LINK = 1;
    const REQ_TYPE_DIRECTORY_LINK = 1;


    //FrameworkPart > DomainType > Operation
    protected $Logic = [
        self::FRAMEWORK_PART_CONFIG => [
            self::DOMAIN_TYPE_LOCALS    => self::REQ_TYPE_DIRECTORY_NOT_LINK,
            self::DOMAIN_TYPE_TESTS     => self::REQ_TYPE_DIRECTORY_LINK,
            self::DOMAIN_TYPE_REALS     => self::REQ_TYPE_DIRECTORY_LINK
        ],
        self::FRAMEWORK_PART_STORAGE    => self::REQ_TYPE_DIRECTORY_NOT_LINK,
        self::FRAMEWORK_PART_VIEWS      => self::REQ_TYPE_DIRECTORY_LINK,
    ];


    protected $TestDomainMask =  '{$LocalName}.fias.myihor.ru';
    protected $LocalNamePlaceholder = '{$LocalName}';

    protected $BaseDir = '..';
    protected $DomainConfigTemplateDir = 'default';

    protected $Paths = [
        'config' =>  'config',
        'storage' => 'storage',
        'views' => 'resources/views/'
    ];

    protected $Domains;
    public function __construct($Domains)
    {
        $this->Domains = $Domains;
    }

    /*public function CheckConfigAll()
    {
        // config/{domain}
        //локальные сайты - реальные папки
        //реальные сайт и поддомены - симлинки на локальные папки

        if (!$this->ConfigCheckLocals()) return;
        if (!$this->ConfigCheckTestDomains()) return;
        if (!$this->ConfigCheckRealDomains()) return;
    }*/

    public function CheckConfigLocals()
    {
        //return $this->IterateDomainsWithOperation(self::FRAMEWORK_PART_CONFIG,self::DOMAIN_TYPE_LOCALS, self::OUT_OPERATION_CHECK);

        $ConfigPath = $this->GetConfigPath();
        chdir($ConfigPath);
        echo "CheckConfigLocals: Start...<br />\n";

        foreach (array_keys($this->Domains) as $local) {
            if (!is_dir($local)) {
                echo 'BAD: local is not dir: ' . $local . '<br />';
            }
            echo "OK: $local is dir <br />\n";
        }
        echo "CheckConfigLocals: Stop.<br />\n";
    }

    /*
    protected function DoOperation($FrameworkPart, $DomainType, $Operation)
    {

    }

    protected function IterateDomainsWithOperation($FrameworkPart, $DomainType  $OperationType, ,$BaseDir, $TargetDir)
    {
        $WorkDir = $this->GetWorkDir()
    }*/

    function ConfigCheckTestDomains()
    {
        $ConfigPath = $this->GetConfigPath();
        chdir($ConfigPath);
        echo "ConfigCheckTestDomains: Start...<br />\n";

        foreach (array_keys($this->Domains) as $local) {
            $TestDomain = $this->GetTestDomainName($local);
            if (!is_link($TestDomain)) {
                echo "BAD: TestDomain ({$TestDomain}) is not link (for local='{$local}').<br />\n";
            }
            else echo "OK: {$TestDomain} is link.<br />\n";
        }
        echo "ConfigCheckTestDomains: Stop.<br />\n";
    }

    function ConfigCheckRealDomains()
    {
        $ConfigPath = $this->GetConfigPath();
        chdir($ConfigPath);
        echo "ConfigCheckRealDomains: Start...<br />\n";

        foreach ($this->Domains as $local=>$RealDomain) {
            if(!$RealDomain) continue;
            if (!is_link($RealDomain)) {
                echo "BAD: RealDomain ({$RealDomain}) is not link (for local='{$local}').<br />\n";
            }
            else
            {
                echo "OK: {$RealDomain} is link.<br />\n";
            }
        }
        echo "ConfigCheckRealDomains: Stop.<br />\n";
    }

    function ConfigMakeTestDomains()
    {
        $ConfigPath = $this->GetConfigPath();
        chdir($ConfigPath);
        echo "ConfigMakeTestDomains: Start...<br />\n";

        foreach (array_keys($this->Domains) as $local) {
            $RealDomain = $this->GetTestDomainName($local);
            if (is_link($RealDomain)) {
                echo "OK: Skip. {$RealDomain} is link.<br />\n";
            }
            else
            {
                echo "OK: Make. '{$RealDomain}' symlink for '{$local}'.<br />\n";
                $LocalTartget = $ConfigPath.DIRECTORY_SEPARATOR.$local;
                $DomainLink = $ConfigPath.DIRECTORY_SEPARATOR.$RealDomain;
                symlink($LocalTartget,$DomainLink);
            }
        }
        echo "ConfigMakeTestDomains: Stop.<br />\n";
    }

    function ConfigMakeRealDomains()
    {
        $ConfigPath = $this->GetConfigPath();
        chdir($ConfigPath);
        echo "ConfigMakeRealDomains: Start...<br />\n";

        foreach ($this->Domains as $local=>$RealDomain) {
            if(!$RealDomain) continue;
            if (is_link($RealDomain)) {
                echo "OK: Skip. {$RealDomain} is link.<br />\n";
            }
            else
            {
                echo "OK: Make. '{$RealDomain}' symlink for '{$local}'.<br />\n";
                $LocalTartget = $ConfigPath.DIRECTORY_SEPARATOR.$local;
                $DomainLink = $ConfigPath.DIRECTORY_SEPARATOR.$RealDomain;
                symlink($LocalTartget,$DomainLink);
            }
        }
        echo "ConfigMakeRealDomains: Stop.<br />\n";
    }

    public function StorageCheckLocals()
    {
        $StoragePath = $this->GetStoragePath();
        chdir($StoragePath);
        echo "StorageCheckLocals: Start...<br />\n";

        foreach (array_keys($this->Domains) as $local) {
            if (!is_dir($local)) {
                echo 'BAD: local is not dir: ' . $local . '<br />';
            }
            echo "OK: $local is dir <br />\n";
        }
        echo "StorageCheckLocals: Stop.<br />\n";
    }

    public function ViewsCheckLocals()
    {
        $WorkPath = $this->GetViewsPath();
        chdir($WorkPath);
        echo "ViewsCheckLocals: Start...<br />\n";

        foreach (array_keys($this->Domains) as $local) {
            $IsDir = is_dir($local);
            $IsLink = is_link($local);
            if (!$IsDir or !$IsLink) echo "BAD: {$local} IsDir:{$IsDir}, IsLink:{$IsLink} <br />\n";
            else echo "OK: $local is dir and link.<br />\n";
        }
        echo "ViewsCheckLocals: Stop.<br />\n";
    }

    public function ViewsMakeAll()
    {
        $WorkPath = $this->GetViewsPath();
        chdir($WorkPath);
        echo "ViewsMakeAll: Start...<br />\n";
        $Target = $this->DomainConfigTemplateDir;

        foreach ($this->Domains as $local=>$RealDomain) {
            $TestDomain = $this->GetTestDomainName($local);
            $Links = [$local,$TestDomain];
            if($RealDomain) $Links[]=$RealDomain;
            foreach($Links as $Link)
            {
                if(is_link($Link)) continue;
                $this->MakeLinkIfNotExists($Target,$Link);
            }
        }
        echo "ViewsMakeAll: Stop.<br />\n";
    }

    protected function MakeLinkIfNotExists($Target,$Link)
    {
	
        symlink($Target,$Link);
    }

    function StorageCheckTests()
    {
        $StoragePath = $this->GetStoragePath();
        chdir($StoragePath);
        echo "ConfigCheckTests: Start...<br />\n";

        foreach (array_keys($this->Domains) as $local) {
            $TestDomain = $this->GetTestDomainName($local);
            if (!is_dir($TestDomain)) {
                echo "BAD: TestDomain ({$TestDomain}) is not dir (for local='{$local}').<br />\n";
            }
            else echo "OK: {$TestDomain} is dir.<br />\n";
        }
        echo "ConfigCheckTests: Stop.<br />\n";
    }

    function StorageCheckReals()
    {
        $StoragePath = $this->GetStoragePath();
        chdir($StoragePath);
        echo "StorageCheckReals: Start...<br />\n";

        foreach ($this->Domains as $local=>$RealDomain) {
            if(!$RealDomain) continue;
            if (!is_dir($RealDomain)) {
                echo "BAD: RealDomain ({$RealDomain}) is not dir (for local='{$local}').<br />\n";
            }
            else
            {
                echo "OK: {$RealDomain} is dir.<br />\n";
            }
        }
        echo "StorageCheckReals: Stop.<br />\n";
    }

    function StorageMakeReals()
    {
        $StoragePath = $this->GetStoragePath();
        chdir($StoragePath);
        echo "StorageMakeReals: Start...<br />\n";

        $StorageTemplateDir = $StoragePath.DIRECTORY_SEPARATOR.$this->DomainConfigTemplateDir;

        foreach ($this->Domains as $local=>$RealDomain) {
            if(!$RealDomain) continue;

//            if (is_dir($RealDomain)) echo "OK: {$RealDomain} is dir.<br />\n";
//            else echo "OK: Copy to '{$RealDomain}' for '{$local} from default'.<br />\n";
//            $RealDomainDir = $StoragePath.DIRECTORY_SEPARATOR.$RealDomain;
//            $this->CopyRecursive($StorageTemplateDir,$RealDomainDir);

	rrmdir($RealDomain);
	echo "OK: Copy to '{$RealDomain}' for '{$local} from default'.<br />\n";
            $RealDomainDir = $StoragePath.DIRECTORY_SEPARATOR.$RealDomain;
            $this->CopyRecursive($StorageTemplateDir,$RealDomainDir);

        }
        echo "StorageMakeReals: Stop.<br />\n";
    }

    protected function GetBasePath()
    {
        $ret = realpath($this->BaseDir);
        return $ret;
    }

    protected function GetConfigPath()
    {
        $ConfigPath = $this->GetBasePath().DIRECTORY_SEPARATOR.$this->Paths['config'];
        return $ConfigPath;
    }

    protected function GetStoragePath()
    {
        $ConfigPath = $this->GetBasePath().DIRECTORY_SEPARATOR.$this->Paths['storage'];
        return $ConfigPath;
    }

    protected function GetViewsPath()
    {
        $ConfigPath = $this->GetBasePath().DIRECTORY_SEPARATOR.$this->Paths['views'];
        return $ConfigPath;
    }

    protected function CopyRecursive($source, $dest)
    {
        if (!file_exists($dest)) mkdir($dest, 0755);
        foreach (
            $iterator = new \RecursiveIteratorIterator(
                new \RecursiveDirectoryIterator($source, \RecursiveDirectoryIterator::SKIP_DOTS),
                \RecursiveIteratorIterator::SELF_FIRST) as $item
        ) {
            if ($item->isDir()) {
                mkdir($dest . DIRECTORY_SEPARATOR . $iterator->getSubPathName());
            } else {
                copy($item, $dest . DIRECTORY_SEPARATOR . $iterator->getSubPathName());
            }
        }
    }

    protected function GetTestDomainName($LocalName)
    {
        $ret = str_replace($this->LocalNamePlaceholder,$LocalName,$this->TestDomainMask);
        return $ret;
    }
}

$dirs = [
    'adygea' => null,
    'altai' => 'altaimaps.ru',
    'altajski' => null,
    'amur' => 'amurmaps.ru',
    'archangelsk' => 'archangelskmaps.ru',
    'astrahan' => 'astrahanmaps.ru',
    'bajkonur' => null,
    'bashkiria' => 'bashkiriamaps.ru',
    'belgorod' => 'belgorodmaps.ru',
    'briansk' => 'brianskmaps.ru',
    'buriat' => 'buriatmaps.ru',
    'chechnja' => null,
    'chelyabinsk' => 'chelyabinskmaps.ru',
    'chukotka' => null,
    'chuvashija' => null,
    'degestan' => 'dagestanmaps.ru',
    'evenkijskij' => null,
    'evrejskaja' => null,
    'habarovskij' => null,
    'hakasija' => null,
    'hantymansijskij' => null,
    'ingushetija' => 'ingushmaps.ru',
    'irkutskaja' => 'irkutskmaps.ru',
    'ivanovskaja' => 'ivanovomaps.ru',
    'kabardino' => null,
    'kaliningradskaja' => 'kaliningradmaps.ru',
    'kalmykija' => 'kalmykmaps.ru',
    'kaluga' => 'kalugamaps.ru',
    'kamchatskij' => 'kamchatkamaps.ru',
    'karachaevo' => null,
    'karelia' => 'kareliamaps.ru',
    'kemerovskaja' => null,
    'kirovskaja' => null,
    'komi' => 'komimaps.ru',
    'komipermjatskij' => null,
    'korjakskij' => null,
    'kostromskaja' => 'kostromamaps.ru',
    'krasnodar' => 'krasnodarmaps.ru',
    'krasnoiarsk' => 'krasnoiarskmaps.ru',
    'kurganskaja' => 'kurganmaps.ru',
    'kurskaja' => 'kurskmaps.ru',
    'leningrad' => 'leningradmaps.ru',
    'lipeckaja' => 'lipeckmaps.ru',
    'magadanskaja' => 'magadanmaps.ru',
    'marijel' => 'marijelmaps.ru',
    'mordovia' => null,
    'moskva' => 'moskvamaps.ru',
    'murmanskaja' => 'murmanskmaps.ru',
    'neneckij' => null,
    'nizhegorodskaja' => null,
    'novgorodskaja' => 'novgorodmaps.ru',
    'novosibirskaja' => 'novosibirskmaps.ru',
    'omskaja' => 'omskmaps.ru',
    'orenburgskaja' => null,
    'orlovskaja' => 'orelmaps.ru',
    'penzenskaja' => 'penzamaps.ru',
    'permskij' => 'permmaps.ru',
    'piter' => null,
    'primorskij' => 'primorskmaps.ru',
    'pskovskaja' => null,
    'rjazanskaja' => null,
    'rostovskaja' => null,
    'saha' => 'sahagps.ru',
    'sahalinskaja' => null,
    'samarskaja' => null,
    'saratovskaja' => null,
    'severnajaosetija' => null,
    'smolenskaja' => null,
    'stavropolskij' => null,
    'sverdlovskaja' => null,
    'tajmyrskij' => null,
    'tambovskaja' => null,
    'tatarstan' => 'tatarstanmaps.ru',
    'tomskaja' => null,
    'tulskaja' => null,
    'tuva' => null,
    'tverskaja' => null,
    'tyumen' => 'tyumenmaps.ru',
    'udmurtija' => null,
    'uljanovskaja' => null,
    'vladimirskaja' => null,
    'volgograd' => 'volgogradmaps.ru',
    'vologodskaja' => 'vologodmaps.ru',
    'voronezhskaja' => 'voronezmaps.ru',
    'yamal' => 'yamalmaps.ru',
    'yaroslavskaja' => null,
    'zabaikalsk' => 'zabaikalskmaps.ru'
];

$DomainBuilder = new Deployer($dirs);

//-- Config
//$DomainBuilder->CheckConfigLocals();
//$DomainBuilder->ConfigCheckTestDomains();
//$DomainBuilder->ConfigCheckRealDomains();
//$DomainBuilder->ConfigMakeTestDomains();
//$DomainBuilder->ConfigMakeRealDomains();

//-- Storage
//$DomainBuilder->StorageCheckLocals();
//$DomainBuilder->StorageCheckTests();
//$DomainBuilder->StorageCheckReals();
$DomainBuilder->StorageMakeReals();

//-- resources/views
//$DomainBuilder->ViewsCheckLocals();
//$DomainBuilder->ViewsCheckTests();
//$DomainBuilder->ViewsCheckReals();
//$DomainBuilder->ViewsMakeReals();
//$DomainBuilder->ViewsMakeAll();


/* bootstrap\cache\
    $domain = '.fias.myihor.ru';
    foreach($dirs as $dir)
    {
        $Symlink = $dir.$domain;
        if(!file_exists($Symlink)) symlink($dir,$Symlink);
    }
*/

