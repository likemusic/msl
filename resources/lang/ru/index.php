<?php

return [
    //common
    'REQUIRES_FIELDS' => '* - Поля обязательные для заполнения',
    'PHONE' => 'Телефон',
    'NAME' => 'Имя',
    'EMAIL' => 'E-mail',
    'BUTTON_SEND' => 'Отправить',
    'BUTTON_OK' => 'OK',
    'BUTTON_CONTINUE' => 'Продолжить',
    'WRITE_US_EMAIL' => 'Также Вы можете написать нам просто на наш e-mail:',
    'BUY' => 'Купить',
    'SEARCH_NOT_FOUND' => 'ничего не найдено.',

    // index
    'REQUST_CALLBACK'   => 'Заказать звонок',
    'WRITE_EMAIL'       => 'Написать на e-mail',
    'MAP'               => 'Карта',
    'MAPS'              => 'Карты',

    //category
    'CATEGORY_PRODUCT_IMG_WARNING'  => '',
    'EDIT_CATEGORY'                 => 'Редактировать категорию',
    'MAPS_FOR_CITIES'               => 'Карты городов',
    'MAPS_FOR_REGION'               => 'Карты районов',
    'MAPS_FOR_COUNTRIES'            => 'Карты всех стран мира',
    'MAPS_DESCRIPTION'              => 'Описание карт',

    //breadcums
    'MAIN_PAGE'                     => 'Главная',

    //products
    'OTHER_PAYMENT_METHODS'         => 'Другие способы оплаты',
    'IMAGE_TEXT_WARNING'            => 'Карта, которую вы получите, будет такого же качества и детализации.',

    //feedback
    'FEEDBACK_MESSAGE'  => 'Вы можете отправить нам сообщение с помощью формы обратной связи:',
    'ERR_WRONG_POST'    => 'Во время отправки произошла ошибка! Попробуйте еще раз...',
    'FEEDBACK_CUSTOMER_MESSAGE_TEXT' => 'Текст сообщения',
    'FEEDBACK_SENT_SUCCESSFULLY' => '<B>Сообщение успешно отправлено.</B><br>Мы ответим Вам в ближайшее время. Спасибо за Ваш запрос!',

    //callback
    'CALLBACK_MESSAGE' => 'Вы можете заказать обратный звонок с помощью формы раcположенной ниже:',
    'CALLBACK_CUSTOMER_MESSAGE_TEXT' => 'Дополнительная информация',
    'CALLBACK_SENT_SUCCESSFULLY' => '<B>Сообщение успешно отправлено.</B><br>Мы ответим Вам в ближайшее время. Спасибо за Ваш запрос!',
    'CALLBACK_EMAIL_SUBJECT' => 'Запрос обратного звонка'
];