<?php namespace App\Helpers\CacheNamer;


use App\Models\PageEntity\StoreEntity\Category\Part\ICategoryPart;

interface ICacheNamer
{
    function GetCategoryPartToCategoryType(ICategoryPart $CategoryPart);
    function CategoryPartInternalChilds(ICategoryPart $CategoryPart, $url);
    function TGetByUrl($Entity,$url);
    function RootByUrlSlug($url);
    function ProductTemplateByProductSetIdAndProductTypeIdAndScale($ProductSetId, $TypeId, $Scale);
    function ProductTypeByUrlSlug($UrlSlugPart);
    function SubProductSetById($id);
    function SubProductTemplateBySearchString($id, $SearchString);

    function SiteCounters();
}