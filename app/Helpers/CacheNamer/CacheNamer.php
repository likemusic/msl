<?php namespace App\Helpers\CacheNamer;


use App\Models\PageEntity\StoreEntity\Category\Part\ICategoryPart;

class CacheNamer implements ICacheNamer
{
    function SubProductTemplateBySearchString($id, $SearchString)
    {
        $CacheId = "SubProductTemplateBySearchString/{$id}/{$SearchString}";
        return $CacheId;
    }

    function SubProductSetById($id)
    {
        $CacheId = "SubProductSet/{id}";
        return $CacheId;
    }

    function ProductTypeByUrlSlug($UrlSlugPart)
    {
        $CacheId = "ProductType/{$UrlSlugPart}";
        return $CacheId;
    }

    function GetCategoryPartToCategoryType(ICategoryPart $CategoryPart)
    {
        $CacheId = 'CategoryPart/Type/'.$CategoryPart->id;
        return $CacheId;
    }

    function CategoryPartInternalChilds(ICategoryPart $CategoryPart, $url)
    {
        $CacheId = "CategoryPart/InternalChilds/{$CategoryPart->id}/{$url}";
        return $CacheId;
    }

    function TGetByUrl($Entity,$url)
    {
        $ClassName = get_class($Entity);

        $CacheId = "{$ClassName}/TGetByUrl/{$url}";
        return $CacheId;
    }

    function RootByUrlSlug($url)
    {
        $CacheId = "CategoryPart/RootByUrlSlug/{$url}";
        return $CacheId;
    }

    function ProductTemplateByProductSetIdAndProductTypeIdAndScale($ProductSetId, $TypeId, $Scale)
    {
        $CacheId = "ProductTemplate/{$ProductSetId}/{$TypeId}/{$Scale}";
        return $CacheId;
    }

    function SiteCounters()
    {
        $CacheId = 'SiteCounters';
        return $CacheId;
    }

}