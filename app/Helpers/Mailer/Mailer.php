<?php namespace App\Helpers\Mailer;

use App\Models\Order\IOrder;
use \Illuminate\Contracts\Mail\Mailer as LaravelMailer;
use Illuminate\Support\Facades\Mail;

class Mailer implements IMailer{
    /**
     * @var LaravelMailer
     */
    protected $mailer;

    function __construct(LaravelMailer $mailer)
    {
        $this->mailer = $mailer;
    }

    function NewPaidOrder(IOrder $order)
    {
        $user = $order->GetUser();

        $CustomerEmail = $user->GetEmail();
        $CustomerName = $user->GetName();
        $ProductName = $order->GetName();

        $Message = "<strong>Название товара:</strong> $ProductName<br />";
        $Subject = 'Спасибо за покупку карты!';

        $Email = [
            'to' => [
                'email' => $CustomerEmail,
                'name'  => $CustomerName
            ],
            'subject' => $Subject,
            'message' => $Message
        ];

        $this->SendEmail($Email);
    }

    function NewCallbackRequest($CallbackRequest)
    {
        //TODO: add view for email
        $Message = trans('index.PHONE').":".$CallbackRequest['phone']."\n".trans('index.NAME').':'.$CallbackRequest['name']."\n".$CallbackRequest['message_text'];
        Mail::raw($Message, function ($message) {
            $message->to(config('site.DOMAIN_EMAIL'), config('site.DOMAIN_EMAIL_NAME'));
            $message->subject(trans('index.CALLBACK_EMAIL_SUBJECT'));
        });
        //TODO: доделать
    }

    function NewFeedback($Feedback)
    {
        //TODO: add view for email
        $Message = trans('index.PHONE').":".$Feedback['phone']."\n".$Feedback['message_text'];
        Mail::raw($Message, function ($message)  use ($Feedback) {
            $message->from($Feedback['email'], $name = null);
            $message->to(config('site.DOMAIN_EMAIL'), config('site.DOMAIN_EMAIL_NAME'));
        });
        //TODO: доделать
    }

    /**
     * @param $email
     */
    protected function SendEmail($email)
    {

    }
} 