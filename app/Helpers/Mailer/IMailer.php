<?php namespace App\Helpers\Mailer;

use App\Models\Order\IOrder;

interface IMailer {

    function NewPaidOrder(IOrder $IOrder);

    function NewCallbackRequest($CallbackRequest);

    function NewFeedback($Feedback);
}