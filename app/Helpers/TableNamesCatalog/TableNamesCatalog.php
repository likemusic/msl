<?php namespace App\Helpers\TableNamesCatalog;

use App\Helpers\TablesNamer\ITablesNamer;

class TableNamesCatalog implements ITableNamesCatalog{

    /**
     * @var ITablesNamer
     */
    protected $tablesNamer;

    function __construct(ITablesNamer $tablesNamer)
    {
        $this->tablesNamer = $tablesNamer;
    }

    function GetCategoryPartsTableName()
    {
        return config('site.tables.category.part');
    }

    function GetCategoryTypesTableName()
    {
        return config('site.tables.category.type');
    }

    function GetCategoryAdmTypesTableName()
    {
        return config('site.tables.category.adm_type');
    }

    function GetCategoryTemplatesTableName()
    {
        return config('site.tables.category.template');
    }

    function GetProductPartsTableName()
    {
        return config('site.tables.product.independent');
    }

    function GetProductPicturesTableName()
    {
        return 'product_pictures';
    }

    function GetProductTemplatesTableName()
    {
        return config('site.tables.product.template');
    }

    function GetProductTypesTableName()
    {
        return config('site.tables.product.types');
    }

    function GetSubProductTemplatesTableName()
    {
        return config('site.tables.subproduct.template');
    }

    function GetSubProductUniquesTableName()
    {
        return config('site.tables.subproduct.description');
    }

    function GetSubProductSetsTableName()
    {
        return config('site.tables.subproduct.set');
    }

    function GetSubProductTypesTableName()
    {
        return 'subproduct_types';
    }

    function GetSubProductTitlesTableName()
    {
        return 'subproduct_titles';
    }

    function GetPivotCategoryTemplatesAndProductTemplatesTableName()
    {
        $ParentTableName = $this->GetCategoryTemplatesTableName();
        $ChildTableName = $this->GetProductTemplatesTableName();
        $ret = $this->tablesNamer->GetLinkTableName($ParentTableName, $ChildTableName);
        return $ret;
    }

    function GetPivotProductTemplatesAndSubProductTemplatesTableName()
    {
        $ParentTableName = $this->GetProductTemplatesTableName();
        $ChildTableName = $this->GetSubProductTemplatesTableName();
        $ret = $this->tablesNamer->GetLinkTableName($ParentTableName, $ChildTableName);
        return $ret;
    }

} 