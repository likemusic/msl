<?php namespace App\Helpers\TableNamesCatalog;

interface ITableNamesCatalog {
    //todo fix names (s)
    function GetCategoryPartsTableName();
    function GetCategoryTypesTableName();
    function GetCategoryAdmTypesTableName();
    function GetCategoryTemplatesTableName();

    function GetProductPartsTableName();
    function GetProductPicturesTableName();
    function GetProductTemplatesTableName();
    function GetProductTypesTableName();

    function GetSubProductSetsTableName();
    function GetSubProductTemplatesTableName();
    function GetSubProductTypesTableName();
    function GetSubProductTitlesTableName();
    function GetSubProductUniquesTableName();

    function GetPivotCategoryTemplatesAndProductTemplatesTableName();
    function GetPivotProductTemplatesAndSubProductTemplatesTableName();
}