<?php namespace App\Helpers\PaymentGateway\W1;

use App\Helpers\PaymentGateway\Base\BuyButton\IRequestData;

class RequestData implements IRequestData{

    /**
     * @var string
     */
    protected $ProductName;

    /**
     * @var string|float|int
     */
    protected $Price;

    /**
     * @var string|int
     */
    protected $InvId;

    protected $RequestMethod = 'get';

    /**
     * @var string
     */
    protected $Url;

    /**
     * @var array
     */
    protected $HiddenFields;

    function __construct($Price, $Name, $InvId=0 )
    {
        $this->ProductName = $Name;
        $this->Price = $Price;
        $this->InvId = $InvId;

        $config = config('w1');
        $this->RequestMethod = $config['method'];

        $this->SetUrl();

        $hiddens = [
            'WMI_MERCHANT_ID'           => $config['login'],
            'WMI_PAYMENT_AMOUNT'        => $Price,
            'WMI_CURRENCY_ID'           => $config['currency_id'],
            'WMI_DESCRIPTION'           => $Name,
            'description'               => $Name,//TODO: нужен для js-кода, пофиксить и убрать
            'WMI_PTDISABLED'            => $config['disabled_currencies'],
            'WMI_SUCCESS_URL'           => $config['success_url'],
            'WMI_FAIL_URL'              => $config['fail_url'],
            'WMI_CUSTOMER_EMAIL'        => '',//TODO: fix it
        ];

        $this->HiddenFields = $hiddens;
    }

    function GetUrl()
    {
        return $this->Url;
    }

    function GetMethod()
    {
        return $this->RequestMethod;
    }

    function GetParams()
    {
        return $this->HiddenFields;
    }

    function GetAll()
    {
        $ret = [
            'url'       => $this->GetUrl(),
            'method'    => $this->GetMethod(),
            'params'    => $this->GetParams()
        ];
        return $ret;
    }

    protected function SetUrl()
    {
        $this->Url = config('sys.w1.url');
    }
} 