<?php namespace App\Helpers\PaymentGateway\Base\RequestHelper\In\PrePayNotify;


interface IPrePayNotifyInRequestPaymentHelper {
    function IsValid();//Получены ли все необходимые парамертры
    function GetData();//Получить массив полученных данных в конечном виде

    function Accept();//Разрешить совершение платежа
    function Reject();//Отменить проведение платежа
} 