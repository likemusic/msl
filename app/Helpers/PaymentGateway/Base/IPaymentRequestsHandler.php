<?php namespace App\Helpers\PaymentGateway\Base;

interface IPaymentRequestsHandler {

    function OnPrePayNotify();

    function OnSuccess();

    function OnFail();
} 