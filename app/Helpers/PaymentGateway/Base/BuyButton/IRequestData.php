<?php namespace App\Helpers\PaymentGateway\Base\BuyButton;

interface IRequestData {

    function GetUrl();

    function GetMethod();

    function GetParams();
} 