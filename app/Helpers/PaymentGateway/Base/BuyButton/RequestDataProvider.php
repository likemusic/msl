<?php namespace App\Helpers\PaymentGateway\Base\BuyButton;

abstract class RequestDataProvider implements IRequestDataProvider{

    protected $RequestDataClassName;

    function GetData($Price, $Name, $InvId = 0)
    {
        $DataProvider = new $this->RequestDataClassName($Price, $Name, $InvId);
        $Data = $DataProvider->GetAll();
        return $Data;
    }
}