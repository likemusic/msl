<?php namespace App\Helpers\PaymentGateway\Base\BuyButton;

interface IRequestDataProvider {
    /**
     * @param $Price
     * @param string $Name
     * @param int|string $InvId
     * @return array
     */
    function GetData($Price, $Name, $InvId = 0);
} 