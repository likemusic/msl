<?php namespace App\Helpers\PaymentGateway\Base\Proxy;

use Illuminate\Http\Request;

interface IProxyServer {

    function GetRedirectUrl(Request $request);
} 