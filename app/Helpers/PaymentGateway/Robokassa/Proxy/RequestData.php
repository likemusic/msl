<?php namespace App\Helpers\PaymentGateway\Robokassa\Proxy;


use App\Helpers\PaymentGateway\Base\BuyButton\IRequestData;
use App\Helpers\PaymentGateway\Robokassa\RequestData as BaseFormDataProvider;

class RequestData extends BaseFormDataProvider implements IRequestData
{
    protected function SetUrl()
    {
        $ProxyConfig = config('robokassa-proxy-client');
        $rk_useproxy = $ProxyConfig['use-proxy'];
        $rk_proxyurl = $ProxyConfig['proxy-url'];
        if($rk_useproxy) $ServerUrl = $rk_proxyurl;
        else $ServerUrl = config('sys.robokassa.url');

        $this->Url = $ServerUrl;
    }
}