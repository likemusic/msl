<?php namespace App\Helpers\PaymentGateway\Robokassa\Proxy;

use App\Helpers\PaymentGateway\Base\Proxy\IProxyServer;
use Illuminate\Http\Request;

class ProxyServer implements IProxyServer {

    function GetRedirectUrl(Request $request)
    {
        $RedirectUrl = config('sys.robokassa.url');
        $QueryString = $request->server('QUERY_STRING');
        $FullRedirectUrl = $RedirectUrl.'?'.$QueryString;
        return $FullRedirectUrl;
    }
} 