<?php namespace App\Helpers\PaymentGateway\Robokassa;

use App\Helpers\PaymentGateway\Base\BuyButton\IRequestDataProvider;
use \App\Helpers\PaymentGateway\Base\BuyButton\RequestDataProvider as BaseRequestDataProvider;

class RequestDataProvider extends BaseRequestDataProvider implements IRequestDataProvider{
    protected $RequestDataClassName = RequestData::class;
}