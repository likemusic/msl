<?php namespace App\Helpers\PicturesFinder;


use Illuminate\Support\Facades\Cache;

class PicturesFinder implements IPicturesFinder{
    protected $ImagesFilePattern = '*.{jpg,JPG,jpeg,JPEG,png,PNG,gif,GIF}';

    protected $UseCache = false;
    protected $Cache = [];

    public function __construct()
    {
        $ConfigCacheKey = 'cache.components.PicturesFinder.GetAllImages';
        $this->UseCache = config($ConfigCacheKey);
    }

    function GetPictures($PicturesDir, $Count)
    {
        $Files = $this->GetAllImages($PicturesDir);
        $ret = $this->GetRandom($Files, $Count);
        return $ret;
    }

    protected function GetRandom($Files, $Count)
    {
        $Keys = array_rand($Files,$Count);
        $ret = [];
        foreach($Keys as $Key)
        {
            $ret []= $Files[$Key];
        }
        return $ret;
    }

    protected function GetAllImages($PicturesDir)
    {
        if(!array_key_exists($PicturesDir,$this->Cache))
        {
            $Images = $this->UseCache ? $this->GetAllImagesCached($PicturesDir) : $this->GetAllImagesReal($PicturesDir);
            $this->Cache[$PicturesDir] = $Images;
        }
        return $this->Cache[$PicturesDir];
    }

    protected function GetAllImagesReal($PicturesDir)
    {
        $CurrentDir = getcwd();
        chdir($PicturesDir);
        $Files = glob($this->ImagesFilePattern, GLOB_NOSORT | GLOB_BRACE);
        chdir($CurrentDir);
        return $Files;
    }

    protected function GetAllImagesCached($PicturesDir)
    {
        $CacheId = 'PicturesFinder/GetAllImages/'.$PicturesDir;
        $ret = Cache::rememberForever($CacheId, function() use($PicturesDir){ return $this->GetAllImagesReal($PicturesDir);});
        return $ret;
    }
}