<?php namespace App\Helpers\PicturesFinder;


interface IPicturesFinder {
    function GetPictures($Dir, $Count);
} 