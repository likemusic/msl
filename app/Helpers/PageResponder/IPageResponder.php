<?php namespace App\Helpers\PageResponder;

use App\Models\PageEntity\IPageEntity;

interface IPageResponder {
    function GetPageResponse(IPageEntity $page, $url=null);
} 