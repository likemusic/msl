<?php namespace App\Helpers\PageResponder;

use App\Helpers\CacheNamer\ICacheNamer;
use App\Helpers\StoreTreeBuilder\IStoreTreeBuilder;
use App\Models\PageEntity\IPageEntity;
use igaster\laravelTheme\Facades\Theme;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Request;

class PageResponder implements IPageResponder
{

    /**
     * @var IStoreTreeBuilder
     */
    protected $IStoreTreeBuilder;

    /**
     * @var ICacheNamer
     */
    protected $ICacheNamer;

    public function __construct(IStoreTreeBuilder $IStoreTreeBuilder, ICacheNamer $ICacheNamer)
    {
        $this->IStoreTreeBuilder = $IStoreTreeBuilder;
        $this->ICacheNamer = $ICacheNamer;
    }

    public function GetPageResponse(IPageEntity $page, $url = null)
    {
        $PageContent = $this->GetPageContent($page, $url);

        //TODO: доделать
        //Должен возвращать Responce или View или String
        return $PageContent;
    }

    protected function GetPageContent(IPageEntity $page, $url)
    {
        $TemplateName = $this->PageTypeToTemplateName($page);
        $Content = $this->EvaluateTemplate($TemplateName, $page, $url);
        return $Content;
    }

    protected function PageTypeToTemplateName(IPageEntity $IPageEntity)
    {
        $templateBindings = config('templates.bindings');
        foreach ($templateBindings as $interfaceName => $templateName) {
            if (is_subclass_of($IPageEntity, $interfaceName)) {
                return $templateName;
            }
        }

        throw new \Exception('Unknown PageEntity type');
    }

    protected function EvaluateTemplate($TemplateName, IPageEntity $IPageEntity, $url)
    {
        $PageEntityTemplateValues = $IPageEntity->GetValues();

        if (isset($PageEntityTemplateValues['breadcrumbs'])) array_pop($PageEntityTemplateValues['breadcrumbs']);//TODO: переделать
        $PageEntityTemplateValues['store_tree'] = $this->IStoreTreeBuilder->GetStoreTree($IPageEntity);
        $PageEntityTemplateValues['url'] = '/' . $url;//TODO: проверить используется ли, если нет - удалить
        //при запросе через ajax отдаем только данные IPageEntity в формате JSON
        if (Request::ajax()) return response()->json($PageEntityTemplateValues);

        //если не ajax - отдаем декорированные данные
        $MainTemplateName = config('templates.main');

        //обязательные переменные
        $TplValues = [
            'main_content_template' => $TemplateName,
            'CategoryTreeTpl' => 'test.tpl.html',//TODO: исправить

            'theme_path' => asset_path(),
            'main_menu' => trans('main_menu'),

            //TODO: доделать
            //должны возварщать модели страниц
            'page_h1' => '',
            'page_title' => '',
            'meta_description' => '',
            'meta_keywords' => '',
        ];

        $TplValues = $TplValues + (array)Theme::config('vars'); //добавляем переменные текущей темы
        $TplValues = $TplValues + config('site');//добавляем переменные сайта

        $TplValues['counters'] = $this->GetSiteCounters();//добавляем счетчики

        $TplValues = $TplValues + trans($MainTemplateName);//добавляем строки шаблона

        $TplValues = $PageEntityTemplateValues + $TplValues;//добавляем переменные вычисленной страницы

        $Content = view($MainTemplateName, $TplValues);
        return $Content;
    }

    protected function GetSiteCounters()
    {
        $CacheId = $this->ICacheNamer->SiteCounters();
        $ret = Cache::rememberForever($CacheId, function () {
            return $this->GetSiteCountersReal();
        });
        return $ret;
    }

    protected function GetSiteCountersReal()
    {
        $Counters = config('sys.counters');
        $ret = [];
        foreach ($Counters as $CounterName => $CounterFilename) {
            $CounterIdName = $this->GetCounterIdName($CounterName);
            $CounterId = config("site.{$CounterIdName}");
            $CounterCode = $this->GetCounterCode($CounterName, $CounterFilename, $CounterId);
            $ret[$CounterName] = $CounterCode;
        }
        return $ret;
    }

    protected function GetCounterIdName($CounterName)
    {
        $CounterIdName = "{$CounterName}_ID";
        return $CounterIdName;
    }

    protected function GetCounterCode($CounterName, $CounterFilename, $CounterId)
    {
        $DefaultCounfigPath = app()->configPath();
        $CounterCodeFilename = $DefaultCounfigPath . DIRECTORY_SEPARATOR . 'sys' . DIRECTORY_SEPARATOR . $CounterFilename;
        $CounterCode = file_get_contents($CounterCodeFilename);
        $CounterIdName = $this->GetCounterIdName($CounterName);
        $CounterCode = str_replace('{' . $CounterIdName . '}', $CounterId, $CounterCode);
        return $CounterCode;
    }

}