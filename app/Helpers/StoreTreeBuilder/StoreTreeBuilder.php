<?php namespace App\Helpers\StoreTreeBuilder;

use App\Models\PageEntity\IPageEntity;
use App\Models\PageEntity\StoreEntity\Base\IStoreEntity;
use App\Models\PageEntity\StoreEntity\Category\ICategory;
use App\Models\PageEntity\StoreEntity\Category\ICategoryRepository;
use App\Models\PageEntity\StoreEntity\GlobalStoreEntityRepository;
use App\Models\PageEntity\StoreEntity\Product\IProduct;
use App\Models\PageEntity\StoreEntity\Product\IProductRepository;
use App\Models\PageEntity\StoreEntity\SubProduct\ISubProduct;
use App\Models\PageEntity\StoreEntity\SubProduct\ISubProductRepository;

class StoreTreeBuilder implements IStoreTreeBuilder{

    /**
     * @var GlobalStoreEntityRepository
     */
    protected $globalStoreEntityRepository;

    /**
     * @var ICategoryRepository
     */
    protected $ICategoryRepository;

    /**
     * @var IProductRepository
     */
    protected $IProductRepository;

    /**
     * @var ISubProductRepository
     */
    protected $ISubproductRepository;


    /**
     * @var IStoreEntity
     */
    protected $SelectedStoreEntity;

    //TODO: replace injected ICategoryPartRepository with ICalculatedCategory(Repository) or IParent(Repository
    function __construct(ICategoryRepository $calculatedCategoryRepository,
        IProductRepository $calculatedProductRepository,
        ISubProductRepository $calculatedSubProductRepository)
    {
        $this->ICategoryRepository = $calculatedCategoryRepository;
        $this->IProductRepository = $calculatedProductRepository;
        $this->ISubproductRepository = $calculatedSubProductRepository;
    }


    /* Tree Item Format:['url' => $url, 'name' => $name, 'childs' => null|[]x ]
     *
     */
    function GetStoreTree(IPageEntity $storeEntity = null)
    {
        if(! $storeEntity instanceof IStoreEntity) $storeEntity = null;

        //Строим дерево снизу вверх
        //по умолчанию показываем только первый уровень вложенности для выделенного элемента
        $this->SelectedStoreEntity = $storeEntity;

        //расскрываем заданную категорию если из дерева ничего не выбрано
        $Config = config('site.category_tree');
        if($Config['auto-expand-on-main'] and !$this->SelectedStoreEntity)
        {
            $CategoryId = $Config['auto-expand-on-main'];
            $AutoExpandCategory = $this->ICategoryRepository->GetById($CategoryId);
            $storeEntity = $AutoExpandCategory;
        }

        $Childs = $this->GetChildsTree($storeEntity);
        $StoreTree = $this->GetParentStoreTreeItemRecursively($storeEntity, $Childs);
        //TODO: use breadcrumbs or $ParentStoreEntity???
        $this->SelectedStoreEntity = null;
        return $StoreTree;
    }

    /**
     * @param IStoreEntity $ParentStoreEntity
     * @param IStoreEntity $CurrentStoreEntity
     * @param null $CurrentStoreTreeChilds
     * @return array
     * @throws \Exception
     *
     * Возвращает дочернее дерево для выбранного элемента
     */
    protected function GetChildsTree(IStoreEntity $ParentStoreEntity=null, IStoreEntity $CurrentStoreEntity=null, $CurrentStoreTreeChilds=null)
    {
        $Childs = [];
        $Config = config('site.category_tree');
        //самый верхний уровень
        if(!$ParentStoreEntity)
        {
            if($Config['always_show_root_elements'])
            {
                $Childs = $this->ICategoryRepository->GetRootUrlNameList();
            }

            //рассрываем заданную категорию если из дерева ничего не выбрано
            /*if($Config['auto-expand-on-main'] and !$this->SelectedStoreEntity)
            {
                $CategoryId = $Config['auto-expand-on-main'];
                $AutoExpandCategory = $this->ICategoryRepository->GetById($CategoryId);
                $SubChilds = $this->GetChildsTree(null,$AutoExpandCategory);
                    //$this->ICategoryRepository->GetChildUrlNameListByEntity($AutoExpandCategory);
                $AutoExpandedUrlName = $this->StoreEntityToUrlNameItem($AutoExpandCategory);

                $Childs = $this->MergeListItem($Childs, $AutoExpandedUrlName, $SubChilds);
            }*/
        }
        elseif($ParentStoreEntity instanceof ICategory)
        {
            /** @var ICategory $Category */
            $Category = $ParentStoreEntity;
            //создаем дерево для выбранной категории
            if($ParentStoreEntity == $this->SelectedStoreEntity)
            {
                //для выбранной категории дочерние категории показываем всегда
                //$ChildsCategory = $Category->GetChildCategoriesUrlNameList();
                $ChildsCategory = $Category->GetInternalChildsUrlNameList();
                $Childs = $ChildsCategory;
                //дочерние товары показываем только если задано в конфиге
                if($Config['show_products'])
                {
                    //$ChildsProducts = $Category->GetChildProductsUrlNameList();
                    $ChildsProducts = $Category->GetExternalChildsUrlNameList();
                    $Childs = array_merge($ChildsCategory, $ChildsProducts);
                }
            }
            //текущая категория не является выбранной (родительская)
            else
            {
                if(!$Config['hide-siblings-for-parents']
                    or (($CurrentStoreEntity == $this->SelectedStoreEntity) and (!$CurrentStoreTreeChilds)))
                {
                    //категории показываем всегда
                    //$ChildsCategory = $Category->GetChildCategoriesUrlNameList();
                    $ChildsCategory = $Category->GetInternalChildsUrlNameList();
                    $Childs = $ChildsCategory;
                    //дочерние товары показываем только если задано в конфиге
                    if($Config['show_products'])
                    {
                        //$ChildsProducts = $Category->GetChildProductsUrlNameList();
                        $ChildsProducts = $Category->GetInternalChildsUrlNameList();
                        $Childs = array_merge($ChildsCategory, $ChildsProducts);
                    }
                }
            }
        }
        //у товаров дочерние только подтовары
        //показываем только если задано в конфиге
        elseif($ParentStoreEntity instanceof IProduct)
        {
            if( $Config['show_subproducts'])
            {
                /** @var IProduct $Product */
                $Product = $ParentStoreEntity;
                $Childs = $Product->GetChildUrlNameList();
            }
        }
        //у подтоваров нету дочерних
        elseif(! $ParentStoreEntity instanceof ISubProduct) throw new \Exception("Invalid ParentStoreEntity type!");

        //Вставляем текущий и его дочерний к текущему списку
        //и при необходимости поднимаем его вверх списка

        if($CurrentStoreEntity and (
            (
                $CurrentStoreEntity instanceof ICategory)
                or (($CurrentStoreEntity instanceof IProduct) and $Config['show_products'])
                or(($CurrentStoreEntity instanceof ISubProduct) and $Config['show_subproducts'])
            )
        )
        {
            $CurrentUrlNameItem = $this->StoreEntityToUrlNameItem($CurrentStoreEntity);

            $MoveToTop = false;
            if(($this->SelectedStoreEntity == $CurrentStoreEntity) and $Config['selected_move_to_top'])
            {
                $SelectedIsRoot = ($CurrentStoreEntity->GetParent()) ? false : true;
                if( ! ($SelectedIsRoot and $Config['dont_move_to_top_selected_roots']))
                {
                    $MoveToTop = true;
                }
            }

            //Вставляем текущий
            $Childs = $this->MergeListItem($Childs, $CurrentUrlNameItem, $CurrentStoreTreeChilds, $MoveToTop);
        }
        return $Childs;
    }

    protected function GetParentStoreTreeItemRecursively(IStoreEntity $storeEntity=null, $ChildStoreTree=null)
    {
        //получаем родительский элемент
        //$ParentEntity = $this->globalStoreEntityRepository->GetParent($ParentStoreEntity);
        $ParentEntity = $storeEntity->GetParent();

        //получаем для него элементы дерева
        $ParentEntityStoreTreeItems = $this->GetChildsTree($ParentEntity, $storeEntity, $ChildStoreTree);

        if($ParentEntity) $ParentEntityStoreTreeItems = $this->GetParentStoreTreeItemRecursively($ParentEntity, $ParentEntityStoreTreeItems);
        return $ParentEntityStoreTreeItems;
    }

    protected function MergeListItem($List, $Item, $ItemChilds, $MoveToTop = false)
    {
        $ToTopItem = null;
        //соседних нету, добавляем в массив текущий элемент
        if(!$List)
        {
            $Item['childs'] = $ItemChilds;
            $ret = [ $Item ];
            return $ret;
        }

        //List Exists
        foreach($List as $key => &$ListItem)
        {
            //Сверяем отдельные ключи, т.к. на данный момент может еще содержатся поле 'type_id'
            if(($ListItem['url'] == $Item['url']) and ($ListItem['genitive'] == $Item['genitive']))
            {
                $ListItem['childs'] = $ItemChilds;
                if($MoveToTop)
                {
                    $ToTopItem = $ListItem;
                    unset($List[$key]);
                }
                break;
            }
        }
        if($MoveToTop and $ToTopItem)
        {
            array_unshift($List, $ToTopItem);
        }
        return $List;
    }

    protected function StoreEntityToUrlNameItem(IStoreEntity $storeEntity)
    {
        $ret = array(
            'url' => $storeEntity->GetUrl(),
            'genitive' => $storeEntity->GetGenitive()
        );
        return $ret;
    }
}