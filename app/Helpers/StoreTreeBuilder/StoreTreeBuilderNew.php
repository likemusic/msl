<?php namespace App\Helpers\StoreTreeBuilder;

use App\Models\PageEntity\IPageEntity;
use App\Models\PageEntity\StoreEntity\Base\IStoreEntity;
use App\Models\PageEntity\StoreEntity\Category\ICategory;
use App\Models\PageEntity\StoreEntity\Category\ICategoryRepository;
use App\Models\PageEntity\StoreEntity\Product\IProduct;
use App\Models\PageEntity\StoreEntity\Product\IProductRepository;
use App\Models\PageEntity\StoreEntity\SubProduct\ISubProduct;
use App\Models\PageEntity\StoreEntity\SubProduct\ISubProductRepository;
use Illuminate\Support\Facades\Cache;

class StoreTreeBuilderNew implements IStoreTreeBuilder{

    //Значения из страниц магазина, необходимые для создание ссылки на страницу
    protected $UrlNameListReqValues = ['url', 'menu_tree_name'];

    protected $Config;

    /**
     * @var IStoreEntity
     */
    protected $SelectedStoreEntity;

    protected $GroupByType;
    protected $AlwaysShowRoots;

    function __construct(ICategoryRepository $calculatedCategoryRepository,
                         IProductRepository $calculatedProductRepository,
                         ISubProductRepository $calculatedSubProductRepository)
    {
        $this->ICategoryRepository = $calculatedCategoryRepository;
        $this->IProductRepository = $calculatedProductRepository;
        $this->ISubproductRepository = $calculatedSubProductRepository;
        $this->Config = config('site.category_tree');
        $this->GroupByType = $this->Config['group_by_type'];
        $this->AlwaysShowRoots = $this->Config['always_show_root_elements'];
    }


    function GetStoreTree(IPageEntity $IPageEntity)
    {
        //Если выбрана не страница магазина
        if ((!$IPageEntity) or (!$IPageEntity instanceof IStoreEntity)) $IPageEntity = null;
        $this->SelectedStoreEntity = $IPageEntity;
        $StoreTree = $this->GetStoreTreeItemRecursivelyUp(true, $IPageEntity);
        return $StoreTree;
    }


    /**
     * @param $IsSelected
     * @param IPageEntity $CurrentStoreEntity - элемент для которого возвращается представление в дереве.
     * @param IStoreEntity $ChildStoreEntity - элемент, который ищем среди дочерних для $CurrentStoreEntity, значение которого меняем на $ChildTreeEntity
     * @param array $ChildTreeEntity - значение на которое меняем представлене $ChildStoreEntity в дереве.
     * @param bool $HaveChilds - указывает что в реальном дереве сущностей, есть дочерние элементы.
     * Необходимо для выделения родительских элементов для выбранного,
     * если сам выбранный элемент и его родители (товары, подтовары) не отображаются в дереве.
     * Иными словам - флаг указывающий на то что это рекурсивный вызов ф-ции.
     * @return array
     */
    protected function GetStoreTreeItemRecursivelyUp($IsSelected,IPageEntity $CurrentStoreEntity=null, IStoreEntity $ChildStoreEntity = null,array $ChildTreeEntity = null, $HaveChilds = false)
    {
        $CurrentTreeEntity = $this->GetStoreTreeItem($IsSelected, $CurrentStoreEntity, $ChildStoreEntity, $ChildTreeEntity, $HaveChilds);
        if($CurrentStoreEntity)
        {
            $ParentStoreEntity = $CurrentStoreEntity->GetParent();
            $ret = $this->GetStoreTreeItemRecursivelyUp(false ,$ParentStoreEntity, $CurrentStoreEntity, $CurrentTreeEntity, true);
        }
        else
        {
            $ret = $CurrentTreeEntity;
        }
        return $ret;
    }

    protected function GetStoreTreeItem($IsSelected,IPageEntity $CurrentStoreEntity=null, IStoreEntity $ChildStoreEntity = null,array $ChildTreeEntity = null, $HaveSelectedChilds = false)
    {
        $ChildsTreeList = $this->GetChildsTree($IsSelected, $CurrentStoreEntity, $ChildStoreEntity, $ChildTreeEntity);
        //TODO: нарушена логика, разобраться (топового не должно быть, но необходим для построения корневых)
        //VirtualTop? Add to config show_virtual_root?
        if($CurrentStoreEntity)
        {
            $CurrentTreeEntity =  $this->StoreEntityToTreeEntity($CurrentStoreEntity, $HaveSelectedChilds, $ChildsTreeList);
            $ret = $CurrentTreeEntity;
        }
        else
        {
            $ret = $ChildsTreeList;
        }
        return $ret;
    }

    /**
     * @param IPageEntity $CurrentStoreEntity
     * @param IStoreEntity $ChildStoreEntity
     * @param array $ChildTreeEntity
     * @return array
     */
    protected function GetChildsTree($IsSelected=false, IPageEntity $CurrentStoreEntity=null, IStoreEntity $ChildStoreEntity=null, array $ChildTreeEntity=null)
    {
        //Флаг указывающий на то, что строим самый верхний уровень. Т.е. все дочерние - roots.
        $CurrentIsTop = (!$CurrentStoreEntity
        //    or ($CurrentStoreEntity instanceof ICategory and !$CurrentStoreEntity->HasParent())
        ) ? true : false;

        //На данный момент AutoExpand есть только для корневых.
        //TODO: возможно на доменах будет иначе. Проверить и исправить
        $HaveAutoExpandChild = (($CurrentIsTop and $this->Config['auto-expand-on-main'])
            //or ($this->Config['auto-expand-on-main'] and ($CurrentStoreEntity instanceof ICategory and !$CurrentStoreEntity->HasParent()))
        ) ? true : false;

        //получаем значения для дочерних, которые надо расскрыть автоматически (если не выбрано ничего в соседних или сами соседние)
        if ($HaveAutoExpandChild and !$this->SelectedStoreEntity) {
            //Получаем автораскрываемый элемент
            $CategoryId = $this->Config['auto-expand-on-main'];
            $AutoExpandStoreEntity = $this->ICategoryRepository->GetById($CategoryId);
            if($AutoExpandStoreEntity)
            {
                $AutoExpandTreeEntity = $this->GetStoreTreeItem(true, $AutoExpandStoreEntity);

                $ChildStoreEntity = $AutoExpandStoreEntity;
                $ChildTreeEntity = $AutoExpandTreeEntity;
            }
        }

        //группировка возможно только для категорий
        $ListFields = $this->UrlNameListReqValues;//поля получаемые для плоского списка (т.е. без группировки - товары, подтовары)
        $GroupedFields = null;//поля извлекаемые для группируемых категорий
        if($this->GroupByType)//Нужно ли группировать по типу. Группируем только если в списке разные типы
        {
            $GroupedFields = $ListFields;
            $GroupedFields[] = 'type_id';//поле по которому группируем
        }

        $CategoryFields = (!$this->GroupByType) ? $ListFields : $GroupedFields;

        $InternalChildValues = $ExternalChildValues = null;

        //Если топ
        if ($CurrentIsTop and $this->AlwaysShowRoots) {
            $InternalChildValues = $this->ICategoryRepository->GetInternalChildsUrlNameListByInternalEntity(null);
        } //Если категория
        elseif ($CurrentStoreEntity instanceof ICategory) {
            //если текущая категория = выбранная
            if ($IsSelected
                //(($ChildStoreEntity == $this->SelectedStoreEntity) and (!isset($ChildTreeEntity['childs'])))//Если нету показываемых дочерних, показываем соседние
                or ((!isset($ChildTreeEntity['childs'])) and (isset($ChildTreeEntity['hrc'])))
                or (($this->SelectedStoreEntity==$ChildStoreEntity) and (!isset($ChildTreeEntity['childs'])))
                or (($this->SelectedStoreEntity->GetParent()==$ChildStoreEntity) and (!isset($ChildTreeEntity['childs'])))
                )//Если нету показываемых дочерних, показываем соседние
            {
                //для выбранной и родительской категории дочерние категории показываем всегда
                //$InternalChildValues = $CurrentStoreEntity->GetInternalChildsValues($CategoryFields);
                //$InternalChildValues = $CurrentStoreEntity->GetChildCategoriesUrlNameList();
                $InternalChildValues = $CurrentStoreEntity->GetInternalChildsUrlNameList();

                //дочерние товары показываем только если задано в конфиге
                if ($this->Config['show_products']) {
                    //$ExternalChildValues = $CurrentStoreEntity->GetExternalChildsValues($ListFields);
                    //$ExternalChildValues = $CurrentStoreEntity->GetChildProductsUrlNameList();
                    $ExternalChildValues = $CurrentStoreEntity->GetExternalChildsUrlNameList();
                }
            }
            elseif (!$this->Config['hide-siblings-for-parents'])
            {
                //$InternalChildValues = $CurrentStoreEntity->GetInternalChildsValues($CategoryFields);
                //$InternalChildValues = $CurrentStoreEntity->GetChildCategoriesUrlNameList($CategoryFields);
                $InternalChildValues = $CurrentStoreEntity->GetInternalChildsUrlNameList($CategoryFields);

                //дочерние товары показываем только если задано в конфиге
                if ($this->Config['show_products']) {
                    //$ExternalChildValues = $CurrentStoreEntity->GetExternalChildsValues($ListFields);
                    //$ExternalChildValues = $CurrentStoreEntity->GetChildProductsUrlNameList($ListFields);
                    $ExternalChildValues = $CurrentStoreEntity->GetExternalChildsUrlNameList($ListFields);
                }
            }

            //текущая категория не является выбранной - родительская (но не автораскрываемая) - ничего не делаем?
            //TODO: проверить
        }

        //Если товар
        //у товаров дочерние только подтовары
        //показываем только если задано в конфиге
        elseif ($CurrentStoreEntity instanceof IProduct) {
            if ($this->Config['show_subproducts']) {
                //$ExternalChildValues = $CurrentStoreEntity->GetExternalChildsValues($ListFields);
                $ExternalChildValues = $CurrentStoreEntity->GetExternalChildsUrlNameList($ListFields);
            }
        }

        //у подтоваров нету дочерних


        //Если надо, делим на группы дочерние элементы по типу
        $IsGrouped = false;
        $GroupedInternalChildValues = null;
        if ($InternalChildValues and $this->Config['group_by_type']) {
            $GroupedInternalChildValues = $this->GroupValuesListByType($InternalChildValues);
            if (count($GroupedInternalChildValues) > 1) {
                $IsGrouped = true;
            }
        }

        //Конвертируем дочерние в элементы дерева
        $ChildsTreeList = null;
        $InternalChildTreeList = $ExternalChildTreeList = [];
        //TODO: разобраться
        $SearchChildValues = ($ChildStoreEntity) ? $ChildStoreEntity->GetValues($ListFields) : null;
        //TODO: если есть $ChildStoreEntity но нету $СhildTreeList то искать не надо
        if ($InternalChildValues) {
            //Если без группировки по типу
            if (!$IsGrouped) {
                //TODO: $InternalChildTreeList = $this->UrlNameListToTreeList($InternalChildValues, $SearchChildValues, $ChildTreeEntity);
                $InternalChildTreeList = $this->ValuesListToTreeList($InternalChildValues, $SearchChildValues, $ChildTreeEntity);
            } else {
                //TODO: сначала в TreeList а затем группировка?
                $InternalChildTreeList = $this->GroupedValuesToTreeList($GroupedInternalChildValues, $SearchChildValues, $ChildTreeEntity);
            }
        }
        //Если есть что показывать на месте дочернего, то показываем дочерний
        elseif($ChildStoreEntity and $ChildTreeEntity)
        {
            $InternalChildTreeList =[$ChildTreeEntity];
        }

        //TODO: $this->UrlNameListToTreeList
        if ($ExternalChildValues) $ExternalChildTreeList = $this->ValuesListToTreeList($ExternalChildValues, $SearchChildValues, $ChildTreeEntity);

        $ChildsTreeList = array_merge($InternalChildTreeList, $ExternalChildTreeList);
        return $ChildsTreeList;
    }

    protected function StoreEntityToTreeEntity(IStoreEntity $IStoreEntity, $HaveChilds=false, $ChildsTreeList=null)
    {
        if((($IStoreEntity instanceof IProduct) and !$this->Config['show_products'])
            or (($IStoreEntity instanceof ISubProduct) and !$this->Config['show_subproducts']))
        {
            return null;
        }

        $Values = $IStoreEntity->GetValues($this->UrlNameListReqValues);
        $TreeEntity = $this->StoreEntityValuesToTreeEntity($Values, $HaveChilds);
        if($ChildsTreeList) $TreeEntity['childs'] = $ChildsTreeList;
        return $TreeEntity;
    }

    protected function StoreEntityValuesToTreeEntity($StoreEntityValues, $HaveChilds=false )
    {
        $TreeEntity = [
            'link' => $StoreEntityValues
                /*[
                'name'  => $StoreEntityValues['genitive'],
                'url'   => $StoreEntityValues['url']
            ]*/
        ];

        if($HaveChilds) $TreeEntity['hrc'] = true;//hrc = have real childs
        return $TreeEntity;
    }

    protected function GroupValuesListByType($InternalChildValues)
    {
        $ret = $this->GroupByField($InternalChildValues, 'type_id');
        ksort($ret);//TODO: перенести в конфиг, и возможно сделать пользовательский порядок
        return $ret;
    }

    protected function GroupByField($List, $key)
    {
        $ret = [];
        foreach($List as $Item)
        {
            $GroupId = $Item[$key];
            if(!isset($ret[$GroupId])) $ret[$GroupId] = [];
            $ret[$GroupId] []= $Item;
        }
        return $ret;
    }

    /**
     * @param $InternalChildValues
     * @param $SearchChildValues
     * @param $ChildTreeEntity
     * @param $Find - Флаг, через который указывается, найден ли элемент $SearchChildValues в $InternalChildValues
     * Необходим для поднятия групп при группировке по типу
     * @return array
     */
    protected function ValuesListToTreeList($InternalChildValues, $SearchChildValues, $ChildTreeEntity, &$Find=null)
    {
        $TreeList = [];
        $MoveSelectedToTop = $this->Config['selected_move_to_top'];
        $SelectedListItem = null;
        $Find = false;

        foreach($InternalChildValues as $InternalChildValue)
        {
            $IsEqValues = $this->IsEqValues($InternalChildValue, $SearchChildValues);
            if(!$IsEqValues)
            {
                $TreeListItem = $this->StoreEntityValuesToTreeEntity($InternalChildValue);
                $TreeList[] = $TreeListItem;
            }
            else
            {
                if($MoveSelectedToTop) $SelectedListItem = $ChildTreeEntity;
                else $TreeListItem = $ChildTreeEntity;
                $Find = true;
            }
        }
        if($MoveSelectedToTop && $SelectedListItem) array_unshift($TreeList, $SelectedListItem);
        //TODO: check for restriction and $SelectedListItem!=null
        return $TreeList;
    }

    protected function GroupedValuesToTreeList($GroupedInternalChildValues, $SearchChildValues, $ChildTreeEntity)
    {
        $TreeList = [];
        $MoveSelectedToTop = $this->Config['selected_move_to_top'];
        $SelectedTreeGroup = null;
        $Find = false;
        foreach($GroupedInternalChildValues as $GroupKey => $GroupedChildValues)
        {
            $TreeGroup = [
                'group' => $GroupKey,
                'childs' => $this->ValuesListToTreeList($GroupedChildValues, $SearchChildValues, $ChildTreeEntity, $Find)
            ];
            if(!$MoveSelectedToTop) $TreeList []= $TreeGroup;
            else
            {
                if(!$Find) $TreeList []= $TreeGroup;
                else
                {
                    $SelectedTreeGroup = $TreeGroup;
                }
            }
        }
        if($MoveSelectedToTop and $SelectedTreeGroup) array_unshift($TreeList, $SelectedTreeGroup);
        return $TreeList;
    }

    protected function IsEqValues($v1, $v2)
    {
        if($v1['url']==$v2['url']) return true;
        else return false;
    }
}