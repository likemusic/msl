<?php namespace App\Helpers\StoreTreeBuilder;

use App\Models\Base\Interfaces\RepositoryItem\IGetStoreTree;

interface IStoreTreeBuilder extends IGetStoreTree {
}