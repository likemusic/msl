<?php namespace App\Helpers\TablesNamer;

/**
 * Interface ITablesNamer
 * @package App\Helpers\TablesNamer
 *
 * Содержит правила/логику для вычисления названий таблиц и их полей
 * (для таблиц изменяемых в зависимости от домена)
 */
interface ITablesNamer {

    function GetLinkTableName($ParentTableName, $ChildTableName);
    function GetLinkTableFieldName($TableName);
}