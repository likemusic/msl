<?php namespace App\Helpers\TablesNamer;

use Illuminate\Support\Pluralizer;

class TablesNamer implements ITablesNamer{
    /**
     * @var Pluralizer
     */
    protected $pluralizer;

    function __construct(Pluralizer $pluralizer)
    {
        $this->pluralizer = $pluralizer;
    }

    function GetLinkTableName($ParentTableName, $ChildTableName)
    {
        $arr = [$ParentTableName, $ChildTableName];
        sort($arr);
        $LinkTableName = "{$arr[0]}_{$arr[1]}";
        return $LinkTableName;
    }

    function GetLinkTableFieldName($TableName)
    {
        $Singular = $this->pluralizer->singular($TableName);
        $FieldName = "{$Singular}_id";
        return $FieldName;
    }
}