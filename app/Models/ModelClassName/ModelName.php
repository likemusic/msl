<?php namespace App\Models\PageEntity\ModelClassName;

class ModelName {

    /**
     * @var ModelClassNameTypes
     */
    public $Category;

    /**
     * @var ModelClassNameTypes
     */
    public $Product;

    /**
     * @var ModelClassNameTypes
     */
    public $SubProduct;

    public function __construct($Category, $Product, $SubProduct)
    {
        $this->Category = $Category;
        $this->Product = $Product;
        $this->SubProduct = $SubProduct;
    }
}