<?php namespace App\Models\PageEntity\ModelClassName;

class ModelClassNameTypes {
    public $Part;
    public $Type;
    public $Template;
    public $Calculated;

    public function __construct($Part, $Type, $Template, $Calculated)
    {
        $this->Part = $Part;
        $this->Type = $Type;
        $this->Template = $Template;
        $this->Calculated = $Calculated;
    }
}