<?php namespace App\Models\PageEntity;

use App\Models\Base\Interfaces\Repository\IRepositoryEntity;

interface IPageEntity extends IRepositoryEntity {
    function GetMetaDescription();
    function GetMetaKeywords();
    function GetTitle();
    function GetName();

    /**
     * @param null|string[] $Fields
     * @return array
     */
    function GetValues($Fields = null);
}