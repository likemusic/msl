<?php namespace App\Models\PageEntity;

use App\Models\PageEntity\Article\IArticleRepository;
use App\Models\PageEntity\Redirect\IRedirectRepository;
use App\Models\PageEntity\SpecificPage\ISpecificPageRepository;
use App\Models\PageEntity\StoreEntity\Base\IStoreEntityRepository;

use App\Models\PageEntity\StoreEntity\IGlobalStoreEntityRepository;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class GlobalPageEntityRepository implements IPageEntityRepository
{

    /**
     * @var ISpecificPageRepository
     */
    protected $ISpecificPageRepository;

    /**
     * @var IArticleRepository
     */
    protected $IArticleRepository;

    /**
     * @var IGlobalStoreEntityRepository
     */
    protected $IStoreTreeEntityRepository;

    /**
     * @var IRedirectRepository
     */
    protected $IRedirectRepository;

    /**
     * @var IPageEntityRepository[]
     */
    protected $IInnerRepositories;

    function __construct(
        ISpecificPageRepository $ISpecificPageRepository,
        IArticleRepository $IArticleRepository,
        IGlobalStoreEntityRepository $IStoreEntityRepository,
        IRedirectRepository $IRedirectRepository
    )
    {
        $this->ISpecificPageRepository = $ISpecificPageRepository;
        $this->IArticleRepository = $IArticleRepository;
        $this->IStoreTreeEntityRepository = $IStoreEntityRepository;
        $this->IRedirectRepository = $IRedirectRepository;

        //$this->IInnerRepositories = func_get_args();
        //$repositories = func_get_args();
        //Временно удаляем редиректы из репозиториев
        //array_pop($repositories);
        $this->IInnerRepositories = [
            $IStoreEntityRepository,//первым потому что основное
            $ISpecificPageRepository,//потому что по умолчанию нету запросов к бд
            $IArticleRepository,
            //$IRedirectRepository - пока не работают
        ];
        //TODO: сделать поиск по редиректам после поиска страниц
    }

    function GetByUrlWithByPath($url, IPageEntity &$FoundByPath = null)
    {
        $ByPath = null;//Станица найденная по пути url-a
        foreach ($this->IInnerRepositories as $IInnerRepository) {
            $StorePage = $IInnerRepository->GetByUrlWithByPath($url, $ByPath);
            if ($StorePage) return $StorePage;
        }
        throw new NotFoundHttpException();
    }
}