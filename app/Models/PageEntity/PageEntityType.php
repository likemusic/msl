<?php namespace App\Models\PageEntity;

class PageEntityType{
    const Article=1;
    const Store=2;
    const Feedback=3;
    const Callback=4;
    const Redirect=5;
}