<?php namespace App\Models\PageEntity\Base\Parent;
use App\Models\PageEntity\Base\Calculated\ICalculationContext;

interface IParentContext extends ICalculationContext{
    function GetParentContext();
}