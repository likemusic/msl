<?php namespace App\Models\PageEntity\Base\Type;

use App\Models\PageEntity\Base\Calculated\ICalculationContext;

interface IPageEntityTypeContext extends ICalculationContext{
    function GetTypeContext();
}