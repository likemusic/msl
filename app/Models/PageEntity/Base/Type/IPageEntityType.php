<?php namespace App\Models\PageEntity\Base\Type;

use App\Models\Base\Interfaces\Repository\IRepositoryEntity;

interface IPageEntityType extends IRepositoryEntity {

}