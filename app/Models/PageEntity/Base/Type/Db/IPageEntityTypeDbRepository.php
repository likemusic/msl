<?php namespace App\Models\PageEntity\Base\Type\Db;

use App\Models\Base\Interfaces\Repository\IDbRepository;
use App\Models\PageEntity\Base\Type\IPageEntityTypeRepository;

interface IPageEntityTypeDbRepository extends IPageEntityTypeRepository, IDbRepository{

} 