<?php namespace App\Models\PageEntity\Base\Type\Db;

use App\Models\Base\Repository\Db\IDbEntity;
use App\Models\PageEntity\Base\IPageEntityType;

interface IPageEntityTypeDb extends IPageEntityType, IDbEntity{

}
