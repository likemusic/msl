<?php namespace App\Models\PageEntity\Base\Type\Db;

use App\Models\Base\DbEntity;
use App\Models\PageEntity\Base\IPageEntityType;

abstract class PageEntityTypeDb extends DbEntity implements IPageEntityType {

} 