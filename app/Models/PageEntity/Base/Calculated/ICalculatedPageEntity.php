<?php namespace App\Models\PageEntity\Base\Calculated;

use App\Models\PageEntity\IPageEntity;

interface ICalculatedPageEntity extends IPageEntity{
    /**
     * @return ICalculationContext
     */
    function GetContext();
}