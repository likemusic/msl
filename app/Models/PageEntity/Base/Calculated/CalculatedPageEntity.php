<?php namespace App\Models\PageEntity\Base\Calculated;

use App\Models\Base\Traits\RepositoryItem\TPageEntity;

abstract class CalculatedPageEntity implements ICalculatedPageEntity{

    use TPageEntity;

    /**
     * @var ICalculationContext
     */
    protected $ICalculationContext;

    public function __construct(ICalculationContext $ICalculationContext)
    {
        $this->ICalculationContext = $ICalculationContext;
    }

    /**
     * @return ICalculationContext
     */
    function GetContext()
    {
        return $this->ICalculationContext;
    }

}