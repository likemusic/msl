<?php

namespace App\Models\PageEntity\Base\Calculated\Template;

trait TPageEntityTemplateContext {
    /**
     * @var IPageEntityTemplate
     */
    protected $ITemplateContext;

    /**
     * @return IPageEntityTemplate
     */
    function GetTemplateContext()
    {
        return $this->ITemplateContext;
    }
} 