<?php namespace App\Models\PageEntity\Base\Calculated\Template\Db;

use App\Models\Base\Interfaces\RepositoryItem\IDbEntity;
use App\Models\PageEntity\Base\Calculated\Template\IPageEntityTemplate;

interface IPageEntityTemplateDb extends IPageEntityTemplate, IDbEntity{

} 