<?php namespace App\Models\PageEntity\Base\Calculated\Template\Db;


use App\Models\Base\Interfaces\Repository\IDbRepository;
use App\Models\PageEntity\Base\Calculated\Template\IPageEntityTemplateRepository;

interface IPageEntityTemplateDbRepository extends IPageEntityTemplateRepository, IDbRepository{

} 