<?php namespace App\Models\PageEntity\Base\Calculated\Template\Db;

use App\Models\Base\Interfaces\IGetName;
use App\Models\Base\Repository\Db\DbRepository;

class PageEntityTemplateDbRepository extends DbRepository implements IPageEntityTemplateDbRepository{

    protected function UrlToUrlTemplateForLike($url, IGetName  $IGetName)
    {
        $Name = $IGetName->GetName();
        $UrlTemplateForLike = str_replace($Name,'%',$url);
        return $UrlTemplateForLike;
    }
}