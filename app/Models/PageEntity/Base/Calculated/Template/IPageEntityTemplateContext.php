<?php namespace App\Models\PageEntity\Base\Calculated\Template;

use App\Models\PageEntity\Base\Calculated\ICalculationContext;

interface IPageEntityTemplateContext extends ICalculationContext{
    /**
     * @return IPageEntityTemplate
     */
    function GetTemplateContext();
} 