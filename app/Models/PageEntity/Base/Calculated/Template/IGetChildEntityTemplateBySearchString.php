<?php namespace App\Models\PageEntity\Base\Calculated\Template;

interface IGetChildEntityTemplateBySearchString {
    function GetChildEntityTemplateBySearchString($SearchString);
} 