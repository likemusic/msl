<?php namespace App\Models\PageEntity\Base\Calculated;

use App\Models\PageEntity\Base\Calculated\ICalculationContext;

interface ICalculatedPageEntityContext extends ICalculationContext {
    /**
     * @return ICalculatedPageEntity
     */
    function GetCalculated();
}
