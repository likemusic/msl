<?php namespace App\Models\PageEntity\Base\Calculated\Part;

use App\Models\PageEntity\Base\Calculated\ICalculationContext;

interface IPageEntityPartContext extends ICalculationContext {
    /**
     * @return IPageEntityPart
     */
    function GetPartContext();
} 