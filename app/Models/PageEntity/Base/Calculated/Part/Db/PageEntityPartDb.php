<?php namespace App\Models\PageEntity\Base\Calculated\Part\Db;

use App\Models\Base\Repository\Db\DbEntity;
use App\Models\Base\Traits\RepositoryItem\TDbEntity;

abstract class PageEntityPartDb extends DbEntity
    implements IPageEntityPartDb{
    use TDbEntity;
}