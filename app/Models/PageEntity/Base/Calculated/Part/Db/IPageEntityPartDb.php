<?php namespace App\Models\PageEntity\Base\Calculated\Part\Db;

use App\Models\Base\Interfaces\RepositoryItem\IDbEntity;
use App\Models\PageEntity\Base\Calculated\Part\IPageEntityPart;

interface IPageEntityPartDb extends IPageEntityPart, IDbEntity{

}