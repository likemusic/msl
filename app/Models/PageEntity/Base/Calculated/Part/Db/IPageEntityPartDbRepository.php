<?php namespace App\Models\PageEntity\Base\Calculated\Part\Db;

use App\Models\Base\Interfaces\Repository\IDbRepository;
use App\Models\PageEntity\Base\Calculated\Part\IPageEntityPartRepository;

interface IPageEntityPartDbRepository extends IPageEntityPartRepository, IDbRepository{

}