<?php namespace App\Models\PageEntity\Base\Calculated\Part;

abstract class PageEntityPartContext implements IPageEntityPartContext{
    use TPageEntityPartContext;

    function __construct(IPageEntityPart $IPartPageEntity)
    {
        $this->IPartContext = $IPartPageEntity;
    }
}