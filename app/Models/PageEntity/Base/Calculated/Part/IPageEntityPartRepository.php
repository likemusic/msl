<?php namespace App\Models\PageEntity\Base\Calculated\Part;

use App\Models\Base\Interfaces\Repository\IGetById;
use App\Models\Base\Interfaces\Repository\IRepository;

interface IPageEntityPartRepository extends IRepository, IGetById{

}