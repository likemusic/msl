<?php namespace App\Models\PageEntity\Base\Calculated\Part;


trait TPageEntityPartContext
{
    /**
     * @var IPageEntityPart
     */
    protected $IPartContext;

    function GetPartContext(){
        return $this->IPartContext;
    }
}