<?php namespace App\Models\PageEntity\Base\Calculated;

abstract class CalculatedPageEntityContext implements ICalculatedPageEntityContext {

    /**
     * @var ICalculatedPageEntity
     */
    protected $ICalculatedPageEntity;

    function __construct(ICalculatedPageEntity $ICalculatedPageEntity)
    {
        $this->ICalculatedPageEntity = $ICalculatedPageEntity;
    }

    function GetCalculated()
    {
        return $this->ICalculatedPageEntity;
    }
}