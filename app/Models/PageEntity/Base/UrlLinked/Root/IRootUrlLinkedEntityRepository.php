<?php namespace App\Models\PageEntity\Base\UrlLinked\Root;

use App\Models\Base\Interfaces\Repository\GetByUrlSlug\IGetRootByUrlSlug;
use App\Models\PageEntity\Base\UrlLinked\IUrlLinkedEntityRepository;

interface IRootUrlLinkedEntityRepository extends IUrlLinkedEntityRepository, IGetRootByUrlSlug {

}
