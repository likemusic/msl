<?php namespace App\Models\PageEntity\Base\UrlLinked\NotRoot;

use App\Models\PageEntity\Base\UrlLinked\IUrlLinkedEntity;

interface INotRootUrlLinkedEntity extends IUrlLinkedEntity {

} 