<?php namespace App\Models\PageEntity\Base\UrlLinked\NotRoot;

use App\Models\Base\Interfaces\Repository\GetByUrlSlug\IGetInternalChildByUrlSlugForExternalEntity;
use App\Models\PageEntity\Base\UrlLinked\IUrlLinkedEntityRepository;

interface INotRootUrlLinkedEntityRepository extends IUrlLinkedEntityRepository, IGetInternalChildByUrlSlugForExternalEntity{

} 