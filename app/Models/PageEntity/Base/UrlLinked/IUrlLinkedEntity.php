<?php namespace App\Models\PageEntity\Base\UrlLinked;

use App\Models\Base\Interfaces\RepositoryItem\GetByUrlSlug\IGetChildByUrlSlug;

interface IUrlLinkedEntity extends IGetChildByUrlSlug{

} 