<?php namespace App\Models\PageEntity\Base\UrlLinked;

//use App\Models\Base\Interfaces\Repository\GetByUrlSlug\IGetChildByUrlSlugForInternalEntity;
//use App\Models\Base\Interfaces\Repository\GetByUrlSlug\IGetExternalChildByUrlSlugForInternalEntity;
//use App\Models\Base\Interfaces\Repository\GetByUrlSlug\IGetInternalChildByUrlSlugForInternalEntity;
use App\Models\Base\Interfaces\Repository\IRepository;
//use App\Models\Base\Interfaces\RepositoryItem\GetByUrlSlug\IGetChildByUrlSlug;

interface IUrlLinkedEntityRepository extends IRepository
    //IGetChildByUrlSlug
    /*
    IGetChildByUrlSlugForInternalEntity,

    IGetInternalChildByUrlSlugForInternalEntity,
    IGetExternalChildByUrlSlugForInternalEntity*/
{
}