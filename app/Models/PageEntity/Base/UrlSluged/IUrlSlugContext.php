<?php namespace App\Models\PageEntity\Base\UrlSluged;

use App\Models\PageEntity\Base\Calculated\ICalculationContext;

interface IUrlSlugContext extends ICalculationContext{
    function GetUrlSlugContext();
} 