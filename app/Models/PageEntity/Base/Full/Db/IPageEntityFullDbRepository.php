<?php namespace App\Models\PageEntity\Base\Full\Db;

use App\Models\Base\Interfaces\Repository\IDbRepository;
use App\Models\PageEntity\Base\Full\IPageEntityFullRepository;

interface IPageEntityFullDbRepository extends IPageEntityFullRepository, IDbRepository {

}