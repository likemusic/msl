<?php namespace App\Models\PageEntity\Base\Full\Db;

use App\Models\Base\Interfaces\RepositoryItem\IDbEntity;
use App\Models\PageEntity\Base\Full\IPageEntityFull;

interface IPageEntityFullDb extends IPageEntityFull, IDbEntity {

}