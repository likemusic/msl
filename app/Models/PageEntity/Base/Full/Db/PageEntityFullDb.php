<?php namespace App\Models\PageEntity\Base\Full\Db;

use App\Models\Base\Repository\Db\DbEntity;
use App\Models\Base\Traits\RepositoryItem\TGetValues;
use App\Models\Base\Traits\RepositoryItem\TPageEntity;

abstract class PageEntityFullDb extends DbEntity implements IPageEntityFullDb {
    use TPageEntity {GetKnowFields as GetKnowFieldsCommon;}

    abstract function GetContent();

    function GetKnowFields()
    {
        $Fields = $this->GetKnowFieldsCommon();
        $Fields['page_content'] = 'GetContent';
        return $Fields;
    }
}