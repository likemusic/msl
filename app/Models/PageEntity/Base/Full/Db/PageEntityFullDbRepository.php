<?php namespace App\Models\PageEntity\Base\Full\Db;

use App\Models\Base\Repository\Db\DbRepository;
use App\Models\Base\Traits\analoguewithioc\GetByUrl\TGetByUrl;
use App\Models\PageEntity\IPageEntity;

abstract class PageEntityFullDbRepository extends DbRepository implements IPageEntityFullDbRepository{
    use TGetByUrl;

    function GetByUrlWithByPath($Url, IPageEntity &$FoundByPath=null)
    {
        //TODO: проверить, доделать
        $FoundByPath = null;
        return $this->GetByUrl($Url);
    }
}