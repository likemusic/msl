<?php namespace App\Models\PageEntity\Base\Full\Values;


use App\Models\Base\Traits\RepositoryItem\TGetValues;

abstract class PageEntityFullValues implements IPageEntityFullValues{
    use TGetValues;

    /**
     * @var array
     */
    protected $Values;

    function __construct(array $Values)
    {
        $this->Values = $Values;
    }

    /*function GetValues($Fields=null)
    {
        if(!$Fields) return $this->Values;
        $ret = array_intersect_key($this->Values, array_fill_keys($Fields,null));
        return $ret;
    }*/
}