<?php namespace App\Models\PageEntity\Base\Full\Values;

use App\Models\PageEntity\Base\Full\IPageEntityFull;

/**
 * Interface IPageEntityFullValues
 * @package App\Models\PageEntity\Base\Full\Values
 *
 * Полная сущность данные которой передаются в конструкторе
 */
interface IPageEntityFullValues extends IPageEntityFull{

} 