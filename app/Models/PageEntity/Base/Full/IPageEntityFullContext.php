<?php namespace App\Models\PageEntity\Base\Full;


use App\Models\PageEntity\Base\Calculated\ICalculationContext;

interface IPageEntityFullContext extends ICalculationContext{

    /**
     * @return IPageEntityFull
     */
    function GetFull();
} 