<?php namespace App\Models\PageEntity\SpecificPage;


use App\Models\PageEntity\IPageEntity;
use App\Models\PageEntity\SpecificPage\MainPage\IMainPageProvider;

class SpecificPageRepository implements ISpecificPageRepository{

    protected $SpecificPageProviders = [];

    function __construct(IMainPageProvider $IMainPage)
    {
        $this->SpecificPageProviders []= $IMainPage;
    }

    function GetByUrlWithByPath($url, IPageEntity &$FoundByPath = null)
    {
        foreach($this->SpecificPageProviders as $SpecificPageProvider)
        {
            $ret = $SpecificPageProvider->GetByUrlWithByPath($url, $FoundByPath);
            if($ret) return $ret;
        }
    }
} 