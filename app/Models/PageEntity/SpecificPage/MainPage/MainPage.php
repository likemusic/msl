<?php namespace App\Models\PageEntity\SpecificPage\MainPage;

use App\Models\Base\Traits\RepositoryItem\TPageEntity;
use App\Models\PageEntity\StoreEntity\IGlobalStoreEntityRepository;

class MainPage implements IMainPage{
    use TPageEntity {GetKnowFields as GetPageEntityKnowFields;}

    /**
     * @var IGlobalStoreEntityRepository
     */
    protected $IGlobalStoreEntityRepository;

    protected $Ids;

    protected $Goods;

    function __construct(IGlobalStoreEntityRepository $IGlobalStoreEntityRepository)
    {
        $this->IGlobalStoreEntityRepository = $IGlobalStoreEntityRepository;
        $this->Ids = config('site.MainPageItems');
        if($this->Ids) $this->Goods = $this->GetGoodsByIds($this->Ids);
    }

    function GetKnowFields()
    {
        $ParentFields = $this->GetPageEntityKnowFields();
        $AddFields = [
            'products'      => 'GetGoods',
        ];

        $Fields = $ParentFields + $AddFields;
        return $Fields;
    }

    protected function GetGoodsByIds($Ids)
    {
        $ret = array_map([$this,'GetGoodById'],$Ids);
        return $ret;
    }

    protected function GetGoodById($Id)
    {
        $ret = $this->IGlobalStoreEntityRepository->GetByVID($Id);
        $ret = $ret->GetValues(['url', 'name', /*'price',*/ 'main_picture']);
        return $ret;
    }

    function GetGoods()
    {
        return $this->Goods;
    }

    //TODO: доделать свойсва страницы
    function GetMetaDescription()
    {
        return null;
    }

    function GetMetaKeywords()
    {
        return null;
    }

    function GetTitle()
    {
        return null;
    }

    function GetName()
    {
        return null;
    }


}