<?php namespace App\Models\PageEntity\Feedback;

use App\Models\Base\Traits\RepositoryItem\TPageEntity;
use App\Models\Base\Traits\RepositoryItem\TSimplePageEntity;
use App\Models\PageEntity\Base\Full\Values\PageEntityFullValues;

class Feedback extends PageEntityFullValues implements IFeedback {
    use TPageEntity, TSimplePageEntity;

    protected $InnerName = 'Обратная связь';
}