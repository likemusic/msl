<?php namespace App\Models\PageEntity;

use App\Models\Base\Interfaces\Repository\GetByUrl\IGetByUrlWithByPath;
use App\Models\Base\Interfaces\Repository\IRepository;

interface IPageEntityRepository extends IGetByUrlWithByPath, IRepository {

}