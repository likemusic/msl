<?php namespace App\Models\PageEntity\Redirect;

use App\Models\PageEntity\Base\Full\Db\PageEntityFullDbRepository;

class RedirectRepository extends PageEntityFullDbRepository implements IRedirectRepository{

    protected $ItemClassName = Redirect::class;
}