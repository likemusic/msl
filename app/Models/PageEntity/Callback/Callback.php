<?php namespace App\Models\PageEntity\Callback;

use App\Models\Base\Traits\RepositoryItem\TPageEntity;
use App\Models\Base\Traits\RepositoryItem\TSimplePageEntity;
use App\Models\PageEntity\Base\Full\Values\PageEntityFullValues;

class Callback extends PageEntityFullValues implements ICallback {
    use TPageEntity, TSimplePageEntity;

    protected $InnerName = 'Заказ обратного звонка';

}