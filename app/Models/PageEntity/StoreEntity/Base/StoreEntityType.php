<?php namespace App\Models\PageEntity\StoreEntity\Base;

class StoreEntityType{
    const Category=1;
    const Product=2;
    const SubProduct=3;
}