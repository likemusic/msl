<?php namespace App\Models\PageEntity\StoreEntity\Base\Calculated\NotRoot;

use App\Models\PageEntity\StoreEntity\Base\Calculated\ICalculatedStoreEntity;

interface INotRootCalculatedStoreEntity extends ICalculatedStoreEntity{

} 