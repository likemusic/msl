<?php namespace App\Models\PageEntity\StoreEntity\Base\Calculated\NotRoot;

use App\Helpers\StoreTreeBuilder\IStoreTreeBuilder;
use App\Models\Base\Traits\RepositoryItem\TGetUrlSlugByTemplate;
use App\Models\PageEntity\StoreEntity\Base\Calculated\CalculatedStoreEntity;
use App\Helpers\PaymentGateway\Base\BuyButton\IRequestDataProvider as PaymentDataProvider;

abstract class NotRootCalculatedStoreEntity extends CalculatedStoreEntity implements INotRootCalculatedStoreEntity{
    /**
     * @var PaymentDataProvider
     */
    protected $IPaymentRequestDataProvider;

    function __construct(INotRootCalculatedStoreEntityContext $ICalculationContext,
                         INotRootCalculatedStoreEntityInnerRepository $ISelfRepository,
                         IStoreTreeBuilder $IStoreTreeBuilder,
                         PaymentDataProvider $IPaymentRequestDataProvider)
    {
        parent::__construct($ICalculationContext, $ISelfRepository, $IStoreTreeBuilder);
        $this->IPaymentRequestDataProvider = $IPaymentRequestDataProvider;
    }

}