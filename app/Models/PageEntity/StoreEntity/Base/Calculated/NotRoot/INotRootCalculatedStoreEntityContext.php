<?php namespace App\Models\PageEntity\StoreEntity\Base\Calculated\NotRoot;

use App\Models\PageEntity\StoreEntity\Base\Calculated\ICalculatedStoreEntityContext;

interface INotRootCalculatedStoreEntityContext extends ICalculatedStoreEntityContext {

} 