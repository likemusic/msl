<?php namespace App\Models\PageEntity\StoreEntity\Base\Calculated\NotRoot;

use App\Helpers\StoreTreeBuilder\IStoreTreeBuilder;
use App\Models\Base\Interfaces\Repository\IRepositoryEntity;
use App\Models\Base\Traits\Repository\GetUrlNameList\TCalculatedUrlNameUrlSlugValuesListToUrlNameList;
use App\Models\PageEntity\Base\Calculated\ICalculatedPageEntity;
use App\Models\PageEntity\Base\Calculated\Template\IPageEntityTemplate;
use App\Models\PageEntity\Base\Calculated\Template\IPageEntityTemplateRepository;
use App\Models\PageEntity\StoreEntity\Base\Calculated\CalculatedStoreEntityInnerRepository;
use App\Models\PageEntity\StoreEntity\Base\Inner\NotRoot\INotRootStoreEntityInnerRepository;
use App\Helpers\PaymentGateway\Base\BuyButton\IRequestDataProvider as IPaymentDataProvider;
use App\Helpers\PicturesFinder\IPicturesFinder;

abstract class NotRootCalculatedStoreEntityInnerRepository  extends CalculatedStoreEntityInnerRepository implements INotRootCalculatedStoreEntityInnerRepository  {
    /**
     * @var IPaymentDataProvider
     */
    protected $IBuyButtonBuilder;
    protected $IStoreTreeBulder;
    protected $IPicturesFinder;

    public function __construct(IPaymentDataProvider $IPaymentDataProvider, IPageEntityTemplateRepository $ITemplateRepository, INotRootStoreEntityInnerRepository $IChildRepository=null)
    {
        $this->IBuyButtonBuilder = $IPaymentDataProvider;
        $this->ITemplateRepository = $ITemplateRepository;
        $this->IChildRepository = $IChildRepository;

        $this->IPicturesFinder = app(IPicturesFinder::class);
    }

    protected function GetStoreTreeBuilder()
    {
        if(!$this->IStoreTreeBulder) $this->IStoreTreeBulder = app(IStoreTreeBuilder::class);
        return $this->IStoreTreeBulder;
    }

    function NewRepositoryItem(IPageEntityTemplate $ITemplate, ICalculatedPageEntity $ParentEntity, $url = null, $type = null )
    {
        $Context    = new $this->NewItemContextClassName($ITemplate, $ParentEntity, $url);
        //TODO: inject IStoreTreeBuilder in Constructor
        $ret        = new $this->NewItemClassName($Context, $this,
            $this->GetStoreTreeBuilder(), $this->IBuyButtonBuilder, $this->IPicturesFinder, $type);
        return $ret;
    }

    /*
    function GetChildUrlNameListByEntity(IRepositoryEntity $Entity)
    {
        $PartEntity = $Entity->GetPart();
        return $this->IPartRepository->GetChildUrlNameListByEntity($PartEntity);
    }*/

    /* Get Child Values */
    function GetInternalChildsValuesByExternalEntity(IRepositoryEntity $entity, $Fields = null)
    {
        $EntityList = $this->GetInternalChildsByExternalEntity($entity);
        $ret = $this->EntityListToValuesList($EntityList, $Fields);
        return $ret;
    }

}