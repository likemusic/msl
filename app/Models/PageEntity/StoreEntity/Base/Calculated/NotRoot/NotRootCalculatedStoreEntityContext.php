<?php namespace App\Models\PageEntity\StoreEntity\Base\Calculated\NotRoot;

use App\Models\PageEntity\Base\Calculated\Template\IPageEntityTemplate;
use App\Models\PageEntity\Base\Calculated\Template\TPageEntityTemplateContext;
use App\Models\PageEntity\Base\Parent\TParentContext;
use App\Models\PageEntity\Base\Type\TPageEntityTypeContext;
use App\Models\PageEntity\Base\UrlSluged\TUrlSlugContext;
use App\Models\PageEntity\IPageEntity;

class NotRootCalculatedStoreEntityContext implements INotRootCalculatedStoreEntityContext{
    use TPageEntityTemplateContext,
        TParentContext,
        TPageEntityTypeContext,
        TUrlSlugContext;

    function __construct(IPageEntityTemplate $IProductTemplate, IPageEntity $IParent, $url )
    {
        $this->ITemplateContext = $IProductTemplate;
        $this->IParentContext = $IParent;
        $this->UrlSlugContext = $url;
    }
}