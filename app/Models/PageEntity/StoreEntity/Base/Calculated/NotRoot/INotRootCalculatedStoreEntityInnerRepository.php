<?php namespace App\Models\PageEntity\StoreEntity\Base\Calculated\NotRoot;

use App\Models\PageEntity\StoreEntity\Base\Calculated\ICalculatedStoreEntityInnerRepository;
use App\Models\PageEntity\StoreEntity\Base\Inner\NotRoot\INotRootStoreEntityInnerRepository;

interface INotRootCalculatedStoreEntityInnerRepository extends ICalculatedStoreEntityInnerRepository, INotRootStoreEntityInnerRepository{

} 