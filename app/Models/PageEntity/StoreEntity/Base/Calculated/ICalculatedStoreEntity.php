<?php namespace App\Models\PageEntity\StoreEntity\Base\Calculated;

use App\Models\PageEntity\Base\Calculated\ICalculatedPageEntity;
use App\Models\PageEntity\StoreEntity\Base\IStoreEntity;

interface ICalculatedStoreEntity extends ICalculatedPageEntity, IStoreEntity{

    function GetVID();
    function GetFullVID();
}