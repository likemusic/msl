<?php namespace App\Models\PageEntity\StoreEntity\Base\Calculated;

use App\Models\PageEntity\StoreEntity\Base\Inner\IStoreEntityInnerRepository;

interface ICalculatedStoreEntityInnerRepository extends IStoreEntityInnerRepository{

    /**
     * @return ICalculatedStoreEntity
     *
     */
    function GetByVID($Id);

    /**
     * @return ICalculatedStoreEntity
     */
    function GetByFullVID($Id);
} 