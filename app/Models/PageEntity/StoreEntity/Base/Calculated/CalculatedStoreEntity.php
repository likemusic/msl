<?php namespace App\Models\PageEntity\StoreEntity\Base\Calculated;

use App\Helpers\StoreTreeBuilder\IStoreTreeBuilder;
use App\Models\Base\Traits\GetByUrlSlug\TGetChildByUrlSlug;
use App\Models\Base\Traits\Repository\GetUrlNameList\TGetExternalChildsUrlNameList;
use App\Models\Base\Traits\RepositoryItem\GetChildsValues\TGetChildCategoriesUrlNameList;
use App\Models\Base\Traits\RepositoryItem\GetChildsValues\TGetExternalChildsValues;
use App\Models\Base\Traits\RepositoryItem\GetChildsValues\TGetInternalChildsValues;
use App\Models\Base\Traits\RepositoryItem\TGetExternalChilds;
use App\Models\Base\Traits\RepositoryItem\UrlNameList\TGetChildUrlNameList;
use App\Models\Base\Traits\RepositoryItem\TGetBreadcrumbs;
use App\Models\Base\Traits\RepositoryItem\TGetParent;
use App\Models\Base\Traits\RepositoryItem\TGetUrl;
use App\Models\PageEntity\Base\Calculated\CalculatedPageEntity;
use App\Models\PageEntity\Base\Calculated\Template\IPageEntityTemplate;

abstract class CalculatedStoreEntity extends CalculatedPageEntity implements ICalculatedStoreEntity{
    use TGetParent;

    use TGetUrl;
    use TGetBreadcrumbs;

    use TGetChildByUrlSlug;

    use TGetExternalChildsValues;
    use TGetInternalChildsValues;

    use TGetExternalChilds;

    use TGetChildUrlNameList;
    use TGetExternalChildsUrlNameList;

    /**
     * @var IStoreTreeBuilder
     */
    protected $IStoreTreeBuilder;

    protected $IParent;

    /**
     * @var IPageEntityTemplate
     */
    protected $ITemplate;

    protected $Description;

    public function __construct(ICalculatedStoreEntityContext $ICalculationContext, ICalculatedStoreEntityInnerRepository $ISelfRepository)
    {
        parent::__construct($ICalculationContext);
        $this->ISelfRepository = $ISelfRepository;
        $this->IParent = $ICalculationContext->GetParentContext();
        $this->ITemplate = $ICalculationContext->GetTemplateContext();
    }

    function GetKnowFields()
    {
        $ParentFields = parent::GetKnowFields();
        $AddFields = [
            'src_genitive'  => 'GetSrcGenitive',
            'name'          => 'GetName',
            'genitive'      => 'GetGenitive',
            'menu_tree_name' => 'GetMenuTreeName',
            'url'           => 'GetUrl',
            'url_slug'      => 'GetUrlSlug',
            'description'   => 'GetDescription',
        ];

        $Fields = $ParentFields + $AddFields;
        return $Fields;
    }


    //TODO: DELETE! FOR DEV TEST ONLY!!!

    /*public function GetChildUrlNameList()
    {
        throw new \Exception();
    }

    function GetExternalChildsValues($Fields)
    {
        throw new \Exception();
    }

    function GetInternalChildsValues($Fields)
    {
        throw new \Exception();
    }*/

    abstract protected function GetPlaceholderCity();

    protected function GetUpdMonthByNum($num)
    {
        $Monthes = [
            'январе',
            'феврале',
            'марте',
            'апреле',
            'мае',
            'июне',
            'июле',
            'августе',
            'сентябре',
            'октябре',
            'ноябре',
            'декабре'
        ];
        return $Monthes[$num-1];
    }

    protected function ReplacePlaceholderCity($str)
    {
        $str = str_replace('{city}', $this->GetPlaceholderCity() , $str);
        return $str;
    }

    protected function ReplacePlaceholderYear($str)
    {
        $str = str_replace('{year}', date("Y"), $str);
        return $str;
    }

    protected function ReplacePlaceholders($str)
    {
        $str = $this->ReplacePlaceholderCity($str);
        $str = $this->ReplacePlaceholderYear($str);

        $UpdDate = strtotime('-3 months');

        $str = str_replace('{upd_month}', $this->GetUpdMonthByNum(date("n")), $str);
        $str = str_replace('{upd_year}', date("Y"), $str);

        $str = str_replace('{theme_path}', asset_path(), $str);

        return $str;
    }

    function GetMetaDescription()
    {
        if($this->MetaDescription === null)
        {
            $ret = $this->ReplacePlaceholderCity($this->ITemplate->meta_description);
            $this->MetaDescription = $ret;
        }
        else $ret = $this->MetaDescription;
        return $ret;
    }

    function GetMetaKeywords()
    {
        if($this->MetaKeywords === null)
        {
            $ret = $this->ReplacePlaceholderCity($this->ITemplate->meta_keywords);
            $this->MetaKeywords = $ret;
        }
        else $ret = $this->MetaKeywords;
        return $ret;
    }

    function GetTitle()
    {
        if($this->Title === null)
        {
            $ret = $this->ReplacePlaceholderCity($this->ITemplate->title);
            $this->Title = $ret;
        }
        else $ret = $this->Title;
        return $ret;
    }

    function GetDescription()
    {
        if($this->Description === null)
        {
            $Description =  $this->ReplacePlaceholders($this->ITemplate->description);
            $this->Description = $Description;
        }
        else $Description = $this->Description;
        return $Description;
    }
}