<?php namespace App\Models\PageEntity\StoreEntity\Base\Calculated;

use App\Models\PageEntity\Base\Parent\IParentContext;
use App\Models\PageEntity\Base\Type\IPageEntityTypeContext;
use App\Models\PageEntity\Base\UrlSluged\IUrlSlugContext;
use App\Models\PageEntity\Base\Calculated\Template\IPageEntityTemplateContext;

interface ICalculatedStoreEntityContext extends IPageEntityTemplateContext, IParentContext, IPageEntityTypeContext, IUrlSlugContext{

} 