<?php namespace App\Models\PageEntity\StoreEntity\Base\Calculated\Root;

use App\Models\PageEntity\Base\Calculated\Part\IPageEntityPartContext;
use App\Models\PageEntity\StoreEntity\Base\Calculated\ICalculatedStoreEntityContext;

interface IRootCalculatedStoreEntityContext extends ICalculatedStoreEntityContext, IPageEntityPartContext {

} 