<?php namespace App\Models\PageEntity\StoreEntity\Base\Calculated\Root;

use App\Models\Base\Interfaces\Repository\GetUrlNameList\IGetRootUrlNameList;
use App\Models\PageEntity\StoreEntity\Base\Calculated\ICalculatedStoreEntityInnerRepository;

interface IRootCalculatedStoreEntityInnerRepository extends ICalculatedStoreEntityInnerRepository, IGetRootUrlNameList{

} 