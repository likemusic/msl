<?php namespace App\Models\PageEntity\StoreEntity\Base\Calculated\Root;

use App\Models\PageEntity\Base\Calculated\Part\IPageEntityPart;
use App\Models\PageEntity\Base\Calculated\Part\TPageEntityPartContext;
use App\Models\PageEntity\Base\Calculated\Template\IPageEntityTemplate;
use App\Models\PageEntity\Base\Calculated\Template\TPageEntityTemplateContext;
use App\Models\PageEntity\Base\Parent\TParentContext;
use App\Models\PageEntity\Base\UrlSluged\TUrlSlugContext;
use App\Models\PageEntity\Base\Type\IPageEntityType;
use App\Models\PageEntity\Base\Type\TPageEntityTypeContext;
use App\Models\PageEntity\IPageEntity;

class RootCalculatedStoreEntityContext implements IRootCalculatedStoreEntityContext{
    use TPageEntityPartContext;
    use TPageEntityTypeContext;
    use TPageEntityTemplateContext;
    use TParentContext;
    use TUrlSlugContext;

    function __construct( IPageEntityPart $ICategoryPart, IPageEntityType $IVariety, IPageEntityTemplate $ICategoryTemplate, $UrlSlug, IPageEntity $IParentCalculatedCategory=null)
    {
        $this->IPartContext = $ICategoryPart;
        $this->IVarietyContext = $IVariety;
        $this->ITemplateContext = $ICategoryTemplate;
        $this->IParentContext = $IParentCalculatedCategory;
        $this->UrlSlugContext = $UrlSlug;
    }

} 