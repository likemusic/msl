<?php namespace App\Models\PageEntity\StoreEntity\Base\Calculated\Root;

use App\Models\Base\Traits\RepositoryItem\GetChildsValues\TGetChildCategoriesUrlNameList;
use App\Models\Base\Traits\RepositoryItem\UrlNameList\TGetInternalChildsUrlNameList;
use App\Models\PageEntity\StoreEntity\Base\Calculated\CalculatedStoreEntity;

abstract class RootCalculatedStoreEntity extends CalculatedStoreEntity implements IRootCalculatedStoreEntity{
    use TGetInternalChildsUrlNameList;
    use TGetChildCategoriesUrlNameList;
}