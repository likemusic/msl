<?php namespace App\Models\PageEntity\StoreEntity\Base\Calculated\Root;

use App\Models\PageEntity\StoreEntity\Base\Calculated\CalculatedStoreEntityInnerRepository;

abstract class RootCalculatedStoreEntityInnerRepository extends CalculatedStoreEntityInnerRepository implements IRootCalculatedStoreEntityInnerRepository  {

} 