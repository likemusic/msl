<?php namespace App\Models\PageEntity\StoreEntity\Base\Calculated;

use App\Models\Base\Interfaces\Repository\IRepository;
use App\Models\Base\Interfaces\Repository\IRepositoryEntity;
use App\Models\Base\Traits\GetByUrlSlug\TGetChildByUrlSlug;
use App\Models\Base\Traits\Repository\GetByUrlSlug\TGetChildByUrlSlugForInternalEntity;
use App\Models\Base\Traits\Repository\GetChilds\TGetExternalChildsByInternalEntity;
use App\Models\Base\Traits\Repository\GetChildValues\ByEntity\TGetExternalChildsValuesByInternalEntity;
use App\Models\Base\Traits\Repository\GetChildValues\ByEntity\TGetInternalChildsValuesByExternalEntity;
use App\Models\Base\Traits\Repository\GetChildValues\TEntityListToValuesList;
use App\Models\Base\Traits\Repository\GetUrlNameList\TCalculatedListToUrlNameList;
use App\Models\Base\Traits\Repository\TGetChildUrlNameListByEntity;
use App\Models\Base\Traits\Repository\TGetExternalChildByUrlSlugForInternalEntity;
use App\Models\PageEntity\Base\Calculated\ICalculatedPageEntity;
use App\Models\PageEntity\Base\Calculated\Template\IPageEntityTemplate;

abstract class CalculatedStoreEntityInnerRepository implements ICalculatedStoreEntityInnerRepository {
    use
        TGetChildByUrlSlug,
        TGetChildByUrlSlugForInternalEntity,
        TGetExternalChildByUrlSlugForInternalEntity,

        TGetChildUrlNameListByEntity,
        TGetExternalChildsValuesByInternalEntity,
        TCalculatedListToUrlNameList,
        TGetInternalChildsValuesByExternalEntity,
        TEntityListToValuesList,
        TGetExternalChildsByInternalEntity;

    /**
     * @var string - Имя класса для создаваемых элементов
     */
    protected $NewItemClassName;
    /**
     * @var string - Имя класса контекста передаваемого в конструктор класса нового элемента
     */
    protected $NewItemContextClassName;

    /**
     * @var IRepository
     */
    protected $ITemplateRepository;

    /**
     * @var IRepository
     */
    protected $IChildRepository;

    function GetExternalChildsUrlNameListByInternalEntity(IRepositoryEntity $entity = null)
    {
        if(!$this->IChildRepository) return [];
        $ExternalChilds = $this->IChildRepository->GetInternalChildsUrlNameListByExternalEntity($entity);
        return $ExternalChilds;
    }

    protected function GetSearchStringByUrlAndParentEntity($url, $ParentEntity)
    {
        //TODO: доделать
        $SearchString = $url;
        return $SearchString;
    }

    protected function TemplateToCalculated(IPageEntityTemplate $ITemplate, ICalculatedPageEntity $ParentEntity, $url=null, $type=null)
    {
        $CalculatedProduct = $this->NewRepositoryItem($ITemplate, $ParentEntity, $url, $type);
        return $CalculatedProduct;
    }

    protected function TemplatesListToCalculatedList($TemplatesList, $ParentEntity, $url=null)
    {
        $ret =[];
        foreach ($TemplatesList as $Template)
        {
            $ret[] = $this->TemplateToCalculated($Template, $ParentEntity, $url);
        }
        return $ret;
    }

    function NewRepositoryItem(IPageEntityTemplate $ITemplate, ICalculatedPageEntity $ParentEntity, $url )
    {
        $Context    = new $this->NewItemContextClassName($ITemplate, $ParentEntity, $url);
        $ret        = new $this->NewItemClassName($Context, $this);
        return $ret;
    }
}