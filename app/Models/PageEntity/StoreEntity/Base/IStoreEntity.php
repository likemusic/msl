<?php namespace App\Models\PageEntity\StoreEntity\Base;

use App\Models\Base\Interfaces\RepositoryItem\GetChilds\IGetExternalChilds;
use App\Models\Base\Interfaces\RepositoryItem\GetChildsValues\ByEntity\IGetExternalChildsValues;
use App\Models\Base\Interfaces\RepositoryItem\GetUrlNameList\IGetChildUrlNameList;
use App\Models\Base\Interfaces\RepositoryItem\IGetBreadcrumbs;
use App\Models\Base\Interfaces\RepositoryItem\IGetGenitive;
use App\Models\Base\Interfaces\RepositoryItem\IGetName;
use App\Models\Base\Interfaces\RepositoryItem\IGetParent;
use App\Models\Base\Interfaces\RepositoryItem\IGetUrl;
use App\Models\Base\Interfaces\RepositoryItem\IGetUrlSlug;
use App\Models\PageEntity\Base\UrlLinked\IUrlLinkedEntity;
use App\Models\PageEntity\IPageEntity;

interface IStoreEntity extends IPageEntity, IUrlLinkedEntity, IGetBreadcrumbs,
    IGetUrlSlug, IGetUrl,
    IGetParent,
    IGetChildUrlNameList,
    IGetExternalChildsValues,
    IGetName,IGetGenitive,
    IGetExternalChilds{

    function GetGenitive();
    function GetDescription();

    function GetMainPicture();

    function GetMenuTreeName();//название показываемое в дереве категорий
}