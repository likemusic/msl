<?php namespace App\Models\PageEntity\StoreEntity\Base\Inner\Root;

use App\Models\Base\Interfaces\Repository\GetUrlNameList\ByEntity\IGetInternalChildsUrlNameListByInternalEntity;
use App\Models\PageEntity\StoreEntity\Base\Inner\IStoreEntityInnerRepository;

interface IRootStoreEntityInnerRepository extends IStoreEntityInnerRepository, IGetInternalChildsUrlNameListByInternalEntity{

}