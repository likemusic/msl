<?php namespace App\Models\PageEntity\StoreEntity\Base\Inner\NotRoot;

use App\Models\PageEntity\StoreEntity\Base\Inner\IStoreEntityInnerRepository;

interface INotRootStoreEntityInnerRepository extends IStoreEntityInnerRepository {

} 