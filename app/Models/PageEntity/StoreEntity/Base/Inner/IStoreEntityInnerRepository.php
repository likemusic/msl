<?php namespace App\Models\PageEntity\StoreEntity\Base\Inner;

use App\Models\Base\Interfaces\Repository\GetChilds\IGetInternalChildsByExternalEntity;
use App\Models\Base\Interfaces\Repository\GetChildsValues\ByEntity\IGetExternalChildsValuesByInternalEntity;
use App\Models\Base\Interfaces\Repository\GetChildsValues\ByEntity\IGetInternalChildsValuesByExternalEntity;
use App\Models\PageEntity\Base\UrlLinked\IUrlLinkedEntityRepository;
use App\Models\Base\Interfaces\Repository\GetUrlNameList\ByEntity\IGetChildUrlNameListByEntity;
use App\Models\Base\Interfaces\Repository\GetUrlNameList\ByEntity\IGetExternalChildsUrlNameListByInternalEntity;
use App\Models\Base\Interfaces\Repository\GetUrlNameList\ByEntity\IGetInternalChildsUrlNameListByExternalEntity;

interface IStoreEntityInnerRepository extends
    IUrlLinkedEntityRepository/*, /*IGetStoreTree,*/,
    IGetChildUrlNameListByEntity,
    IGetInternalChildsUrlNameListByExternalEntity,
    IGetExternalChildsUrlNameListByInternalEntity,

    IGetInternalChildsValuesByExternalEntity,
    IGetExternalChildsValuesByInternalEntity,

    IGetInternalChildsByExternalEntity
{

}
