<?php namespace App\Models\PageEntity\StoreEntity\Base\StoreEntityGood;

use App\Models\Base\Interfaces\RepositoryItem\IGetPrice;
use App\Models\PageEntity\StoreEntity\Base\IStoreEntity;

interface IStoreEntityGood extends IStoreEntity, IGetPrice{
    function GetPicturesDir();
    function GetPictures();
} 