<?php namespace App\Models\PageEntity\StoreEntity\Base\StoreEntityGood;

use App\Models\Base\Interfaces\Repository\IRepositoryEntity;
use App\Models\Base\Traits\RepositoryItem\TGetBuyButton;
use App\Models\PageEntity\StoreEntity\Base\Calculated\NotRoot\NotRootCalculatedStoreEntity;

abstract class StoreEntityGood extends NotRootCalculatedStoreEntity implements IStoreEntityGood{
    use TGetBuyButton;

    function GetKnowFields()
    {
        $ParentFields = parent::GetKnowFields();
        $AddFields = [
            'breadcrumbs'   => 'GetBreadcrumbs',
            'price'         => 'GetPrice',

            'pictures'      => 'GetPictures',
            'main_picture'  => 'GetMainPicture',

            'buy_button'    => 'GetBuyButton',
            'productJson'   => 'GetProductJson',
            //TODO: добавить страницу "Другие способы оплаты"
            //'other_payment_method_url' => $this->GetOtherPaymentMethodUrl()
        ];

        $Fields = $ParentFields + $AddFields;
        return $Fields;
    }

    public function GetProductJson(){
        $productJson = array(
            'id'=>$this->GetVID(),
            'name'=> $this->GetName(),
            'price'=> $this->GetPrice()
        );
        $productJson =  json_encode($productJson, JSON_UNESCAPED_UNICODE);//json_encode_win1251
        return $productJson;
    }

    public  function GetMenuTreeName()
    {
        return $this->GetGenitive();
    }

}