<?php namespace App\Models\PageEntity\StoreEntity\Product\Template;

use App\Models\PageEntity\Base\Calculated\Template\IPageEntityTemplateRepository;

interface IProductTemplateRepository extends IPageEntityTemplateRepository{

}