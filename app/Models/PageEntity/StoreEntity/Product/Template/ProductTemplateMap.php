<?php namespace App\Models\PageEntity\StoreEntity\Product\Template;

use Analogue\ORM\EntityMap;
use App\Models\PageEntity\StoreEntity\Product\Type\ProductType;
use App\Models\PageEntity\StoreEntity\SubProduct\Template\SubProductTemplate;

class ProductTemplateMap extends EntityMap{

    protected $with = ['ProductType'];

    /*function __construct()
    {
        //удаляем из ранней загрузки кэшируемые свойства
        $this->DeteteCachedEaglerLoadRelations();
    }*/

    //удаляем из ранней загрузки кэшируемые свойства
    /*protected function DeteteCachedEaglerLoadRelations()
    {
        $CacheKey = 'cache.components.Product.ProductTemplate.ProductType';
        $UseCache = config($CacheKey);
        if($UseCache) $this->with = [];
    }*/

    /*public function subproductTemplates(ProductTemplate $ProductTemplate)
    {
            return $this->belongsToMany($ProductTemplate, SubProductTemplate::class,null, null, 'subproduct_template_id');
    }*/

    public function ProductType(ProductTemplate $ProductTemplate)
    {
        return $this->belongsTo($ProductTemplate, ProductType::class);
    }
}