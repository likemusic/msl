<?php namespace App\Models\PageEntity\StoreEntity\Product\Template;

use App\Models\PageEntity\Base\Calculated\Template\Db\PageEntityTemplateDb;
use Illuminate\Support\Facades\Cache;

/**
 * @property mixed subProductsIds
 */
class ProductTemplate extends PageEntityTemplateDb implements IProductTemplate{

    /*public function __construct()
    {
        parent::__construct();

        //Устанавливаем кэшируемые early loaded св-ва
        //$this->setEntityAttribute('ProductType',$this->GetCachedProductType());
    }*/

    public function GetChildEntityTemplateBySearchString($searchString)
    {
        return $this->subproductTemplatesBySearchString($searchString)->first();
    }

    /*public function GetProductType()
    {
        $UseCache = true;
        $ret = (!$UseCache) ? $this->ProductType : $this->GetCachedProductType();
        return $ret;
    }*/

    /*protected function GetCachedProductType()
    {
        $CacheId = 'ProductTemplate->ProductType-'.$this->type_id;
        $ret = Cache::rememberForever($CacheId,function(){
            return $this->ProductType;
        });
        return $ret;
    }*/
}
