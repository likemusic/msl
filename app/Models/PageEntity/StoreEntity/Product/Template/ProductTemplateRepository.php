<?php namespace App\Models\PageEntity\StoreEntity\Product\Template;

use App\Helpers\CacheNamer\ICacheNamer;
use App\Models\PageEntity\Base\Calculated\Template\Db\PageEntityTemplateDbRepository;
use Illuminate\Support\Facades\Cache;

class ProductTemplateRepository extends PageEntityTemplateDbRepository implements IProductTemplateRepository{
    protected $ItemClassName = ProductTemplate::class;
    /**
     * @var ICacheNamer $ICacheNamer
     */
    protected $ICacheNamer;

    public function __construct(ICacheNamer $ICacheNamer)
    {
        parent::__construct();
        $this->ICacheNamer = $ICacheNamer;
    }

    function GetByProductSetIdAndProductTypeIdAndScale($ProductSetId, $TypeId, $Scale)
    {
        $CacheId = $this->ICacheNamer->ProductTemplateByProductSetIdAndProductTypeIdAndScale($ProductSetId, $TypeId, $Scale);
        $ret = Cache::rememberForever($CacheId,
            function()use($ProductSetId, $TypeId, $Scale){
                return $this->GetByProductSetIdAndProductTypeIdAndScaleReal($ProductSetId, $TypeId, $Scale);});
        return $ret;
    }

    protected function GetByProductSetIdAndProductTypeIdAndScaleReal($ProductSetId, $TypeId, $Scale)
    {
        $ret = $this->mapper
            ->join('product_sets_product_templates','product_templates.id','=','product_sets_product_templates.product_template_id')
            ->where('product_sets_product_templates.product_set_id',$ProductSetId)
            ->where('product_type_id',$TypeId)
            ->where('scale',$Scale)
            ->select('product_templates.*','product_templates.id');
        $ret = $ret->first();
        //TODO: add name based on helpers
        return $ret;
    }
}