<?php namespace App\Models\PageEntity\StoreEntity\Product\Template;

use App\Models\PageEntity\Base\Calculated\Template\IGetChildEntityTemplateBySearchString;
use App\Models\PageEntity\Base\Calculated\Template\IPageEntityTemplate;

interface IProductTemplate extends IPageEntityTemplate, IGetChildEntityTemplateBySearchString {

}