<?php namespace App\Models\PageEntity\StoreEntity\Product\Independent;

use App\Models\PageEntity\StoreEntity\Base\Calculated\NotRoot\INotRootCalculatedStoreEntityInnerRepository;
use App\Models\PageEntity\StoreEntity\Product\IProductRepository;

interface IIndependentProductRepository  extends IProductRepository, INotRootCalculatedStoreEntityInnerRepository{

} 