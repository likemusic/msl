<?php namespace App\Models\PageEntity\StoreEntity\Product\Independent;

use App\Models\PageEntity\StoreEntity\Base\Calculated\ICalculatedStoreEntityContext;

interface IIndependentProductContext extends ICalculatedStoreEntityContext {

}
