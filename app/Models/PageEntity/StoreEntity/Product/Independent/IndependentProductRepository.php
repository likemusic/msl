<?php namespace App\Models\PageEntity\StoreEntity\Product\Independent;

use App\Helpers\StoreTreeBuilder\IStoreTreeBuilder;
use App\Models\Base\Interfaces\Repository\IRepositoryEntity;
use App\Models\Base\Traits\GetByUrlSlug\TGetChildByUrlSlug;
use App\Models\Base\Traits\Repository\GetChildValues\ByEntity\TGetExternalChildsValuesByInternalEntity;
use App\Models\Base\Traits\Repository\GetChildValues\ByEntity\TGetInternalChildsValuesByExternalEntity;
use App\Models\Base\Traits\Repository\GetChildValues\TEntityListToValuesList;
use App\Models\Base\Traits\Repository\GetUrlNameList\TCalculatedListToUrlNameList;
use App\Models\Base\Traits\Repository\GetUrlNameList\TGetInternalChildsUrlNameListByExternalEntity;
use App\Models\Base\Traits\Repository\TGetChildUrlNameListByEntity;
use App\Models\PageEntity\StoreEntity\Base\Calculated\NotRoot\NotRootCalculatedStoreEntityInnerRepository;
use App\Models\PageEntity\StoreEntity\Category\ICategoryRepository;
use App\Models\PageEntity\StoreEntity\Product\Part\IProductPartRepository;
use App\Helpers\PaymentGateway\Base\BuyButton\IRequestDataProvider as IPaymentDataProvider;

class IndependentProductRepository /*extends NotRootCalculatedStoreEntityInnerRepository*/ implements IIndependentProductRepository{
    use TGetChildByUrlSlug,
        TGetChildUrlNameListByEntity,
        TGetExternalChildsValuesByInternalEntity,
        TGetInternalChildsUrlNameListByExternalEntity,
        TCalculatedListToUrlNameList,
        TGetInternalChildsValuesByExternalEntity,
        TEntityListToValuesList;

    protected $NewItemClassName = IndependentProduct::class;
    protected $NewItemContextClassName = IndependentProductContext::class;

    /**
     * @var IStoreTreeBuilder
     */
    protected $IStoreTreeBuilder;

    /**
     * @var IPaymentDataProvider
     */
    protected $IPaymentRequestDataProvider;

    /**
     * @var IProductPartRepository
     */
    protected $IPartRepository;
    function __construct(
        IProductPartRepository $IPartRepository, IPaymentDataProvider $IPaymentRequestDataProvider)
    {
        $this->IPartRepository = $IPartRepository;
        $this->IPaymentRequestDataProvider = $IPaymentRequestDataProvider;
    }


    //-- Child Values

    /*function GetInternalChildsValuesByExternalEntity(IRepositoryEntity $entity, $Fields=null)
    {
        $EntityList = $this->GetInternalChildsByExternalEntity($entity);

        /*$CalculatedCategoryContext = $entity->GetContext();
        $CategoryPart = $CalculatedCategoryContext->GetPartContext();
        $ProductParts = $CategoryPart->product_parts;

        $IndependentProductsValues = $this->ProductPartsToIndependentProductsValues($ProductParts, $entity, $Fields);

        return $IndependentProductsValues;
    }*/

    function GetInternalChildsByExternalEntity(IRepositoryEntity $Entity = null)
    {
        $CalculatedCategoryContext = $Entity->GetContext();
        $CategoryPart = $CalculatedCategoryContext->GetPartContext();
        $ProductParts = $this->IPartRepository->GetInternalChildsByExternalEntity($CategoryPart);
        //$CategoryPart->product_parts;

        $IndependentProducts = $this->ProductPartsToIndependentProducts($ProductParts, $Entity);
        return $IndependentProducts;
    }

    protected function ProductPartsToIndependentProductsValues($ProductParts, $entity, $Fields)
    {
        $IndependentProducts = $this->ProductPartsToIndependentProducts($ProductParts, $entity);
        if(!$IndependentProducts) return null;

        $IndependentProductsValues = $this->IndependentProductsToValues($IndependentProducts, $Fields);
        return $IndependentProductsValues;
        //$Values = $IndependentProduct->GetValues($Fields);
    }

    protected function ProductPartsToIndependentProducts($ProductParts, $entity)
    {
        $ret = [];
        foreach($ProductParts as $ProductPart)
        {
            $ret []= $this->ProductPartToIndependentProduct($ProductPart, $entity );
        }
        return $ret;
    }

    protected function ProductPartToIndependentProduct($ProductPart, $entity)
    {
        $ret = new $this->NewItemClassName($ProductPart,$entity, $this->IPaymentRequestDataProvider);
        return $ret;
    }

    protected function IndependentProductsToValues($IndependentProducts, $Fields)
    {
        $ret = [];
        foreach($IndependentProducts as $IndependentProduct)
        {
            $ret []= $IndependentProduct->GetValues($Fields);
        }
        return $ret;
    }

    //-- Childs UrlName List
    function GetExternalChildsUrlNameListByInternalEntity(IRepositoryEntity $entity)
    {
        //У старинных нету подтоваров
        return null;
    }


    //-- By UrlSlug

    /**
     * @param $url
     * @param $ParentEntity
     * @return \App\Models\Base\Interfaces\Repository\GetByUrlSlug\IRepositoryEntity
     *
     * Возвращает по url дочерний элемент для элемента внешней коллекции
     */
    function GetInternalChildByUrlSlugForExternalEntity($url, $ParentEntity)
    {
        $ProductPart = $this->GetProductPartByUrlSlug($url, $ParentEntity);
        if(!$ProductPart) return null;
        $IndependentProduct = $this->ProductPartToIndependentProduct($ProductPart, $ParentEntity);
        return $IndependentProduct;
    }

    function GetProductPartByUrlSlug($url, $ParentEntity)
    {
        $ret = $this->IPartRepository->GetInternalChildByUrlSlugForInternalEntity($url, $ParentEntity);
        return $ret;
    }

    //TODO: копипасту между Independent и Calculated Product(and repositories) объединить в трейт или общий класс
    function GetByVID($Id)
    {
        $Chunks = explode('-',$Id);
        $count = count($Chunks);
        if($count != 2) throw new \Exception('Invalid product VID:'.$Id);
        $ParentId = $Chunks[0];

        //TODO: inplement by injection (without recursive)
        $ICategoryRepository = app(ICategoryRepository::class);
        $ParentEntity = $ICategoryRepository->GetByVID($ParentId);

        $ProductChunks = $Chunks[1];
        $ProductChunks = explode(':', $ProductChunks);
        if(count($ProductChunks)!=2) throw new \Exception('Invalid CalculatedProduct vid'.$Id);
        list($PartId, $TemplateId) = $ProductChunks;

        if((!$PartId) or ($TemplateId)) return null;
            //throw new \Exception('Invalid independent product vid:'.$Id);

        $IPart = $this->IPartRepository->GetById($PartId);
        $ret = $this->ProductPartToIndependentProduct($IPart,$ParentEntity);
        return $ret;
    }

    function GetByFullVID($Id)
    {
        $Chunks = explode('-',$Id);
        $count = count($Chunks);
        if($count != 2) throw new \Exception('Invalid independent product VID:'.$Id);
        $ParentId = $Chunks[0];

        //TODO: inplement by injection (without recursive)
        $ICategoryRepository = app(ICategoryRepository::class);
        $ParentEntity = $ICategoryRepository->GetByFullVID($ParentId);

        $ProductChunks = $Chunks[1];
        $ProductChunks = explode(':', $ProductChunks);
        if(count($ProductChunks)!=4) throw new \Exception("Invalid IndependentProduct Id:".$Id);

        list($PartId, $SetId, $TemplateId, $TypeId) = $ProductChunks;

        if((!$PartId) or ($TemplateId) or ($SetId) or ($TypeId)) return null;
            //throw new \Exception('Invalid calculated product FullVid:'.$Id);

        //TODO: сдлеать с проверкой всех параметров
        $IPart = $this->IPartRepository->GetById($TemplateId);
        $ret = $this->ProductPartToIndependentProduct($IPart,$ParentEntity);
        return $ret;
    }

}