<?php namespace App\Models\PageEntity\StoreEntity\Product\Independent;

use App\Models\Base\Interfaces\Repository\IRepositoryEntity;
use App\Models\Base\Traits\RepositoryItem\TPartBasedCalculatedStoreTreeItem;
use App\Models\PageEntity\StoreEntity\Base\StoreEntityGood\StoreEntityGood;
use App\Models\PageEntity\StoreEntity\Category\ICategory;
use App\Models\PageEntity\StoreEntity\Product\Part\IProductPart;
use App\Helpers\PaymentGateway\Base\BuyButton\IRequestDataProvider as IPaymentDataProvider;
use App\Models\PageEntity\StoreEntity\Product\ProductPicture\ProductPicture;

class IndependentProduct extends StoreEntityGood implements IIndependentProduct{
    use TPartBasedCalculatedStoreTreeItem;

    /**
     * @var IProductPart
     */
    protected $IPart;

    /**
     * @var ICategory
     */
    protected $IParent;

    protected $Price;
    protected $Name;
    protected $PicturesDir;
    protected $Pictures;
    protected $MainPicture;
    protected $FullVID;
    protected $VID;

    //TODO: сделать кнструктор через Context по аналогии с другими Calculated
    public function __construct(IProductPart $IProductPart, ICategory $ICategory, IPaymentDataProvider $IPaymentRequestDataProvider)
    {
        $this->IPart = $IProductPart;
        $this->IParent = $ICategory;
        $this->IPaymentRequestDataProvider = $IPaymentRequestDataProvider;
    }

    public function GetPrice()
    {
        if($this->Price === null)
        {
            $this->Price = (float) $this->IPart->price;
        }
        return $this->Price;
    }

    function GetName()
    {
        if(!$this->Name)
        {
            $this->Name = $this->IPart->name;
        }
        return $this->Name;
    }

    function GetPicturesDir()
    {
        if($this->PicturesDir === null)
        {
            $this->PicturesDir = '/data/pictures';
        }
        //TODO: доделать
        return $this->PicturesDir;
    }

    function GetPictures()
    {
        if($this->Pictures === null)
        {
            $ProductPictures = $this->IPart->pictures;
            $ProductPictures = $ProductPictures->all();
            $this->Pictures = array_map([$this,'ProductPictureToScaledPicture'], $ProductPictures);
        }
        return $this->Pictures;
    }

    function GetMainPicture()
    {
        if($this->MainPicture === null)
        {
            $ProductPicture = $this->IPart->product_picture;
            $Image = $ProductPicture->enlarged;

            $PicturesDir = $this->GetPicturesDir();
            $this->MainPicture = $PicturesDir.'/main_pic/'.$Image;
        }
        return $this->MainPicture;
        //TODO: доделать
    }

    function ProductPictureToScaledPicture($ProductPicture)
    {
        $PicturesDir = $this->GetPicturesDir();
        $Enlargged = $ProductPicture->enlarged;
        $ret = [
            'full'  => "{$PicturesDir}/{$Enlargged}",
            'big'   => "{$PicturesDir}/main_pic/{$Enlargged}",
            'small' => "{$PicturesDir}/lmenu/{$Enlargged}"
        ];

        return $ret;
    }

    function GetMetaDescription()
    {
        if($this->MetaDescription === null)
        {
            $Template = "Подробная старинная карта {genitive}";
            $this->MetaDescription = $this->ReplacePlaceholders($Template);
        }
        return $this->MetaDescription;
    }

    function ReplacePlaceholders($str)
    {
        $str = parent::ReplacePlaceholders($str);
        $str = str_replace('{genitive}',$this->IPart->genitive,$str);
        return $str;
    }

    function GetMetaKeywords()
    {
        if($this->MetaKeywords === null)
        {
            $this->MetaKeywords = $this->GetMetaDescription();
        }
        return $this->MetaKeywords;
    }

    function GetTitle()
    {
        if($this->Title === null)
        {
            $this->Title = $this->GetName();
        }
        return $this->Title;
    }

    function GetDescription()
    {
        if($this->Description)
        {
            $this->Description =  $this->ReplacePlaceholders($this->IPart->description);
        }
        return $this->Description;
    }

    protected function GetPlaceholderCity()
    {
        return $this->IPart->genitive;
    }

    function GetFullVID()
    {
        if($this->FullVID === null)
        {
            $ParentVID = $this->IParent->GetFullVID();
            $CalcTypeId = 2;
            $PartId = $this->IPart->GetId();
            $SetId = $TemplateId = $TypeId = null;
            $VID = "{$ParentVID}-{$PartId}:{$SetId}:{$TemplateId}:{$TypeId}";
            $this->FullVID = $VID;
        }
        return $this->FullVID;
    }

    function GetVID()
    {
        if($this->VID === null)
        {
            $ParentVID = $this->IParent->GetVID();
            $PartId = $this->IPart->GetId();
            $TemplateId = null;
            $VID = "{$ParentVID}-{$PartId}:{$TemplateId}";
            $this->VID = $VID;
        }
        return $this->VID;
    }
}