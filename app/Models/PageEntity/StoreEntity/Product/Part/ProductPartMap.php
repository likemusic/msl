<?php namespace App\Models\PageEntity\StoreEntity\Product\Part;

use Analogue\ORM\EntityMap;
use App\Helpers\TableNamesCatalog\ITableNamesCatalog;
use App\Models\PageEntity\StoreEntity\Product\ProductPicture\ProductPicture;

class ProductPartMap extends EntityMap{

    protected $with = ['product_picture'];

    //TODO: impliment injection
    function __construct()
    {
        $tableNamesCatalog = app(ITableNamesCatalog::class);
        $this->table = $tableNamesCatalog->GetProductPartsTableName();
    }

    function product_picture(ProductPart $productPart)
    {
        return $this->belongsTo($productPart, ProductPicture::class);
    }

    function pictures(ProductPart $productPart)
    {
        return $this->hasMany($productPart, ProductPicture::class, 'product_id');
    }
} 