<?php namespace App\Models\PageEntity\StoreEntity\Product\Part;

use App\Models\Base\Interfaces\RepositoryItem\IGetUrlSlug;
use App\Models\PageEntity\Base\Calculated\Part\IPageEntityPart;

interface IProductPart extends IPageEntityPart, IGetUrlSlug{

} 