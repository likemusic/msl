<?php namespace App\Models\PageEntity\StoreEntity\Product\Part;


use App\Models\Base\Interfaces\Repository\IRepositoryEntity;
use App\Models\PageEntity\Base\Calculated\Part\Db\PageEntityPartDbRepository;

class ProductPartRepository extends PageEntityPartDbRepository implements IProductPartRepository{
    protected $ItemClassName = ProductPart::class;

    protected $InternalChildsByExternalEntity = [];

    function GetInternalChildsValuesByExternalEntity(IRepositoryEntity $entity, $Fields = null)
    {
        $ret = $this->GetInternalChildsByExternalEntity($entity);
        $ret = array_intersect_key($ret, array_fill_keys($Fields,null));
        return $ret;
    }

    function GetInternalChildsByExternalEntity(IRepositoryEntity $entity)
    {
        $ParentEntityId = $entity->id;

        if(!array_key_exists($ParentEntityId,$this->InternalChildsByExternalEntity))
        {
            $ret = $this->mapper->where(['id'=>$entity->id])->orderBy('sort_order')->orderBy('id')->get();
            $this->InternalChildsByExternalEntity[$ParentEntityId] = $ret;
        }
        return $this->InternalChildsByExternalEntity[$ParentEntityId];
    }

    /**
     * @param $url
     * @param ICategoryPart $ParentEntity
     * @return mixed
     */
    function GetInternalChildByUrlSlugForInternalEntity($url, $ParentEntity)
    {
        //TODO: исправить после установки url_slug
        return $this->GetInternalChildByUrlForInternalEntity($url, $ParentEntity);
    }

    protected function GetInternalChildByUrlForInternalEntity($url, $ParentEntity)
    {
        $res = $this->mapper->where('url','=',$url);//TODO: fill db and fix
        $res = $res->where('category_id','=', $ParentEntity->GetContext()->GetPartContext()->id);
        $ret = $res->first();
        return $ret;
    }
}