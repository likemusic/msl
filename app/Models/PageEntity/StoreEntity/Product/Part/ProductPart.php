<?php namespace App\Models\PageEntity\StoreEntity\Product\Part;


use App\Models\Base\Traits\RepositoryItem\TGetUrlSlugSimple;
use App\Models\Base\Traits\RepositoryItem\TGetName;
use App\Models\PageEntity\Base\Calculated\Part\Db\PageEntityPartDb;

class ProductPart  extends PageEntityPartDb implements IProductPart{
    use TGetName/*, TGetUrlSlugSimple*/;

    function GetUrlSlug()
    {
        return $this->url;
    }
} 