<?php namespace App\Models\PageEntity\StoreEntity\Product\Part;


use App\Models\Base\Interfaces\Repository\GetByUrlSlug\IGetInternalChildByUrlSlugForInternalEntity;
use App\Models\Base\Interfaces\Repository\GetChildsValues\ByEntity\IGetInternalChildsValuesByExternalEntity;
use App\Models\PageEntity\Base\Calculated\Part\IPageEntityPartRepository;

interface IProductPartRepository extends IPageEntityPartRepository,
    IGetInternalChildsValuesByExternalEntity,
    IGetInternalChildByUrlSlugForInternalEntity
    {

} 