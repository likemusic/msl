<?php namespace App\Models\PageEntity\StoreEntity\Product\Set;

use App\Models\Base\Repository\Db\DbEntity;

class ProductSet extends DbEntity implements IProductSet {
    /**
     * @var ProductSetRepository
     */
    protected $ISelfRepository;

    function __construct(ProductSetRepository $ISelfRepository)
    {
        $this->ISelfRepository = $ISelfRepository;
    }

    public function GetChildEntityTemplateBySearchString($searchString)
    {
        return $this->productTemplatesBySearchString($searchString)->first();
    }

    public function GetProductTemplateByProductTypeIdAndScale($ProductTypeId, $Scale)
    {
        $ret = $this->ISelfRepository->GetProductTemplateByProductTypeIdAndScale($ProductTypeId, $Scale);
        return $ret;
        //return $this->productTemplateByProductTypeIdAndScale($ProductTypeId, $Scale)->first();
    }
} 