<?php namespace App\Models\PageEntity\StoreEntity\Product\Set;

use Analogue\ORM\EntityMap;
use App\Models\PageEntity\StoreEntity\Product\Template\ProductTemplate;

class ProductSetMap extends EntityMap{

    //protected $with = ['productTemplates'];

    public function productTemplates(ProductSet $productSet)
    {
        return $this->belongsToMany($productSet, ProductTemplate::class)->orderBy('sort_order')->orderBy('name');
    }

    public function productTemplatesBySearchString(ProductSet $productSet, $SearchString)
    {
        return $this->belongsToMany($productSet, ProductTemplate::class)->where('product_templates.url_slug', '=', $SearchString);
        //TODO: переименовать поле url в url_part или подобное в зависимости от требований
    }

    public function productTemplateByProductTypeIdAndScale(ProductSet $productSet, $ProductTypeId, $Scale=null)
    {
        return $this->belongsToMany($productSet, ProductTemplate::class)
            ->where('product_templates.product_type_id', '=', $ProductTypeId)->where('product_templates.scale','=',$Scale);
    }
}