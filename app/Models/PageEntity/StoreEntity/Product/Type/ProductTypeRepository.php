<?php namespace App\Models\PageEntity\StoreEntity\Product\Type;

use App\Helpers\CacheNamer\ICacheNamer;
use App\Models\PageEntity\Base\Type\Db\PageEntityTypeDbRepository;
use Illuminate\Support\Facades\Cache;

class ProductTypeRepository extends PageEntityTypeDbRepository  implements IProductTypeRepository {
    protected $ItemClassName = ProductType::class;

    /**
     * @var ICacheNamer
     */
    protected $ICacheNamer;

    public function __construct(ICacheNamer $ICacheNamer)
    {
        parent::__construct();
        $this->ICacheNamer = $ICacheNamer;
    }

    function GetByUrlSlugPart($UrlSlugPart)
    {
        $CacheId = $this->ICacheNamer->ProductTypeByUrlSlug($UrlSlugPart);
        $ret = Cache::rememberForever($CacheId,
            function()use($UrlSlugPart){
                return $this->GetByUrlSlugPartReal($UrlSlugPart);
            });
        return $ret;
    }

    function GetByUrlSlugPartReal($UrlSlugPart)
    {   //TODO:cache it
        return $this->firstMatching(['url_slug_part'=>$UrlSlugPart]);
    }
}