<?php namespace App\Models\PageEntity\StoreEntity\Product\Type;

use App\Models\PageEntity\Base\Type\IPageEntityTypeRepository;

interface IProductTypeRepository extends IPageEntityTypeRepository{
    function GetByUrlSlugPart($UrlSlugPart);
} 