<?php namespace App\Models\PageEntity\StoreEntity\Product;

use App\Models\Base\Interfaces\Repository\ILinkedRepository;
use App\Models\PageEntity\StoreEntity\Base\Inner\IStoreEntityInnerRepository;

interface IProductRepository extends IStoreEntityInnerRepository, ILinkedRepository {

}