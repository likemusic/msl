<?php namespace App\Models\PageEntity\StoreEntity\Product\Aggregator;

use App\Models\PageEntity\StoreEntity\Product\IProductRepository;

interface IProductAggregatorRepository extends IProductRepository{

} 