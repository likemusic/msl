<?php namespace App\Models\PageEntity\StoreEntity\Product\Aggregator;

use App\Models\Base\Interfaces\Repository\IRepositoryEntity;
use App\Models\PageEntity\StoreEntity\Product\Calculated\ICalculatedProductRepository;
use App\Models\PageEntity\StoreEntity\Product\Independent\IIndependentProductRepository;
use App\Models\PageEntity\StoreEntity\Product\IProductRepository;

class ProductAggregatorRepository implements IProductAggregatorRepository {

    /**
     * @var IProductRepository[]
     */
    protected $InnerRepositories;

    function __construct(ICalculatedProductRepository $ICalculatedProductRepository,
        IIndependentProductRepository $IIndependentProductRepository
    )
    {
        $this->InnerRepositories = [$ICalculatedProductRepository, $IIndependentProductRepository];
    }

    function GetChildByUrlSlug($url)
    {
        return $this->call(__FUNCTION__, func_get_args(),true);
    }

    function GetChildUrlNameListByEntity(IRepositoryEntity $Entity)
    {
        return $this->call(__FUNCTION__, func_get_args());
    }

    function GetExternalChildsUrlNameListByInternalEntity(IRepositoryEntity $entity)
    {
        return $this->call(__FUNCTION__, func_get_args());
    }

    function GetExternalChildsValuesByInternalEntity(IRepositoryEntity $entity, $Fields)
    {
        return $this->call(__FUNCTION__, func_get_args());
    }

    /**
     * @param $url
     * @param $ParentEntity
     * @return \App\Models\Base\Interfaces\Repository\GetByUrlSlug\IRepositoryEntity
     *
     * Возвращает по url дочерний элемент для элемента внешней коллекции
     */
    function GetInternalChildByUrlSlugForExternalEntity($url, $ParentEntity)
    {
        return $this->call(__FUNCTION__,func_get_args(), true);
    }

    function GetInternalChildsUrlNameListByExternalEntity(IRepositoryEntity $entity = null)
    {
        return $this->call(__FUNCTION__,func_get_args());
    }

    function GetInternalChildsValuesByExternalEntity(IRepositoryEntity $entity, $Fields = null)
    {
        return $this->call(__FUNCTION__,func_get_args());
    }

    function GetInternalChildsByExternalEntity(IRepositoryEntity $Entity = null, $Fields = null){
        return $this->call(__FUNCTION__,func_get_args());
    }

    function GetByVID($Id)
    {
        return $this->call(__FUNCTION__,func_get_args(),true);
    }

    function GetByFullVID($Id)
    {
        return $this->call(__FUNCTION__,func_get_args(),true);
    }


    protected function call($method, $arguments = [], $one=false) {
        $ret = [];
        foreach ($this->InnerRepositories as $InnerRepository) {
            $res = call_user_func_array([$InnerRepository, $method], $arguments);
            if($res and $one) return $res;
            if(is_array($res)) $ret = array_merge($ret, $res);
        }

        return $ret;
    }
} 