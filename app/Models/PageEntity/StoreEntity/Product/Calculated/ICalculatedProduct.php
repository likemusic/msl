<?php namespace App\Models\PageEntity\StoreEntity\Product\Calculated;

use App\Models\PageEntity\StoreEntity\Base\Calculated\NotRoot\INotRootCalculatedStoreEntity;
use App\Models\PageEntity\StoreEntity\Product\IProduct;

interface ICalculatedProduct extends IProduct, INotRootCalculatedStoreEntity{

}