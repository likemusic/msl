<?php namespace App\Models\PageEntity\StoreEntity\Product\Calculated;

use App\Helpers\PaymentGateway\Base\BuyButton\IRequestDataProvider as IPaymentDataProvider;
use App\Helpers\PicturesFinder\IPicturesFinder;
use App\Helpers\StoreTreeBuilder\IStoreTreeBuilder;
use App\Models\PageEntity\StoreEntity\Base\StoreEntityGood\StoreEntityGood;
use App\Models\PageEntity\StoreEntity\Product\Type\IProductType;
use Illuminate\Support\Facades\Log;

class CalculatedProduct extends StoreEntityGood implements ICalculatedProduct {
    //use TGetUrlSlugByTemplate;

    /**
     * @var IPicturesFinder
     */
    protected $IPicturesFinder;

    protected $Genitive;
    protected $Price;
    protected $Pictures;
    protected $MainPicture;
    protected $PicturesDir;
    protected $VID;
    protected $FullVID;
    protected $UrlSlug;

    protected $SrcGenitive;
    /**
     * @var IProductType
     */
    protected $IProductType;

    public function __construct(ICalculatedProductContext $ICalculatedProductContext,  ICalculatedProductRepository $ICalculatedProductRepository, IStoreTreeBuilder $IStoreTreeBuilder, IPaymentDataProvider $IRequestDataProvider, IPicturesFinder $IPicturesFinder, IProductType $IProductType=null)
    {
        parent::__construct($ICalculatedProductContext, $ICalculatedProductRepository, $IStoreTreeBuilder, $IRequestDataProvider );
        $this->IPicturesFinder = $IPicturesFinder;
        $this->IProductType = $IProductType;
    }

    function GetKnowFields()
    {
        $ParentFields = parent::GetKnowFields();
        $AddFields = [
            'outer_childs'  => 'GetExternalChildsUrlNameList',
        ];

        $Fields = $ParentFields + $AddFields;
        return $Fields;
    }

    function GetName()
    {
        if($this->Name === null) {
            $ParentCategoryContext = $this->IParent->GetContext();
            $CategoryPart = $ParentCategoryContext->GetPartContext();
            $ProductTemplate = $this->ITemplate;
            $Name = str_replace('{city}', $CategoryPart->GetGenitive(), $ProductTemplate->name);
            $this->Name = $Name;
        }
        else $Name = $this->Name;
        return $Name;
    }

    function GetSrcGenitive()
    {
        if($this->SrcGenitive === null)
        {
            $ParentCategoryContext = $this->IParent->GetContext();
            $CategoryPart = $ParentCategoryContext->GetPartContext();
            $SrcGenitive = $CategoryPart->GetSrcGenitive();//TODO: or $this->IParent->GetGenitive()?
            $this->SrcGenitive = $SrcGenitive;
        }
        return $this->SrcGenitive;
    }

    public function GetGenitive()
    {
        if($this->Genitive === null)
        {
            $ParentCategoryContext = $this->IParent->GetContext();
            $CategoryPart = $ParentCategoryContext->GetPartContext();
            $Genitive = $CategoryPart->GetGenitive();//TODO: or $this->IParent->GetGenitive()?
            $this->Genitive = $Genitive;
        }
        else $Genitive = $this->Genitive;
        return $Genitive;
    }

    public function GetPrice()
    {
        if($this->Price == null)
        {
            $ProductTemplate = $this->ITemplate;
            $Price = (float) $ProductTemplate->price;
            $this->Price = $Price;
        }
        else $Price = $this->Price;
        return $Price;
    }

    function GetDescription()
    {
        if($this->Description === null)
        {
            $Description =  $this->ReplacePlaceholders($this->ITemplate->description);
            $this->Description = $Description;
        }
        else $Description = $this->Description;
        return $Description;
    }

    function GetPicturesDir()
    {
        if($this->PicturesDir === null)
        {
            $ProductTemplateImagesDir = $this->ISelfRepository->GetTemplateImagesDir();
            $ProductTemplate = $this->ITemplate;
            $this->PicturesDir  = $ProductTemplateImagesDir.$ProductTemplate->pictures_dir;
        }
        return $this->PicturesDir;
    }

    function GetMainPicture()
    {
        if($this->MainPicture === null)
        {
            $Pictures = $this->GetPictures();
            if(!isset($Pictures[0])) return null;
            $MainPicture = $Pictures[0]['big'];
            $this->MainPicture = $MainPicture;
        }
        else $MainPicture = $this->MainPicture;
        return $MainPicture;
        /*
                $Image = $Pictures[0];
                $PicturesDir = $this->GetPicturesDir();
                $FullPictureFilename = $PicturesDir.'/'.$Image['big'];
        */
        //TODO: доделать
    }

    protected function GetNoPictureImg()
    {

    }

    function GetPictures()
    {
        /*$ImagesSeparator = config('sys.engine.product_images_separator');
        $ProductTemplate = $this->ICalculationContext->GetTemplateContext();
        $PicturesStr = $ProductTemplate->pictures;
        if(!$PicturesStr) return null;
        $this->GetFakePictures();
        $Pictures = explode($ImagesSeparator, $PicturesStr);*/

        if($this->Pictures === null)
        {
            $SrcPictures = $this->GetRandomPictures();
            $Pictures = $this->SrcPicturesToScaledPictures($SrcPictures);
            $this->Pictures = $Pictures;
        }
        else $Pictures = $this->Pictures;
        return $Pictures;
    }

    protected function SrcPicturesToScaledPictures($SrcPictures)
    {
        $ret = [];
        foreach($SrcPictures as $SrcPicture) $ret []= $this->SrcPictureToScaledPicture($SrcPicture);
        return $ret;
    }

    protected function SrcPictureToScaledPicture($SrcPicture)
    {
        $PicturesDir = $this->GetPicturesDir();
        $ret = [
            'full'  => "{$PicturesDir}/{$SrcPicture}",
            'big'   => "{$PicturesDir}/380/{$SrcPicture}",
            'small' => "{$PicturesDir}/80/{$SrcPicture}"
        ];

        return $ret;
    }

    protected function GetRandomPictures()
    {
        $FullPicturesDir = public_path().$this->GetPicturesDir();
        if(!is_dir($FullPicturesDir))
        {
            Log::warning("Directory with images not exists ({$FullPicturesDir})!");
            return [];
        }

        $Count = $this->ISelfRepository->GetPicturesPerStoreGood();
        $Pictures = $this->IPicturesFinder->GetPictures($FullPicturesDir, $Count);
        return $Pictures;
    }

    protected function GetFakePictures()
    {
        //$BaseDir = asset_path();
        //$FullImageUrl = "{$BaseDir}/{$NoFotoImage}";
        //$NoFotoImage = Theme::config('vars')['NO_PHOTO_IMAGE'];
        $NoFotoImage = '1.jpg';
        $ret = array_fill(0,5,$NoFotoImage);
        return $ret;
    }

    protected function ReplaceSubPlaceholders($str)
    {
        $SubsUrlNameList = $this->GetExternalChildsUrlNameList();
        //{sub:{url_slug}} - например {sub:yandex}

        $RegEx = '|\{sub:\w\}|';
        $RegEx = '|\{sub:(\{?[\w_-]+\}?)\}|';

        $str = preg_replace_callback($RegEx,function(array $matches) use($SubsUrlNameList){
            return $SubsUrlNameList[$matches[1]]['url'];
        },$str);
        return $str;
    }

    protected function ReplacePlaceholders($str)
    {
        $str = parent::ReplacePlaceholders($str);

        $str = $this->ReplaceSubPlaceholders($str);
        $str = str_replace('{category}',$this->GetParent()->GetUrl(),$str);

        return $str;
    }

    protected function GetPlaceholderCity()
    {
        return $this->ICalculationContext->GetParentContext()->GetGenitive();
    }

    function GetFullVID()
    {
        if($this->FullVID === null)
        {
            $ParentVID = $this->IParent->GetFullVID();
            $CalcTypeId = 1;
            $PartId = null;
            $SetId = $this->ICalculationContext->GetSetContext()->GetId();
            $TemplateId = $this->ITemplate->GetId();
            $TypeId = $this->ICalculationContext->GetTypeContext()->GetId();
            $VID = "{$ParentVID}-{$PartId}:{$SetId}:{$TemplateId}:{$TypeId}";
            $this->FullVID = $VID;
        }
        return $this->FullVID;
    }

    function GetVID()
    {
        if($this->VID === null)
        {
            $ParentVID = $this->IParent->GetVID();
            $PartId = null;
            $TemplateId = $this->ITemplate->GetId();
            $VID = "{$ParentVID}-{$PartId}:{$TemplateId}";
            $this->VID = $VID;
        }
        return $this->VID;
    }

    function GetUrlSlug()
    {
        if($this->UrlSlug === null)
        {
            $TypeName = $this->GetProductType()->url_slug_part;
            $ParentUrlSlug = $this->IParent->GetUrlSlug();

            $Scale = '';
            if($this->ITemplate->scale)
            {
                $Scale = "_1cm-{$this->ITemplate->scale}";
            }
            $UrlSlug = "{$TypeName}_karta_{$ParentUrlSlug}{$Scale}";
            $this->UrlSlug = $UrlSlug;
        }
        else $UrlSlug = $this->UrlSlug;
        return $UrlSlug;
    }

    public function GetProductType()
    {
        if(!$this->IProductType)
        {
            $this->IProductType = $this->ITemplate->ProductType;
        }
        return $this->IProductType;
    }
}