<?php namespace App\Models\PageEntity\StoreEntity\Product\Calculated;

use App\Models\PageEntity\StoreEntity\Base\Calculated\NotRoot\INotRootCalculatedStoreEntityInnerRepository;
use App\Models\PageEntity\StoreEntity\Product\IProductRepository;

interface ICalculatedProductRepository extends IProductRepository, INotRootCalculatedStoreEntityInnerRepository{

}