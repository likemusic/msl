<?php namespace App\Models\PageEntity\StoreEntity\Product\Calculated;

use App\Models\PageEntity\StoreEntity\Base\Calculated\NotRoot\INotRootCalculatedStoreEntityContext;

interface ICalculatedProductContext extends INotRootCalculatedStoreEntityContext {

}