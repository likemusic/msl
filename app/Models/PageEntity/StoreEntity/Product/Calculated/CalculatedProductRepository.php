<?php namespace App\Models\PageEntity\StoreEntity\Product\Calculated;

use App\Models\Base\Interfaces\Repository\IRepositoryEntity;
use App\Models\Base\Traits\Repository\GetUrlNameList\TGetInternalChildsUrlNameListByExternalEntity;
use App\Models\PageEntity\StoreEntity\Base\Calculated\NotRoot\NotRootCalculatedStoreEntityInnerRepository;
use App\Models\PageEntity\StoreEntity\Category\Calculated\ICalculatedCategory;
use App\Models\PageEntity\StoreEntity\Category\ICategoryRepository;
use App\Models\PageEntity\StoreEntity\Category\Template\ICategoryTemplate;
use App\Models\PageEntity\StoreEntity\Product\Template\IProductTemplateRepository;
use App\Models\PageEntity\StoreEntity\Product\Type\IProductTypeRepository;
use App\Models\PageEntity\StoreEntity\SubProduct\ISubProductRepository;
use App\Helpers\PaymentGateway\Base\BuyButton\IRequestDataProvider as IPaymentDataProvider;
use League\Flysystem\Exception;

class CalculatedProductRepository extends NotRootCalculatedStoreEntityInnerRepository implements ICalculatedProductRepository{
    use TGetInternalChildsUrlNameListByExternalEntity;
    protected $NewItemClassName = CalculatedProduct::class;
    protected $NewItemContextClassName = CalculatedProductContext::class;

    /**
     * @var IProductTypeRepository
     */
    protected $IProductTypeRepository;
    //protected $ICalculatedCategoryRepository;

    protected $ProductTemplateImagesDir;
    protected $PicturesPerStoreGood;

    public function __construct(IPaymentDataProvider $IPaymentDataProvider, IProductTemplateRepository $IProductTemplateRepository, ISubProductRepository $ISubProductRepository, IProductTypeRepository $IProductTypeRepository)
    {
        parent::__construct($IPaymentDataProvider, $IProductTemplateRepository, $ISubProductRepository);
        $this->IProductTypeRepository = $IProductTypeRepository;
        $this->ProductTemplateImagesDir = config('site.dirs.product_templates_images');
        $this->PicturesPerStoreGood = config('sys.engine.pictures_per_store_good');
    }

/*    public function __wakeup()
    {
        // TODO: Implement __wakeup() method.
        $this->ITemplateRepository = app(IProductTemplateRepository::class);
        //TODO: fix this hack
    }
*/
    public function GetTemplateImagesDir()
    {
        return $this->ProductTemplateImagesDir;
    }

    public function GetPicturesPerStoreGood()
    {
        return $this->PicturesPerStoreGood;
    }
    /**
     * @param $url
     * @param ICalculatedCategory $ParentEntity
     * @return \App\Models\PageEntity\StoreEntity\Product\Calculated\CalculatedProduct
     */
    /*function GetChildByUrlSlugAndExternalParentEntity($url, $ParentEntity)
    {
        return $this->GetInternalChildByUrlSlugForExternalEntity($url, $ParentEntity);
    }*/

    function GetInternalChildByUrlSlugForExternalEntity($url, $ParentEntity)
    {
        $ParentUrlSlug = $ParentEntity->GetUrlSlug();
        if(strpos($url,'_karta_')=== false) return null;
        list($TypeName,$Scale) = explode('_karta_'.$ParentUrlSlug,$url,2);
        if($Scale)  $Scale = substr($Scale,5);
        $ProductType = $this->ProductTypeNameToProductType($TypeName);
        $TypeId = $ProductType->id;
        //$SearchString = $this->GetSearchStringByUrlAndParentEntity($url, $ParentEntity);
        /**
         * @var ICategoryTemplate
         */
        $IParentType = $ParentEntity->GetContext()->GetTypeContext();
        $ProductSet = $IParentType->productSet;

        //$CurrentTemplate = $ProductSet->GetChildEntityTemplateBySearchString($SearchString);
        //$CurrentTemplate = $ProductSet->GetProductTemplateByProductTypeIdAndScale($TypeId, $Scale);

        $ProductSetId = $IParentType->product_set_id;
        $CurrentTemplate = $this->ITemplateRepository->GetByProductSetIdAndProductTypeIdAndScale($ProductSetId, $TypeId, $Scale);

        if(!$CurrentTemplate) return null;

        $CurrentCalculated = $this->TemplateToCalculated($CurrentTemplate, $ParentEntity, $url, $ProductType);
        return $CurrentCalculated;
    }

    protected function ProductTypeNameToProductType($TypeName)
    {
        $ProductType = $this->IProductTypeRepository->GetByUrlSlugPart($TypeName);
        return $ProductType;
    }

    function GetInternalChildsByExternalEntity(IRepositoryEntity $Entity = null, $Fields = null)
    {
        $CalculatedCategory = $Entity;
        $CategoryType = $CalculatedCategory->GetContext()->GetTypeContext();
        $ProductSet = $CategoryType->GetProductSet();
        if(!$ProductSet) return [];
        $ProductTemplates = $ProductSet->productTemplates;

        $CalculatedProducts = [];
        foreach($ProductTemplates as $ProductTemplate)
        {
            //$url = $entity->GetUrl();
            //TODO: get Url inside
            $CalculatedProduct = $this->NewRepositoryItem($ProductTemplate,$Entity);
            $CalculatedProducts []= $CalculatedProduct;
        }
        return $CalculatedProducts;
    }

    function GetByVID($Id)
    {
        $Chunks = explode('-',$Id);
        $count = count($Chunks);
        if($count != 2) throw new \Exception('Invalid product VID:'.$Id);
        $ParentId = $Chunks[0];

        //TODO: inplement by injection (without recursive)
        $ICategoryRepository = app(ICategoryRepository::class);
        $ParentEntity = $ICategoryRepository->GetByVID($ParentId);

        $ProductChunks = $Chunks[1];
        $ProductChunks = explode(':', $ProductChunks);
        if(count($ProductChunks)!=2) throw new \Exception('Invalid CalculatedProduct vid'.$Id);
        list($PartId, $TemplateId) = $ProductChunks;

        if(($PartId) or (!$TemplateId)) return null;
            //throw new \Exception('Invalid calculated product vid:'.$Id);

        $ITemplate = $this->ITemplateRepository->GetById($TemplateId);
        $ret = $this->TemplateToCalculated($ITemplate,$ParentEntity);
        return $ret;
    }

    function GetByFullVID($Id)
    {
        $Chunks = explode('-',$Id);
        $count = count($Chunks);
        if($count != 2) throw new \Exception('Invalid product VID:'.$Id);
        $ParentId = $Chunks[0];

        //TODO: inplement by injection (without recursive)
        $ICategoryRepository = app(ICategoryRepository::class);
        $ParentEntity = $ICategoryRepository->GetByFullVID($ParentId);

        $ProductChunks = $Chunks[1];
        $ProductChunks = explode(':', $ProductChunks);
        if(count($ProductChunks)!=4) throw new \Exception("Invalid CalculatedProduct Id:".$Id);

        list($PartId, $SetId, $TemplateId, $TypeId) = $ProductChunks;

        if(($PartId) or (!$TemplateId)) return null;
            //throw new \Exception('Invalid calculated product FullVid:'.$Id);

        //TODO: сдлеать с проверкой всех параметров
        $ITemplate = $this->ITemplateRepository->GetById($TemplateId);
        $ret = $this->TemplateToCalculated($ITemplate,$ParentEntity);
        return $ret;
    }
}