<?php namespace App\Models\PageEntity\StoreEntity;

use App\Models\PageEntity\IPageEntity;
use App\Models\PageEntity\StoreEntity\Category\ICategoryRepository;
use App\Models\PageEntity\StoreEntity\Product\IProductRepository;
use App\Models\PageEntity\StoreEntity\SubProduct\ISubProductRepository;

class GlobalStoreEntityRepository implements IGlobalStoreEntityRepository{
    /**
     * @var ICategoryRepository
     */
    protected $ICategoryRepository;

    /**
     * @var IProductRepository
     */
    protected $IProductRepository;

    /**
     * @var ISubProductRepository
     */
    protected $ISubProductRepository;

    public function __construct( ICategoryRepository $ICategoryRepository, IProductRepository $IProductRepository, ISubProductRepository $ISubProductRepository )
    {
        $this->ICategoryRepository = $ICategoryRepository;
        $this->IProductRepository = $IProductRepository;
        $this->ISubProductRepository = $ISubProductRepository;
    }

    /**
     * @param $Url
     * @param IPageEntity $FoundByPath
     * @return IPageEntity|null
     */
    function GetByUrlWithByPath($Url, IPageEntity &$FoundByPath=null)
    {
        $FoundByPath=null;

        $UrlChunks = explode( '/', $Url);
        $UrlChunksCount = count($UrlChunks);
        $i=0;

        $LatestEntity = $Entity = $this->ICategoryRepository->GetRootByUrlSlug($UrlChunks[$i]);
        //не совпала даже корневая категория
        if(!$LatestEntity) return null;

        $Stop = false;
        for($i++;($i<$UrlChunksCount) && (!$Stop);$i++)
        {
            $Entity = $LatestEntity->GetChildByUrlSlug($UrlChunks[$i]);
            if($Entity) $LatestEntity = $Entity;
            else $Stop = true;
        }

        //Через категории раскрыли все части url - возвращаем найденную страницу
        if((!$Stop) && ($i==$UrlChunksCount))
        {
            return $LatestEntity;
        }
        else
        {
            $FoundByPath = $LatestEntity;
            return null;
        }
    }


    function GetByVID($Id)
    {
        $func = 'GetByVID';
        $ret = $this->GetByRepository($Id, $func);
        return $ret;
    }

    function GetByFullVID($Id)
    {
        $func = 'GetByFullVID';
        $ret = $this->GetByRepository($Id, $func);
        return $ret;
    }

    function GetRoots()
    {
        return $this->ICategoryRepository->GetVirtualRoot();
    }

    protected function GetByRepository($Id,$func)
    {
        $Parts = explode('-',$Id);
        $count = count($Parts);
        $repository = null;
        switch($count)
        {
            case 1: $repository = $this->ICategoryRepository;;break;
            case 2: $repository = $this->IProductRepository; break;
            case 3: $repository = $this->ISubProductRepository; break;
            default: throw new \Exception('Invalid StoreRepositoryId:'.$Id);
        }
        $ret = call_user_func([$repository, $func],$Id) ;
        return $ret;
    }


}