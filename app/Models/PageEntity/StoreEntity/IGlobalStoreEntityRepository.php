<?php namespace App\Models\PageEntity\StoreEntity;

use App\Models\PageEntity\StoreEntity\Base\IStoreEntityRepository;

interface IGlobalStoreEntityRepository extends IStoreEntityRepository{

    function GetByVID($Id);
    function GetByFullVID($Id);

    function GetRoots();
}
