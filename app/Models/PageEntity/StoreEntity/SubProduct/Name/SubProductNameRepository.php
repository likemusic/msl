<?php namespace App\Models\PageEntity\StoreEntity\SubProduct\Name;


use App\Models\PageEntity\Base\Calculated\Part\Db\PageEntityPartDbRepository;

class SubProductNameRepository  extends PageEntityPartDbRepository implements ISubProductNameRepository{
    protected $ItemClassName = SubProductName::class;

}