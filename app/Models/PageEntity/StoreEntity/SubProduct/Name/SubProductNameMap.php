<?php namespace App\Models\PageEntity\StoreEntity\SubProduct\Name;

use Analogue\ORM\EntityMap;

class SubProductNameMap extends EntityMap{

    protected $table = 'subproduct_names';
} 