<?php namespace App\Models\PageEntity\StoreEntity\SubProduct\Name;


use App\Models\PageEntity\Base\Calculated\Part\IPageEntityPartRepository;

interface ISubProductNameRepository extends IPageEntityPartRepository {

} 