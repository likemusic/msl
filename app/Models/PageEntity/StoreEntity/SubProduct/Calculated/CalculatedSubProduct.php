<?php namespace App\Models\PageEntity\StoreEntity\SubProduct\Calculated;

use \App\Helpers\PaymentGateway\Base\BuyButton\IRequestDataProvider as IPaymentDataProvider;
use \App\Helpers\StoreTreeBuilder\IStoreTreeBuilder;
use App\Models\Base\Traits\RepositoryItem\TGetUrlSlugByTemplate;
use \App\Models\PageEntity\StoreEntity\Base\StoreEntityGood\StoreEntityGood;
use App\Models\PageEntity\StoreEntity\SubProduct\Uniques\ISubProductUnique;

class CalculatedSubProduct extends StoreEntityGood implements ICalculatedSubProduct {
    use TGetUrlSlugByTemplate { GetUrlSlug as protected GetUrlSlugByTemplate;}

    /**
     * @var ISubProductUnique
     */
    protected $IUnique;

    protected $Genitive;
    protected $FullVID;
    protected $VID;

    public function __construct(ICalculatedSubProductContext $IContext,  ICalculatedSubProductRepository $ISelfRepository, IStoreTreeBuilder $IStoreTreeBuilder, IPaymentDataProvider $IRequestDataProvider)
    {
        parent::__construct($IContext, $ISelfRepository, $IStoreTreeBuilder, $IRequestDataProvider );
        $this->SetUnique();
    }

    protected function SetUnique()
    {
        $ProductTypeId = $this->GetProductTypeId();
        $Uniques = $this->ITemplate->Uniques;
        if(count($Uniques)==0) return;

        foreach($Uniques as $Unique)
        {
            if($Unique->product_type_id == $ProductTypeId)
            {
                $this->IUnique = $Unique;
                return;
            }
        }
    }


    function GetKnowFields()
    {
        $ParentFields = parent::GetKnowFields();
        $AddFields = [
            'shortcode'     => 'GetShortcode',
        ];

        $Fields = $ParentFields + $AddFields;
        return $Fields;
    }

    function GetShortcode()
    {
        return $this->ITemplate->GetUrlSlug();//TODO: fix
    }

    protected function GetProductTypeId()
    {
        $SubProductTemplate = $this->ITemplate;
        $CalculatedProduct = $this->IParent;
        $ProductTemplate = $CalculatedProduct->GetContext()->GetTemplateContext();
        $ProductTypeId = $ProductTemplate->product_type_id;
        return $ProductTypeId;
    }

    function GetName()
    {
        if($this->Name === null)
        {
            $Name = $Name = $this->GetUniqueNameTemplate();
            if(!$Name) $Name = $this->GetTemplateNameTemplate();
            $Name = $this->mb_ucfirst($this->ReplacePlaceholders($Name));
            $this->Name = $Name;
        }
        return $this->Name;
    }

    function GetTemplateNameTemplate()
    {
        $Name = $this->ITemplate->Name->name;
        return $Name;
    }

    function GetUniqueNameTemplate()
    {
        if(!$this->IUnique) return null;
        $Name = $this->IUnique->Name->name;
        return $Name;
    }

    function GetGenitive()
    {
        if($this->Genitive === null)
        {
            $Genitive = $this->GetName();
            $this->Genitive = $Genitive;
        }
        else $Genitive = $this->Genitive;
        return $Genitive;
    }

    function GetSrcGenitive()
    {
        return $this->GetName();
    }

    function GetTitle()
    {
        if($this->Title === null)
        {
            $TitleTemplate = $this->GetUniqueTitleTemplate();
            if(!$TitleTemplate) $TitleTemplate = $this->GetTemplateTitleTemplate();
            $Title = $this->ReplaceTitlePlaceholders($TitleTemplate);
            $this->Title =  $this->mb_ucfirst($Title);
        }
        return $this->Title;
    }

    protected function mb_ucfirst($str) {
        $fc = mb_strtoupper(mb_substr($str, 0, 1));
        return $fc.mb_substr($str, 1);
    }

    protected function GetUniqueTitleTemplate()
    {
        if(!$this->IUnique) return null;

        $ret = $this->IUnique->Title->title;
        /*$TitleTypeId = $this->ITemplate->title_type_id;
        $SubProductTitleTypes = config('sys.subproduct_title_types');
        $TitleTemplate = $SubProductTitleTypes[$TitleTypeId];*/
        return $ret;
    }

    protected function GetTemplateTitleTemplate()
    {
        $Title = $this->ITemplate->Title->title;
        return $Title;
    }

    protected function ReplacePlaceholders($str)
    {
        $str = parent::ReplacePlaceholders($str);
        $str = str_replace('{target}',$this->ITemplate->GetTarget(),$str);
        $str = str_replace('{target_ru}',$this->ITemplate->GetTargetRu(),$str);

        $str = str_replace('{pname}',$this->IParent->GetName(),$str);
        //$str = str_replace('{sname}',$this->IParent->GetName(),$str);
        return $str;
    }

    protected function ReplaceTitlePlaceholders($str)
    {
        $str = $this->ReplacePlaceholders($str);
        $str = $this->ReplaceSnamePlaceholder($str);
        $str = $this->ReplaceMapTypeAltPlaceholder($str);
        $str = $this->ReplaceMapTypeWhatPlaceholder($str);
        return $str;
    }

    protected function ReplaceSnamePlaceholder($str)
    {
        $ret = str_replace('{sname}',$this->GetName(),$str);
        return $ret;
    }

    protected function ReplaceMapTypeAltPlaceholder($str)
    {
        $MapTypeAlt = $this->IParent->GetProductType()->alt;
        $str = str_replace('{map_type_alt}',$MapTypeAlt,$str);
        return $str;
    }

    protected function ReplaceMapTypeWhatPlaceholder($str)
    {
        $MapTypeWhat = $this->IParent->GetProductType()->what;
        $str = str_replace('{map_type_what}',$MapTypeWhat,$str);
        return $str;
    }

    function GetDescription()
    {
        if($this->Description === null)
        {
            if(!$this->IUnique) return null;

            $Template =  $this->IUnique->description;
            $ret = $this->ReplacePlaceholders($Template);
            $this->Description = $ret;
        }
        else $ret = $this->Description;
        return $ret;
    }

    //TODO: ДОДЕЛАТЬ ОБЯЗАТЕЛЬНО
    function GetPicturesDir()
    {
        return $this->IParent->GetPicturesDir();
        /*if(!$this->IUnique) return null;
        $SubProductUniquePicturesDir = $this->IUnique->pictures_dir;
        return $SubProductUniquePicturesDir;*/
    }

    function GetPictures()
    {
        return $this->IParent->GetPictures();
    }

    function GetMainPicture()
    {
        return $this->IParent->GetMainPicture();
    }

    function GetPrice()
    {
        return $this->IParent->GetPrice();
    }


    function GetChildByUrlSlug($url)
    {
        return null;
    }

    protected function GetPlaceholderCity()
    {
        return $this->ICalculationContext->GetParentContext()->GetContext()->GetParentContext()->GetGenitive();
    }

/*    protected function GetPlaceholderCity()
    {
        $city = $this->GetParent()->GetName();
        return $city;
    }*/

    function GetUrlSlug()
    {
        if($this->UrlSlug === null)
        {
            $str = $this->GetUrlSlugByTemplate();
            $str = $this->ReplacePlaceholderYear($str);
            $ParentUrlSlug = $this->IParent->GetUrlSlug();
            $str = $ParentUrlSlug.'_'.$str;
            $this->UrlSlug = $str;
        }
        else $str = $this->UrlSlug;
        return $str;
    }

    function GetFullVID()
    {
        if($this->FullVID === null)
        {
            $ParentVID = $this->IParent->GetFullVID();
            $SetId = $this->ICalculationContext->GetSetContext()->GetId();
            $TemplateId = $this->ITemplate->GetId();
            $TypeId = $this->ICalculationContext->GetTypeContext()->GetId();
            $UniqueId = $this->IUnique->GetId();
            $NameId = $this->IUnique->subproduct_name_id;
            $TitleId = $this->IUnique->subproduct_title_id;
            $VID = "{$ParentVID}-{$SetId}:{$TemplateId}:{$TypeId}:{$UniqueId}:{$NameId}:{$TitleId}";
            $this->FullVID = $VID;
        }
        return $this->FullVID;
    }

    function GetVID()
    {
        if($this->VID === null)
        {
            $ParentVID = $this->IParent->GetVID();
            $TemplateId = $this->ITemplate->GetId();
            $VID = "{$ParentVID}-{$TemplateId}";
            $this->VID = $VID;
        }
        return $this->VID;
    }

}