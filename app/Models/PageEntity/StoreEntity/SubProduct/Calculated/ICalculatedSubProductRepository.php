<?php namespace App\Models\PageEntity\StoreEntity\SubProduct\Calculated;

use App\Models\PageEntity\StoreEntity\Base\Calculated\NotRoot\INotRootCalculatedStoreEntityInnerRepository;
use App\Models\PageEntity\StoreEntity\SubProduct\ISubProductRepository;

interface ICalculatedSubProductRepository extends ISubProductRepository, INotRootCalculatedStoreEntityInnerRepository{

}