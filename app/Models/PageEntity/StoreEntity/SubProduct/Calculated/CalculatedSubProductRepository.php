<?php namespace App\Models\PageEntity\StoreEntity\SubProduct\Calculated;

use App\Models\Base\Interfaces\Repository\IRepositoryEntity;
use App\Models\Base\Traits\Repository\GetUrlNameList\TGetInternalChildsUrlNameListByExternalEntity;
use App\Models\PageEntity\StoreEntity\Base\Calculated\NotRoot\NotRootCalculatedStoreEntityInnerRepository;
use App\Models\PageEntity\StoreEntity\Product\IProductRepository;
use App\Models\PageEntity\StoreEntity\Product\Template\IProductTemplate;
use App\Models\PageEntity\StoreEntity\Product\Template\ProductTemplate;
use App\Models\PageEntity\StoreEntity\SubProduct\Set\ISubProductSetRepository;
use App\Models\PageEntity\StoreEntity\SubProduct\Template\ISubProductTemplateRepository;
use App\Helpers\PaymentGateway\Base\BuyButton\IRequestDataProvider as IPaymentDataProvider;
use League\Flysystem\Exception;

class CalculatedSubProductRepository extends NotRootCalculatedStoreEntityInnerRepository implements ICalculatedSubProductRepository{
    use TGetInternalChildsUrlNameListByExternalEntity;

    protected $NewItemClassName = CalculatedSubProduct::class;
    protected $NewItemContextClassName = CalculatedSubProductContext::class;

    /**
     * @var ISubProductSetRepository
     */
    protected $ISetRepository;

    public function __construct(IPaymentDataProvider $IPaymentDataProvider,
                                ISubProductTemplateRepository $ISubProductTemplateRepository,
        ISubProductSetRepository $ISetRepository
    )
    {
        parent::__construct($IPaymentDataProvider, $ISubProductTemplateRepository);
        $this->ISetRepository = $ISetRepository;
    }

    /**
     * @param $url
     * @param ICalculatedSubProduct $ParentEntity
     * @return \App\Models\PageEntity\StoreEntity\Product\Calculated\CalculatedProduct
     */
    function GetInternalChildByUrlSlugForExternalEntity($url, $ParentEntity)
    {
        $SearchString = $this->GetSearchStringByUrlAndParentEntity($url, $ParentEntity);

        //$ProductTemplate = $ParentEntity->GetContext()->GetTemplateContext();
        $ProductType = $ParentEntity->GetProductType();
        //$SubProductSet = $ProductType->SubProductSet;
        $SubProductSet = $this->ISetRepository->find($ProductType->subproduct_set_id);

        $SubProductTemplate = $this->ITemplateRepository->SubProductTemplateBySearchString($SubProductSet,$SearchString);
        if(!$SubProductTemplate) return null;

        $CurrentCalculated = $this->TemplateToCalculated($SubProductTemplate, $ParentEntity, $url);
        return $CurrentCalculated;
    }

    protected function GetSearchStringByUrlAndParentEntity($url, $ParentEntity)
    {
        //TODO: доделать
        $SearchString = $this->SetPlaceholderYear($url);
        $SearchString = $this->RemoveParentUrlSlug($SearchString,$ParentEntity);
        return $SearchString;
    }

    protected function RemoveParentUrlSlug($SearchString,$ParentEntity)
    {
        $SearchString = substr($SearchString,strlen($ParentEntity->GetUrlSlug())+1);
        return $SearchString;
    }

    protected function SetPlaceholderYear($str)
    {
        $str = str_replace(date("Y"), '{year}', $str);
        return $str;
    }

    function GetInternalChildByUrlSlugForInternalEntity($url, $ParentEntity)
    {
        return null;
    }

    function GetInternalChildsUrlNameListByInternalEntity(IRepositoryEntity $entity=null)
    {
        return [];
    }

    /**
     * @param IRepositoryEntity|ICalculatedCategory $entity
     * @throws \Exception
     */
    /*function GetInternalChildsUrlNameListByExternalEntity(IRepositoryEntity $entity=null)
    {
        $CalculatedSubproducts = $this->GetInternalChildsByExternalEntity($entity, ['url','name']);
        $ret = $this->CalculatedListToUrlNameList($CalculatedSubproducts);
        return $ret;
    }*/


    function GetInternalChildsByExternalEntity(IRepositoryEntity $entity=null)
    {
        /** @var IProductTemplate $ProductTemplate */
        //$ProductTemplate = $entity->GetContext()->GetTemplateContext();
        $ProductType = $entity->GetProductType();
        //$SubProductSet = $ProductType->SubProductSet;
        $SubProductSet = $this->ISetRepository->find($ProductType->subproduct_set_id);

        if (!$SubProductSet) return null;

        $SubProductsTemplates = $SubProductSet->SubProductTemplates;
        if(count($SubProductsTemplates)==0) return null;

        $CalculatedSubProducts = $this->TemplatesListToCalculatedList($SubProductsTemplates->all(), $entity);
        return $CalculatedSubProducts;
    }

    function GetByVID($Id)
    {
        $Chunks = explode('-',$Id);
        $count = count($Chunks);
        if($count != 3) throw new \Exception('Invalid subproduct VID:'.$Id);

        $ParentVID = join('-',[$Chunks[0],$Chunks[1]]);
        //TODO: inplement by injection (without recursive)
        $IParentRepository = app(IProductRepository::class);
        $ParentEntity = $IParentRepository->GetByVID($ParentVID);

        $TemplateId = $Chunks[2];

        if(!$TemplateId) throw new \Exception('Invalid calculated product vid:'.$Id);

        $ITemplate = $this->ITemplateRepository->GetById($TemplateId);
        $ret = $this->TemplateToCalculated($ITemplate,$ParentEntity);
        return $ret;
    }

    function GetByFullVID($Id)
    {
        $Chunks = explode('-',$Id);
        $count = count($Chunks);
        if($count != 3) throw new \Exception('Invalid subproduct FullVID:'.$Id);

        $ParentVID = join('-',[$Chunks[0],$Chunks[1]]);
        //TODO: inplement by injection (without recursive)
        $IParentRepository = app(IProductRepository::class);
        $ParentEntity = $IParentRepository->GetByFullVID($ParentVID);

        $SubProductChunks = $Chunks[2];
        $SubProductChunks = explode(':',$SubProductChunks);
        if(count($SubProductChunks)!=6) throw new \Exception("Invalid SubProduct Id:".$Id);

        list($SetId, $TemplateId, $TypeId, $UniqueId, $NameId, $TitleId) = $SubProductChunks;

        if((!$SetId) or (!$TemplateId)or (!$TypeId)) throw new \Exception('Invalid calculated product FullVid:'.$Id);

        //TODO: сдлеать с проверкой всех параметров
        $ITemplate = $this->ITemplateRepository->GetById($TemplateId);
        $ret = $this->TemplateToCalculated($ITemplate,$ParentEntity);
        return $ret;
    }

}