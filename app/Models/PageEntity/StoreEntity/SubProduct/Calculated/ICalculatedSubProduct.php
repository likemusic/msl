<?php namespace App\Models\PageEntity\StoreEntity\SubProduct\Calculated;

use App\Models\PageEntity\StoreEntity\Base\Calculated\NotRoot\INotRootCalculatedStoreEntity;
use App\Models\PageEntity\StoreEntity\SubProduct\ISubProduct;

interface ICalculatedSubProduct extends ISubProduct, INotRootCalculatedStoreEntity{

}