<?php namespace App\Models\PageEntity\StoreEntity\SubProduct\Calculated;

use App\Models\PageEntity\StoreEntity\Base\Calculated\NotRoot\INotRootCalculatedStoreEntityContext;

interface ICalculatedSubProductContext extends INotRootCalculatedStoreEntityContext{

}