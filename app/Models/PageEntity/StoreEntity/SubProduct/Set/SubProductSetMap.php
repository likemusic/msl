<?php namespace App\Models\PageEntity\StoreEntity\SubProduct\Set;

use Analogue\ORM\EntityMap;
use App\Models\PageEntity\StoreEntity\SubProduct\Template\SubProductTemplate;

class SubProductSetMap extends EntityMap{

    protected $table = 'subproduct_sets';
    protected $with = ['SubProductTemplates'];

    function SubProductTemplates(SubProductSet $subProductSet)
    {
        return $this->belongsToMany($subProductSet, SubProductTemplate::class,null,'subproduct_set_id','subproduct_template_id');
    }

/*
    public function SubProductTemplatesBySearchString(SubProductSet $subProductSet, $SearchString)
    {
        return $this->subproductTemplates($subProductSet)->where('subproduct_templates.url_slug', '=', $SearchString);
        //TODO: переименовать поле url в url_part или подобное в зависимости от требований
    }
*/
}