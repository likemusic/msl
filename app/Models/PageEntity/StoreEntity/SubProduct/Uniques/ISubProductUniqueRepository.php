<?php namespace App\Models\PageEntity\StoreEntity\SubProduct\Uniques;

use App\Models\PageEntity\Base\Calculated\Template\IPageEntityTemplateRepository;

interface ISubProductUniqueRepository extends IPageEntityTemplateRepository {

} 