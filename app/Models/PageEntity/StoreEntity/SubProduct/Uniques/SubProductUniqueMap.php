<?php namespace App\Models\PageEntity\StoreEntity\SubProduct\Uniques;


use Analogue\ORM\EntityMap;
use App\Models\PageEntity\StoreEntity\SubProduct\ISubProduct;
use App\Models\PageEntity\StoreEntity\SubProduct\Name\SubProductName;
use App\Models\PageEntity\StoreEntity\SubProduct\Title\SubProductTitle;

class SubProductUniqueMap extends EntityMap{
    protected $table = 'subproduct_uniques';
    protected $with = ['Name','Title'];

    function Title(ISubProductUnique $ISubProductUnique)
    {
        return $this->belongsTo($ISubProductUnique, SubProductTitle::class,'subproduct_title_id');
    }

    function Name(ISubProductUnique $ISubProductUnique)
    {
        return $this->belongsTo($ISubProductUnique, SubProductName::class, 'subproduct_name_id');
    }
} 