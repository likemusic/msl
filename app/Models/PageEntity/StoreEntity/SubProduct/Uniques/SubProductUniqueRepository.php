<?php namespace App\Models\PageEntity\StoreEntity\SubProduct\Uniques;


use App\Models\PageEntity\Base\Calculated\Template\Db\PageEntityTemplateDbRepository;

class SubProductUniqueRepository  extends PageEntityTemplateDbRepository implements ISubProductUniqueRepository{
    protected $ItemClassName = SubProductUnique::class;

}