<?php namespace App\Models\PageEntity\StoreEntity\SubProduct\Title;


use App\Models\PageEntity\Base\Calculated\Part\Db\PageEntityPartDbRepository;

class SubProductTitleRepository  extends PageEntityPartDbRepository implements ISubProductTitleRepository{
    protected $ItemClassName = SubProductTitle::class;

}