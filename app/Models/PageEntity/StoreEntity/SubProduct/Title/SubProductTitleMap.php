<?php namespace App\Models\PageEntity\StoreEntity\SubProduct\Title;

use Analogue\ORM\EntityMap;

class SubProductTitleMap extends EntityMap{
    protected $table = 'subproduct_titles';
} 