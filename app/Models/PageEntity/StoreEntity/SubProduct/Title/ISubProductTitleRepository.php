<?php namespace App\Models\PageEntity\StoreEntity\SubProduct\Title;


use App\Models\PageEntity\Base\Calculated\Part\IPageEntityPartRepository;

interface ISubProductTitleRepository extends IPageEntityPartRepository {

} 