<?php namespace App\Models\PageEntity\StoreEntity\SubProduct\Template;

use App\Models\PageEntity\Base\Calculated\Template\Db\PageEntityTemplateDb;
use App\Models\PageEntity\StoreEntity\Product\Type\IProductType;

class SubProductTemplate extends PageEntityTemplateDb implements ISubProductTemplate {

    protected $Target;
    protected $TargetRu;
    protected $UrlSlug;

    public function GetChildEntityTemplateBySearchString($searchString)
    {
        return null;
    }

    function SubProductUnique($ProductTypeId)
    {
        return $this->Unique($ProductTypeId);
    }

    function GetTarget()
    {
        if($this->Target === null)
        {
            $str = $this->target;
            $this->Target = $this->ReplacePlaceholderYear($str);
        }
        return $this->Target;
    }

    function GetTargetRu()
    {
        if($this->TargetRu === null)
        {
            $str = $this->target_ru;
            $this->TargetRu = $this->ReplacePlaceholderYear($str);
        }
        return $this->TargetRu;
    }

    function GetUrlSlug()
    {
        if($this->UrlSlug === null)
        {
            $str = $this->url_slug;
            $this->UrlSlug = $this->ReplacePlaceholderYear($str);
        }
        return $this->UrlSlug;
    }

    //TODO: inject
    protected function ReplacePlaceholderYear($str)
    {
        $str = str_replace('{year}', date("Y"), $str);
        return $str;
    }

}
