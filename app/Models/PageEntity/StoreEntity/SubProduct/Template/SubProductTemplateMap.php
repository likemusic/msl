<?php namespace app\Models\PageEntity\StoreEntity\SubProduct\Template;

use Analogue\ORM\EntityMap;
use App\Models\PageEntity\StoreEntity\Product\Type\IProductType;
use App\Models\PageEntity\StoreEntity\Product\Type\ProductType;
use App\Models\PageEntity\StoreEntity\SubProduct\Name\SubProductName;
use App\Models\PageEntity\StoreEntity\SubProduct\Title\SubProductTitle;
use App\Models\PageEntity\StoreEntity\SubProduct\Uniques\SubProductUnique;

class SubProductTemplateMap extends EntityMap{
    protected $table = 'subproduct_templates';
    protected $with = ['Uniques','Name','Title'];

/*    function SubProductUniques(SubProductTemplate $subProductTemplate)
    {
        $this->hasMany($subProductTemplate, SubPdocuctUnique);
    }
*/
    //TODO: доделать
/*
    function ProductTypesWithPivot(SubProductTemplate $subProductTemplate)
    {
        return $this->belongsToMany($subProductTemplate,ProductType::class,'subproduct_uniques','subproduct_template_id')->withPivot('name', 'title', 'description','pictures_dir');
    }

    function ProductTypeWithPivot(SubProductTemplate $subProductTemplate, $ProductTypeId)
    {
        /*return $this->belongsToMany($subProductTemplate,ProductType::class,'subproduct_uniques','subproduct_template_id')
            ->where('product_type_id','=',$ProductTypeId)
            ->withPivot('name', 'title','description','pictures_dir');
    }*/

    /*function Unique(SubProductTemplate $subProductTemplate, $ProductTypeId)
    {
        return $this->hasOne($subProductTemplate,SubProductUnique::class,'subproduct_template_id')->where('product_type_id','=',$ProductTypeId);
    }*/

    function Uniques(SubProductTemplate $subProductTemplate)
    {
        return $this->hasMany($subProductTemplate,SubProductUnique::class,'subproduct_template_id');
    }

    function Name(SubProductTemplate $subProductTemplate)
    {
        return $this->belongsTo($subProductTemplate, SubProductName::class,'subproduct_name_id');
    }

    function Title(SubProductTemplate $subProductTemplate)
    {
        return $this->belongsTo($subProductTemplate, SubProductTitle::class,'subproduct_title_id');
    }
}