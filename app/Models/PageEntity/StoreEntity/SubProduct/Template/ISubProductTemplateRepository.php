<?php namespace App\Models\PageEntity\StoreEntity\SubProduct\Template;

use App\Models\PageEntity\Base\Calculated\Template\IPageEntityTemplateRepository;

interface ISubProductTemplateRepository extends IPageEntityTemplateRepository{

}