<?php namespace App\Models\PageEntity\StoreEntity\SubProduct\Template;

use App\Models\PageEntity\Base\Calculated\Template\IGetChildEntityTemplateBySearchString;
use App\Models\PageEntity\Base\Calculated\Template\IPageEntityTemplate;

interface ISubProductTemplate extends IPageEntityTemplate, IGetChildEntityTemplateBySearchString {

}