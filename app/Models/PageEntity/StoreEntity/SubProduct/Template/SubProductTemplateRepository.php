<?php namespace App\Models\PageEntity\StoreEntity\SubProduct\Template;

use App\Helpers\CacheNamer\ICacheNamer;
use App\Models\PageEntity\Base\Calculated\Template\Db\PageEntityTemplateDbRepository;
use App\Models\PageEntity\StoreEntity\SubProduct\Set\SubProductSet;
use Illuminate\Support\Facades\Cache;

class SubProductTemplateRepository extends PageEntityTemplateDbRepository implements ISubProductTemplateRepository{
    protected $ItemClassName = SubProductTemplate::class;

    /**
     * @var ICacheNamer
     */
    protected $ICacheNamer;

    public function __construct(ICacheNamer $ICacheNamer)
    {
        parent::__construct();
        $this->ICacheNamer = $ICacheNamer;
    }

    /*    function GetWhithRel($id)
        {
            $ret = $this->mapper->with('SubProductTemplates')->find($ProductType->subproduct_set_id);
            return $ret;
        }
    */

    public function SubProductTemplateBySearchString(SubProductSet $subProductSet, $SearchString)
    {
        $SubProductSetId = $subProductSet->id;
        $CacheId = $this->ICacheNamer->SubProductTemplateBySearchString($SubProductSetId, $SearchString);

        $ret = Cache::rememberForever($CacheId,function()use($SubProductSetId, $SearchString){
            return $this->SubProductTemplatesBySearchStringReal($SubProductSetId, $SearchString);
        });

        return $ret;
    }

    public function SubProductTemplatesBySearchStringReal($subProductSetId, $SearchString)
    {
        $ret = $this->mapper
            ->join('subproduct_sets_subproduct_templates','subproduct_templates.id','=','subproduct_sets_subproduct_templates.subproduct_template_id')
            ->where(['subproduct_sets_subproduct_templates.subproduct_set_id'=>$subProductSetId])
            ->where(['url_slug' =>$SearchString])
            ->select('subproduct_templates.*','subproduct_templates.id')
            ->first();
        return $ret;

        /*return $this->subproductTemplates($subProductSet)->where('subproduct_templates.url_slug', '=', $SearchString);
        //TODO: переименовать поле url в url_part или подобное в зависимости от требований*/
    }

}