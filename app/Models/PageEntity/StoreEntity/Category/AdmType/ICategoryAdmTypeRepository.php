<?php namespace App\Models\PageEntity\StoreEntity\Category\AdmType;

use App\Models\PageEntity\Base\Type\IPageEntityTypeRepository;

interface ICategoryAdmTypeRepository extends IPageEntityTypeRepository {

} 