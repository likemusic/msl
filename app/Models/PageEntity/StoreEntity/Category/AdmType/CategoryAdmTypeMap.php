<?php namespace App\Models\PageEntity\StoreEntity\Category\AdmType;

use Analogue\ORM\EntityMap;
use App\Helpers\TableNamesCatalog\ITableNamesCatalog;
use App\Models\PageEntity\StoreEntity\Category\Template\CategoryTemplate;
use App\Models\PageEntity\StoreEntity\Product\Set\ProductSet;
use App\Models\PageEntity\StoreEntity\Product\Template\ProductTemplate;

class CategoryAdmTypeMap extends EntityMap{

    /*protected $with = ['template'];*/

    function __construct()
    {
        $tableNamesCatalog = app(ITableNamesCatalog::class);
        $this->table = $tableNamesCatalog->GetCategoryAdmTypesTableName();
    }
/*
    public function template(CategoryType $CategoryType)
    {
        return $this->belongsTo($CategoryType, CategoryTemplate::class);
    }

    public function productSet(CategoryType $categoryType)
    {
        return $this->belongsTo($categoryType, ProductSet::class);
    }*/
}