<?php namespace App\Models\PageEntity\StoreEntity\Category\AdmType;

use App\Models\PageEntity\Base\Type\IPageEntityType;
use App\Models\PageEntity\Base\Calculated\Template\IGetChildEntityTemplateBySearchString;

interface ICategoryAdmType extends IPageEntityType/*, IGetChildEntityTemplateBySearchString*/ {
    //function GetCalculatedCategory(ICategoryPartContext $IGetByUrlCategoryPartContext);
    //function GetCalculatedProductByUrlSlug($url, ICategoryPartContext $IGetByUrlCategoryPartContext);
}