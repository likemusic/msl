<?php namespace App\Models\PageEntity\StoreEntity\Category\AdmType;

use App\Models\Base\Repository\Db\DbEntity;
use App\Models\PageEntity\StoreEntity\Category\Template\ICategoryTemplate;

/**
 * @property ICategoryTemplate categoryTemplate
 */
/**
 * @property int[] productTemplatesIds
 */
class CategoryAdmType extends DbEntity implements ICategoryAdmType {

/*    function GetCalculatedCategory($IGetByUrlCategoryPartContext)
    {
        $ICategoryTemplate = $this->categoryTemplate;
        if(!$ICategoryTemplate) throw new \Exception('Not set CategoryTemplate for CategoryType');

        $IGetByUrlCategoryTypeContext = $this->GetSelfGetByUrlContext($IGetByUrlCategoryPartContext);
        $CalculatedCategory = $ICategoryTemplate->GetCalculated($IGetByUrlCategoryTypeContext);
        return $CalculatedCategory;
    }

    function GetCalculatedProductByUrlSlug($url, $IGetByUrlCategoryPartContext)
    {
        $CategoryTemplate = $this->categoryTemplate;
        if(!$CategoryTemplate) return false;

        $IGetByUrlCategoryTypeContext = $this->GetSelfGetByUrlContext($IGetByUrlCategoryPartContext);
        $CalculatedProduct = $CategoryTemplate->GetCalculatedProductByUrlSlug($url, $IGetByUrlCategoryTypeContext);
        return $CalculatedProduct;
    }

    /**
     * @param ICategoryPartContext $IGetByUrlCategoryPartContext
     * @return ICategoryTypeContext
     */
/*    protected function GetSelfGetByUrlContext($IGetByUrlCategoryPartContext)
    {
        $SelfGetByUrlContext = new CategoryTypeContext($IGetByUrlCategoryPartContext, $this);
        return $SelfGetByUrlContext;
    }
*/
    /*  public function categories()
        {
            return $this->hasMany('App\Models\Category');
        }
    */
}
