<?php namespace App\Models\PageEntity\StoreEntity\Category\Type;

use App\Models\PageEntity\Base\Type\IPageEntityTypeRepository;

interface ICategoryTypeRepository extends IPageEntityTypeRepository {

} 