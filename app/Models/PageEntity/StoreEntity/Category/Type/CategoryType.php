<?php namespace App\Models\PageEntity\StoreEntity\Category\Type;

use App\Models\Base\Repository\Db\DbEntity;
use App\Models\PageEntity\StoreEntity\Category\Template\ICategoryTemplate;
use Illuminate\Support\Facades\Cache;

/**
 * @property ICategoryTemplate categoryTemplate
 */
/**
 * @property int[] productTemplatesIds
 */
class CategoryType extends DbEntity implements ICategoryType {

    protected $ISelfRepository;

    public function __construct(ICategoryTypeRepository $ICategoryTypeRepository)
    {
        parent::__construct();
        $this->ISelfRepository=$ICategoryTypeRepository;
    }

    /*public function __sleep()
    {
        $Fields = get_object_vars($this);

        $ret = [
            'ISelfRepository',
            'hidden',
            'attributes',
            'EntityMap'
        ];
        return $ret;
    }*/


    function GetCalculatedCategory($IGetByUrlCategoryPartContext)
    {
        $ICategoryTemplate = $this->categoryTemplate;
        if(!$ICategoryTemplate) throw new \Exception('Not set CategoryTemplate for CategoryType');

        $IGetByUrlCategoryTypeContext = $this->GetSelfGetByUrlContext($IGetByUrlCategoryPartContext);
        $CalculatedCategory = $ICategoryTemplate->GetCalculated($IGetByUrlCategoryTypeContext);
        return $CalculatedCategory;
    }

    function GetCalculatedProductByUrlSlug($url, $IGetByUrlCategoryPartContext)
    {
        $CategoryTemplate = $this->categoryTemplate;
        if(!$CategoryTemplate) return false;

        $IGetByUrlCategoryTypeContext = $this->GetSelfGetByUrlContext($IGetByUrlCategoryPartContext);
        $CalculatedProduct = $CategoryTemplate->GetCalculatedProductByUrlSlug($url, $IGetByUrlCategoryTypeContext);
        return $CalculatedProduct;
    }

    function GetProductSet()
    {
        $ret = ($this->ISelfRepository->UseProductSetCache())
            ? $this->GetProductSetCached() : $this->GetProductSetReal();
        return $ret;
    }

    protected function GetProductSetReal()
    {
        //return $this->productSet;
        //return $this->productSet1;
        return $this->productSetWithTemplates;
    }

    protected function GetProductSetCached()
    {
        $CacheId = 'CategoryType/productSet/'.$this->product_set_id;
        $ret = Cache::rememberForever($CacheId, function(){return $this->GetProductSetReal();});
        return $ret;
    }

    /**
     * @param ICategoryPartContext $IGetByUrlCategoryPartContext
     * @return ICategoryTypeContext
     */
/*    protected function GetSelfGetByUrlContext($IGetByUrlCategoryPartContext)
    {
        $SelfGetByUrlContext = new CategoryTypeContext($IGetByUrlCategoryPartContext, $this);
        return $SelfGetByUrlContext;
    }
*/
    /*  public function categories()
        {
            return $this->hasMany('App\Models\Category');
        }
    */
}
