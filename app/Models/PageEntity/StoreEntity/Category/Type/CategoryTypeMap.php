<?php namespace App\Models\PageEntity\StoreEntity\Category\Type;

use Analogue\ORM\EntityMap;
use App\Helpers\TableNamesCatalog\ITableNamesCatalog;
use App\Models\PageEntity\StoreEntity\Category\Template\CategoryTemplate;
use App\Models\PageEntity\StoreEntity\Product\Set\ProductSet;

class CategoryTypeMap extends EntityMap{

    protected $with = ['template',
        'productSet',
        'productSetWithTemplates'
    ];

    function __construct()
    {
        $tableNamesCatalog = app(ITableNamesCatalog::class);
        $this->table = $tableNamesCatalog->GetCategoryTypesTableName();
    }

    public function template(CategoryType $CategoryType)
    {
        return $this->belongsTo($CategoryType, CategoryTemplate::class);
    }

    public function productSet(CategoryType $categoryType)
    {
        return $this->belongsTo($categoryType, ProductSet::class,'product_set_id');
    }

    public function productSetWithTemplates(CategoryType $categoryType)
    {
        return $this->belongsTo($categoryType, ProductSet::class,'product_set_id')->with('productTemplates.ProductType');
    }

}