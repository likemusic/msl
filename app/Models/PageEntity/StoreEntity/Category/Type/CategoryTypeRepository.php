<?php namespace App\Models\PageEntity\StoreEntity\Category\Type;

use App\Models\PageEntity\Base\Type\Db\PageEntityTypeDbRepository;

class CategoryTypeRepository extends PageEntityTypeDbRepository implements ICategoryTypeRepository{
    protected $ItemClassName = CategoryType::class;

    protected $UseProductSetCache = false;

    public function __construct()
    {
        parent::__construct();

        $ConfigCacheKey = 'cache.components.CategoryType.GetProductSet';
        $this->UseProductSetCache = config($ConfigCacheKey);
    }

    public function UseProductSetCache()
    {
        return $this->UseProductSetCache;
    }
}