<?php namespace App\Models\PageEntity\StoreEntity\Category;

use App\Models\Base\Interfaces\RepositoryItem\GetChildsValues\ByEntity\IGetInternalChildsValues;
use App\Models\Base\Interfaces\RepositoryItem\GetUrlNameList\IGetChildCategoriesUrlNameList;
use App\Models\Base\Interfaces\RepositoryItem\GetUrlNameList\IGetChildProductsUrlNameList;
use App\Models\Base\Interfaces\RepositoryItem\ITreeRepositoryItem;
use App\Models\PageEntity\StoreEntity\Base\IStoreEntity;

interface ICategory
    extends IStoreEntity, ITreeRepositoryItem, IGetChildCategoriesUrlNameList,
    IGetChildProductsUrlNameList,IGetInternalChildsValues
{
    function GetLevel();
    function GetId();
}