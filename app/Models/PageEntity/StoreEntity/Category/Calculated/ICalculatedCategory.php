<?php namespace App\Models\PageEntity\StoreEntity\Category\Calculated;

use App\Models\PageEntity\StoreEntity\Base\Calculated\Root\IRootCalculatedStoreEntity;
use App\Models\PageEntity\StoreEntity\Category\ICategory;

interface ICalculatedCategory extends ICategory, IRootCalculatedStoreEntity{

} 