<?php namespace App\Models\PageEntity\StoreEntity\Category\Calculated;

use App\Models\Base\Interfaces\Repository\ITreeRepository;
use App\Models\PageEntity\StoreEntity\Base\Calculated\Root\IRootCalculatedStoreEntityInnerRepository;
use App\Models\PageEntity\StoreEntity\Category\ICategoryRepository;

interface ICalculatedCategoryRepository extends ICategoryRepository, ITreeRepository,IRootCalculatedStoreEntityInnerRepository{

}