<?php namespace App\Models\PageEntity\StoreEntity\Category\Calculated;

use App\Models\Base\Interfaces\Repository\IRepositoryEntity;
use App\Models\Base\Traits\Repository\TGetRootUrlNameList;
use App\Models\PageEntity\StoreEntity\Base\Calculated\Root\RootCalculatedStoreEntityInnerRepository;
use App\Models\PageEntity\StoreEntity\Category\Part\ICategoryPartRepository;
use App\Models\PageEntity\StoreEntity\Category\Part\ICategoryPart;
use App\Models\PageEntity\StoreEntity\Category\Template\ICategoryTemplateRepository;
use App\Models\PageEntity\StoreEntity\Category\Type\ICategoryTypeRepository;
use App\Models\PageEntity\StoreEntity\Product\Calculated\ICalculatedProductRepository;
use App\Models\PageEntity\StoreEntity\Product\IProductRepository;
use Illuminate\Support\Facades\Cache;

class CalculatedCategoryRepository extends RootCalculatedStoreEntityInnerRepository implements ICalculatedCategoryRepository{
    use TGetRootUrlNameList;
    /**
     * @var ICategoryPartRepository
     */
    protected $IPartRepository;

    /**
     * @var ICategoryTemplateRepository
     */
    protected $ITemplateRepository;

    /**
     * @var ICategoryTypeRepository
     */
    protected $ICategoryTypeRepository;

    /**
     * @var ICalculatedProductRepository
     */
    protected $IChildRepository;


    protected $ErrorMessageOnGetChildByExternalEntity = 'Category is root, therefor ParentEntity must be empty!';


    protected $InternalChildsByInternalEntity=[];

    protected $UseInnerChildsUrlNameListCache = false;
    protected $InternalChildsUrlNameListByInternalEntity = [];

    function __construct(
        ICategoryPartRepository $ICategoryPartRepository,
        ICategoryTemplateRepository $ICategoryTemplateRepository,
        ICategoryTypeRepository $ICategoryTypeRepository,
        IProductRepository $ICalculatedProductRepository
    )
    {
        $this->IPartRepository = $ICategoryPartRepository;
        $this->ITemplateRepository = $ICategoryTemplateRepository;
        $this->ICategoryTypeRepository = $ICategoryTypeRepository;
        $this->IChildRepository = $ICalculatedProductRepository;
        $this->UseInnerChildsUrlNameListCache = config('cache.components.CategoryInnerChildsUrlNameList');
    }

    function FindBySearchString($str)
    {

        $Parts = $this->IPartRepository->FindBySearchString($str);
        $ret = $this->PartsListToCalculatedList($Parts);
        return $ret;
    }

    function GetById($id)
    {
        $Part = $this->IPartRepository->GetById($id);
        if(!$Part) return null;
        $Calculated = $this->CategoryPartToCalculatedCategory($Part, '');
        return $Calculated;
    }

    /* ------ By UrlSlug ----- */
    function GetRootByUrlSlug($url)
    {
        $HaveVirtualRoot = $this->IPartRepository->GetVirtualRootCategoryId();
        if(!$HaveVirtualRoot) $ret = $this->GetInternalChildByUrlSlugForExternalEntity($url, null);
        else{
            $RootCategory = $this->GetVirtualRoot();
            /** @var ICalculatedCategory $RootCategory */
            if('' == $url) $ret = $RootCategory;
            else{
                $ret = $RootCategory->GetChildByUrlSlug($url);
            }
        }
        return $ret;
    }

    function GetVirtualRoot()
    {
        $RootCategoryPart = $this->IPartRepository->GetVirtualRoot();
        $CalculatedCategory = $this->CategoryPartToCalculatedCategory($RootCategoryPart);
        return $CalculatedCategory;
    }

    function GetInternalChildByUrlSlugForExternalEntity($url, $ParentEntity)
    {
        if($ParentEntity) throw new \Exception($this->ErrorMessageOnGetChildByExternalEntity);
        $ICategoryPart = $this->IPartRepository->GetRootByUrlSlug($url);
        if(!$ICategoryPart) return null;

        $CalculatedCategory = $this->CategoryPartToCalculatedCategory($ICategoryPart, $url);
        return $CalculatedCategory;
    }

    function GetInternalChildByUrlSlugForInternalEntity($url, $ParentEntity)
    {
        $ParentCategoryPart = $ParentEntity->GetContext()->GetPartContext();
        $ICategoryPart = $this->IPartRepository->GetInternalChildByUrlSlugForInternalEntity($url, $ParentCategoryPart);
        if(!$ICategoryPart) return null;

        $CalculatedCategory = $this->CategoryPartToCalculatedCategory($ICategoryPart, $url, $ParentEntity);
        return $CalculatedCategory;
    }

    function GetInternalChildsUrlNameListByExternalEntity(IRepositoryEntity $entity=null)
    {
        if($entity) throw new \Exception($this->ErrorMessageOnGetChildByExternalEntity);//TODO: может быть должен возвращать Roots при null?
        return $this->GetInternalChildsUrlNameListByInternalEntity($entity);
    }

    function GetInternalChildsUrlNameListByInternalEntity(IRepositoryEntity $entity = null)
    {
        //TODO: Добавить кэширование в свойствах
        $Id = $entity ? $entity->GetId() : 0;
        if(!array_key_exists($Id, $this->InternalChildsUrlNameListByInternalEntity))
        {
            $ret = $this->UseInnerChildsUrlNameListCache ?
                $this->GetInternalChildsUrlNameListByInternalEntityCached($entity)
                : $this->GetInternalChildsUrlNameListByInternalEntityReal($entity);
            $this->InternalChildsUrlNameListByInternalEntity[$Id] = $ret;
        }
        return $this->InternalChildsUrlNameListByInternalEntity[$Id];
    }

    protected function GetInternalChildsUrlNameListByInternalEntityCached(IRepositoryEntity $entity = null)
    {
        $Id = $entity ? $entity->GetId() : 0;
        $CacheId = 'Category/InternalChildsUrlNameList/'.$Id;

        $ret = Cache::rememberForever($CacheId,function() use ($entity){return $this->GetInternalChildsUrlNameListByInternalEntityReal($entity);});
        return $ret;
    }

    //TODO: $ads as param
    function GetInternalChildsUrlNameListByInternalEntityReal(IRepositoryEntity $entity = null)
    {
        $CalculatedList = $this->GetInternalChildsByInternalEntity($entity);
        $adds = ['src_genitive','type_id'];
        $UrlNameList = $this->CalculatedListToUrlNameList($CalculatedList, $adds);
        return $UrlNameList;
    }

    function GetChildCategoriesUrlNameList($Entity)
    {
        return $this->GetInternalChildsUrlNameListByInternalEntity($Entity);
    }

    function GetChildProductsUrlNameListByEntity($Entity)
    {
        return $this->GetExternalChildsUrlNameListByInternalEntity($Entity);
    }

    /* --- Get Values --- */
    function GetInternalChildsValuesByExternalEntity(IRepositoryEntity $entity, $Fields=null)
    {
        if($entity) throw new \Exception($this->ErrorMessageOnGetChildByExternalEntity);
        return $this->GetInternalChildsValuesByInternalEntity(null,$Fields);
    }

    function GetInternalChildsValuesByInternalEntity(IRepositoryEntity $entity=null, $Fields)
    {
        $InternalChilds = $this->GetInternalChildsByInternalEntity($entity);
        if(!$InternalChilds) return null;
        $InternalChildsValues = $this->EntityListToValuesList($InternalChilds, $Fields);
        return $InternalChildsValues;
    }

    protected function PartValuesToUrlNameListItem($PartValues, $CurrentUrl)
    {
        $ret = [
            'genitive'  => $PartValues['genitive'],
            'url'   => $CurrentUrl.'/'.$PartValues['url_slug'],
            'type_id' => $PartValues['type_id']
        ];
        return $ret;
    }

    protected function CategoryPartToCalculatedCategory(ICategoryPart $ICategoryPart, $url=null, ICalculatedCategory $Parent = null)
    {
        $ICategoryType = $ICategoryPart->GetCategoryType();
        $ICategoryTemplate = $ICategoryType->template;

        if((!$Parent) and ($ICategoryPart->HasParent()))
        {
            $IPartParent = $ICategoryPart->GetInternalParent();
            $Parent = $this->CategoryPartToCalculatedCategory($IPartParent);
        }

        $Context = new CalculatedCategoryContext($ICategoryPart,$ICategoryType, $ICategoryTemplate, $url, $Parent);
        $CalculatedCategory = new CalculatedCategory($Context, $this);
        return $CalculatedCategory;
    }

    protected function PartsListToCalculatedList($ICategoryParts, $ParentEntity=null)
    {
        $CalculatedCategories = [];
        foreach($ICategoryParts as $ICategoryPart)
        {
            $CalculatedCategory = $this->CategoryPartToCalculatedCategory($ICategoryPart, null, $ParentEntity);
            $CalculatedCategories []= $CalculatedCategory;
        }
        return $CalculatedCategories;
    }

    function GetInternalParent(IRepositoryEntity $entity)
    {
        $CategoryPart = $entity->GetContext()->GetPartContext();
        $ICategoryPartParent = $ICategoryParts = $this->IPartRepository->GetInternalParent($CategoryPart);
        $InternalParent = $this->CategoryPartToCalculatedCategory($ICategoryPartParent);
        return $InternalParent;
    }

    function GetInternalChildsByInternalEntity(IRepositoryEntity $entity=null, $Fields=null)
    {
        $Id = $entity ? $entity->GetId() : 0;
        if(!array_key_exists($Id, $this->InternalChildsByInternalEntity))
        {
            $ParentCategoryPart = $entity ? $entity->GetContext()->GetPartContext() : null;

            //TODO: Convert calculated fields to part fields and versa
            $ICategoryParts = $this->IPartRepository->GetInternalChildsByInternalEntity($ParentCategoryPart);

            if(count($ICategoryParts)==0) return [];

            $CalculatedCategories = $this->PartsListToCalculatedList($ICategoryParts, $entity);

            $this->InternalChildsByInternalEntity[$Id] = $CalculatedCategories;
        }
        return $this->InternalChildsByInternalEntity[$Id];
    }

    function GetInternalChildsByExternalEntity(IRepositoryEntity $Entity = null, $Fields = null)
    {
        if($Entity) throw new \Exception();
        return $this->GetInternalChildsByInternalEntity($Entity, $Fields);
    }

    function GetByVID($Id)
    {
        $IPart = $this->IPartRepository->GetById($Id);
        $ret = $this->CategoryPartToCalculatedCategory($IPart);
        return $ret;
    }

    function GetByFullVID($Id)
    {
        //TODO: сделать с проверкой всех под-id
        $Chunks = explode(':',$Id);
        $count = count($Chunks);
        if($count!=3) throw new \Exception('Invalid category full id:'.$Id);
        $PartId = $Chunks[0];
        $ret = $this->GetByVID($PartId);
        return $ret;
    }
}