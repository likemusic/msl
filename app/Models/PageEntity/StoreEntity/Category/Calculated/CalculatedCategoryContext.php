<?php namespace App\Models\PageEntity\StoreEntity\Category\Calculated;

use App\Models\PageEntity\StoreEntity\Base\Calculated\Root\RootCalculatedStoreEntityContext;
use App\Models\PageEntity\StoreEntity\Category\Part\ICategoryPart;
use App\Models\PageEntity\StoreEntity\Category\Template\ICategoryTemplate;
use App\Models\PageEntity\StoreEntity\Category\Type\ICategoryType;

class CalculatedCategoryContext extends RootCalculatedStoreEntityContext implements ICalculatedCategoryContext{

    function __construct( ICategoryPart $ICategoryPart, ICategoryType $ICategoryType, ICategoryTemplate $ICategoryTemplate, $UrlSlug, ICalculatedCategory $IParentCalculatedCategory=null)
    {
        parent::__construct($ICategoryPart, $ICategoryType, $ICategoryTemplate, $UrlSlug, $IParentCalculatedCategory );
    }
}