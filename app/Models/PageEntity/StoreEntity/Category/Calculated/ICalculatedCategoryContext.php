<?php namespace App\Models\PageEntity\StoreEntity\Category\Calculated;

use App\Models\PageEntity\StoreEntity\Base\Calculated\Root\IRootCalculatedStoreEntityContext;

interface ICalculatedCategoryContext extends IRootCalculatedStoreEntityContext{

}