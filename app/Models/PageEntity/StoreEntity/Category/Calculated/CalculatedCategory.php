<?php namespace App\Models\PageEntity\StoreEntity\Category\Calculated;

//use App\Helpers\StoreTreeBuilder\IStoreTreeBuilder;
use App\Models\Base\Traits\RepositoryItem\TPartBasedCalculatedStoreTreeItem;
use App\Models\Base\Traits\RepositoryItem\TTreeRepositoryItem;
use App\Models\PageEntity\StoreEntity\Base\Calculated\Root\RootCalculatedStoreEntity;
use App\Models\PageEntity\StoreEntity\Category\Part\ICategoryPart;

class CalculatedCategory extends RootCalculatedStoreEntity implements ICalculatedCategory{
    use TPartBasedCalculatedStoreTreeItem, TTreeRepositoryItem;
    /**
     * @var ICategoryPart
     */
    protected $IPart;

    protected $Products;
    protected $InnerChildsGroupedBuType;
    protected $TypeId;
    protected $HasParent;
    protected $FullVID;
    protected $VID;
    protected $SrcGenitive;
    protected $MenuTreeName;

    public function __construct(ICalculatedCategoryContext $ICalculatedCategoryContext, ICalculatedCategoryRepository $ICalculatedCategoryRepository)
    {
        parent::__construct($ICalculatedCategoryContext, $ICalculatedCategoryRepository);
        $this->IPart = $ICalculatedCategoryContext->GetPartContext();
    }

    function GetKnowFields()
    {
        $ParentFields = parent::GetKnowFields();
        $AddFields = [
            'products'      => 'GetProductsValues',
            'inner_childs'  => 'GetInternalChildsUrlNameList',
            'outer_childs'  => 'GetExternalChildsUrlNameList',
            'inner_childs_grouped_by_type' => 'GetInnerChildsGroupedByType',
            'type_id'       => 'GetTypeId',
            'main_picture'  => 'GetMainPicture',
            'page_h1'           => 'GetGenitive',
            //'breadcrumbs'       => $this->GetBreadcrumbs(),
            //TODO: сделать 'inner_childs' = type1 + typ2 + ... typeN
            //'inner_childs_type1'    => $this->GetInternalChildsUrlNameList(['type'=>1]),
            //'inner_childs_type2'    => $this->GetInternalChildsUrlNameList(['type'=>1]),
            //'sub_city'          => $this->GetInnerChilds(['type'=>1]),
            //'sub_region'          => $this->GetInnerChilds(['type'=>2]),
        ];

        $Fields = $ParentFields + $AddFields;
        return $Fields;
    }

    public function GetId()
    {
        return $this->IPart->id;
    }

    public function GetLevel()
    {
        return $this->IPart->level;
    }

    function HasParent()
    {
        if($this->HasParent === null)
        {
            $this->HasParent = $this->IPart->HasParent();
        }
        return $this->HasParent;
    }

    function GetSrcName()
    {
        $ret = $this->IPart->GetSrcName();
        return $ret;
    }

    function GetName()
    {
        //return $this->IPart->GetName();

        if($this->Name === null)
        {
            $Template = $this->ITemplate->name;
            $Name = str_replace('{city}', $this->IPart->GetGenitive(), $Template);
            $this->Name = $Name;
        }
        else $Name = $this->Name;
        return $Name;
    }

    function GetMenuTreeName()
    {
        if(!$this->MenuTreeName)
        {
            $this->MenuTreeName = $this->IPart->GetMenuTreeName();
        }
        return $this->MenuTreeName;
    }

    function GetSrcGenitive()
    {
        $ret = $this->IPart->GetSrcGenitive();
        return $ret;
    }

    function GetGenitive()
    {
        return $this->IPart->GetGenitive();
        /*
        if(!$this->Genitive)
        {
            $Template = $this->ITemplate->name;
            $Name = str_replace('{city}', $this->IPart->GetGenitive(), $Template);
            $this->Genitive = $Name;
        }
        return $this->Genitive;*/
    }

    function GetMainPicture()
    {
        //TODO: сделать
        return null;
    }

    function GetProductsValues()
    {
        if($this->Products === null)
        {
            $ret = $this->GetExternalChildsValues(['url', 'name', 'price', 'main_picture']);
            $this->Products = $ret;
        }
        else $ret = $this->Products;
        return $ret;
    }

    function GetInnerChildsGroupedByType()
    {
        if($this->InnerChildsGroupedBuType === null)
        {
            $InnerChilds = $this->GetInternalChildsUrlNameList();
            if(!$InnerChilds) return null;
            $ret = $this->GroupByField($InnerChilds, 'type_id');
        }
        else $ret = $this->InnerChildsGroupedBuType;
        return $ret;
    }

    public function GetTypeId()
    {
        if($this->TypeId === null)
        {
            $ret = $this->IPart->type_id;
            $this->TypeId = $ret;
        }
        else $ret = $this->TypeId;
        return $ret;
    }

    protected function GetPlaceholderCity()
    {
        return $this->IPart->GetGenitive();
    }

    protected function GroupByField($List, $key)
    {
        $ret = [];
        foreach($List as $Item)
        {
            $ret[$Item[$key]] []= $Item;
        }
        return $ret;
    }

    //TODO: удалить или использовать через GetExternal...UrlNameList()
    function GetChildProductsUrlNameList()
    {
        //return $this->ISelfRepository->GetChildProductsUrlNameListById($this->IPart->id);
        return $this->GetExternalChildsUrlNameList();
    }

    function GetFullVID()
    {
        if($this->FullVID === null)
        {
            //$EntityId = 1;
            $PartId = $this->IPart->GetId();
            $TypeId = $this->type_id;
            $TemplateId = $this->ITemplate->GetId();
            $VID = "{$PartId}:{$TypeId}:{$TemplateId}";
            $this->FullVID = $VID;
        }
        return $this->FullVID;
    }

    function GetVID()
    {
        if($this->VID === null)
        {
            //$EntityId = 1;
            $PartId = $this->IPart->GetId();
            $this->VID = $PartId;
        }
        return $this->VID;
    }
}