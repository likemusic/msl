<?php namespace App\Models\PageEntity\StoreEntity\Category\Part;

use App\Helpers\CacheNamer\ICacheNamer;
use App\Models\Base\Interfaces\Repository\IRepositoryEntity;
use App\Models\PageEntity\Base\Calculated\Part\Db\PageEntityPartDbRepository;
use Illuminate\Support\Facades\Cache;

class CategoryPartRepository extends PageEntityPartDbRepository implements ICategoryPartRepository{

    protected $ItemClassName = CategoryPart::class;
    //TODO: may be do on class name without current namespace

    protected $VirtualRootCategoryId=null;//влияет только на иерархию чпу
    protected $UseTypeCache = false;

    protected $UseCacheInternalChildByUrlSlugForInternalEntity = false;

    protected $UseCacheRootByUrlSlug=false;
    /**
     * @var ICacheNamer
     */
    protected $ICacheNamer;
    function __construct(ICacheNamer $ICacheNamer)
    {
        parent::__construct();
        $this->VirtualRootCategoryId = config('site.category_part.root_category_id');
        $this->UseTypeCache = config('cache.components.CategoryPart.type');
        $this->UseCacheInternalChildByUrlSlugForInternalEntity = config('cache.components.CategoryPart.InternalChildsByUrlSlugForInternalEntity');
        $this->UseCacheRootByUrlSlug = config('cache.components.CategoryPart.RootByUrlSlug');
        $this->ICacheNamer = $ICacheNamer;
    }

    function GetCacheNamer()
    {
        return $this->ICacheNamer;
    }

    function UseTypeCache()
    {
        return $this->UseTypeCache;
    }

    function GetVirtualRootCategoryId()
    {
        return $this->VirtualRootCategoryId;
    }

    function GetVirtualRoot()
    {
        $ret = $this->GetById($this->GetVirtualRootCategoryId());
        return $ret;
    }

    /**
     * @param $url
     * @param ICategoryPart $ParentEntity
     * @return mixed
     */
    function GetInternalChildByUrlSlugForInternalEntity($url, $ParentEntity)
    {
        //TODO: cache null return values
        $ret = ($this->UseCacheInternalChildByUrlSlugForInternalEntity)
            ? $this->GetInternalChildByUrlSlugForInternalEntityCached($url,$ParentEntity)
            : $this->GetInternalChildByUrlSlugForInternalEntityReal($url,$ParentEntity);

        return $ret;
    }

    protected function GetInternalChildByUrlSlugForInternalEntityReal($url,$ParentEntity)
    {
        return $this->internalChilds($ParentEntity, $url);
        //return $ParentEntity->internalChilds($url)->first();
    }

    protected function internalChilds(CategoryPart $CategoryPart, $url_slug=null)
    {
        $matching = ['parent'=>$CategoryPart->id];
        $ret = $this->mapper->where($matching);
        if(!$url_slug) $ret = $ret->orderBy('sort_order')->orderBy('type_id')->orderBy('src_name')->orderBy('name');
        else $ret->where('url_slug','=', $url_slug);
        $ret = $ret->first();
        return $ret;
    }

    protected function GetInternalChildByUrlSlugForInternalEntityCached($url,$ParentEntity)
    {
        $CacheId = $this->ICacheNamer->CategoryPartInternalChilds($ParentEntity,$url);
        $ret = Cache::rememberForever($CacheId, function() use($url,$ParentEntity){return $this->GetInternalChildByUrlSlugForInternalEntityReal($url,$ParentEntity);});
        return $ret;
    }

    function GetRootChildsValues($Fields)
    {
        return $this->GetInternalChildsValuesByExternalEntity(null,$Fields);
    }

    function GetRoots()
    {
        $ret = $this->GetInternalChildsById(0);
        /*if($this->VirtualRootCategoryId){
            $VirtualRoots = $this->GetInternalChildsById($this->VirtualRootCategoryId);
            $ret []= $VirtualRoots;
        }*/
        return $ret;
    }

    function GetInternalChildsByInternalEntity(IRepositoryEntity $Entity = null, $Fields = null)
    {   //TODO: добавить кэширование
        if($Entity) return $this->GetInternalChilds($Entity);
        else return $this->GetRoots();
    }

    protected function GetInternalChilds(IRepositoryEntity $entity=null)
    {
        $ret = $this->mapper->where(['parent'=>$entity->id]);
        $ret = $this->AddOrdersScope($ret);
        $ret = $ret->get();
        return $ret;
        //return $entity->internalChilds;
    }

    function GetInternalChildsValuesByExternalEntity(IRepositoryEntity $entity, $Fields=null)
    {
        $Id = ($entity) ? $entity : 0;
        return $this->GetInternalChildsValuesById($Id, $Fields);
    }

    protected function GetInternalChildsValuesById($Id, $Fields)
    {
        $res = $this->mapper->query()->getQuery();
        $res = $this->AddRootsParentScopeRestriction($res, $Id);
        $res = $this->AddOrdersScope($res);
        $arr = $res->get($Fields);
        return $arr;
    }

    function GetInternalChildsValuesByInternalEntity(IRepositoryEntity $entity=null, $Fields)
    {
        $Id = ($entity) ? $entity->id : 0;
        return $this->GetInternalChildsValuesById($Id, $Fields);
    }

    protected function GetInternalChildsById($Id)
    {
        //Если $Id = 0 - запрос корневых
        //Корневые заданы в кофиге
        $ret = $this->mapper;
        $ret = $this->AddRootsParentScopeRestriction($ret, $Id);
        $ret = $this->AddOrdersScope($ret);
        //TODO: переделать на стандартную связь Laravel (hasMany)
        $ret =  $ret->get();
        return $ret;//TODO: implement order_by by global scopes
    }

    protected function AddOrdersScope($ret)
    {
        $ret = $ret->orderBy('sort_order')->orderBy('type_id')->orderBy('src_name')->orderBy('name');
        return $ret;
    }

    protected function AddRootsParentScopeRestriction($res, $Id=0)
    {
        if($Id>0) return $res->where(['parent'=> $Id]);

        $RootsConfig = config('site.category_part');

        if($RootsConfig['strategy']=='include')
        {
            $includes = $RootsConfig['include'];
            $res = $res->whereIn('id', $includes);
        }
        elseif($RootsConfig['strategy']=='exclude')
        {
            $excludes = $RootsConfig['exclude'];
            $res = $res->whereNotIn('id',$excludes);
        }
        else//$RootsConfig['strategy']=='all'
        {
            $res = $res->where(['parent'=> $Id]);
        }
        return $res;
    }

    protected function AddRootsIdScopeRestriction($res)
    {
        $RootsConfig = config('category_part');

        if($RootsConfig['strategy']=='include')
        {
            $includes = $RootsConfig['include'];
            $res = $res->whereIn('id', $includes);
        }
        elseif($RootsConfig['strategy']=='exclude')
        {
            $excludes = $RootsConfig['exclude'];
            $res = $res->whereNotIn('id',$excludes);
        }
        return $res;
    }

    function GetRootByUrlSlug($url)
    {
        $ret = ($this->UseCacheRootByUrlSlug)
            ? $this->GetRootByUrlSlugCached($url)
            : $this->GetRootByUrlSlugReal($url);
        return $ret;
    }

    /**
     * @param $url
     * @return ICategoryPart
     */
    function GetRootByUrlSlugReal($url)
    {
        $res = $this->GetRealRootByUrlSlug($url);
        if($res) return $res;

        $res = $this->GetVirtualRootByUrlSlug($url);
         return $res;
    }

    function GetRootByUrlSlugCached($url)
    {
        $CacheId = $this->ICacheNamer->RootByUrlSlug($url);
        $ret = Cache::rememberForever($CacheId,function()use($url){
            return $this->GetRootByUrlSlugReal($url);
        });
        return $ret;
    }

        protected function GetRealRootByUrlSlug($url)
    {
        $res = $this->mapper->where('url_slug','=',$url);
        $res = $this->AddRootsParentScopeRestriction($res);
        $ret = $res->first();
        return $ret;
    }

    protected function GetVirtualRootByUrlSlug($url)
    {
        $VirtualRootCategoryId = config('site.category_part.root_category_id');
        if(!$VirtualRootCategoryId) return null;

        if(!$url)
        {
            $ret = $this->GetById($VirtualRootCategoryId);
        }
        else
        {
            $res = $this->mapper->where('url_slug','=',$url)->where('parent','=',$VirtualRootCategoryId);
            $ret = $res->first();
        }
        return $ret;
    }

    function GetInternalParent(IRepositoryEntity $entity)
    {
	return $this->GetById($entity->parent);
        //return $entity->internalParent;
    }

    function FindBySearchString($str)
    {
/*        $ret = Cache::rememberForever('SearchResult-' . $str,
            function () use($str){
*/
                $parts = $this->MakeRealSearchArray($str);

                $ret = $this->mapper;
                foreach ($parts as $part) {
                    $ret = $ret->where('src_name', 'like', "%{$part}%");
                }

                if ($this->VirtualRootCategoryId) $ret = $ret->where('root_id', '=', $this->VirtualRootCategoryId);
                $ret = $ret->orderBy('level')->orderBy('src_name');

                $ret = $ret->get();
                return $ret;
/*            }
        );
        return $ret;
*/    }

    protected function MakeRealSearchArray($str)
    {
        $string2="";
        $arrparts = explode(' ', $str);
        $searchstrings = [];
        foreach( $arrparts as $key=> $val )//зачем удаляем последний символ???
        {
            if ( strlen( trim($val) ) > 3 )
            {
                $searchstrings[] = mb_substr(trim($val),0,-1);
            }else  $searchstrings[] = trim($val);
        }

        $ret = [];
        foreach( $searchstrings as $val )
        {
            //if(strlen($val)<3) continue;
            if(($val=='район') || ($val=='райо')) continue;
            if(($val=='карт')) continue;
            $ret[] = $val;
        }

        return $ret;
    }
}