<?php namespace App\Models\PageEntity\StoreEntity\Category\Part;

use App\Models\Base\Interfaces\Repository\GetByUrlSlug\IGetInternalChildByUrlSlugForInternalEntity;
use App\Models\Base\Interfaces\Repository\GetChildsValues\ByEntity\IGetInternalChildsValuesByExternalEntity;
use App\Models\Base\Interfaces\Repository\GetChildsValues\ByEntity\IGetInternalChildsValuesByInternalEntity;
use App\Models\Base\Interfaces\Repository\GetChildsValues\ByEntity\IGetRootChildsValues;
use App\Models\Base\Interfaces\Repository\IFindBySearchString;
use App\Models\Base\Interfaces\Repository\ITreeRepository;
use App\Models\PageEntity\Base\Calculated\Part\IPageEntityPartRepository;
use App\Models\PageEntity\Base\UrlLinked\Root\IRootUrlLinkedEntityRepository;

interface ICategoryPartRepository extends IPageEntityPartRepository,/* IRootUrlLinkedEntityRepository,*/
    IGetRootChildsValues,

    IGetInternalChildsValuesByExternalEntity,
    IGetInternalChildsValuesByInternalEntity,

    IGetInternalChildByUrlSlugForInternalEntity,
    ITreeRepository,
    IFindBySearchString
    {
        function GetVirtualRootCategoryId();
    }
