<?php namespace App\Models\PageEntity\StoreEntity\Category\Part;

use App\Models\Base\Interfaces\RepositoryItem\IGetUrlSlug;
use App\Models\Base\Interfaces\RepositoryItem\ITreeRepositoryItem;
use App\Models\PageEntity\Base\Calculated\Part\IPageEntityPart;

interface ICategoryPart extends IPageEntityPart, IGetUrlSlug, ITreeRepositoryItem{

}