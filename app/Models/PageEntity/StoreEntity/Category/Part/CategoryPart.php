<?php namespace App\Models\PageEntity\StoreEntity\Category\Part;

use App\Helpers\CacheNamer\ICacheNamer;
use App\Models\Base\Traits\RepositoryItem\TGetUrlSlugSimple;
use App\Models\Base\Traits\RepositoryItem\TTreeRepositoryItem;
use App\Models\PageEntity\Base\Calculated\ICalculatedPageEntityContext;
use App\Models\PageEntity\Base\Calculated\Part\Db\PageEntityPartDb;
use App\Models\PageEntity\StoreEntity\Category\Type\ICategoryType;

use App\Models\Base\Traits\RepositoryItem\TGetName;
use Illuminate\Support\Facades\Cache;


/**
 * @property ICategoryType type
 * @property int id
 */
class CategoryPart extends PageEntityPartDb implements ICategoryPart
{
    use TGetName, TTreeRepositoryItem;
    use TGetUrlSlugSimple {GetUrlSlug as protected TraitGetUrlSlug;}
    /**
     * @var ICategoryPartRepository
     */
    protected $ICategoryPartRepository;

    /**
     * @var ICalculatedPageEntityContext
     */
    protected $ICalculatedParentPageEntityContext;

    protected $VirtualRootCategoryId=null;

    protected $IsVirtualRoot = null;

    protected $InternalParentLoaded = false;

    protected $InternalParent = null;

    protected $HasParent = null;

    protected $SrcGenitive;
    protected $Name;

    protected $Genitive;

    /**
     * @var ICacheNamer
     */
    protected $ICacheNamer;

    function __construct(ICategoryPartRepository $ICategoryPartRepository, array $attributes = array(), ICalculatedPageEntityContext $ICalculatedParentPageEntityContext = null)
    {
        parent::__construct();

        $this->ICategoryPartRepository = $ICategoryPartRepository;
        $this->ICalculatedParentPageEntityContext = $ICalculatedParentPageEntityContext;

        $VirtualRootCategoryId = $ICategoryPartRepository->GetVirtualRootCategoryId();
        $this->VirtualRootCategoryId = $VirtualRootCategoryId;
        $this->ICacheNamer = $ICategoryPartRepository->GetCacheNamer();
    }

    /*public function __sleep()
    {
        //$Fields = get_object_vars($this->ICategoryPartRepository);
        #$ser = serialize($this->ICategoryPartRepository);
        #print_r(array_keys(get_object_vars($this)));
        $ret = [
            'ICategoryPartRepository',
            'EntityMap',
            'attributes'
        ];
        return $ret;
    }*/

    public function GetCategoryType()
    {
        $UseCache = $this->ICategoryPartRepository->UseTypeCache();
        $ret = $UseCache ? $this->GetCategoryTypeCached() : $this->GetCategoryTypeReal();
        return $ret;
    }

    protected function GetCategoryTypeReal()
    {
        return $this->type;
    }

    protected function GetCategoryTypeCached()
    {
        $CacheId = $this->ICacheNamer->GetCategoryPartToCategoryType($this);
        $ret = Cache::rememberForever($CacheId, function(){return $this->GetCategoryTypeReal();});
        return $ret;
    }

    public function IsVirtualRoot()
    {
        if($this->IsVirtualRoot === null)
        {
            $this->IsVirtualRoot = ($this->id == $this->VirtualRootCategoryId);
        }
        return $this->IsVirtualRoot;
    }

    function GetUrlSlug()
    {
        if(!$this->IsVirtualRoot()) return $this->TraitGetUrlSlug();
        else return '';
    }

    function HasParent()
    {
        if($this->HasParent === null)
        {
            if($this->IsVirtualRoot()) $this->HasParent = false;
            elseif($this->parent > 0) $this->HasParent = true;
            else $this->HasParent = false;
        }
        return $this->HasParent;
    }

    function GetInternalParent()
    {
        if(!$this->InternalParentLoaded)
        {
            if(!$this->HasParent()) $this->InternalParent = null;
            else $this->InternalParent = $this->GetParent();
            $this->InternalParentLoaded = true;
        }
        return $this->InternalParent;
    }

    protected function GetParent()
    {
	return $this->ICategoryPartRepository->GetInternalParent($this);

        //return $this->internalParent;
        /*
        $ret = Cache::rememberForever('CategoryPart-Parent-' . $this->parent,
            function (){return $this->internalParent;}
        );
        //$ret = $this->internalParent;

        //$ser = serialize($ret);
        return $ret;*/
    }

    function GetSrcName()
    {
        return $this->src_name;
    }

    function GetName()
    {
        if($this->Name === null)
        {
            $Name = $this->name;
            if(!$Name) $Name = $this->GetCalculatedName();
            $this->Name = $Name;
        }
        return $this->Name;
    }

    function GetSrcGenitive()
    {
        if($this->SrcGenitive === null)
        {
            $SrcGenitive = $this->src_genitive;
            if(!$SrcGenitive) $SrcGenitive = $this->GetSrcName();
            $this->SrcGenitive = $SrcGenitive;
        }
        return $this->SrcGenitive;
    }

    function GetGenitive()
    {
        if($this->Genitive === null)
        {
            $Genitive = $this->genitive;
            if(!$Genitive) $Genitive = $this->GetCalculatedGenitive();
            $this->Genitive = $Genitive;
        }
        return $this->Genitive;
    }

    protected function GetCalculatedName()
    {
        $SrcName = $this->GetSrcName();
        $ret = $this->GetWithAdmType($SrcName);
        return $ret;
    }

    protected function GetCalculatedGenitive()
    {
        $SrcName = $this->GetSrcGenitive();
        $ret = $this->GetWithAdmType($SrcName);
        return $ret;
    }

    function GetMenuTreeName()
    {
        $Type = $this->adm_type;
        $ret = ($Type && $Type->show) ? $this->GetGenitive() : $this->GetSrcGenitive();
        return $ret;
    }

    protected function GetWithAdmType($SrcName)
    {
        $Type = $this->adm_type;
        $TypeShortName = $Type->used_genitive;
        $ret = null;
        //if(!$Type->show) $ret = $SrcName;
        //else
        {
            switch($Type->use_position)
            {
                case 0: $ret = $SrcName; break;
                case 1: $ret = "{$TypeShortName} {$SrcName}"; break;
                case 2: $ret = "{$SrcName} {$TypeShortName}"; break;
            }
        }
        return $ret;
    }

}