<?php namespace App\Models\PageEntity\StoreEntity\Category\Part;

use Analogue\ORM\EntityMap;
use App\Helpers\CacheNamer\ICacheNamer;
use App\Helpers\TableNamesCatalog\ITableNamesCatalog;
use App\Models\PageEntity\StoreEntity\Category\AdmType\CategoryAdmType;
use App\Models\PageEntity\StoreEntity\Category\Type\CategoryType;
use App\Models\PageEntity\StoreEntity\Product\Part\ProductPart;
use Illuminate\Support\Facades\Cache;

class CategoryPartMap extends EntityMap {
    public $UseDisabledScope = true;
    protected $with = [
        'type',
        'adm_type',
        //'product_parts',

        //'internalChilds',
        //'internalParent'
    ];//TODO: раскомментить?

    /**
     * @var ICacheNamer
     */
    protected $ICacheNamer;

    //TODO: impliment injection
    function __construct()
    {
        $tableNamesCatalog = app(ITableNamesCatalog::class);
        $this->table = $tableNamesCatalog->GetCategoryPartsTableName();
        $this->ICacheNamer = app(ICacheNamer::class);
    }

    public function getEagerloadedRelationships()
    {
        //закэшированные свойства убираем из ранней загрузки
        if(config('cache.components.CategoryPart.type'))
        {
            unset($this->with[ array_search('type',$this->with)]);
        }
        return $this->with;
    }


    public function type(CategoryPart $CategoryPart)
    {
        return $this->belongsTo($CategoryPart, CategoryType::class);
    }

    public function adm_type(CategoryPart $CategoryPart)
    {
        return $this->belongsTo($CategoryPart, CategoryAdmType::class);
    }

    /*public function product_parts(CategoryPart $CategoryPart)
    {
        return $this->hasMany($CategoryPart, ProductPart::class,'category_id')->orderBy('sort_order')->orderBy('id');
    }*/

    /*public function internalChilds(CategoryPart $CategoryPart, $url_slug=null)
    {
        $ret = $this->hasMany($CategoryPart, CategoryPart::class, 'parent');
        if(!$url_slug) $ret->orderBy('sort_order')->orderBy('type_id')->orderBy('src_name')->orderBy('name');
        else $ret->where('url_slug','=', $url_slug);
        return $ret;
    }*/

    public function internalParent(CategoryPart $CategoryPart)
    {
        return $this->belongsTo($CategoryPart, CategoryPart::class, 'parent');
    }
}