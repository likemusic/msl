<?php namespace App\Models\PageEntity\StoreEntity\Category\Template;

use App\Models\PageEntity\Base\Calculated\Template\Db\PageEntityTemplateDbRepository;

class CategoryTemplateRepository extends PageEntityTemplateDbRepository implements ICategoryTemplateRepository{
    protected $ItemClassName = CategoryTemplate::class;
}