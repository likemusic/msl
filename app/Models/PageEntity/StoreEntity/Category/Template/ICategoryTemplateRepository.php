<?php namespace App\Models\PageEntity\StoreEntity\Category\Template;

use App\Models\PageEntity\Base\Calculated\Template\IPageEntityTemplateRepository;

interface ICategoryTemplateRepository extends IPageEntityTemplateRepository{

}