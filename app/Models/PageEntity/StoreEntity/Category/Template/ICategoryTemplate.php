<?php namespace App\Models\PageEntity\StoreEntity\Category\Template;

use App\Models\PageEntity\Base\Calculated\Template\IPageEntityTemplate;

/**
 * Interface ITemplate
 * @package App\Models\PageEntity\StoreEntity\Category\Template
 * @property $productSet
 */
interface ICategoryTemplate extends IPageEntityTemplate {

}