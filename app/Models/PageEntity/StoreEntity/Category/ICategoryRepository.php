<?php namespace App\Models\PageEntity\StoreEntity\Category;

//use App\Models\Base\Interfaces\Repository\GetChilds\IGetInternalChildsByExternalEntity;
use App\Models\Base\Interfaces\Repository\GetChilds\IGetExternalChildsByInternalEntity;
use App\Models\Base\Interfaces\Repository\GetChilds\IGetInternalChildsByInternalEntity;
use App\Models\Base\Interfaces\Repository\GetChildsValues\ByEntity\IGetInternalChildsValuesByInternalEntity;
use App\Models\Base\Interfaces\Repository\GetUrlNameList\ByEntity\IGetChildCategoriesUrlNameList;
use App\Models\Base\Interfaces\Repository\GetUrlNameList\ByEntity\IGetChildProductsUrlNameListByEntity;
use App\Models\Base\Interfaces\Repository\IFindBySearchString;
use App\Models\Base\Interfaces\Repository\IGetById;
use App\Models\Base\Interfaces\Repository\ILinkedRepository;
use App\Models\Base\Interfaces\Repository\ISelfLinkedRepository;
use App\Models\PageEntity\StoreEntity\Base\Inner\Root\IRootStoreEntityInnerRepository;

interface ICategoryRepository extends IRootStoreEntityInnerRepository,
    ISelfLinkedRepository,
    ILinkedRepository,
    IGetById,
    IFindBySearchString,

    IGetChildCategoriesUrlNameList,
    IGetChildProductsUrlNameListByEntity,

    IGetInternalChildsValuesByInternalEntity,
    IGetInternalChildsByInternalEntity,
    IGetExternalChildsByInternalEntity
{
    function GetVirtualRoot();
}