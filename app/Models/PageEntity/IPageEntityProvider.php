<?php namespace App\Models\PageEntity;

use App\Models\Base\Interfaces\Repository\GetByUrl\IGetByUrlWithByPath;

/*
    В отличии от IPageEntityRepository представляет не коллекцию страниц
    а с какую-то одноу страницу
*/
interface IPageEntityProvider extends IGetByUrlWithByPath{

}