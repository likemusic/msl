<?php namespace App\Models\PageEntity\Article;

use App\Helpers\CacheNamer\ICacheNamer;
use App\Models\Base\Traits\analoguewithioc\GetByUrl\TGetByUrl;
use App\Models\Base\Traits\analoguewithioc\GetByUrl\TGetByUrlCached;
use App\Models\PageEntity\Base\Full\Db\PageEntityFullDbRepository;

class ArticleRepository extends PageEntityFullDbRepository implements IArticleRepository{
    use TGetByUrlCached;// {GetByUrl insteadof TGetByUrl;}

    protected $ItemClassName = Article::class;
    /**
     * @var ICacheNamer
     */
    protected $ICacheNamer;

    protected $UseCacheTGetByUrl;

    function __construct(ICacheNamer $ICacheNamer)
    {
        parent::__construct();
        $this->ICacheNamer = $ICacheNamer;
        $this->UseCacheTGetByUrl = config('cache.components.Article.GetByUrl');
    }
}