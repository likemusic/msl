<?php namespace App\Models\PageEntity\Article;
use App\Models\Base\Traits\RepositoryItem\TPageEntity;
use App\Models\PageEntity\Base\Full\Db\PageEntityFullDb;

/**
 * @property mixed aux_page_text
 */
class Article extends PageEntityFullDb implements IArticle{

    function GetMetaDescription()
    {
        return $this->meta_description;
    }

    function GetMetaKeywords()
    {
        return $this->meta_keywords;
    }

    function GetTitle()
    {
        return $this->title;
    }

    function GetName()
    {
        return $this->h1;
    }

    function GetContent()
    {
        return $this->content;
    }


}
