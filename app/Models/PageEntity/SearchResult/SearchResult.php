<?php namespace App\Models\PageEntity\SearchResult;

use App\Models\Base\Traits\RepositoryItem\TGetKnowFields;
use App\Models\Base\Traits\RepositoryItem\TGetPrevKnowFieldsByPageEntity;
use App\Models\Base\Traits\RepositoryItem\TSimplePageEntity;
use App\Models\PageEntity\Base\Full\Values\PageEntityFullValues;
use App\Models\PageEntity\StoreEntity\Category\ICategory;

class SearchResult extends PageEntityFullValues implements ISearchResult{
    use TSimplePageEntity, TGetKnowFields, TGetPrevKnowFieldsByPageEntity
    {
        TGetKnowFields::GetKnowFields insteadof TGetPrevKnowFieldsByPageEntity;
    }

    protected $InnerName = 'Поиск карт на сайте';
    //TODO: вынести стандартный код в класс или трейт

    protected $Results;
    protected  $Str;

    public function __construct(array $SerchResults = null, $Str)
    {
        $this->Results = $this->CategoriesToUrlName($SerchResults);
        $this->Str = $Str;
    }

    protected function CategoriesToUrlName($Categories)
    {
        $ret = array_map([$this,'CategoryToUrlName'],$Categories);
        return $ret;
    }

    protected function CategoryToUrlName(ICategory $Category)
    {
        $ret = ['url'=> $Category->GetUrl(),'name'=>$Category->GetName()];
        return $ret;
    }

    public function GetSearchResults()
    {
        return $this->Results;
    }

    public function GetStr()
    {
        return $this->Str;
    }

    function GetNewKnowFields()
    {
        $AddFields = [
            'results'   => 'GetSearchResults',
            'str'       => 'GetStr'
        ];
        return $AddFields;
    }
}