<?php namespace App\Models\Order;

//TODO: migrate to SplEnum?
class OrderStateEnum {

    const CREATED = 1;
    const PAID = 2;
    const CLOSED = 3;
} 