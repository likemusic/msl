<?php namespace App\Models\Order;

use App\Models\Base\Interfaces\Repository\IGetById;

interface IOrderRepository extends IGetById{
    function UpdateState($OrderId, $OrderStatus);
} 