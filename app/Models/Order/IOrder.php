<?php namespace App\Models\Order;

use App\Models\User\IUser;

interface IOrder {

    /**
     * @return IUser
     */
    function GetUser();

    /**
     * @return string
     */
    function GetName();
} 