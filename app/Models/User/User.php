<?php namespace App\Models\User;

//use App\User as LaravelUser;
use Analogue\LaravelAuth\User as AnalogueUser;

class User extends AnalogueUser implements IUser{
    function GetName(){return $this->name;}
    function GetEmail(){return $this->email;}
}