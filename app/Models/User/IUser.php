<?php namespace App\Models\User;

interface IUser {
    function GetName();
    function GetEmail();
} 