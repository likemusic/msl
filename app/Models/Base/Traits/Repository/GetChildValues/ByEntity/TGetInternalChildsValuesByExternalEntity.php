<?php namespace App\Models\Base\Traits\Repository\GetChildValues\ByEntity;


use App\Models\Base\Interfaces\Repository\IRepositoryEntity;

trait TGetInternalChildsValuesByExternalEntity {

    function GetInternalChildsValuesByExternalEntity(IRepositoryEntity $entity, $Fields = null)
    {
        $CalculatedList = $this->GetInternalChildsByExternalEntity($entity);
        $ValuesList = $this->EntityListToValuesList($CalculatedList, $Fields);
        return $ValuesList;
    }
} 