<?php namespace App\Models\Base\Traits\Repository\GetChildValues\ByEntity;

use App\Models\Base\Interfaces\Repository\IRepositoryEntity;

trait TGetExternalChildsValuesByInternalEntity {

    function GetExternalChildsValuesByInternalEntity(IRepositoryEntity $entity, $Fields)
    {
        $CalculatedProducts = $this->IChildRepository->GetInternalChildsValuesByExternalEntity($entity, $Fields );
        return $CalculatedProducts;
    }
} 