<?php namespace App\Models\Base\Traits\Repository\GetChildValues;


trait TEntityListToValuesList {

    protected function EntityListToValuesList($EntityList, $Fields)
    {
        $ret = [];
        foreach($EntityList as $Entity)
        {
            $ret []= $Entity->GetValues($Fields);
        }
        return $ret;
    }
} 