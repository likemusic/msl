<?php namespace App\Models\Base\Traits\Repository\GetUrlNameList;


trait TCalculatedListToUrlNameList {
    protected function CalculatedListToUrlNameList($CalculatedSubProducts, $adds = null)
    {
        $UrlNameList = [];
        if(!$CalculatedSubProducts) return $UrlNameList;
        //TODO: сделать без создания CalculatedProduct
        foreach($CalculatedSubProducts as $CalculatedSubProduct)
        {
            $UrlNameItem = [
                'url'   => $CalculatedSubProduct->GetUrl(),
                'menu_tree_name'  => $CalculatedSubProduct->GetMenuTreeName()
            ];


            if($adds) {
                foreach($adds as $Field)
                {
                    $UrlNameItem[$Field] = $CalculatedSubProduct->GetValue($Field);
                }
            }
            //TODO: убрать этот костыль сделанный для возможности добалвения ссылок на подтовары в описании товара через шорт-коды по полю url_slug
            $Key = (isset($UrlNameItem['shortcode'])) ? $UrlNameItem['shortcode'] : $CalculatedSubProduct->GetUrlSlug();
            $UrlNameList [$Key]= $UrlNameItem;
        }
        return $UrlNameList;
    }
} 