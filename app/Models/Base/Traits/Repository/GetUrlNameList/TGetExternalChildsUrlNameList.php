<?php namespace App\Models\Base\Traits\Repository\GetUrlNameList;

trait TGetExternalChildsUrlNameList {

    protected $ExternalChildsUrlNameList;

    protected function GetExternalChildsUrlNameList()
    {
        if($this->ExternalChildsUrlNameList === null)
        {
            $ret = $this->ISelfRepository->GetExternalChildsUrlNameListByInternalEntity($this);
            $this->ExternalChildsUrlNameList = $ret;
        }
        else $ret = $this->ExternalChildsUrlNameList;
        return $ret;
    }
} 