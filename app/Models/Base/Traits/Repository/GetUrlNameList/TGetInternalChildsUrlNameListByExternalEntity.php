<?php namespace App\Models\Base\Traits\Repository\GetUrlNameList;


use App\Models\Base\Interfaces\Repository\IRepositoryEntity;
use App\Models\PageEntity\StoreEntity\Product\IProduct;

trait TGetInternalChildsUrlNameListByExternalEntity {

    function GetInternalChildsUrlNameListByExternalEntity(IRepositoryEntity $entity=null)
    {
        //TODO: реализовать по нормальному, без извлечения всех полей, только name и url_slug
        //TODO: GetName() GetUrl() вынести в репозиторий (чтобы можно быть вызывать из репозитория, только для выбранных полей)
        /*$ValuesList = $this->GetInternalChildsValuesByExternalEntity($entity, ['url','name','url_slug']);
        $ret = $this->CalculatedUrlNameUrlSlugValuesListToUrlNameList($ValuesList);
        return $ret;*/

        $CalculatedSubproducts = $this->GetInternalChildsByExternalEntity($entity);

        //TODO: fix this hack
        $adds = ($entity instanceof IProduct) ? ['shortcode'] : null;
        $ret = $this->CalculatedListToUrlNameList($CalculatedSubproducts, $adds );
        return $ret;

    }

    /*protected function CalculatedUrlNameUrlSlugValuesListToUrlNameList($List)
    {
        $UrlNameList = [];
        if(!$List) return null;
        //TODO: сделать без создания CalculatedProduct
        foreach($List as $Item)
        {
            $UrlNameItem = [
                'url'   => $Item['url'],
                'name'  => $Item['name']
            ];

            $UrlNameList[$Item['url_slug']]= $UrlNameItem;
        }
        return $UrlNameList;
    }*/
} 