<?php namespace App\Models\Base\Traits\Repository;

use App\Models\Base\Interfaces\Repository\IRepositoryEntity;

trait TGetExternalChildByUrlSlugForInternalEntity {

    function GetExternalChildByUrlSlugForInternalEntity($url, IRepositoryEntity $ParentEntity)
    {
        $CalculatedProduct = $this->IChildRepository->GetInternalChildByUrlSlugForExternalEntity($url, $ParentEntity);
        return $CalculatedProduct;
    }
}