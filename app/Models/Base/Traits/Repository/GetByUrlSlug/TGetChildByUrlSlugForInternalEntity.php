<?php namespace App\Models\Base\Traits\Repository\GetByUrlSlug;


use App\Models\Base\Interfaces\Repository\ILinkedRepository;
use App\Models\Base\Interfaces\Repository\ISelfLinkedRepository;

trait TGetChildByUrlSlugForInternalEntity {

    /**
     * @param $url
     * @param ICalculatedPageEntity $ParentEntity
     * @return null
     */
    function GetChildByUrlSlugForInternalEntity($url, $ParentEntity)
    {
        if($this instanceof ISelfLinkedRepository)
        {
            $CalculatedCategory = $this->GetInternalChildByUrlSlugForInternalEntity($url, $ParentEntity);
            if($CalculatedCategory) return $CalculatedCategory;
        }

        if($this instanceof ILinkedRepository)
        {
            $CalculatedProduct = $this->GetExternalChildByUrlSlugForInternalEntity($url, $ParentEntity);
            if($CalculatedProduct) return $CalculatedProduct;
        }

        return null;
    }

} 