<?php namespace App\Models\Base\Traits\Repository;

trait TGetRootUrlNameList {
    function GetRootUrlNameList()
    {
        return $this->GetInternalChildsUrlNameListByInternalEntity();
    }
} 