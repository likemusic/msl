<?php namespace App\Models\Base\Traits\Repository\GetChilds;

use App\Models\Base\Interfaces\Repository\IRepositoryEntity;

trait TGetExternalChildsByInternalEntity {

    function GetExternalChildsByInternalEntity($entity=null)
    {
        $CalculatedProducts = $this->IChildRepository->GetInternalChildsByExternalEntity($entity);
        return $CalculatedProducts;
    }
}