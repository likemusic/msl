<?php namespace App\Models\Base\Traits\Repository;

use App\Models\Base\Interfaces\Repository\IRepositoryEntity;

trait TGetChildUrlNameListByEntity {

    function GetChildUrlNameListByEntity(IRepositoryEntity $Entity)
    {
        $InternalChildUrlNameList = $this->GetInternalChildsUrlNameListByInternalEntity($Entity);
        $ExternalChildUrlNameList = $this->GetExternalChildsUrlNameListByInternalEntity($Entity);

        $ret = array_merge($InternalChildUrlNameList, $ExternalChildUrlNameList);
        return $ret;
    }
} 