<?php namespace App\Models\Base\Traits\GetByUrlSlug;

trait TGetChildByUrlSlug {

    function GetChildByUrlSlug($url)
    {
        return $this->ISelfRepository->GetChildByUrlSlugForInternalEntity($url, $this);
    }
}