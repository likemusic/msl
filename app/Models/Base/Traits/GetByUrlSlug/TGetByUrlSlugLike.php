<?php namespace App\Models\Base\Traits;

trait TGetByUrlSlugLike {
    public function GetByUrlSlugLike($url, $ids=null)
    {
        $ClassName = $this->ClassName;
        $res = $ClassName::where('url','like',$url);
        if($ids)
        {
            $res = $res->whereIn('id',$ids);
        }
        $ret = $res->first();
        return $ret;
    }
}