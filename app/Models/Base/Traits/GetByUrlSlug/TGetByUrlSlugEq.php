<?php namespace App\Models\Base\Traits;

trait TGetByUrlSlugEq {

    public function GetByUrlSlugEq($url, $ids=null)
    {
        $ClassName = $this->ClassName;
        $res = $ClassName::where('url','=',$url);
        if($ids)
        {
            $res = $res->whereIn('id',$ids);
        }
        $ret = $res->first();
        return $ret;
    }
}