<?php namespace App\Models\Base\Traits\analoguewithioc;


use Illuminate\Support\Facades\Cache;

trait TGetByIdCached
{
    use TGetById {GetById as protected GetByIdNotCached;}
    /**
     * @return int
     */
    function GetById($id)
    {
        $CacheId = get_class($this).'/'.$id;
        $ret = Cache::rememberForever($CacheId, function()use($id){
            return $this->GetByIdNotCached($id);
        });
        return $ret;
    }
}