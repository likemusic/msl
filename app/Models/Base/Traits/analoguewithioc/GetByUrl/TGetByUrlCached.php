<?php namespace App\Models\Base\Traits\analoguewithioc\GetByUrl;


use Illuminate\Support\Facades\Cache;

trait TGetByUrlCached{
    use TGetByUrl{GetByUrl as GetByUrlReal;}

    public function GetByUrl($url, $ids=null)
    {
        $ret = ($this->UseCacheTGetByUrl)
            ? $this->GetByUrlCached($url, $ids)
            : $this->GetByUrlReal($url, $ids);
        return $ret;
    }

    protected function GetByUrlCached($url, $ids=null)
    {
        //TODO: delete ids or add as key to cache
        $CacheId = $this->ICacheNamer->TGetByUrl($this,$url);
        $ret = Cache::rememberForever($CacheId,
            function()use($url,$ids){return $this->GetByUrlReal($url, $ids);}            );
        return $ret;
    }
}