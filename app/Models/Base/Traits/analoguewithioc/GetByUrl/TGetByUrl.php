<?php namespace App\Models\Base\Traits\analoguewithioc\GetByUrl;

trait TGetByUrl {
    public function GetByUrl($url, $ids=null)
    {
        $res = $this->mapper->where('url','=',$url);
        if($ids)
        {
            $res = $res->whereIn('id',$ids);
        }
        $ret = $res->first();
        return $ret;
    }
}