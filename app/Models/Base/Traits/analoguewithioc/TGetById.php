<?php namespace App\Models\Base\Traits\analoguewithioc;

trait TGetById {
    /**
     * @return int
     */
    function GetById($id)
    {
        $ret = $this->find($id);
        return $ret;
    }
}