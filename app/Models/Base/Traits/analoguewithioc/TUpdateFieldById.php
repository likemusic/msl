<?php namespace App\Models\Base\Traits\analoguewithioc;

trait TUpdateFieldById {
    function UpdateFieldById($id, $field, $value)
    {
        $res = $this->find($id);
        $res->$field = $value;
        $this->store($res);
    }
} 