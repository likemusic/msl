<?php namespace App\Models\Base\Traits\l5repository\GetByUrl;

trait TGetByUrl {
    public function GetByUrl($url, $ids=null)
    {
        $res = $this->model->where('url','=',$url);
        if($ids)
        {
            $res = $res->whereIn('id',$ids);
        }
        $ret = $res->first();
        return $ret;
    }
}