<?php namespace App\Models\Base\Traits\RepositoryItem;

trait TPageEntity {

    use TGetValues;

    /* Values */
    protected $MetaDescription;
    protected $MetaKeywords;
    protected $Title;
    protected $Name;

    function GetKnowFields()
    {
        $KnownFields = [
            'meta_description'  => 'GetMetaDescription',
            'meta_keywords'     => 'GetMetaKeywords',
            'page_title'        => 'GetTitle',
            'page_h1'           => 'GetName',
        ];
        return $KnownFields;
    }
} 