<?php namespace App\Models\Base\Traits\RepositoryItem;

trait TGetUrl{

    protected $Url=null;
    function GetUrl()
    {
        if(!$this->Url)
        {
            $Parent = $this->GetParent();
            $ParentUrl =  ($Parent) ? $Parent->GetUrl() : '';
            $Url = ($ParentUrl != '/') ? $ParentUrl.'/'.$this->GetUrlSlug() : '/'.$this->GetUrlSlug();
            $this->Url = $Url;
        }
        else $Url = $this->Url;
        return $Url;
    }
}