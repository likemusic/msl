<?php namespace App\Models\Base\Traits\RepositoryItem;

use App\Models\Base\Interfaces\RepositoryItem\IGetBreadcrumbs;

trait TGetBreadcrumbs {
    protected $Breadcrumbs;

    function GetBreadcrumbs()
    {
        if($this->Breadcrumbs === null)
        {
            $Parent = $this->GetParent();
            $BreadCrumbs =  ($Parent) ? $Parent->GetBreadcrumbs() : [];
            $BreadCrumbs[] = [
                'url' => $this->GetUrl(),
                'name' => $this->GetName()
            ];
            $this->Breadcrumbs = $BreadCrumbs;
        }
        else $BreadCrumbs = $this->Breadcrumbs;
        return $BreadCrumbs;
    }
}