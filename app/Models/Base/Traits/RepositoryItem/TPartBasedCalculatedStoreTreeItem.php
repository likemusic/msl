<?php namespace App\Models\Base\Traits\RepositoryItem;


trait TPartBasedCalculatedStoreTreeItem {

    protected $UrlSlug=null;
    protected $Genitive;

    function GetUrlSlug()
    {
        if($this->UrlSlug===null)
        {
            $CategoryPart =  $this->IPart;
            $UrlSlug = $CategoryPart->GetUrlSlug();
            $this->UrlSlug = $UrlSlug;
        }
        else $UrlSlug = $this->UrlSlug;
        return $UrlSlug;
    }

    function GetGenitive()
    {
        if($this->Genitive === null)
        {
            $Genitive = $this->IPart->GetGenitive();
            $this->Genitive = $Genitive;
        }
        else $Genitive = $this->Genitive;
        return $Genitive;
    }

} 