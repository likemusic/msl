<?php namespace App\Models\Base\Traits\RepositoryItem;

trait TTreeRepositoryItem {

    function GetInternalChilds()
    {
        return $this->ISelfRepository->GetInternalChildsByInternalEntity($this);
    }

    function GetInternalParent()
    {
        return $this->ISelfRepository->GetInternalParent($this);
    }
} 