<?php namespace App\Models\Base\Traits\RepositoryItem;


use App\Models\Base\Traits\RepositoryItem\TPageEntity;

trait TGetPrevKnowFieldsByPageEntity {
    use TPageEntity {GetKnowFields as protected GetKnowFieldsPageEntity;}

    function GetPrevKnowFields()
    {
        return $this->GetKnowFieldsPageEntity();
    }
}