<?php namespace App\Models\Base\Traits\RepositoryItem\GetChildsValues;

trait TGetChildCategoriesUrlNameList {

    function GetChildCategoriesUrlNameList()
    {
        return $this->ISelfRepository->GetChildCategoriesUrlNameListByEntity($this);
    }
}