<?php namespace App\Models\Base\Traits\RepositoryItem\GetChildsValues;

trait TGetExternalChildsValues {

    function GetExternalChildsValues($Fields)
    {
        return $this->ISelfRepository->GetExternalChildsValuesByInternalEntity($this, $Fields);
    }
}