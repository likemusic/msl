<?php namespace App\Models\Base\Traits\RepositoryItem\UrlNameList;

trait TGetChildUrlNameList {

    function GetChildUrlNameList()
    {
        return $this->ISelfRepository->GetChildUrlNameListById($this->IPart->id);
    }
}