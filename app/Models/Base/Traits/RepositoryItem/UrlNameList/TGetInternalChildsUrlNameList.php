<?php namespace App\Models\Base\Traits\RepositoryItem\UrlNameList;

trait TGetInternalChildsUrlNameList {
    protected $InternalChildsUrlNameList;

    function GetInternalChildsUrlNameList()
    {
        if($this->InternalChildsUrlNameList === null )
        {
            $ret = $this->ISelfRepository->GetInternalChildsUrlNameListByInternalEntity($this);
            $this->InternalChildsUrlNameList = $ret;
        }
        else $ret = $this->InternalChildsUrlNameList;
        return $ret;
    }
}