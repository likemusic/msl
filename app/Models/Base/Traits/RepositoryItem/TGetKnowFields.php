<?php namespace App\Models\Base\Traits\RepositoryItem;


trait TGetKnowFields {

    abstract function GetPrevKnowFields();
    abstract function GetNewKnowFields();

    function GetKnowFields()
    {
        $ParentFields = $this->GetPrevKnowFields();
        $NewFields = $this->GetNewKnowFields();

        $Fields = $ParentFields + $NewFields;
        return $Fields;
    }
}