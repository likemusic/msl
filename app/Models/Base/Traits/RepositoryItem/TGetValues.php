<?php namespace App\Models\Base\Traits\RepositoryItem;

trait TGetValues {

    abstract function GetKnowFields();

    function GetValues($Fields = null)
    {
        $ret = [];
        $KnownFields = $this->GetKnowFields();
        $KnownKeys = array_keys($KnownFields);

        if(!$Fields) $Fields = $KnownKeys;
        /*else
        {
            //проверяем все ли поля имеются в объекте
            $UnknownFields = array_diff($Fields, $KnownKeys);
            if(count($UnknownFields)>0) throw new \Exception('Unknown fields!');
        }*/
        //TODO: just for optimisation

        foreach($Fields as $FieldName)
        {
            $ret[$FieldName] = $this->GetField($FieldName, $KnownFields);
        }
        return $ret;
    }

    function GetValue($FieldName)
    {
        $KnownFields = $this->GetKnowFields();
        $ret = $this->GetField($FieldName, $KnownFields);
        return $ret;
    }

    function GetField($FieldName, $KnownFields)
    {
        if(!isset($KnownFields[$FieldName])) throw new \Exception("Unknown field name ({$FieldName})!");

        $callable = [$this,$KnownFields[$FieldName]];
        $ret = call_user_func($callable);
        return $ret;
    }
} 