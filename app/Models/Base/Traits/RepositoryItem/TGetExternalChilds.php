<?php namespace App\Models\Base\Traits\RepositoryItem;


trait TGetExternalChilds {
    function GetExternalChilds()
    {
        return $this->ISelfRepository->GetExternalChildsByInternalEntity($this);
    }
} 