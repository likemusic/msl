<?php namespace App\Models\Base\Traits\RepositoryItem;


trait TGetBuyButton {

    protected $BuyButton;

    protected function GetBuyButton()
    {
        if($this->BuyButton === null)
        {
            $ProductName = $this->GetName();
            $ProductPrice = $this->GetPrice();

            $ret = $this->IPaymentRequestDataProvider->GetData($ProductPrice, $ProductName);
            $this->BuyButton = $ret;
        }
        else $ret = $this->BuyButton;
        return $ret;
    }
}