<?php namespace App\Models\Base\Traits\RepositoryItem;


trait TSimplePageEntity {

    public function GetMetaDescription()
    {
        return $this->InnerName;
    }

    public function GetMetaKeywords()
    {
        return $this->InnerName;
    }

    public function GetTitle()
    {
        return $this->InnerName;
    }

    public function GetName()
    {
        return $this->InnerName;
    }
}