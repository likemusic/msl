<?php namespace App\Models\Base\Traits\RepositoryItem;

trait TGetUrlSlugByTemplate {

    protected $UrlSlug;

    function GetUrlSlug()
    {
        if($this->UrlSlug === null)
        {
            $UrlSlug = $this->ITemplate->url_slug;
            $this->UrlSlug = $UrlSlug;
        }
        else $UrlSlug = $this->UrlSlug;
        return $UrlSlug;
    }
}