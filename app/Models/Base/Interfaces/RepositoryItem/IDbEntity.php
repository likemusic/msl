<?php namespace App\Models\Base\Interfaces\RepositoryItem;

use App\Models\Base\Interfaces\Repository\IRepositoryEntity;

interface IDbEntity extends IRepositoryEntity, IGetId {

}