<?php namespace App\Models\Base\Interfaces\RepositoryItem;

use App\Models\PageEntity\IPageEntity;

interface IGetStoreTree {
    function GetStoreTree(IPageEntity $IPageEntity);
} 