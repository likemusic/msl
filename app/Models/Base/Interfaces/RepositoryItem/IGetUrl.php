<?php namespace App\Models\Base\Interfaces\RepositoryItem;

interface IGetUrl {
    /**
     * @return string Возвращает относительный путь
     */
    function GetUrl();
} 