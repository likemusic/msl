<?php namespace App\Models\Base\Interfaces\RepositoryItem\GetByUrlSlug;


interface IGetChildByUrlSlug {
    function GetChildByUrlSlug($url);
} 