<?php namespace App\Models\Base\Interfaces\RepositoryItem;


interface ITreeRepositoryItem {

    function GetInternalChilds();
    function GetInternalParent();
    function HasParent();
} 