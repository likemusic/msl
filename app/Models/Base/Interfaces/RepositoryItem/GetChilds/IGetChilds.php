<?php namespace App\Models\Base\Interfaces\RepositoryItem\GetChilds;

interface IGetChilds {
    function GetChilds();
} 