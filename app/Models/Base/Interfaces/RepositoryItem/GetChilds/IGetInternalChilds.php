<?php namespace App\Models\Base\Interfaces\RepositoryItem\GetChilds;

interface IGetInternalChilds {
    function GetInternalChilds();
} 