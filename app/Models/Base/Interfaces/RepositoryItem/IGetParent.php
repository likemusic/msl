<?php namespace App\Models\Base\Interfaces\RepositoryItem;

use App\Models\PageEntity\StoreEntity\Base\IStoreEntity;

interface IGetParent {
    /**
     * @return IStoreEntity
     */
    function GetParent();
} 