<?php namespace App\Models\Base\Interfaces\RepositoryItem;

interface IGetId {
    function GetId();
}
