<?php namespace App\Models\Base\Interfaces\RepositoryItem\GetChildsValues\ByEntity;

interface IGetExternalChildsValues {
    function GetExternalChildsValues($Fields);
} 