<?php namespace App\Models\Base\Interfaces\RepositoryItem\GetChildsValues\ByEntity;

interface IGetInternalChildsValues {
    function GetInternalChildsValues($Fields);
} 