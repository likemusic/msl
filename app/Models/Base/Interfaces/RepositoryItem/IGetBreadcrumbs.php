<?php namespace App\Models\Base\Interfaces\RepositoryItem;

interface IGetBreadcrumbs {
    /**
     * @return array[] - Возвращает массив ссылок образующих путь к текущей странице
     */
    function GetBreadcrumbs();
} 