<?php namespace App\Models\Base\Interfaces\Repository;


interface IFindBySearchString {

    function FindBySearchString($str);
} 