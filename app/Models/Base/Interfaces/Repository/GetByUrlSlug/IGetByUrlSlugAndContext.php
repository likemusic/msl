<?php namespace App\Models\Base\Interfaces\Repository\GetByUrlSlug;

use App\Models\Base\Interfaces\GetByUrl\IGetByUrlContext;
use App\Models\PageEntity\IPageEntity;

interface IGetByUrlSlugAndContext {
    /**
     * @param $Url
     * @param IGetByUrlContext $Context
     * @return IPageEntity
     */
    function GetByUrlWithByPath($Url, IGetByUrlContext $Context);
}