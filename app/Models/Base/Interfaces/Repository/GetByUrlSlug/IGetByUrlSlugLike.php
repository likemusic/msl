<?php namespace App\Models\Base\Interfaces\Repository\GetByUrlSlug;


interface IGetByUrlSlugLike {
    public function GetByUrlSlugLike($url, $ids=null);
}