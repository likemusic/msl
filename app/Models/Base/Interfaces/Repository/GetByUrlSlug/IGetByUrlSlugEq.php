<?php namespace App\Models\Base\Interfaces\Repository\GetByUrlSlug;

interface IGetByUrlSlugEq {
    public function GetByUrlSlugEq($url, $ids=null);
}