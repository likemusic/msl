<?php namespace App\Models\Base\Interfaces\Repository\GetByUrlSlug;

interface IGetInternalChildByUrlSlugForExternalEntity {
    /**
     * @param $url
     * @param $ParentEntity
     * @return IRepositoryEntity
     *
     * Возвращает по url дочерний элемент для элемента внешней коллекции
     */

    function GetInternalChildByUrlSlugForExternalEntity($url, $ParentEntity);
}