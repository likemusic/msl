<?php namespace App\Models\Base\Interfaces\Repository\GetByUrlSlug;

use App\Models\Base\Interfaces\Repository\IRepositoryEntity;

interface IGetExternalChildByUrlSlugForInternalEntity {

    function GetExternalChildByUrlSlugForInternalEntity($url, IRepositoryEntity $entity);
} 