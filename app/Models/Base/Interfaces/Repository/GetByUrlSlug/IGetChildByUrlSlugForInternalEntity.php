<?php namespace App\Models\Base\Interfaces\Repository\GetByUrlSlug;


interface IGetChildByUrlSlugForInternalEntity {

    function GetChildByUrlSlugForInternalEntity($url, $ParentEntity);
} 