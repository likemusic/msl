<?php namespace App\Models\Base\Interfaces\Repository\GetByUrlSlug;

interface IGetInternalChildByUrlSlugForInternalEntity {
    function GetInternalChildByUrlSlugForInternalEntity($url, $ParentEntity);
}