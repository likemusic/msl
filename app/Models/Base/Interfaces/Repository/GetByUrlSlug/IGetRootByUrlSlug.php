<?php namespace App\Models\Base\Interfaces\Repository\GetByUrlSlug;

interface IGetRootByUrlSlug {
    /**
     * @param $url
     * @return mixed
     */
    function GetRootByUrlSlug($url);
}