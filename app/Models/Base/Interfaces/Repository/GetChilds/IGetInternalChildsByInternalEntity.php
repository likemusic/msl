<?php namespace App\Models\Base\Interfaces\Repository\GetChilds;


use App\Models\Base\Interfaces\Repository\IRepositoryEntity;

interface IGetInternalChildsByInternalEntity {

    function GetInternalChildsByInternalEntity(IRepositoryEntity $Entity = null, $Fields = null);
} 