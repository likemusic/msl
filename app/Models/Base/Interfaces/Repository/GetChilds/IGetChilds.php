<?php namespace App\Models\Base\Interfaces\Repository\GetChilds;


interface IGetChilds {

    function GetChilds($Entity=null);
} 