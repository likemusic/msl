<?php namespace App\Models\Base\Interfaces\Repository\GetChilds;

interface IGetExternalChildsByInternalEntity {

    function GetExternalChildsByInternalEntity($Entity=null);
} 