<?php namespace App\Models\Base\Interfaces\Repository\GetChilds;

use App\Models\Base\Interfaces\Repository\IRepositoryEntity;

interface IGetInternalChildsByExternalEntity {

    function GetInternalChildsByExternalEntity(IRepositoryEntity $Entity = null);
} 