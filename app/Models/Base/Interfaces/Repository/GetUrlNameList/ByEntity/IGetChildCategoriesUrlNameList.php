<?php namespace App\Models\Base\Interfaces\Repository\GetUrlNameList\ByEntity;

interface IGetChildCategoriesUrlNameList {
    function GetChildCategoriesUrlNameList($Entity);
} 