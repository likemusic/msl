<?php namespace App\Models\Base\Interfaces\Repository\GetUrlNameList\ByEntity;

use App\Models\Base\Interfaces\Repository\IRepositoryEntity;

interface IGetChildUrlNameListByEntity {
    function GetChildUrlNameListByEntity(IRepositoryEntity $Entity);
} 