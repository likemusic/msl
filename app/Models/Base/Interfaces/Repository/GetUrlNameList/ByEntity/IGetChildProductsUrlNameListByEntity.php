<?php namespace App\Models\Base\Interfaces\Repository\GetUrlNameList\ByEntity;

interface IGetChildProductsUrlNameListByEntity {
    function GetChildProductsUrlNameListByEntity($Entity);
} 