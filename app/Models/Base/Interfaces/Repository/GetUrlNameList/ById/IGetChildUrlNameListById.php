<?php namespace App\Models\Base\Interfaces\Repository\GetUrlNameList\ById;

interface IGetChildUrlNameListById {

    function GetChildUrlNameListById($id);
} 