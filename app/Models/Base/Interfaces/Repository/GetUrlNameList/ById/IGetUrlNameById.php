<?php namespace App\Models\Base\Interfaces\Repository\GetUrlNameList\ById;

interface IGetUrlNameById {
    function GetUrlNameById($id);
} 