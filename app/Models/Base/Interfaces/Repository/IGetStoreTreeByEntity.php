<?php namespace App\Models\Base\Interfaces\Repository;

interface IGetStoreTreeByEntity {

    function GetStoreTreeByEntity(IRepositoryEntity $entity);
} 