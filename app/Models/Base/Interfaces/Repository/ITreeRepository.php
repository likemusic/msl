<?php namespace App\Models\Base\Interfaces\Repository;

use App\Models\Base\Interfaces\Repository\GetChilds\IGetInternalChildsByInternalEntity;

interface ITreeRepository extends IGetInternalChildsByInternalEntity {

    function GetInternalParent(IRepositoryEntity $entity);

} 