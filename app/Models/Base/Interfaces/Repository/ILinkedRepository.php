<?php namespace App\Models\Base\Interfaces\Repository;
use App\Models\Base\Interfaces\Repository\GetByUrlSlug\IGetInternalChildByUrlSlugForExternalEntity;

/**
 * Interface ILinkedRepository
 * @package App\Models\Base\Interfaces\Repository
 * Репозиторий элементы которой могут быть связаны с коллециями других репозиториев (отношением parent-child)
 */
interface ILinkedRepository extends IGetInternalChildByUrlSlugForExternalEntity{
}