<?php namespace App\Models\Base\Interfaces\Repository;

interface IUpdateFieldById {
    function UpdateFieldById($id, $field, $value);

} 