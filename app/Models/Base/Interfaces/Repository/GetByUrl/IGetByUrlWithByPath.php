<?php namespace App\Models\Base\Interfaces\Repository\GetByUrl;

use App\Models\PageEntity\IPageEntity;

interface IGetByUrlWithByPath {
    function  GetByUrlWithByPath($url, IPageEntity &$FoundByPath = null);
}