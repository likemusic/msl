<?php namespace App\Models\Base\Interfaces\Repository\GetByUrl;

use App\Models\PageEntity\IPageEntity;

interface IGetByUrl {
    /**
     * @param $Url
     * @return IPageEntity
     */
    function GetByUrl($Url);
}