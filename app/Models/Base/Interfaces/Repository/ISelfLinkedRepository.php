<?php namespace App\Models\Base\Interfaces\Repository;

use App\Models\Base\Interfaces\Repository\GetByUrlSlug\IGetInternalChildByUrlSlugForInternalEntity;

interface ISelfLinkedRepository extends IGetInternalChildByUrlSlugForInternalEntity{

} 