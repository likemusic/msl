<?php namespace App\Models\Base\Interfaces\Repository;

interface IGetById {
    function GetById($id);
}