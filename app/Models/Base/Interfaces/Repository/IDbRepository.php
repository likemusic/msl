<?php namespace App\Models\Base\Interfaces\Repository;

interface IDbRepository extends IRepository, IGetById, IUpdateFieldById {

}