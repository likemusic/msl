<?php namespace App\Models\Base\Interfaces\Repository\GetChildsValues\ByEntity;

interface IGetRootChildsValues {

    function GetRootChildsValues($Fields);
} 