<?php namespace App\Models\Base\Interfaces\Repository\GetChildsValues\ByEntity;

use App\Models\Base\Interfaces\Repository\IRepositoryEntity;

interface IGetInternalChildsValuesByInternalEntity {

    function GetInternalChildsValuesByInternalEntity(IRepositoryEntity $entity=null, $Fields);
}