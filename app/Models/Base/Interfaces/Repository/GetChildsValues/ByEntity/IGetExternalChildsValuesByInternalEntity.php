<?php namespace App\Models\Base\Interfaces\Repository\GetChildsValues\ByEntity;

use App\Models\Base\Interfaces\Repository\IRepositoryEntity;

interface IGetExternalChildsValuesByInternalEntity {

    function GetExternalChildsValuesByInternalEntity(IRepositoryEntity $entity, $Fields);
}