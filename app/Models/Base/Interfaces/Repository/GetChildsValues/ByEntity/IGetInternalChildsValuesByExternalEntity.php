<?php namespace App\Models\Base\Interfaces\Repository\GetChildsValues\ByEntity;

use App\Models\Base\Interfaces\Repository\IRepositoryEntity;

interface IGetInternalChildsValuesByExternalEntity {

    function GetInternalChildsValuesByExternalEntity(IRepositoryEntity $entity, $Fields=null);
} 