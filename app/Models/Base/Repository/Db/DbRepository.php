<?php namespace App\Models\Base\Repository\Db;

use Analogue\ORM\Repository;
use App\Models\Base\Interfaces\Repository\IDbRepository;
use App\Models\Base\Traits\analoguewithioc\TGetByIdCached;
use App\Models\Base\Traits\analoguewithioc\TUpdateFieldById;

abstract class DbRepository extends Repository implements IDbRepository{
    use TGetByIdCached;
    use TUpdateFieldById;
    /**
     * @var
     */
    protected $ItemClassName;

    public function __construct()
    {
        parent::__construct($this->ItemClassName);
    }

    /*function model()
    {
        return $this->ItemClassName;
    }*/
}