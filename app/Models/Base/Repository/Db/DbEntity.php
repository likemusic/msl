<?php namespace App\Models\Base\Repository\Db;

use Analogue\ORM\Entity;
use App\Models\Base\Interfaces\RepositoryItem\IDbEntity;
use App\Models\Base\Traits\RepositoryItem\TDbEntity;

abstract class DbEntity extends Entity implements IDbEntity{
    use TDbEntity;

    /*function __construct(array $attributes = array())
    {
    }*/
    //TODO: проверить поддерживает ли иницализацию через массив в конструкторе
}