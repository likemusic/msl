<?php namespace App\Models\Base\GlobalQueryScopes\Disabled;

use Analogue\ORM\System\ScopeInterface;
use Analogue\ORM\System\Query;

class DisabledScope implements ScopeInterface{

    /**
     * Apply the scope to a given Query builder.
     *
     * @param  \Analogue\ORM\System\Query  $builder
     * @return void
     */
    public function apply(Query $query)
    {
        //$entityMap = $query->getMapper()->getEntityMap();
        $query->where('disabled','0');
    }

    /**
     * Remove the scope from the Query builder.
     *
     * @param  \Analogue\ORM\System\Query  $builder
     * @return void
     */
    public function remove(Query $builder)
    {

    }

} 