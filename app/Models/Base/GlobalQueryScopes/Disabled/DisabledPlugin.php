<?php namespace App\Models\Base\GlobalQueryScopes\Disabled;

use Analogue\ORM\Plugins\AnaloguePlugin;
use Analogue\ORM\System\Mapper;

class DisabledPlugin extends AnaloguePlugin{

    /**
     * Register the plugin
     *
     * @return void
     */
    public function register()
    {
        $host = $this;

        // Hook any mapper init and check the mapping include soft deletes.
        $this->manager->registerGlobalEvent('initialized', function ($mapper) use ($host)
        {
            $entityMap = $mapper->getEntityMap();

            if(isset($entityMap->UseDisabledScope) and $entityMap->UseDisabledScope) {
                $host->registerDisabled($mapper);
            }
        });
    }

    /**
     * By hooking to the mapper initialization event, we can extend it
     * with the softDelete capacity.
     *
     * @param  \Analogue\ORM\System\Mapper 	$mapper
     * @return void
     */
    protected function registerDisabled(Mapper $mapper)
    {
        $entityMap = $mapper->getEntityMap();

        // Add Scopes
        $mapper->addGlobalScope(new DisabledScope);

        //$host = $this;

        // Register RestoreCommand
        //$mapper->addCustomCommand('Analogue\ORM\Plugins\SoftDeletes\Restore');
    }

    /**
     * Get custom events provided by the plugin
     *
     * @return array
     */
    public function getCustomEvents()
    {
        return [];
        //return ['disabling','disabled','enabling','enable'];
    }
} 