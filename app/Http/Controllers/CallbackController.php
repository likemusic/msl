<?php namespace App\Http\Controllers;

use App\Helpers\Mailer\IMailer;
use App\Helpers\PageResponder\IPageResponder;
use Illuminate\Http\Request;
use App\Models\PageEntity\Callback\Callback;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\MessageBag;

class CallbackController extends ControllerWithPageResponder{

    /**
     * @var IMailer
     */
    protected $mailer;

    function __construct(IPageResponder $IPageResponder, IMailer $mailer)
    {
        parent::__construct($IPageResponder);
        $this->mailer = $mailer;
    }

    function getIndex(Request $request)
    {
        $Page = new Callback([]);
        $Response = $this->IPageResponder->GetPageResponse($Page);
        return $Response;
    }

    function postIndex(Request $request)
    {
        $validationRules = [
            'phone'    => 'required',
        ];

        $this->validate($request, $validationRules);

        $input = $request->only('phone', 'name', 'message_text' );
        $input['errors'] = new MessageBag();
        $input['alerts'] = [];

        try{
            $this->mailer->NewCallbackRequest($input);
            $input['send_success'] = true;
        }
        catch(\Exception $e)
        {
            $input['errors']->add('send',trans('index.ERR_WRONG_POST')."<br/>(".$e->getMessage().')');
        }

        $Page = new Callback($input);
        $Response = $this->IPageResponder->GetPageResponse($Page);
        return $Response;
    }


} 