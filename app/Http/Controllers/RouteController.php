<?php namespace App\Http\Controllers;

use App\Helpers\Templater\ITemplater;
use App\Helpers\PageResponder\IPageResponder;
use App\Models\PageEntity\IPageEntityRepository;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Redirect;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class RouteController extends Controller
{
    /**
     * @var IPageEntityRepository
     */
    protected $PageRepository;

    /**
     * @var ITemplater
     */
    protected $Templater;

    /**
     * @var IPageResponder
     */
    protected $IPageResponder;

    public function __construct(IPageEntityRepository $pageRepository, ITemplater $templater, IPageResponder $IPageResponder)
    {
        $this->middleware('guest');
        $this->PageRepository = $pageRepository;
        $this->Templater = $templater;
        $this->IPageResponder = $IPageResponder;
    }

    /**
     * Show the application welcome screen to the user.
     * @param string $url
     * @return Response
     */
    public function index($url)
    {
        //Для корня получаем '/',
        //для всего остального, почемуто значение без начального слеша
        //Фиксим это, чтобы всегда было значение без слеаша
        //TODO: разобратся почему так и исправить
        if ('/' == $url) $url = '';

        $ByPath = null;
//        try {

            $Page = $this->GetPageByUrl($url, $ByPath);
            $Response = $this->IPageResponder->GetPageResponse($Page, $url);
            return $Response;
//        } catch (\Exception $e) {
//            if (!($e instanceof NotFoundHttpException)) {
//                Log::error('CODE: ' . $e->getCode() . ' Message: ' . $e->getMessage() . ' File: ' . $e->getFile()) . ' Line: ' . $e->getLine();
//            }
//            return redirect('/',302);
//        }
    }

    protected function GetPageByUrl($url, &$ByPath)
    {
        $Page = $this->PageRepository->GetByUrlWithByPath($url, $ByPath);
        return $Page;
    }
}
