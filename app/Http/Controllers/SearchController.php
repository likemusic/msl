<?php namespace App\Http\Controllers;

use App\Helpers\PageResponder\IPageResponder;
use App\Models\PageEntity\SearchResult\SearchResult;
use App\Models\PageEntity\StoreEntity\Category\ICategoryRepository;
use Illuminate\Http\Request;
use Illuminate\Support\MessageBag;

class SearchController extends ControllerWithPageResponder{

    /**
     * @var ICategoryRepository
     */
    protected $ICategoryRepository;

    function __construct(IPageResponder $IPageResponder, ICategoryRepository $ICategoryRepository)
    {
        parent::__construct($IPageResponder);
        $this->ICategoryRepository = $ICategoryRepository;
    }

    function getIndex(Request $request)
    {
        $str = $request->input('str');

        $input['errors'] = new MessageBag();
        $input['alerts'] = [];

        $Categories = [];
        if(!$str)//нету строки запроса - просто показываем форму поиска
        {

        }
        else//есть строка запроса - ищем
        {
            $Categories = $this->ICategoryRepository->FindBySearchString($str);
            if(!$Categories) $input['alerts'] = 'По вашему запросу ничего не найдено!';
        }

        $Page = new SearchResult($Categories, $str);
        $Response = $this->IPageResponder->GetPageResponse($Page);
        return $Response;
    }

} 