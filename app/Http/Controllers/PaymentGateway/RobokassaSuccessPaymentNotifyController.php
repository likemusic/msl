<?php namespace App\Http\Controllers\PaymentGateway;

use App\Helpers\Mailer\IMailer;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Order\IOrderRepository;
use App\Models\Order\OrderStateEnum;

class RobokassaSuccessPaymentNotifyController extends Controller{

    function index(Request $request, IOrderRepository $orderRepository, IMailer $mailer)
    {
        $order_id = $request->input('InvId');
        //TODO: check sign
        if($order_id>0)
        {
            $orderRepository->UpdateState($order_id, OrderStateEnum::PAID);
            $order = $orderRepository->GetById($order_id);
            $mailer->NewPaidOrder($order);
        }

        return "OK{$order_id}";
    }
} 