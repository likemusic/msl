<?php namespace App\Http\Controllers\PaymentGateway;

use App\Helpers\Mailer\IMailer;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Order\IOrderRepository;
use App\Models\Order\OrderStateEnum;

class W1SuccessPaymentNotifyController extends Controller{

    function index(Request $request, IOrderRepository $orderRepository, IMailer $mailer)
    {
        $WMI_ORDER_STATE = $request->input('WMI_ORDER_STATE');
        $order_id = $request->input('order_id');
        //TODO: check sign
        if (($order_id > 0) && ($WMI_ORDER_STATE == 'Accepted')) {
            $orderRepository->UpdateState($order_id, OrderStateEnum::PAID);
            $order = $orderRepository->GetById($order_id);
            $mailer->NewPaidOrder($order);
            return 'WMI_RESULT=OK';
        }
        //TODO: доделать
        return '';
    }
}