<?php namespace App\Http\Controllers\PaymentGateway;

use App\Helpers\PaymentGateway\Base\Proxy\IProxyServer;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class RobokassaProxyController extends Controller {

    function index(Request $request, IProxyServer $IProxyServer)
    {
        $UseAsServer = config('robokassa-proxy-server.is-server');
        if(!$UseAsServer) abort(404);
        //TODO: Add log on abort
        $RedirectUrl = $IProxyServer->GetRedirectUrl($request);
        return redirect($RedirectUrl);
    }
} 