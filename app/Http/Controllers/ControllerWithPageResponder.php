<?php namespace App\Http\Controllers;

use App\Helpers\PageResponder\IPageResponder;

abstract class ControllerWithPageResponder extends Controller{

    /**
     * @var IPageResponder
     */
    protected $IPageResponder;

    public function __construct(IPageResponder $IPageResponder)
    {
        $this->IPageResponder = $IPageResponder;
    }
} 