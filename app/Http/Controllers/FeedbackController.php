<?php namespace App\Http\Controllers;

use App\Helpers\Mailer\IMailer;
use App\Helpers\PageResponder\IPageResponder;
use App\Models\PageEntity\Feedback\Feedback;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\MessageBag;

class FeedbackController extends ControllerWithPageResponder{

    /**
     * @var IMailer
     */
    protected $mailer;

    function __construct(IPageResponder $IPageResponder, IMailer $mailer)
    {
        parent::__construct($IPageResponder);
        $this->mailer = $mailer;
    }

    function getIndex(Request $request)
    {
        $Page = new Feedback([]);
        $Response = $this->IPageResponder->GetPageResponse($Page);
        return $Response;
    }

    function postIndex(Request $request)
    {
        $validationRules = [
            'email'    => 'required|email',
            'message_text'      => 'required'
        ];

        $this->validate($request, $validationRules);

        $input = $request->only('email', 'phone', 'message_text' );
        $input['errors'] = new MessageBag();
        $input['alerts'] = [];

        try{
            $this->mailer->NewFeedback($input);
            $input['send_success'] = true;
        }
        catch(\Exception $e)
        {
            $input['errors']->add('send',trans('index.ERR_WRONG_POST')."<br/>(".$e->getMessage().')');
        }

        $Page = new Feedback($input);
        $Response = $this->IPageResponder->GetPageResponse($Page);
        return $Response;
    }
} 