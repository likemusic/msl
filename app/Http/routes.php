<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

/*
Route::get('/', 'WelcomeController@index');

Route::get('home', 'HomeController@index');

Route::get('smarty', function(){
    return view('smarty');
});
*/
Route::controllers([
	//'auth' => 'Auth\AuthController',
	//'password' => 'Auth\PasswordController',
    'feedback'=> 'FeedbackController',
    'callback'=> 'CallbackController',
    'search' => 'SearchController'
]);


//Редирект при оплате через робокассу на сервере
Route::any('PaymentInit.php', 'PaymentGateway\RobokassaProxyController@index');

//Обработка запросов от Робокассы
//Success Payment
Route::any( 'payment_ok_robokassa', 'PaymentGateway\RobokassaSuccessPaymentNotifyController@index');
Route::any( 'payment_ok_w1', 'PaymentGateway\W1SuccessPaymentNotifyController@index');


Route::get('{url}', 'RouteController@index')->where(['url'=>'.*']);
