<?php namespace App\Console;

use App\Console\Commands\DomainsConfigCache;
use App\Console\Commands\DomainsConfigClear;
use App\Console\Commands\ExportUrls;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use App\LaravelOveride\Bootstrap\DetectEnvironment;
use App\LaravelOveride\Bootstrap\LoadConfiguration;


class Kernel extends ConsoleKernel {

    /**
     * The bootstrap classes for the application.
     *
     * @var array
     */
    protected $bootstrappers = [
        DetectEnvironment::class,//'Illuminate\Foundation\Bootstrap\DetectEnvironment',
        LoadConfiguration::class,//'Illuminate\Foundation\Bootstrap\LoadConfiguration',
        'Illuminate\Foundation\Bootstrap\ConfigureLogging',
        'Illuminate\Foundation\Bootstrap\HandleExceptions',
        'Illuminate\Foundation\Bootstrap\RegisterFacades',
        'Illuminate\Foundation\Bootstrap\SetRequestForConsole',
        'Illuminate\Foundation\Bootstrap\RegisterProviders',
        'Illuminate\Foundation\Bootstrap\BootProviders',
    ];
	/**
	 * The Artisan commands provided by your application.
	 *
	 * @var array
	 */
	protected $commands = [
		'App\Console\Commands\Inspire',
        DomainsConfigCache::class,
        DomainsConfigClear::class,
        ExportUrls::class
	];

	/**
	 * Define the application's command schedule.
	 *
	 * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
	 * @return void
	 */
	protected function schedule(Schedule $schedule)
	{
		$schedule->command('inspire')
				 ->hourly();
	}

}
