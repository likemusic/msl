<?php

namespace App\Console\Commands;

use App\Models\PageEntity\StoreEntity\Base\IStoreEntity;
use App\Models\PageEntity\StoreEntity\Category\ICategory;
use App\Models\PageEntity\StoreEntity\IGlobalStoreEntityRepository;
use App\Models\PageEntity\StoreEntity\Product\IProduct;
use Illuminate\Console\Command;

class ExportUrls extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'export:urls {file} {root=0}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Export url to "name,url" list.';

    /**
     * @var IGlobalStoreEntityRepository
     */
    protected $IGlobalStoreEntityRepository;

    protected $Filename;

    protected $EnableCategories = false;
    protected $EnableProducts = true;
    protected $EnableSubProducts = false;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(IGlobalStoreEntityRepository $IGlobalStoreEntityRepository)
    {
        parent::__construct();
        $this->IGlobalStoreEntityRepository = $IGlobalStoreEntityRepository;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $CategoryPartId = 0;
        if($this->argument('root')) list($name ,$CategoryPartId) = explode('=', $this->argument('root'),2);
        list($name ,$this->Filename) = explode('=',$this->argument('file'),2);

        $CalculatedCategory = $CategoryPartId
            ? $this->IGlobalStoreEntityRepository->GetByVID($CategoryPartId)
            : $this->IGlobalStoreEntityRepository->GetRoots();

        $this->AddCatagoryUrlRecursively($CalculatedCategory);
        $this->info('Export urls - Done!');
    }

    protected function AddCatagoryUrlRecursively(ICategory $ICategory)
    {
        if($this->EnableCategories)
        {
            $this->AddToFile($ICategory);
        }

        $Products = $ICategory->GetExternalChilds();
        foreach($Products as $Product)
        {
            $this->AddProductUrlRecursively($Product);
        }

        $SubCategories = $ICategory->GetInternalChilds();
        foreach($SubCategories as $SubCategory)
        {
            $this->AddCatagoryUrlRecursively($SubCategory);
        }
    }

    protected function AddProductUrlRecursively(IProduct $IProduct)
    {
        if($this->EnableProducts)
        {
            $this->AddToFile($IProduct);
        }

        if($this->EnableSubProducts)
        {
            $SubProducts = $IProduct->GetExternalChilds();
            foreach($SubProducts as $SubProduct)
            {
                $this->AddSubProductUrl($SubProduct);
            }
        }
    }

    protected function AddToFile(IStoreEntity $IStoreEntity)
    {
        $Name = $IStoreEntity->GetName();
        $Url = $IStoreEntity->GetUrl();
        file_put_contents($this->Filename,"{$Name}\t{$Url}\n",FILE_APPEND);
    }
}
