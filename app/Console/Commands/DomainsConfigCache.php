<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Foundation\Console\ConfigCacheCommand;

class DomainsConfigCache extends ConfigCacheCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'config:cache';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description.';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('Cache configs for all domains...');
        $this->call('config:clear');

        $domains = $this->laravel->domains();//TODO:$this->laravel->make('domains')
        foreach($domains as $domain)
        {
            $this->CacheDomainConfig($domain);
        }
    }

    protected function CacheDomainConfig($domain)
    {
        $config = $this->getFreshConfigurationForDomain($domain);

        $CachedConfigPath = $this->laravel->getCachedConfigPath($domain);
        $CachedConfigDir = dirname($CachedConfigPath);
        if(!is_dir($CachedConfigDir)) mkdir($CachedConfigDir,0777,true);

        $this->files->put(
            $CachedConfigPath, '<?php return '.var_export($config, true).';'.PHP_EOL
        );

        $this->info("Configuration for domain '{$domain}' cached successfully!");
    }

    /**
     * Boot a fresh copy of the application configuration.
     *
     * @return array
     */
    protected function getFreshConfigurationForDomain($domain)
    {
        $app = require $this->laravel->basePath().'/bootstrap/app.php';

        $app->make('Illuminate\Contracts\Console\Kernel')->bootstrap();

        return $app['config']->all();
    }
}
