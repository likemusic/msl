<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Foundation\Console\ConfigClearCommand;

class DomainsConfigClear extends ConfigClearCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'config:clear';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description.';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $domains = $this->laravel->domains();
        foreach($domains as $domain)
        {
            $this->clearDomainConfig($domain);
        }
    }

    protected function clearDomainConfig($domain)
    {
        $this->files->delete($this->laravel->getCachedConfigPath($domain));
        $this->info("Configuration cache for domain '{$domain}' cleared!");
    }
}
