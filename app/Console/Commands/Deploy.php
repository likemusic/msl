<?php

namespace app\Console\Commands;

use Illuminate\Console\Command;

class Deploy extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'deploy';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Deploy all domains in config.';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info("Deploy all domains.");
        return;
    }
}
