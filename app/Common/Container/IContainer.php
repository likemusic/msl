<?php namespace App\Common\Container;

use Illuminate\Contracts\Container\Container;

interface IContainer extends Container{

    /**
     * Register a shared binding in the container that will wrapped by dynamic proxy with Lazy Loading.
     *
     * @param  string  $abstract
     * @param  \Closure|string|null  $concrete
     * @return void
     */
    public function LazyLoadingProxySingleton($abstract, $concrete = null);
} 