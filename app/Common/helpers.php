<?php
if ( ! function_exists('domain'))
{
    /**
     * Get the path to the base of the install.
     *
     * @return string
     */
    function domain()
    {
        return app('domain');
    }

    function view_theme()
    {
        if(! config('themes.enabled')) return null;
        return config('themes.active');
    }

    function view_theme_dir()
    {
        $ActiveTheme = view_theme();
        if(!$ActiveTheme) return null;

        $ConfigThemeDir = config('themes.themes.'.$ActiveTheme.'.views-path');
        $ThemeDir =  ($ConfigThemeDir) ? $ConfigThemeDir : $ActiveTheme;
        return $ThemeDir;
    }

    function view_theme_path()
    {
        $ThemeDir = view_theme_dir();
        if(!$ThemeDir) return null;

        $ViewPath = config('view.paths')[0];
        $ThemePath = $ViewPath.DIRECTORY_SEPARATOR.$ThemeDir.DIRECTORY_SEPARATOR;
        return $ThemePath;
    }

    function asset_path()
    {
        $AssetPath = '/';
        $ActiveTheme = view_theme();
        if(!$ActiveTheme) return $AssetPath;
        $ConfigAssetPath = config('themes.themes.'.$ActiveTheme.'.asset-path');
        $AssetPath .=  ($ConfigAssetPath) ?: $ActiveTheme;
        return $AssetPath;
    }

    //Возвращает содержимое файла в папке с конфигурационными файлами, для текущго окружения текущего домена
    //Используется для загрузки кодов счетчиков для разных доменов
    function config_file_content($filename)
    {
        $app = app();

        $ConfigDir = $app->configPathDomainEnv();
        $FullFilename = $ConfigDir.'/'.$filename;
        if(file_exists($FullFilename)) return file_get_contents($FullFilename);

        $ConfigDir = $app->configPathEnv();
        $FullFilename = $ConfigDir.'/'.$filename;
        if(file_exists($FullFilename)) return file_get_contents($FullFilename);

        $ConfigDir = $app->configPathDomain();
        $FullFilename = $ConfigDir.'/'.$filename;
        if(file_exists($FullFilename)) return file_get_contents($FullFilename);

        $ConfigDir = $app->configPath();
        $FullFilename = $ConfigDir.'/'.$filename;
        if(file_exists($FullFilename)) return file_get_contents($FullFilename);

        return null;
    }
}