<?php namespace App\LaravelOveride\Bootstrap;

use Illuminate\Contracts\Foundation\Application;
use Symfony\Component\Finder\SplFileInfo;
use Symfony\Component\Finder\Finder;
use Illuminate\Contracts\Config\Repository as RepositoryContract;

class LoadConfiguration extends \Illuminate\Foundation\Bootstrap\LoadConfiguration {

    protected $FileFinder;

    function __construct()
    {
        $this->FileFinder = Finder::create()->files()->name('*.php')->sortByName();;
    }
    /**
     * Load the configuration items from all of the files.
     *
     * @param  \Illuminate\Contracts\Foundation\Application  $app
     * @param  \Illuminate\Contracts\Config\Repository  $config
     * @return void
     */
    protected function loadConfigurationFiles(Application $app, RepositoryContract $config)
    {
        //TODO: проверить работу при оптимизации конфигов в кэш
        $this->overloadConfigurationFiles($app->configPath(), $config);
        $this->overloadConfigurationFiles($app->configPathDomain(), $config);
        $this->overloadConfigurationFiles($app->configPathEnv(), $config);
        $this->overloadConfigurationFiles($app->configPathDomainEnv(), $config);
    }

    protected function overloadConfigurationFiles($path, $config)
    {
        $files = $this->getConfigurationFilesNew($path);
        foreach ($files as $key => $path)
        {
            $oldConfigValues = $config->get($key);
            $oldConfigValues = (!is_null($oldConfigValues)) ? $oldConfigValues : [];
            $newConfigValues = require $path;

            $config->set($key,array_replace_recursive($oldConfigValues, $newConfigValues));
        }
    }

    /**
     * Get all of the configuration files for the application.
     *
     * @param  \Illuminate\Contracts\Foundation\Application  $app
     * @return array
     */
    protected function getConfigurationFilesNew($path)
    {
        $files = [];
        if(!is_readable($path)) return array();
        $FileFinder = clone $this->FileFinder;//TODO: проверить что быстрее, создание с нужными параметрами или клонирование и оставить более быстрый

        foreach ($FileFinder->in($path) as $file)
        {
            $nesting = $this->getConfigurationNesting($file, $path);
            $files[$nesting.basename($file->getRealPath(), '.php')] = $file->getRealPath();
        }

        return $files;
    }

    /**
     * Get the configuration file nesting path.
     *
     * @param  \Symfony\Component\Finder\SplFileInfo  $file
     * @return string
     */
    protected function getConfigurationNesting(SplFileInfo $file, $base_path)
    {
        $directory = dirname($file->getRealPath());

        if ($tree = trim(str_replace($base_path, '', $directory), DIRECTORY_SEPARATOR))
        {
            $tree = str_replace(DIRECTORY_SEPARATOR, '.', $tree).'.';
        }

        return $tree;
    }


} 