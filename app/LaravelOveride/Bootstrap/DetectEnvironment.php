<?php namespace App\LaravelOveride\Bootstrap;

use Dotenv;
use Illuminate\Contracts\Foundation\Application;

class DetectEnvironment  extends \Illuminate\Foundation\Bootstrap\DetectEnvironment{
    /**
     * Bootstrap the given application.
     *
     * @param  \Illuminate\Contracts\Foundation\Application  $app
     * @return void
     */
    public function bootstrap(Application $app)
    {
        Dotenv::makeMutable();//for overwrite variable values
        $envFile = $app->environmentFile();

        $this->loadDotenv($app->configPath(),$envFile);//default domain, default env
        $this->loadDotenv($app->configPathDomain(), $envFile);//current domain, default env

        $app->detectEnvironment(function()
        {
            return env('APP_ENV', 'production');
        });

        $this->loadDotenv($app->configPathEnv(), $envFile );//default domain, current env
        $this->loadDotenv($app->configPathDomainEnv(), $envFile );//default domain, current env
    }

    protected function loadDotenv($path, $file)
    {
        $envFilenameWithPath = $path.DIRECTORY_SEPARATOR.$file;
        if(is_readable($envFilenameWithPath)) Dotenv::load($path, $file );
    }
}