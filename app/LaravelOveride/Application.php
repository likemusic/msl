<?php namespace App\LaravelOveride;

class Application extends \Illuminate\Foundation\Application{
    /**
     * The current used domain.
     *
     * @var string
     */
    protected $domain;

    //protected $domainsDirectory = "domains";

    protected $defaultDomain = 'default';

    protected $defaultEnv = 'default';


    /**
     * Create a new Illuminate application instance.
     *
     * @param  string|null  $basePath
     * @param string|null  $domain
     * @return void
     */

    public function __construct($basePath = null, $domain=null)
    {
        $this->domain = $domain ?: $this->defaultDomain;
        parent::__construct($basePath);
        $this->instance('domain',$this->domain);
    }

    public function configBasePath()
    {
        return $this->basePath.DIRECTORY_SEPARATOR.'config';
    }

    /**
     * Get the path to the application configuration files (for default domain, default env).
     *
     * @return string
     */
    public function configPath()
    {
        return $this->configBasePath().DIRECTORY_SEPARATOR.$this->defaultDomain.DIRECTORY_SEPARATOR.$this->defaultEnv;
    }

    /**
     * Get the path to the application configuration files (for current domain, current env).
     *
     * @return string
     */
    public function configPathEnv()
    {
        return $this->configBasePath().DIRECTORY_SEPARATOR.$this->defaultDomain.DIRECTORY_SEPARATOR.$this->environment();
    }

    /**
     * Get the path to the application configuration files (for default domain, default env).
     *
     * @return string
     */
    public function configPathDomain()
    {
        return $this->configBasePath().DIRECTORY_SEPARATOR.$this->domain.DIRECTORY_SEPARATOR.$this->defaultEnv;
    }

    /**
     * Get the path to the application configuration files (for default domain, current env).
     *
     * @return string
     */
    public function configPathDomainEnv()
    {
        return $this->configBasePath().DIRECTORY_SEPARATOR.$this->domain.DIRECTORY_SEPARATOR.$this->environment();
    }


    public function publicPath()
    {
        return $_SERVER['DOCUMENT_ROOT'] ?: $this->basePath.DIRECTORY_SEPARATOR.'public';
    }

    /**
     * Get the path to the storage directory.
     *
     * @return string
     */
    public function storagePath()
    {
        return $this->storagePath ?: $this->basePath.DIRECTORY_SEPARATOR.'storage'.DIRECTORY_SEPARATOR.$this->domain;
    }

    /**
     * Get the path to the configuration cache file.
     *
     * @return string
     */
    public function getCachedConfigPath($domain=null)
    {
        $domain = ($domain) ? $domain : $this->make('domain');
        return $this->basePath().'/bootstrap/cache/'.$domain.'/config.php';
    }

    public function domains()
    {
        $ConfigPath = $this->configBasePath();
        //имена папок в папке config
        $domains = glob($ConfigPath.'/*',GLOB_ONLYDIR);
        $domains = array_map('basename',$domains);
        return $domains;
    }

}