<?php namespace App\Providers;

use App\Models\Base\GlobalQueryScopes\Disabled\DisabledPlugin;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider {

	/**
	 * Bootstrap any application services.
	 *
	 * @return void
	 */
	public function boot()
	{
        $analogue = app('analogue');
        $analogue->registerPlugin(DisabledPlugin::class);
    }

	/**
	 * Register any application services.
	 *
	 * This service provider is a great spot to register your various container
	 * bindings with the application. As you can see, we are registering our
	 * "Registrar" implementation here. You can add your own bindings too!
	 *
	 * @return void
	 */
	public function register()
	{
		$this->app->bind(
			'Illuminate\Contracts\Auth\Registrar',
			'App\Services\Registrar'
		);

        //надо ли использовать Smarty
        //$config = $this->app('config');
        $useSmarty = config('view.use_smarty');
        if($useSmarty)
        {
            $smartyServiceProvider = config('view.smarty_provider');
            $this->app->register($smartyServiceProvider);
        }

         //PageEntities

        $this->app->singleton(
            \App\Models\PageEntity\IPageEntityRepository::class,
            \App\Models\PageEntity\GlobalPageEntityRepository::class);

        $this->app->singleton(
            \App\Models\PageEntity\Article\IArticleRepository::class,
            \App\Models\PageEntity\Article\ArticleRepository::class);


        //PageEntities -> StoreEntities
        /*$this->app->singleton(
            \App\Models\PageEntity\StoreEntity\Base\IStoreEntityRepository::class,
            \App\Models\PageEntity\StoreEntity\GlobalStoreEntityRepository::class);*/

        $this->app->singleton(
            \App\Models\PageEntity\StoreEntity\IGlobalStoreEntityRepository::class,
            \App\Models\PageEntity\StoreEntity\GlobalStoreEntityRepository::class);

        //IGlobalStoreEntityRepository

        //PageEntities -> StoreEntities ->Category
        $this->app->singleton(
            \App\Models\PageEntity\StoreEntity\Category\ICategoryRepository::class,
            \App\Models\PageEntity\StoreEntity\Category\Calculated\CalculatedCategoryRepository::class);

        $this->app->singleton(
            \App\Models\PageEntity\StoreEntity\Category\Part\ICategoryPartRepository::class,
            \App\Models\PageEntity\StoreEntity\Category\Part\CategoryPartRepository::class);

        $this->app->singleton(
            \App\Models\PageEntity\StoreEntity\Category\Template\ICategoryTemplateRepository::class,
            \App\Models\PageEntity\StoreEntity\Category\Template\CategoryTemplateRepository::class);

        $this->app->singleton(
            \App\Models\PageEntity\StoreEntity\Category\Type\ICategoryTypeRepository::class,
            \App\Models\PageEntity\StoreEntity\Category\Type\CategoryTypeRepository::class);

        //PageEntities -> StoreEntities -> Product
        $this->app->singleton(
            \App\Models\PageEntity\StoreEntity\Product\IProductRepository::class,
            \App\Models\PageEntity\StoreEntity\Product\Aggregator\ProductAggregatorRepository::class);

        $this->app->singleton(
            \App\Models\PageEntity\StoreEntity\Product\Calculated\ICalculatedProductRepository::class,
            \App\Models\PageEntity\StoreEntity\Product\Calculated\CalculatedProductRepository::class);

        $this->app->singleton(
            \App\Models\PageEntity\StoreEntity\Product\Independent\IIndependentProductRepository::class,
            \App\Models\PageEntity\StoreEntity\Product\Independent\IndependentProductRepository::class);

        $this->app->singleton(
            \App\Models\PageEntity\StoreEntity\Product\Part\IProductPartRepository::class,
            \App\Models\PageEntity\StoreEntity\Product\Part\ProductPartRepository::class);

        $this->app->singleton(
            \App\Models\PageEntity\StoreEntity\Product\Template\IProductTemplateRepository::class,
            \App\Models\PageEntity\StoreEntity\Product\Template\ProductTemplateRepository::class);

        $this->app->singleton(
            \App\Models\PageEntity\StoreEntity\Product\Type\IProductType::class,
            \App\Models\PageEntity\StoreEntity\Product\Type\ProductType::class);

        $this->app->singleton(
            \App\Models\PageEntity\StoreEntity\Product\Type\IProductTypeRepository::class,
            \App\Models\PageEntity\StoreEntity\Product\Type\ProductTypeRepository::class);

        //PageEntities -> StoreEntities -> SubProduct
        $this->app->singleton(
            \App\Models\PageEntity\StoreEntity\SubProduct\ISubProductRepository::class,
            \App\Models\PageEntity\StoreEntity\SubProduct\Calculated\CalculatedSubProductRepository::class);

        $this->app->singleton(
            \App\Models\PageEntity\StoreEntity\SubProduct\Template\ISubProductTemplateRepository::class,
            \App\Models\PageEntity\StoreEntity\SubProduct\Template\SubProductTemplateRepository::class);

        $this->app->singleton(
            \App\Models\PageEntity\StoreEntity\SubProduct\Set\ISubProductSetRepository::class,
            \App\Models\PageEntity\StoreEntity\SubProduct\Set\SubProductSetRepository::class);

        //PageEntities -> Redirect
        $this->app->singleton(
            \App\Models\PageEntity\Redirect\IRedirectRepository::class,
            \App\Models\PageEntity\Redirect\RedirectRepository::class);

        //Helpers
        $this->app->singleton(
            \App\Helpers\Templater\ITemplater::class,
            \App\Helpers\Templater\StrReplaceTemplater::class);

        $this->app->singleton(
            \App\Helpers\PageResponder\IPageResponder::class,
            \App\Helpers\PageResponder\PageResponder::class);

        $this->app->singleton(
            \App\Helpers\Mailer\IMailer::class,
            \App\Helpers\Mailer\Mailer::class
        );

        $this->app->singleton(
            \App\Helpers\TablesNamer\ITablesNamer::class,
            \App\Helpers\TablesNamer\TablesNamer::class
        );

        $this->app->singleton(
            \App\Helpers\TableNamesCatalog\ITableNamesCatalog::class,
            \App\Helpers\TableNamesCatalog\TableNamesCatalog::class
        );

        $this->app->singleton(
            \App\Helpers\PicturesFinder\IPicturesFinder::class,
            \App\Helpers\PicturesFinder\PicturesFinder::class
        );

        $this->app->singleton(
            \App\Helpers\CacheNamer\ICacheNamer::class,
            \App\Helpers\CacheNamer\CacheNamer::class
        );

        /* TODO: impement Payment helpers and controllers
                $this->app->singleton(
                    \App\Helpers\PaymentGateway\Base\BuyButton\IRequestDataProvider::class,
                    \App\Helpers\PaymentGateway\Robokassa\Proxy\RequestDataProvider::class
                );
        */
        //TODO: Separate proxies for any paymentgateway or one for all
        $this->app->singleton(
            \App\Helpers\PaymentGateway\Base\Proxy\IProxyServer::class,
            \App\Helpers\PaymentGateway\Robokassa\Proxy\ProxyServer::class
        );

        $this->app->singleton(
            \App\Helpers\PaymentGateway\Base\BuyButton\IRequestDataProvider::class,
            \App\Helpers\PaymentGateway\W1\RequestDataProvider::class
            //\App\Helpers\PaymentGateway\Robokassa\Proxy\RequestDataProvider::class
        );

        //Domain Entities
        $this->app->singleton(
            \App\Models\Order\IOrderRepository::class,
            \App\Models\Order\OrderRepository::class
        );

        $this->app->singleton(
            \App\Models\User\IUserRepository::class,
            \App\Models\User\UserRepository::class
        );

        $this->app->singleton(
            \App\Helpers\StoreTreeBuilder\IStoreTreeBuilder::class,
            \App\Helpers\StoreTreeBuilder\StoreTreeBuilderNew::class
        );

        $this->app->singleton(
            \App\Models\PageEntity\SpecificPage\ISpecificPageRepository::class,
            \App\Models\PageEntity\SpecificPage\SpecificPageRepository::class
        );

        $this->app->singleton(
            \App\Models\PageEntity\SpecificPage\MainPage\IMainPageProvider::class,
            \App\Models\PageEntity\SpecificPage\MainPage\MainPageProvider::class
        );

        $this->app->singleton(
            \App\Models\PageEntity\SpecificPage\MainPage\IMainPage::class,
            \App\Models\PageEntity\SpecificPage\MainPage\MainPage::class
        );

	}
}
