<?php namespace database\seeds\Base;

use App\Helpers\TableNamesCatalog\ITableNamesCatalog;
use App\Helpers\TablesNamer\ITablesNamer;
use Illuminate\Database\Seeder;

class BaseSeeder extends Seeder{
    /**
     * @var ITableNamesCatalog
     */
    protected $tableNamesCatalog;

    /**
     * @var ITablesNamer
     */
    protected $tablesNamer;

    function __construct(){
        $this->tableNamesCatalog = app(ITableNamesCatalog::class);
        $this->tablesNamer = app(ITablesNamer::class);
    }

}