<?php

use database\seeds\Base\BaseSeeder;
use Illuminate\Support\Facades\DB;

class CategoriesTableSeeder extends BaseSeeder
{
    //config
    protected $CategoriesNames;
    protected $CategoriesCount;

    protected $CategoriesLevels;

    protected $TypesPerLevel;

    //inner
    protected $IteratedCategoriesNames = [];

    public function __construct()
    {
        parent::__construct();
        $Config = config('sys.seeds');
        $this->CategoriesNames = $Config['categories_names'];
        $this->CategoriesCount = $Config['categories_count'];
        $this->CategoriesLevels = $Config['categories_levels'];
        $this->TypesPerLevel = $Config['types_per_level'];
    }
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $ParentId = 0;
        $Id = 1;
        $ParentNumsArr = [];
        $Level = 1;
        $this->IteratedCategoriesNames = $this->ArrayFill($this->CategoriesCount, $this->CategoriesNames);
        $this->InsertRecursively($Level, $ParentId, $ParentNumsArr, $Id);
    }

    protected function InsertRecursively($Level, $Parent, $ParentNumsArr, &$id)
    {
        $i = 0;
        foreach($this->IteratedCategoriesNames as $CategoryName)
        {
            $CurrentNumsArr = $ParentNumsArr;
            $CurrentNumsArr []= ($i+1);
            $CurrentNumsStr = join('.',$CurrentNumsArr);
            $Name = "{$CategoryName} {$CurrentNumsStr} ({$id})";

            $Values = [
                'id' => $id,

                'name' => $Name,
                'genitive' => $Name,
                'src_name' => $Name,
                'src_genitive' => $Name,

                'parent' => $Parent,

                'type_id' => (int)(($this->TypesPerLevel/$this->CategoriesCount)*$i + 1),

                'level' => $Level,

                'adm_parent' => $Parent,
                'adm_level' => $Level,

                'url_slug' => $Name
            ];
            DB::table($this->tableNamesCatalog->GetCategoryPartsTableName())->insert($Values);
            $ChildParent = $id;
            $id++;
            if($Level < $this->CategoriesLevels)
            {
                $this->InsertRecursively($Level+1,$ChildParent,$CurrentNumsArr,$id);
            }
            $i++;
        }
    }

    protected function ArrayFill($length, $values) {
        $result = array();
        foreach (new InfiniteIterator(new ArrayIterator($values)) as $element) {
            if (!$length--) return $result;
            $result[] = $element;
        }
        return $result;
    }
}
