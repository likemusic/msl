<?php

use database\seeds\Base\BaseSeeder;

class CategoryTypesTableSeeder extends BaseSeeder
{
    protected $MaxCount = 10;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($i = 1; $i <= $this->MaxCount;$i++)
        {
            $Values = [
                'id' => $i,
                'name' => 'Category Type '.$i,
                'template_id' => $i
            ];

            DB::table($this->tableNamesCatalog->GetCategoryTypesTableName())->insert($Values);
        }
    }
}