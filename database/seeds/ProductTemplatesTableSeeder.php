<?php

use database\seeds\Base\BaseSeeder;
use \App\Helpers\PicturesFinder\IPicturesFinder;

class ProductTemplatesTableSeeder extends BaseSeeder{

    protected $TemplatesCount = 10;

    protected $PicturesDirs = [
        'GPS_karta_goroda',
        'SPUTNIK_karta_goroda_20m'
    ];

    protected $PicturesPerTemplate = 5;

    protected $ImagesFilesSeparator = ',';

    /**
     * @var IPicturesFinder
     */
    protected $IPicturesFinder;

    public function __construct(IPicturesFinder $IPicturesFinder)
    {
        $this->IPicturesFinder = $IPicturesFinder;
    }

    public function run()
    {
        for($i = 1; $i <= $this->TemplatesCount; $i++)
        {
            $Name = "Product Template {$i}";
            $PicturesDir = $this->GetPicturesDir($i-1);

            $Values = [
                'id' => $i,
                'name' => "{$Name}",
                'description' => "{$Name} - Description",
                'url_slug' => $Name,
                'price' => $i*1000/3,
                'pictures_dir' => $PicturesDir,
                'pictures'  => join($this->ImagesFilesSeparator, $this->GetPictures($PicturesDir)),

                'title' => "{$Name} - Title",
                'h1' => "{$Name} - h1",
                'meta_keywords' => "{$Name} - meta_keywords",
                'meta_description' => "{$Name} - meta_description",
            ];

            DB::table($this->tableNamesCatalog->GetProductTemplatesTableName())->insert($Values);
        }
    }

    protected function GetPicturesDir($i)
    {
        $Count = count($this->PicturesDirs);
        $Index = $i % $Count;

        $Dir = $this->PicturesDirs[$Index];
        return $Dir;
    }

    protected function GetPictures($PicturesDir)
    {
        $PicturesDirFullPath = public_path().config('site.dirs.product_templates_images').$PicturesDir;
        return $this->IPicturesFinder->GetPictures($PicturesDirFullPath, $this->PicturesPerTemplate);
    }
}