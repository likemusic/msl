<?php

use database\seeds\Base\BaseSeeder;

class PivotCategoryTemplatesProductTemplatesTableSeeder extends BaseSeeder{
    protected $TemplatesCount = 10;
    protected $ProductsPerTemplate = 10;

    public function run()
    {
        for($i = 1; $i <= $this->TemplatesCount; $i++) {

            for ($j = 1; $j <= $this->ProductsPerTemplate; $j++)
            {
                $Values = [
                    'id' => 0,
                    'category_template_id' => $i,
                    'product_template_id' => $j
                ];
                //TODO: get field nave by tableNamesHelper

                DB::table($this->tableNamesCatalog->GetPivotCategoryTemplatesAndProductTemplatesTableName())->insert($Values);
            }
        }
    }
} 