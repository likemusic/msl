<?php

use database\seeds\Base\BaseSeeder;

class PivotProductTemplatesSubProductTemplatesTableSeeder extends BaseSeeder{
    protected $TemplatesCount = 10;
    protected $ProductsPerTemplate = 10;

    public function run()
    {
        for($i = 1; $i <= $this->TemplatesCount; $i++) {

            for ($j = 1; $j <= $this->ProductsPerTemplate; $j++)
            {
                $Values = [
                    'id' => 0,
                    'product_template_id' => $i,
                    'subproduct_template_id' => $j,
                ];
                //TODO: get field nave by tableNamesHelper

                DB::table($this->tableNamesCatalog->GetPivotProductTemplatesAndSubProductTemplatesTableName())->insert($Values);
            }
        }
    }
}