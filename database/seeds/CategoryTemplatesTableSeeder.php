<?php

use database\seeds\Base\BaseSeeder;

class CategoryTemplatesTableSeeder extends BaseSeeder
{
    protected $TemplatesCount = 10;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($i = 1; $i <= $this->TemplatesCount; $i++)
        {
            $Name = "Category Template {$i}";
            $Values = [
                'id' => $i,
                'name' => $Name,
                'description' => "{$Name} - Description",
                'title' => "{$Name} - Title",
                'meta_keywords' => "{$Name} - meta_keywords",
                'meta_description' => "{$Name} - meta_description"
            ];

            DB::table($this->tableNamesCatalog->GetCategoryTemplatesTableName())->insert($Values);
        }
    }
}
