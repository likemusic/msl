<?php

use Illuminate\Database\Schema\Blueprint;
use database\migrations\Base\CreateDomainSpecificTableMigration;

class CreateCategoryTypeTable extends CreateDomainSpecificTableMigration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->GetTableName(), function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('template_id')->unsigned()->nullable()->references('id')->on( $this->tableNamesCatalog->GetCategoryTemplatesTableName());
            $this->timestamps($table);
        });
    }

    protected function GetTableName()
    {
        return $this->tableNamesCatalog->GetCategoryTypesTableName();
    }
}
