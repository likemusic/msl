<?php

use \database\migrations\Base\CreateDomainSpecificLinkTableMigration;

class CreatePivotCategoryTypeAndProductTemplateTable extends CreateDomainSpecificLinkTableMigration
{
    protected function GetParentTableName()
    {
        return $this->tableNamesCatalog->GetCategoryTypesTableName();
    }

    protected function GetChildTableName()
    {
        return $this->tableNamesCatalog->GetProductTemplatesTableName();
    }
}
