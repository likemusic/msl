<?php

use Illuminate\Database\Schema\Blueprint;
use database\migrations\Base\BaseMigration;

class CreateOrdersTable extends BaseMigration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->nullable()->references('id')->on('users');//пользователь
            $table->string('good_id');//код товара
            $this->timestamps($table);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('orders');
    }
}
