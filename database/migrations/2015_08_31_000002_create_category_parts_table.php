<?php

use Illuminate\Database\Schema\Blueprint;
use database\migrations\Base\CreateDomainSpecificTableMigration;

class CreateCategoryPartTable extends CreateDomainSpecificTableMigration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->GetTableName(), function (Blueprint $table) {
            $table->increments('id');

            //формы названия населенного пунта
            $table->string('name')->nullabe();//название в именительном пажеде с указанием типа населенного пункта ("город Борисов")
            $table->string('genitive')->nullabe();//название в родительном падеже с указанием типа населенного пункта ("города Борисова")
            $table->string('genitive_translit')->nullabe();//транслит поля genitive
            /*  В изначальной БД по нему (cat_h1_translit) строилось поле UID (url).
                Url отличается от genitive_translit тем что не должно содержать слешей и других спецсимволов и дублей (к дублям добавляется id).
                Возможно будет использоваться для наведения порядка с url, и настройки редиректов.
             */

            $table->string('src_name')->nullabe();//название в именительном падеже без указания типа населенного пункта ("Борисов")
            $table->string('src_genitive')->nullabe();//название в родительном падеже без указания типа населенного пункта ("Борисова")
            //TODO: разобраться что используется а что нет, и удалить nullable

            /**
             * Храним в таблице сразу 2 иерархии:
             * - показываемую на сайте (parent);
             * - по административному делению (adm_parent);
             */
            $table->integer('parent')->unsigned()->nullable();
            $table->integer('type_id')->unsigned()->nullable()->references('id')->on(config('site.tables.category.type'));//тип - используется для классификации/сортировки и связи с шаблоном
            $table->integer('level');//уроввень в иерархии показа на сайте

            //иерархия по административно-территориальному делению
            $table->integer('adm_parent')->unsigned()->nullable();
            $table->integer('adm_level')->unsigned()->nullable();//уровень в дереве административно-территориального деления
            $table->integer('adm_type_id')->unsigned()->nullable();//тип административной единицы (деревня, район, кожуун и т.д.)

            $table->string('url_slug')->nullable();;//при иерархической структуре сайта
            $table->string('url')->nullable();//при плоской структуре сайта (значения старого сайта)

            $table->integer('group_id')->unsigned()->nullable();//к какой группе карт относится
            //на данный момент есть 3 группы карт: Росси, Все страны мира, Старинные карты.

            $table->integer('sort_order')->nullable();

            $this->timestamps($table);

            $table->index('src_name');//для поиска по имени
            $table->index('parent');//для постоения дерева
            $table->index(['parent','sort_order','type_id', 'src_name', 'name']);//для сортировки при постоении дерева
            $table->unique('url_slug');//для поиска
            $table->unique('url');//для поиска
        });
    }

    protected function GetTableName()
    {
        return $this->tableNamesCatalog->GetCategoryPartsTableName();
    }
}
