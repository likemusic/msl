<?php

use Illuminate\Database\Schema\Blueprint;
use database\migrations\Base\BaseMigration;

class CreateArticlesTable extends BaseMigration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('articles', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title')->nullable();
            $table->string('url')->nullable();
            $table->string('h1')->nullable();
            $table->mediumText('content')->nullable();
            $table->string('meta_keywords')->nullable();
            $table->string('meta_description')->nullable();
            $table->integer('content_type')->unsigned()->nullable();
            $this->timestamps($table);

            $this->unique('url');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('articles');
    }
}
