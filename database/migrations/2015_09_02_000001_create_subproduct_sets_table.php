<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use database\migrations\Base\CreateDomainSpecificTableMigration;

class CreateSubproductSetsTable extends CreateDomainSpecificTableMigration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->GetTableName(), function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('subproduct_template_id')->nullable();
            $table->timestamps();
        });
    }

    function GetTableName()
    {
        $this->tableNamesCatalog->GetSubProductSetsTableName();
    }
}
