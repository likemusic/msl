<?php

use Illuminate\Database\Schema\Blueprint;
use database\migrations\Base\CreateDomainSpecificTableMigration;

class CreateCategoryTemplateTable extends CreateDomainSpecificTableMigration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->GetTableName(), function (Blueprint $table) {
            $table->increments('id');
            $table->string('tpl_name');
            $table->string('name')->nullable();

            $table->string('meta_keywords')->nullable();
            $table->string('meta_description')->nullable();

            $table->string('title')->nullable();
            $table->mediumText('description')->nullable();//содержание блока описания на странице
            //$table->string('h1')->nullable(); - не нужно, т.к. везде карты

            $this->timestamps($table);
        });
    }

    protected function GetTableName()
    {
        return $this->tableNamesCatalog->GetCategoryTemplatesTableName();
    }
}
