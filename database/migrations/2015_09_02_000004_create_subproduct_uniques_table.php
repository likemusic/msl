<?php

use Illuminate\Database\Schema\Blueprint;
use database\migrations\Base\CreateDomainSpecificTableMigration;

class CreateSubproductDescriptionsTable extends CreateDomainSpecificTableMigration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->GetTableName(), function (Blueprint $table) {
            $table->increments('id');
            $table->integer('product_type_id');
            $table->integer('subproduct_template_id');
            $table->string('name');
            $table->string('title');
            $table->mediumText('description');
            $table->string('pictures_dir');
            $table->timestamps();

            $this->index('product_type_id');
            $this->index('subproduct_template_id');
            $this->index('subproduct_template_id', 'product_type_id');
        });
    }

    protected function GetTableName()
    {
        return $this->tableNamesCatalog->GetSubProductUniquesTableName();
    }
}
