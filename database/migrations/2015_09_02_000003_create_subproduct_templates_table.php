<?php

use Illuminate\Database\Schema\Blueprint;
use database\migrations\Base\CreateDomainSpecificTableMigration;

class CreateSubproductTemplateTable extends CreateDomainSpecificTableMigration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(config('site.tables.subproduct.template'), function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('url_slug');

            $table->string('target');
            $table->string('target_ru');

            $table->string('meta_keywords')->nullable();
            $table->string('meta_description')->nullable();

/*
            $table->string('title')->nullable();
            $table->string('h1')->nullable();
*/
            $table->integer('order')->nullable();

            $table->integer('subproduct_title_id')->nullable();
            $table->integer('subproduct_type_id')->nullable();

            $this->timestamps($table);

            $table->index('url_slug');//для поиска по url
        });
    }

    protected function GetTableName()
    {
        return $this->tableNamesCatalog->GetSubProductTemplatesTableName();
    }
}
