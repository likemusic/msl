<?php namespace database\migrations\Base;

use App\Helpers\TableNamesCatalog\ITableNamesCatalog;
use database\migration\Base\TCreateDomainSpecificTableMigration;
use Illuminate\Support\Facades\Schema;

abstract class CreateDomainSpecificTableMigration {
    use TCreateDomainSpecificTableMigration, TTimestamps;

    //function __construct(ITableNamesCatalog $tableNamesCatalog){
    function __construct(){

        //$this->tableNamesCatalog = $tableNamesCatalog;
        $this->tableNamesCatalog = app(ITableNamesCatalog::class);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop($this->GetTableName());
    }
}