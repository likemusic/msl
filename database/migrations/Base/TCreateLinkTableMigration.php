<?php namespace database\migration\Base;

use App\Helpers\TablesNamer\ITablesNamer;

trait TCreateLinkTableMigration{
    use TCreateDomainSpecificTableMigration;

    /**
     * @var ITablesNamer
     */
    protected $tablesNamer;

    abstract protected function GetParentTableName();
    abstract protected function GetChildTableName();

    protected function GetParentFieldName()
    {
        $TableName = $this->GetParentTableName();
        return $this->GetLinkTableFieldName($TableName);
    }

    protected function GetChildFieldName()
    {
        $TableName = $this->GetChildTableName();
        return $this->GetLinkTableFieldName($TableName);
    }

    protected function GetLinkTableFieldName($TableName)
    {
        return $this->tablesNamer->GetLinkTableFieldName($TableName);
    }
}