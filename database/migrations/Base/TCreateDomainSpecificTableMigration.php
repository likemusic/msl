<?php namespace database\migration\Base;

use App\Helpers\TableNamesCatalog\ITableNamesCatalog;
use Illuminate\Support\Facades\Schema;

trait TCreateDomainSpecificTableMigration{

    /**
     * @var ITableNamesCatalog
     */
    protected $tableNamesCatalog;


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop($this->GetTableName());
    }

    abstract protected function GetTableName();
}