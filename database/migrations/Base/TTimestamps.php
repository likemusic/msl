<?php namespace database\migrations\Base;

use Illuminate\Database\Schema\Blueprint;

trait TTimestamps {
    /**
     * @param $table
     *
     * Вынес в отдельную ф-цию, т.к. при переходе на более новую версию надо будет использовать
     * ROW подстановки (для MySQL >= 5.6.5
     * Current Open Server version = 5.6.22-log
     */
    protected function timestamps(Blueprint $table)
    {
        $table->timestamps(); //- default managed by Eloquent, not by DB (MySQL)

        //$table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
        //$table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
    }
} 