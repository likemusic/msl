<?php namespace database\migrations\Base;


use App\Helpers\TableNamesCatalog\ITableNamesCatalog;
use App\Helpers\TablesNamer\ITablesNamer;
use database\migration\Base\TCreateLinkTableMigration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

abstract class CreateDomainSpecificLinkTableMigration  {
    use TCreateLinkTableMigration;

    function __construct(){
        $this->tableNamesCatalog = app(ITableNamesCatalog::class);
        $this->tablesNamer = app(ITablesNamer::class);
    }

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->GetTableName(), function (Blueprint $table) {
            $table->increments('id');
            $ParentFieldName = $this->GetParentFieldName();
            $ChildFieldName = $this->GetChildFieldName();
            $table->integer($ParentFieldName)->unsigned();
            $table->integer($ChildFieldName)->unsigned();
            $table->timestamps();
            $table->index([$ParentFieldName, $ChildFieldName],'parent_child');
        });
    }

    protected function GetTableName()
    {
        $ParentTableName = $this->GetParentTableName();
        $ChildTableName = $this->GetChildTableName();
        $ret = $this->tablesNamer->GetLinkTableName($ParentTableName, $ChildTableName);
        return $ret;
    }
}