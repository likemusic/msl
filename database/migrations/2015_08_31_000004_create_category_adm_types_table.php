<?php

use Illuminate\Database\Schema\Blueprint;
use database\migrations\Base\BaseMigration;

class CreateCategoryAdmTypesTable extends BaseMigration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('category_adm_types', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullabe();//название в именительном пажеде с указанием типа населенного пункта ("город Борисов")
            $this->timestamps($table);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('category_adm_types');
    }
}
