<?php

use \database\migrations\Base\CreateDomainSpecificLinkTableMigration;

class CreatePivotProductTypesSubbproductSetsTable extends CreateDomainSpecificLinkTableMigration
{
    protected function GetParentTableName()
    {
        return $this->tableNamesCatalog->GetProductTypesTableName();
    }

    protected function GetChildTableName()
    {
        return $this->tableNamesCatalog->GetSubProductSetsTableName();
    }
}
