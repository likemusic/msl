<?php

use Illuminate\Database\Schema\Blueprint;
use database\migrations\Base\BaseMigration;

class CreateCategoryGroupsTable extends BaseMigration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('category_groups', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullabe();//название в именительном падеже без указания типа населенного пункта ("Борисов")

            $this->timestamps($table);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('category_groups');
    }
}
