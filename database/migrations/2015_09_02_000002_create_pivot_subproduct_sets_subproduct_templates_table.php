<?php

use \database\migrations\Base\CreateDomainSpecificLinkTableMigration;

class CreatePivotSubproductSetsSubproductTemplatesTable extends CreateDomainSpecificLinkTableMigration
{
    protected function GetParentTableName()
    {
        return $this->tableNamesCatalog->GetSubProductSetsTableName();
    }

    protected function GetChildTableName()
    {
        return $this->tableNamesCatalog->GetProductTemplatesTableName();
    }
}
