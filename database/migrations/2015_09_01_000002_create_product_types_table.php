<?php

use Illuminate\Database\Schema\Blueprint;
use database\migrations\Base\CreateDomainSpecificTableMigration;

class CreateProductTypesTable extends CreateDomainSpecificTableMigration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->GetTableName(), function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('subproduct_set_id');
            $table->timestamps();
        });
    }

    protected function GetTableName()
    {
        return $this->tableNamesCatalog->GetProductTypesTableName();
    }
}
