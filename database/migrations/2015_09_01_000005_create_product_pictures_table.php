<?php

use Illuminate\Database\Schema\Blueprint;
use database\migrations\Base\CreateDomainSpecificTableMigration;

class CreateProductPicturesTable extends CreateDomainSpecificTableMigration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->GetTableName(), function (Blueprint $table) {
            $table->increments('id');
            $table->integer('product_id');//кол-во картинок

            $TextFieldsLength = 50;
            //формы названия населенного пунта
            $table->string('filename',$TextFieldsLength)->nullabe();
            $table->string('thumbnail', $TextFieldsLength)->nullabe();
            $table->string('enlarged', $TextFieldsLength)->nullabe();

            $table->smallInteger('type_id')->nullabe();
        });
    }

    protected function GetTableName()
    {
        return $this->tableNamesCatalog->GetProductPicturesTableName();
    }
}
