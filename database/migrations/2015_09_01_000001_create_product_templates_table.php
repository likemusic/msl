<?php

use Illuminate\Database\Schema\Blueprint;
use database\migrations\Base\CreateDomainSpecificTableMigration;

class CreateProductTemplateTable extends CreateDomainSpecificTableMigration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(config('site.tables.product.template'), function (Blueprint $table) {
            $table->increments('id');
            $table->string('tpl_name');

            $table->string('name');
            $table->mediumText('description');

            $table->string('url_slug');

            $table->decimal('price', 9, 2);

            $table->string('pictures_dir');//папка с картинками
            $table->string('pictures');//имена файлов картинок
            $table->integer('pictures_count');//кол-во картинок
            //TODO: разобраться с `pictures` и `pictures_count`

            $table->string('title')->nullable();
            $table->string('h1')->nullable();//TODO: `name` всегда ранво `h1` - если да, удалить `h1`
            $table->string('meta_keywords')->nullable();
            $table->string('meta_description')->nullable();

            $table->integer('product_type_id');//тип карты: топографическая, GPS и т.д.
            $table->integer('sort_order')->nullable();

            $this->timestamps($table);

            $table->index('url_slug');//для поиска по url
        });
    }

    protected function GetTableName()
    {
        return $this->tableNamesCatalog->GetProductTemplatesTableName();
    }
}
