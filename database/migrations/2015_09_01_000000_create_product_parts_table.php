<?php

use Illuminate\Database\Schema\Blueprint;
use database\migrations\Base\CreateDomainSpecificTableMigration;

class CreateProductPartsTable extends CreateDomainSpecificTableMigration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(config('site.tables.product.template'), function (Blueprint $table) {
            $table->increments('id');
            $table->integer('category_id');//кол-во картинок

            //формы названия населенного пунта
            $table->string('name')->nullabe();//название в именительном пажеде с указанием типа населенного пункта ("город Борисов")
            $table->string('genitive')->nullabe();//название в родительном падеже с указанием типа населенного пункта ("города Борисова")
            $table->string('genitive_translit')->nullabe();//транслит поля genitive
            /*  В изначальной БД по нему (cat_h1_translit) строилось поле UID (url).
                Url отличается от genitive_translit тем что не должно содержать слешей и других спецсимволов и дублей (к дублям добавляется id).
                Возможно будет использоваться для наведения порядка с url, и настройки редиректов.
             */

            $table->string('src_name')->nullabe();//название в именительном падеже без указания типа населенного пункта ("Борисов")
            $table->string('src_genitive')->nullabe();//название в родительном падеже без указания типа населенного пункта ("Борисова")
            //TODO: разобраться что используется а что нет, и удалить nullable

            $table->string('url_slug')->nullable();;//при иерархической структуре сайта
            $table->string('url')->nullable();//при плоской структуре сайта (значения старого сайта)

            $table->integer('sort_order')->nullable();

            $table->integer('product_picture_id')->nullable();//id картинки из таблицы product_pictures

            $table->mediumText('description')->nullable();//содержание блока описания на странице

            $this->timestamps($table);

            $table->index('src_name');//для поиска по имени
            $table->unique('url_slug');//для поиска
            $table->unique('url');//для поиска
        });
    }

    protected function GetTableName()
    {
        return $this->tableNamesCatalog->GetProductPartsTableName();
    }
}
